
 #  Copyright 2018 Ricardo Luis Souza Ardissone
 #
 #  This file is part of BroaNetCo.
 #
 #  BroaNetCo is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  BroaNetCo is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.


from data import *


def sp() -> GSInit:
    return gsinit_t1(
        'Player',
        random_ai(),
    )


def mp() -> GSInit:
    return gsinit_t1(
        'Player',
        'Player',
    )


def obs() -> GSInit:
    return gsinit_t1(
        random_ai(),
        random_ai(),
    )

def concealment(concealment: int = 0):
    return [{'LineOfSight': concealment}, {'Simple': ()}]

def detection(detection: int = 1):
    return [{'Range': 1}, {'LineOfSight': detection}]


def melee_attack(name: str, ticks: Tick, chance: Mil, hp_diff: HpPts,
                 attacks_per_part: usize) -> Action:
    return Action(
        name=name,
        sound_effects=[{
            "Play": "hit16.mp3"
        }],
        source_effects=[sourcetickdiff(ticks)],
        target_effects=[action_target_effect_combater(
            ActionTargetting(hostile=True, max_distance=1), [{
                "Repeated": [{
                    "Repeated": [{
                        "Random": {
                            "chance": {
                                "Number": chance
                            },
                            "failure": OptNone(),
                            "success": {
                                "Target": {
                                    "HpDiff": {
                                        "Number": hp_diff
                                    }
                                }
                            }
                        }
                    }, {
                        "Number": attacks_per_part
                    }]
                }, {
                    "Source": {
                        "Part": []
                    }
                }]
            },
            sctlcombrstt()
            ]
        )])


def ranged_attack_effect(chance: Mil, hp_diff: HpPts):
    return {
            'Random': {
                "chance": {
                    'Number': chance
                },
                "success":
                OptSome({
                    'Missable': {
                        'Target': {
                            'MissablePartSize': {
                                'HpDiff': {
                                    'Number': hp_diff
                                }
                            }
                        }
                    }
                }),
                "failure":
                OptNone(),
            }
        }


def ranged_attack_effect_normal(chance: Mil, hp_diff: HpPts, stddev: Mil):
    return {
            'Random': {
                "chance": {
                    'Number': chance
                },
                "success":
                OptSome({
                    'Missable': {
                        'Target': {
                            'MissablePartSizeNormal': {
                                'stddev': stddev,
                                'hit': {
                                    'HpDiff': {
                                        'Number': hp_diff
                                    }
                                },
                                'miss': OptNone(),
                            }
                        }
                    }
                }),
                "failure":
                OptNone(),
            }
    }


def musket() -> Status:
    return Status(
        name="Musket",
        actions=[
            melee_attack("Bayonet", 50, 300, -15, 5),
        ],
        substatuses=ActVec(
            active=[0],
            possibilities=[
                Status(
                    name=" (Loaded)",
                    substatuses=ActVec(),
                    actions=[
                        Action(
                            name="Fire",
                            sound_effects=[{
                                "Play": "rifle_3"
                            }],
                            source_effects=[
                                sourcetickdiff(40),
                                sourcessss("Musket", 1)
                            ],
                            target_effects=[action_target_effect_combater(
                                ActionTargetting(hostile=True, max_distance=2), [{
                                        "Repeated": [
                                            ranged_attack_effect_normal(850, -30, stddev={'Number':150}), {
                                                "Source": {
                                                    "PartName": "Musketeer"
                                                }
                                            }
                                        ]
                                },
                                sctlcombrstt()
                                ]
                            )])
                    ],
                ),
                Status(
                    name=" (Unloaded)",
                    substatuses=ActVec(),
                    actions=[
                        Action(
                            name="Reload",
                            sound_effects=[{
                                "Play": "wood2"
                            }],
                            source_effects=[
                                sourcetickdiff(70),
                                sourcessss("Musket",0),
                            ],
                            target_effects=[],
                        )
                    ])
            ]))


def cannon_status() -> Status:
    def sourceeffect(tick, status):
        return [
            sourcetickdiff(tick),
            sourcessss("Cannon", status),
        ]

    def sourceeffectdiv(tick, status, gunnername="Gunner"):
        return [
            {
                    "TickDiff": {
                        "Calc": (
                            "Div",
                            {
                                "Number": tick
                            },
                            {
                                "PartName": gunnername
                            },
                        )
                    }
            },
            sourcessss("Cannon", status),
        ]

    return Status(
        name="Cannon",
        actions=[
            melee_attack("Melee", 50, 250, -12, 5),
            move("QMove cannon", 20),
        ],
        substatuses=ActVec(
            active=[0],
            possibilities=[
                Status(
                    name=" (Unloaded)",
                    substatuses=ActVec(),
                    actions=[
                        Action(
                            name="Roundshot",
                            sound_effects=[{
                                "Play": "wood2"
                            }],
                            source_effects=sourceeffectdiv(500, 1),
                            target_effects=[],
                        ),
                        Action(
                            name="Grapeshot",
                            sound_effects=[{
                                "Play": "wood2"
                            }],
                            source_effects=sourceeffectdiv(500, 2),
                            target_effects=[],
                        ),
                    ]),
                Status(
                    name=" (Roundshot)",
                    substatuses=ActVec(),
                    actions=[
                        Action(
                            name="Fire",
                            sound_effects=[{
                                "Play": "Explosion-LS100155"
                            }],
                            source_effects=sourceeffect(50, 0),

                            target_effects=[action_target_effect_combater(ActionTargetting(hostile=True, max_distance=5), [
                                ranged_attack_effect(970, -90),
                                sctlcombrstt(),
                            ])]),
                        Action(
                            name="Empty barrel",
                            sound_effects=[],
                            source_effects=sourceeffect(20, 0),
                            target_effects=[],
                        ),
                    ],
                ),
                Status(
                    name=" (Grapeshot)",
                    substatuses=ActVec(),
                    actions=[
                        Action(
                            name="Fire",
                            sound_effects=[{
                                "Play": "Explosion-LS100155"
                            }],
                            source_effects=sourceeffect(50, 0),
                            target_effects=[action_target_effect_combater(ActionTargetting(hostile=True, max_distance=2), [
                                ranged_attack_effect(970, -150),
                                sctlcombrstt(),
                            ])],
                        ),
                        Action(
                            name="Empty barrel",
                            sound_effects=[],
                            source_effects=sourceeffect(20, 0),
                            target_effects=[],
                        ),
                    ],
                ),
            ]))


def sword() -> Status:
    return Status(
        name="Sword",
        actions=[
            melee_attack("Attack", 50, 500, -6, 4),
        ],
        substatuses=ActVec(),
    )


def axe() -> Status:
    return Status(
        name="Axe",
        actions=[
            melee_attack("Attack", 60, 400, -15, 2),
        ],
        substatuses=ActVec(),
    )


def line_reg(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = NTDGroup([simple_com_2_namedtexture])
    return Combater(
        name="Line Infantry" + regn,
        statuses=[
            musket(),
            move_status(" infantry", 10),
        ],
        parts=[Part(
            name="Musketeer",
            hp=Stat(30),
            size=30,
        )] * 17 + [
            Part(
                name="Officer",
                hp=Stat(30),
                size=30,
            ),
            Part(
                name="Standard-bearer",
                hp=Stat(30),
                size=30,
            ),
            Part(
                name="Drummer",
                hp=Stat(30),
                size=30,
            ),
        ],
        spacing=10,
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command=morale(basic=970, weight_basic=100, weight_hp=200,officer={"name":"Officer", "base": 500, "bonus":600, "weight": 200}),
        detection=detection(2),
        concealment=concealment(0),
    )

def light_reg(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
        ingame=True
) -> Combater:
    if comtexture is None:
        comtexture = NTDGroup([simple_com_2b_namedtexture])
    return Combater(
        name="Light Infantry" + regn,
        statuses=[
            musket(),
            move_status(" light infantry", 7),
        ],
        parts=[Part(
            name="Light Infantry",
            hp=Stat(30),
            size=20,
        )] * 9 + [
            Part(
                name="Officer",
                hp=Stat(30),
                size=20,
            ),
        ],
        spacing=30,
        ingame=ingame,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command=morale(basic=970, weight_basic=100, weight_hp=150,officer={"name":"Officer", "base": 500, "bonus":600, "weight": 200}),
        detection=detection(2),
        concealment=concealment(1),
    )

def cuirassier(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = NTDGroup([simple_com_namedtexture])
    return Combater(
        name="Cuirassier" + regn,
        statuses=[
            sword(),
            move_status(" cavalry", 5),
        ],
        parts=[Part(
            name="Cuirassier",
            hp=Stat(40),
            size=50,
        )] * 8 + [
            Part(
                name="Officer",
                hp=Stat(40),
                size=50,
            ),
            Part(
                name="Bugler",
                hp=Stat(40),
                size=50,
            ),
        ],
        spacing=25,
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command=morale(basic=1200, weight_basic=100, weight_hp=200,officer={"name":"Officer", "base": 500, "bonus":600, "weight": 200}),
        detection=detection(3),
        concealment=concealment(0),
        )


def cannon(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = NTDGroup([simple_com_3_namedtexture])
    gunner = Part(
        name="Gunner",
        hp=Stat(30),
        size=30,
    )
    return Combater(
        name="Cannon" + regn,
        statuses=[cannon_status()],
        parts=[
            gunner,
            gunner,
            gunner,
            Part(
                name="Officer",
                hp=Stat(30),
                size=30,
            ),
            Part(
                name="Bugler",
                hp=Stat(30),
                size=30,
            ),
            Part(
                name="Cannon",
                hp=OptNone(),
                size=OptNone(),
            ),
        ] * 2,
        spacing=15,
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command=morale(basic=990, weight_basic=100, weight_hp=500,officer={"name":"Officer", "base": 500, "bonus":600, "weight": 200}),
        detection=detection(2),
        concealment=concealment(0),
        )


def barbarian(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = NTDGroup([simple_com_blobs_namedtexture])
    return Combater(
        name="Band" + regn,
        statuses=[
            axe(),
            move_status(" barbarian", 10),
        ],
        parts=[Part(
            name="Barbarian",
            hp=Stat(35),
            size=35,
        )] * 20,
        spacing=10,
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command={'AI': 'Random'},
        detection=detection(2),
        concealment=concealment(0),
)


def gsinit_t1(
        fac0type: str,
        fac1type: str,
) -> GSInit:
    faction_p0 = FactionID(0)
    faction_p1 = FactionID(1)

    terrains = {
        "grass":
        MapElement(
            name="Grass",
            ticks=OptSome(10),
            protection_behind=80,
            protection_inside=100,
            texture_data=[TextureData("map_grass.svg")],
        ),
        "water":
        MapElement(
            name="Water",
            ticks=OptSome(20),
            protection_behind=0,
            protection_inside=200,
            detection=[{'LineOfSight': -1}],
            texture_data=[TextureData("map_water.svg")],
        ),
        "forest":
        MapElement(
            name="Forest",
            ticks=OptSome(15),
            protection_behind=900,
            protection_inside=500,
            concealment=[{'LineOfSight': +1}],
            texture_data=[TextureData("map_forest.svg")],
        ),
        "hill":
        MapElement(
            name="Hill",
            ticks=OptSome(15),
            protection_behind=1000,
            protection_inside=300,
            detection=[{'LineOfSight': +1}],
            concealment=[{'LineOfSight': -1}],
            texture_data=[TextureData("map_hill.svg")],
        ),
        "mountain":
        MapElement(
            name="Mountain",
            ticks=OptNone(),
            protection_behind=1000,
            protection_inside=900,
            texture_data=[TextureData("map_mountain.svg")],
        ),
        "flag_hill":
        MapElement(
            name="Victory Hill",
            ticks=OptSome(15),
            protection_behind=1000,
            protection_inside=300,
            detection=[{'LineOfSight': +1}],
            concealment=[{'LineOfSight': -1}],
            texture_data=[TextureData("map_hill.svg"), TextureData("map_flag.svg")],
        ),
    }
    map_size = 7
    g = terrains['grass']
    w = terrains['water']
    f = terrains['forest']
    h = terrains['hill']
    m = terrains['mountain']
    v = terrains['flag_hill']
    map = [
           [g,g,g,w,w,g,g,g],
          [f,g,g,g,w,g,g,g,h],
         [f,f,f,g,w,g,g,g,h,h],
        [g,f,g,f,w,g,f,g,g,h,h],
       [g,g,g,g,w,g,h,f,g,h,m,m],
      [g,g,h,g,f,w,g,g,g,g,h,h,m],
     [g,g,g,g,f,w,w,w,h,g,g,h,h,h],
    [g,g,g,g,w,w,v,f,w,w,h,g,f,f,h],
     [g,h,h,f,w,w,h,w,h,m,h,f,f,f],
      [g,h,f,h,w,w,w,w,m,h,g,f,f],
       [g,g,g,w,h,g,g,w,h,g,g,f],
        [g,g,w,f,g,f,w,w,w,w,w],
         [f,w,f,h,g,g,g,f,f,f],
          [w,f,h,g,g,f,f,g,f],
           [g,g,g,g,g,f,f,f],
    ] # yapf: disable
    ggsa = []
    lnum = 0
    for line in map:
        cnum = 0
        for column in line:
            if lnum < map_size:
                x = cnum - lnum
                y = map_size - cnum
            else:
                y = (2 * map_size) - lnum - cnum
                x = -map_size + cnum
            ggsa.append({
                'SetMapElement': {
                    'location': MapLocation(x=x, y=y),
                    'element': column,
                }
            })
            cnum += 1
        lnum += 1

    ggsa.extend([
        # {'SetRNG': [0, 3, 5, 1]},
        last_man_standing,
        expand_map_range_combater(2, m),
        king_of_the_hill(MapLocation(x=-1, y=1), 500),
        choice_aigm(43, 50, faction_p0, "Choose where to spawn reinforcements", [
            ("West", gseffect_cloneat(9, MapLocation(x=-7, y=+7), [])),
            ("East", gseffect_cloneat(9, MapLocation(x=+7, y=-7), [])),
        ]),
        {
            'AddFaction':
            Faction(
                name="South",
                color=Color(a=FACTION_COLOR_TRANSPARENCY, b=1.0, g=0.0, r=0.0),
                faction_type=fac0type)
        },
        {
            'AddFaction':
            Faction(
                name="North",
                color=Color(a=FACTION_COLOR_TRANSPARENCY, b=0.0, g=0.0, r=1.0),
                faction_type=fac1type)
        },
        {
            'AddCombater':
            line_reg(
                " 1",
                ComLocation(
                    coord=MapLocation(x=-2, y=-2),
                    dir='XZ',
                ),
                0,
                faction_p0,
            )
        },
        {
            'AddCombater':
            line_reg(
                " 2",
                ComLocation(
                    coord=MapLocation(x=-1, y=-4),
                    dir='XZ',
                ),
                3,
                faction_p0,
            )
        },
        {
            'AddCombater':
            line_reg(
                " 3",
                ComLocation(
                    coord=MapLocation(x=-5, y=-1),
                    dir='XZ',
                ),
                2,
                faction_p0,
            )
        },
        {
            'AddCombater':
            line_reg(
                " 4",
                ComLocation(
                    coord=MapLocation(x=-2, y=-4),
                    dir='XZ',
                ),
                9,
                faction_p0,
            )
        },
        {
            'AddCombater':
            cuirassier(
                " 1",
                ComLocation(
                    coord=MapLocation(x=-7, y=0),
                    dir='XZ',
                ),
                4,
                faction_p0,
            )
        },
        {
            'AddCombater':
            cuirassier(
                " 2",
                ComLocation(
                    coord=MapLocation(x=-3, y=-3),
                    dir='XZ',
                ),
                4,
                faction_p0,
            )
        },
        {
            'AddCombater':
            cannon(
                " 1",
                ComLocation(
                    coord=MapLocation(x=-4, y=-1),
                    dir='XZ',
                ),
                9,
                faction_p0,
            )
        },
        {
            'AddCombater':
            cannon(
                " 2",
                ComLocation(
                    coord=MapLocation(x=-4, y=-2),
                    dir='XZ',
                ),
                9,
                faction_p0,
            )
        },
        {
            'AddCombater':
            barbarian(
                " 1",
                ComLocation(
                    coord=MapLocation(x=-3, y=-1),
                    dir='XZ',
                ),
                20,
                faction_p0,
            )
        },
        {
            'AddCombater':
            light_reg(
                " 1",
                ComLocation(
                    coord=MapLocation(x=0, y=0),
                    dir='XZ',
                ),
                5,
                faction_p0,
                ingame=False,
            )
        },
        {
            'AddCombater':
            light_reg(
                " 2",
                ComLocation(
                    coord=MapLocation(x=-4, y=0),
                    dir='XZ',
                ),
                5,
                faction_p0,
            )
        },


        {
            'AddCombater':
            line_reg(
                " 21",
                ComLocation(
                    coord=MapLocation(x=+5, y=+0),
                    dir='XZ',
                ),
                0,
                faction_p1,
            )
        },
        {
            'AddCombater':
            line_reg(
                " 22",
                ComLocation(
                    coord=MapLocation(x=-1, y=+6),
                    dir='XZ',
                ),
                3,
                faction_p1,
            )
        },
        {
            'AddCombater':
            line_reg(
                " 23",
                ComLocation(
                    coord=MapLocation(x=+4, y=+1),
                    dir='XZ',
                ),
                2,
                faction_p1,
            )
        },
        {
            'AddCombater':
            line_reg(
                " 24",
                ComLocation(
                    coord=MapLocation(x=+0, y=+5),
                    dir='XZ',
                ),
                9,
                faction_p1,
            )
        },
        {
            'AddCombater':
            cuirassier(
                " 21",
                ComLocation(
                    coord=MapLocation(x=+4, y=+2),
                    dir='XZ',
                ),
                4,
                faction_p1,
            )
        },
        {
            'AddCombater':
            cuirassier(
                " 22",
                ComLocation(
                    coord=MapLocation(x=+1, y=+5),
                    dir='XZ',
                ),
                4,
                faction_p1,
            )
        },
        {
            'AddCombater':
            cannon(
                " 21",
                ComLocation(
                    coord=MapLocation(x=+3, y=+2),
                    dir='XZ',
                ),
                9,
                faction_p1,
            )
        },
        {
            'AddCombater':
            cannon(
                " 22",
                ComLocation(
                    coord=MapLocation(x=+1, y=+4),
                    dir='XZ',
                ),
                9,
                faction_p1,
            )
        },
        {
            'AddCombater':
            barbarian(
                " 1",
                ComLocation(
                    coord=MapLocation(x=+0, y=+2),
                    dir='XZ',
                ),
                20,
                faction_p1,
            )
        },
        {
            'AddCombater':
            light_reg(
                " 21",
                ComLocation(
                    coord=MapLocation(x=-0, y=+4),
                    dir='XZ',
                ),
                5,
                faction_p1,
            )
        },
        {
            'AddCombater':
            light_reg(
                " 22",
                ComLocation(
                    coord=MapLocation(x=+3, y=0),
                    dir='XZ',
                ),
                5,
                faction_p1,
            )
        },
    ])

    return GSInit(god_gsapply=ggsa)
