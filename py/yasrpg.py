
 #  Copyright 2018 Ricardo Luis Souza Ardissone
 #
 #  This file is part of BroaNetCo.
 #
 #  BroaNetCo is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  BroaNetCo is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.


from data import *


def sp() -> GSInit:
    return gsinit_t1(
        'Player',
        random_ai(),
        False,
    )


def mp() -> GSInit:
    return gsinit_t1(
        'Player',
        'Player',
        False,
    )


def obs() -> GSInit:
    return gsinit_t1(
        random_ai(),
        random_ai(),
        False,
    )

def endless_sp() -> GSInit:
    return gsinit_t1(
        'Player',
        random_ai(),
        True,
    )


def endless_mp() -> GSInit:
    return gsinit_t1(
        'Player',
        'Player',
        True,
    )


def endless_obs() -> GSInit:
    return gsinit_t1(
        random_ai(),
        random_ai(),
        True,
    )


def simple_attack(name: str, ticks: Tick, chance: Mil, hp_diff,
                  attacks_per_part: usize, max_distance: usize = 1, heal=False,
                  suicide=False, timed=None, normal=1000, critical=0, critical_mul=5,
) -> Action:
    target_effects = [{
        "HpDiff": hp_diff
    },]
    critical_target_effects = [{
        "HpDiff": {"Calc": ("Mul", hp_diff, {"Number": critical_mul})}
    },]
    if timed:
        (timed_delay, timed_hpdiff) = timed
        target_effects.append(
            {
                "Timed": (
                    {
                        "Calc": (
                            "Add",
                            {
                                "NumberCalc":  {
                                    "Tick": (),
                                }
                            },
                            {
                                "Number": timed_delay
                            }
                        )
                    },
                    {
                        "HpDiff": timed_hpdiff
                    }
                )
            }
        )


    teffs = [
                {
                    "Repeated": [{
                        "Repeated": [{
                            "Random": {
                                "chance": {
                                    "Number": chance
                                },
                                "failure": OptNone(),
                                "success": {
                                    "Target": {
                                        "RandomWeighted": [
                                            ({"Group": target_effects}, {"Number": normal}),
                                            ({"Group": critical_target_effects}, {"Number": critical}),
                                        ]
                                    }
                                }
                            }
                        }, {
                            "Number": attacks_per_part
                        }]
                    }, {
                        "Source": {
                            "Part": []
                        }
                    }]
                },
                sctlcombrstt(),
            ]
    if suicide:
        teffs.append({
            'Source': {
                'Kill': ()
            },
        })


    return Action(
        name=name,
        sound_effects=[{
            "Play": "hit16.mp3"
        }],
        source_effects=[sourcetickdiff(ticks)],
        target_effects=[action_target_effect_combater(
            ActionTargetting(hostile=not heal, max_distance=max_distance, friendly=heal,
                             oneself=heal), teffs)])


def spawn(name: str, ticks: Tick, comb_id: usize, max_distance: usize = 1) -> Action:
    return Action(
        name=name,
        sound_effects=[{
            "Play": "hit16.mp3"
        }],
        source_effects=[sourcetickdiff(ticks)],
        target_effects=[action_target_effect_combater(
            ActionTargetting(terrain=True, max_distance=max_distance), [
                {
                    "Target": {
                        'Terrain': {
                            "CloneAt": (
                                comb_id,
                                []
                            )
                        }
                    }
                },
                sctlcombrstt(),
            ]
        )]
    )

def teleport(name: str, ticks: Tick, max_distance: usize = 1) -> Action:
    return Action(
        name=name,
        sound_effects=[{
            "Play": "hit16.mp3"
        }],
        source_effects=[sourcetickdiff(ticks)],
        target_effects=[action_target_effect_terrain(
            ActionTargetting(terrain=True, max_distance=max_distance), [
                combrstt(),
                {
                    'Teleport': ()
                },
            ]

        )]
    )


def missile_spawn(name: str, ticks: Tick, comb_id: usize, max_distance: usize = 1) -> Action:
    return Action(
        name=name,
        sound_effects=[{
            "Play": "hit16.mp3"
        }],
        source_effects=[sourcetickdiff(ticks)],
        target_effects=[action_target_effect_combater(
            ActionTargetting(hostile=True, max_distance=max_distance), [
                {
                    'CloneAtSource': (
                        comb_id,
                        [
                            {
                                'SetCommandSingleTarget': (
                                    0,
                                    'Die'
                                )
                            }
                        ]
                    )
                },
                sctlcombrstt(),
            ]
        )]
    )

F2_COMB_MAIN_ATTACK = 0

def taunt(name: str, ticks: Tick, max_distance: usize = 1) -> Action:
    return Action(
        name=name,
        sound_effects=[{
            "Play": "hit16.mp3"
        }],
        source_effects=[sourcetickdiff(ticks)],
        target_effects=[action_target_effect_combater(
            ActionTargetting(hostile=True, max_distance=max_distance), [
                {
                    'Switch': (
                        {
                            'SetCommandSingleTarget': (
                                F2_COMB_MAIN_ATTACK,
                                OptNone(),
                            )
                        }
                    )
                },
                sctlcombrstt(),
            ]
        )]
    )

def aoe_attack(name: str, ticks: Tick, chance: Mil, hp_diff, max_distance: usize = 1, aoe: usize = 1):
    return Action(
        name=name,
        sound_effects=[{
            "Play": "hit16.mp3"
        }],
        source_effects=[sourcetickdiff(ticks)],
        target_effects=[action_target_effect_terrain(
            ActionTargetting(friendly=True, oneself=True, hostile=True, terrain=True, max_distance=max_distance), [
                {
                    "Target": {
                        'AreaOfEffect': (
                            aoe,
                            {
                                'CombaterAt': {
                                    "Random": {
                                        "chance": {
                                            "Number": chance
                                        },
                                        "failure": OptNone(),
                                        "success": {
                                            "HpDiff": hp_diff
                                        }
                                    }
                                }
                            }
                        )
                    }
                },
                combrstt(),
            ]
        )]
    )

def ranged_attack_effect(chance: Mil, hp_diff):
    return {
        'Random': {
            "chance": {
                'Number': chance
            },
            "success":
            OptSome({
                'Missable': {
                    'Target': {
                        'MissablePartSize': {
                            'HpDiff': hp_diff
                        }
                    }
                }
            }),
            "failure":
            OptNone(),
        }
    }


def ranged_attack_effect_normal(chance: Mil, hp_diff, stddev: Mil):
    return {
        'Random': {
            "chance": {
                'Number': chance
            },
            "success":
            OptSome({
                'Missable': {
                    'Target': {
                        'MissablePartSizeNormal': {
                            'stddev': stddev,
                            'hit': {
                                'HpDiff': hp_diff
                            },
                            'miss': OptNone(),
                        }
                    }
                }
            }),
            "failure":
            OptNone(),
        }
    }


def bow() -> Status:
    return Status(
        name="bow",
        actions=[
        ],
        substatuses=ActVec(
            active=[0],
            possibilities=[
                Status(
                    name=" (loaded)",
                    substatuses=ActVec(),
                    actions=[
                        Action(
                            name="fire",
                            sound_effects=[{
                                "Play": "rifle_3"
                            }],
                            source_effects=[
                                sourcetickdiff(40),
                                sourcessss("bow", 1),
                            ],
                            target_effects=[action_target_effect_combater(
                                ActionTargetting(hostile=True, max_distance=2), [{
                                    "Repeated": [
                                        ranged_attack_effect_normal(850, numbercalc(diceroll(-7, 6, -5)), stddev={'Number':150}), {
                                            "Source": {
                                                "Part": []
                                            }
                                        }
                                    ]
                                },
                                sctlcombrstt()
                                ]
                            )])
                    ],
                ),
                Status(
                    name=" (unloaded)",
                    substatuses=ActVec(),
                    actions=[
                        Action(
                            name="reload",
                            sound_effects=[{
                                "Play": "wood2"
                            }],
                            source_effects=[
                                sourcetickdiff(70),
                                sourcessss("bow", 0),
                            ],
                            target_effects=[],
                        )
                    ])
            ]))

MAGIC_MISSILE_COMB_ID=1

def scepter() -> Status:
    return Status(
        name="scepter",
        actions=[
            simple_attack("Attack (Melee) (Scepter)", 40, 900, numbercalc(diceroll(-10, 8, -5)), 1, critical=100),
            simple_attack("Ice Ray (Scepter)", 80, 900, numbercalc(diceroll(-10, 10, -5)), 1, 5, critical=100),
            spawn("Spawn Skeleton (Scepter)", 200, 0),
            missile_spawn("Magic Missiles (Scepter)", 100, MAGIC_MISSILE_COMB_ID, 6),
            aoe_attack("Fireball (Scepter)", chance=800, ticks=300, hp_diff=numbercalc(diceroll(-10, 10, -5)), max_distance=4, aoe=1),
            teleport("Teleport (Scepter)", ticks=200, max_distance=3),
        ],
        substatuses=ActVec(),
    )


def sword() -> Status:
    return Status(
        name="Sword",
        actions=[
            simple_attack("Attack (Sword)", 40, 900, numbercalc(diceroll(-10, 8, -5)), 1, critical=100),
        ],
        substatuses=ActVec(),
    )
def shouts() -> Status:
    return Status(
        name="Shouts",
        actions=[
            taunt("Taunt (Shouts)", 100, 1),
        ],
        substatuses=ActVec(),
    )
def lifemace() -> Status:
    return Status(
        name="Mace Of Life",
        actions=[
            simple_attack("Attack (Mace)", 60, 900, numbercalc(diceroll(-10, 10, -5)), 1, critical=100),
            simple_attack("Heal (Mace)", 50, 900, numbercalc(diceroll(+5, 6, 10)), 2, heal=True, critical=100),
        ],
        substatuses=ActVec(),
    )
def knife() -> Status:
    return Status(
        name="Poisoned Knife",
        actions=[
            simple_attack("attack (knife)", 25, 900, numbercalc(diceroll(-3, 3, +2)), 2, timed=(30, numbercalc(diceroll(-5,10,-3))), critical=100),
        ],
        substatuses=ActVec(),
    )
def axe() -> Status:
    return Status(
        name="axe",
        actions=[
            simple_attack("attack (axe)", 50, 850, numbercalc(diceroll(-8, 7, -2)), 1),
        ],
        substatuses=ActVec(),
    )

def ring_fly() -> Status:
    return Status(
        name="Ring of Flight",
        actions=[
            teleport("Fly (Ring of Flight)", 40, 1),
        ]
    )

def missile_status() -> Status:
    return Status(
        name="missile",
        actions=[
            simple_attack("explode", 1, 950, numbercalc(diceroll(-8, 7, -2)), 1, suicide=True),
        ],
        substatuses=ActVec(),
    )

def missile_com(
        name: str,
        loc: ComLocation,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = LPCGroup([
            # TODO: fix
            "weapons/left hand/either/arrow.png"
        ])
    return Combater(
        name=name,
        statuses=[
            missile_status(),
            move_status(" missile", 1)
        ],
        parts=[],
        ingame=False,
        location=loc,
        tick=0,
        faction=fac,
        texture_data=comtexture,
        command='Direct',
    )

def warrior(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = LPCGroup([
            "body/male/light.png",
            "facial/male/beard/black.png",
            "feet/shoes/male/black_shoes_male.png",
            "torso/male/11.png",
            "hair/male/plain/raven.png",
            "head/plate/2.png",
            "hands/bracers/male/1.png",
            "legs/armor/male/7.png",
            "hands/gloves/male/2.png",
            "shoulders/male/7.png",
            "weapons/left hand/male/kiteshield_gray.png",
            "weapons/right hand/male/pickaxe.png",
        ])
    return Combater(
        name="Warrior" + regn,
        statuses=[
            sword(),
            shouts(),
            move_status(" warrior", 10)
        ],
        parts=[Part(
            name="Warrior",
            hp=Stat(100),
            size=50,
        )],
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command='Direct',
    )

def LPCGroup(paths):
    paths2 = []
    for path in paths:
        paths2.append("Universal-LPC-spritesheet/spritesheets/" + path)
    return NTDGroup(paths2, "LPC")

def skeleton(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = LPCGroup([
            "body/male/skeleton.png",
            "weapons/right hand/male/axe.png",
            "hands/gloves/male/5.png",
        ])
    return Combater(
        name="Skeleton" + regn,
        statuses=[
            axe(),
            move_status(" skeleton", 10)
        ],
        parts=[Part(
            name="Warrior",
            hp=Stat(30),
            size=40,
        )],
        ingame=False,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command='Direct',
    )

def cleric(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = LPCGroup([
            "body/female/light.png",
            "torso/leather/chest_female.png",
            "weapons/right hand/female/warhammer.png",
            "weapons/left hand/male/shield_silver.png",
            "hair/female/female_braid/Female_Hair_braid_darkblond.png",
            "head/hoods/female/chain_hood_female.png",
            "legs/armor/female/7.png",
            "feet/shoes/female/black_shoes_female.png",
        ])
    return Combater(
        name="Cleric" + regn,
        statuses=[
            lifemace(),
            move_status(" cleric", 12)
        ],
        parts=[Part(
            name="Cleric",
            hp=Stat(120),
            size=45,
        )],
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command='Direct',
    )

def rogue(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = LPCGroup([
            "body/male/light.png",
            "legs/pants/Pants_blue.png",
            "torso/shirts/longsleeve/male/male_shirt_green.png",
            "weapons/left hand/either/arrow.png",
            "hair/male/male_curly/Male_Hair_curly_darkred.png",
            "torso/chain/tabard/male_tabard_green.png",
            "feet/shoes/male/black_shoes_male.png",
            "head/caps/male/leather_cap_male.png",
        ])
    return Combater(
        name="Rogue" + regn,
        statuses=[
            bow(),
            knife(),
            ring_fly(),
            move_status(" rogue", 7)
        ],
        parts=[Part(
            name="Rogue",
            hp=Stat(70),
            size=40,
        )],
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command='Direct',
    )

def wizard(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = LPCGroup([
            "body/male/darkelf2.png",
            "head/caps/male/moon-male.png",
            "torso/shirts/longsleeve/male/white_longsleeve.png",
            "hands/gloves/male/1.png",
            "weapons/both hand/formal_cane.png",
            "torso/back/cape/normal/female/cape_white.png",
            "feet/shoes/male/maroon_shoes_male.png",
            "legs/pants/male/white_pants_male.png",
        ])
    return Combater(
        name="Wizard" + regn,
        statuses=[
            scepter(),
            move_status(" wizard", 12)
        ],
        parts=[Part(
            name="Wizard",
            hp=Stat(60),
            size=45,
        )],
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command='Direct',
    )

def goblin(
        regn: str,
        loc: ComLocation,
        tick: Tick,
        fac: FactionID,
        comtexture=None,
) -> Combater:
    if comtexture is None:
        comtexture = LPCGroup([
            "body/male/orcs/1.png",
            "legs/pants/male/male_pants_brown.png",
            "hair/male/messy1/green.png",
            "weapons/right hand/male/axe.png",
        ])
    return Combater(
        name="Goblin" + regn,
        statuses=[
            axe(),
            move_status(" goblin", 10)
        ],
        parts=[Part(
            name="Goblin",
            hp=Stat(50),
            size=50,
        )],
        ingame=True,
        location=loc,
        tick=tick,
        faction=fac,
        texture_data=comtexture,
        command=morale(basic=1100, weight_basic=100, weight_hp=200),
    )

def gsinit_t1(
        fac0type: str,
        fac1type: str,
        endless: bool,
) -> GSInit:
    faction_p0 = FactionID(0)
    faction_p1 = FactionID(1)

    terrains = {
        "grass":
        MapElement(
            name="Grass",
            ticks=OptSome(10),
            protection_behind=80,
            protection_inside=100,
            texture_data=[TextureData("map_grass.svg")],
        ),
        "water":
        MapElement(
            name="Water",
            ticks=OptSome(20),
            protection_behind=0,
            protection_inside=200,
            texture_data=[TextureData("map_water.svg")],
        ),
        "forest":
        MapElement(
            name="Forest",
            ticks=OptSome(15),
            protection_behind=900,
            protection_inside=500,
            texture_data=[TextureData("map_forest.svg")],
        ),
        "hill":
        MapElement(
            name="Hill",
            ticks=OptSome(15),
            protection_behind=1000,
            protection_inside=300,
            texture_data=[TextureData("map_hill.svg")],
        ),
        "mountain":
        MapElement(
            name="Mountain",
            ticks=OptNone(),
            protection_behind=1000,
            protection_inside=900,
            texture_data=[TextureData("map_mountain.svg")],
        ),
    }
    map_size = 5
    g = terrains['grass']
    w = terrains['water']
    f = terrains['forest']
    h = terrains['hill']
    m = terrains['mountain']
    map = [
         [w,g,g,m,m,g],
        [g,w,g,f,g,g,w],
       [m,f,w,f,f,g,w,g],
      [m,f,f,w,g,g,w,g,g],
     [g,h,g,g,w,h,w,g,g,g],
    [g,h,h,g,g,w,w,w,f,g,g],
     [g,g,g,g,g,w,w,f,g,g],
      [g,g,f,f,g,f,f,h,m],
       [g,g,f,f,g,g,h,m],
        [g,f,m,m,m,g,g],
         [g,m,m,m,g,g],
    ] # yapf: disable
    ggsa = []
    lnum = 0
    for line in map:
        cnum = 0
        for column in line:
            if lnum < map_size:
                x = cnum - lnum
                y = map_size - cnum
            else:
                y = (2 * map_size) - lnum - cnum
                x = -map_size + cnum
            ggsa.append({
                'SetMapElement': {
                    'location': MapLocation(x=x, y=y),
                    'element': column,
                }
            })
            cnum += 1
        lnum += 1

    skeleton1 = skeleton(
                "",
                ComLocation(
                    coord=MapLocation(x=0, y=0),
                    dir='XZ',
                ),
                0,
                faction_p0,
        )

    skeleton_p1 = skeleton(
                "",
                ComLocation(
                    coord=MapLocation(x=0, y=0),
                    dir='XZ',
                ),
                0,
                faction_p1,
        )

    magicmissile1 = missile_com(
        "Magic Missiles",
        ComLocation(
            coord=MapLocation(x=0, y=0),
            dir='XZ',
        ),
        faction_p0,
    )

    if endless:
        ggsa.append({
            'AddAIGameMaster': (
                0,
                {
                    'Spawn': {
                        'positions': [
                            MapLocation(x=-1, y=5),
                            MapLocation(x=-2, y=5),
                            MapLocation(x=+2, y=+3),
                            MapLocation(x=+3, y=+2),
                            MapLocation(x=+5, y=-4),
                            MapLocation(x=+5, y=-5),
                        ],
                        'combater': 2,
                        'next_tick': 100,
                        'interval': 100,
                    }
                },
            )
        })
    else:
        ggsa.append(last_man_standing)

    ggsa.extend([
        # {'SetRNG': [0, 3, 5, 1]},
        {
            'AddFaction':
            Faction(
                name="Party",
                color=Color(a=FACTION_COLOR_TRANSPARENCY, b=1.0, g=0.0, r=0.0),
                faction_type=fac0type)
        },
        {
            'AddFaction':
            Faction(
                name="Monsters",
                color=Color(a=FACTION_COLOR_TRANSPARENCY, b=0.0, g=0.0, r=1.0),
                faction_type=fac1type)
        },
        {
            'AddCombater': skeleton1
        },
        {
            'AddCombater': magicmissile1,
        },
        {
            'AddCombater': skeleton_p1
        },
        {
            'AddCombater':
            warrior(
                "",
                ComLocation(
                    coord=MapLocation(x=-3, y=3),
                    dir='XY',
                ),
                16,
                faction_p0,
            )
        },
        {
            'AddCombater':
            cleric(
                "",
                ComLocation(
                    coord=MapLocation(x=-4, y=4),
                    dir='XY',
                ),
                32,
                faction_p0,
            )
        },
        {
            'AddCombater':
            wizard(
                "",
                ComLocation(
                    coord=MapLocation(x=-5, y=5),
                    dir='XY',
                ),
                50,
                faction_p0,
            )
        },
        {
            'AddCombater':
            rogue(
                "",
                ComLocation(
                    coord=MapLocation(x=-4, y=5),
                    dir='XY',
                ),
                5,
                faction_p0,
            )
        },
        {
            'AddCombater':
            goblin(
                " 2",
                ComLocation(
                    coord=MapLocation(x=2, y=-2),
                    dir='YX',
                ),
                10,
                faction_p1,
            )
        },
        {
            'AddCombater':
            goblin(
                " 3",
                ComLocation(
                    coord=MapLocation(x=2, y=0),
                    dir='YX',
                ),
                50,
                faction_p1,
            )
        },
        {
            'AddCombater':
            goblin(
                " 4",
                ComLocation(
                    coord=MapLocation(x=1, y=0),
                    dir='YX',
                ),
                30,
                faction_p1,
            )
        },
        {
            'AddCombater':
            goblin(
                " 6",
                ComLocation(
                    coord=MapLocation(x=1, y=1),
                    dir='YX',
                ),
                120,
                faction_p1,
            )
        },
        {
            'AddCombater':
            goblin(
                " 1",
                ComLocation(
                    coord=MapLocation(x=3, y=-4),
                    dir='YX',
                ),
                140,
                faction_p1,
            )
        },
        {
            'AddCombater':
            goblin(
                " 5",
                ComLocation(
                    coord=MapLocation(x=4, y=-4),
                    dir='YX',
                ),
                18,
                faction_p1,
            )
        },
    ])

    return GSInit(god_gsapply=ggsa)
