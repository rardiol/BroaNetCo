
 #  Copyright 2018 Ricardo Luis Souza Ardissone
 #
 #  This file is part of BroaNetCo.
 #
 #  BroaNetCo is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  BroaNetCo is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.

from data import *

def sp() -> GSInit:
    return gsinit_t1(
        'Player',
        random_ai(),
        random_ai(),
    )


def mp() -> GSInit:
    return gsinit_t1(
        'Player',
        'Player',
        'Player',
    )


def coop() -> GSInit:
    return gsinit_t1(
        'Player',
        'Player',
        random_ai(),
    )


def gsinit_t1(
        fac0type: str,
        fac1type: str,
        fac2type: str,
) -> GSInit:
    simple_com_namedtexture = "simple_com.svg"
    faction_p0 = FactionID(0)
    faction_p1 = FactionID(1)
    faction_p2 = FactionID(2)

    sword = Status(
        name="Sword",
        actions=[
            Action(
                name="Attack",
                sound_effects=[],
                source_effects=[sourcetickdiff(4)],
                target_effects=[action_target_effect_combater(ActionTargetting(hostile=True, max_distance=3), [
                    sctlcombrstt(),
                     {'Group': [
                        {'Target': {'HpDiff': {'Number': -5}}},
                     ]},
                ])],
            ),
        ],
    )
    movestatus = move_status(" unit", 9)

    wall = MapElement(
        name="wall",
        ticks=None,
        protection_behind=1000,
        protection_inside=1000,
        texture_data=[TextureData("map_wall.svg")],
    )
    floor = MapElement(
        name="floor",
        ticks=None,
        protection_behind=None,
        protection_inside=None,
        texture_data=[TextureData("map_floor.svg")],
    )

    ggsa = [
       # {'SetRNG': [0, 3, 5, 1]},
        last_man_standing,
        {'SetMapElement': {
            'location': MapLocation(x=0, y=0),
            'element': wall,
        }},
        {'SetMapElement': {
            'location': MapLocation(x=0, y=2),
            'element': floor,
        }},
        {'SetMapElement': {
            'location': MapLocation(x=0, y=1),
            'element': floor,
        }},
        {'SetMapElement': {
            'location': MapLocation(x=-1, y=2),
            'element': floor,
        }},
        {'AddFaction': Faction(
            name="Player",
            color=Color(a=FACTION_COLOR_TRANSPARENCY, b=1.0, g=0.0, r=0.0),
            faction_type=fac0type
        )},
        {'AddFaction': Faction(
            name="Other",
            color=Color(a=FACTION_COLOR_TRANSPARENCY, b=0.0, g=0.0, r=1.0),
            faction_type=fac1type
        )},
        {'AddFaction': Faction(
            name="Third",
            color=Color(a=FACTION_COLOR_TRANSPARENCY, b=0.0, g=1.0, r=1.0),
            faction_type=fac2type
        )},
        {'AddCombater': Combater(
            name="player",
            statuses=[
                sword,
                movestatus,
            ],
            parts=[
                Part(
                    name="psold",
                    hp=Stat(10),
                    size=700,
                ),
            ],
            ingame=True,
            location=ComLocation(
                coord=MapLocation(x=0, y=2),
                dir='YZ',
            ),
            tick=12,
            faction=faction_p0,
            texture_data=NTDGroup([simple_com_namedtexture]),
        )},
        {'AddCombater': Combater(
            name="second",
            statuses=[
                sword,
                movestatus,
            ],
            parts=[
                Part(
                    name="psold",
                    hp=Stat(10),
                    size=700,
                ),
            ],
            ingame=True,
            location=ComLocation(
                coord=MapLocation(x=0, y=1),
                dir='XZ',
            ),
            tick=10,
            faction=faction_p1,
            texture_data=NTDGroup([simple_com_namedtexture]),
        )},
        {'AddCombater': Combater(
            name="third",
            statuses=[
                sword,
                movestatus,
            ],
            parts=[
                Part(
                    name="psold",
                    hp=Stat(10),
                    size=700,
                ),
            ],
            ingame=True,
            location=ComLocation(
                coord=MapLocation(x=-1, y=2),
                dir='YZ',
            ),
            tick=12,
            faction=faction_p2,
            texture_data=NTDGroup([simple_com_namedtexture]),
        )},
    ]

    return GSInit(god_gsapply=ggsa)
