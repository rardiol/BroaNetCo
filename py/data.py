
 #  Copyright 2018 Ricardo Luis Souza Ardissone
 #
 #  This file is part of BroaNetCo.
 #
 #  BroaNetCo is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  BroaNetCo is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.


import json


def OptNone():
    return None


def OptSome(i):
    return i


class defJSONEncode():
    def toJSONSerializable(self):
        return self.__dict__


class MyJSONEncoder(json.JSONEncoder):
    def default(self, o):
        return o.toJSONSerializable()


def GSInit(god_gsapply = None):
    return { 'GGSA' : god_gsapply or [] }


def FactionID(i):
    return i


class Faction(defJSONEncode):
    def __init__(self, name=None, faction_type=None, color=None, logged=None):
        self.name = name or "nameless faction"
        self.faction_type = faction_type or "Player"
        self.color = color or {'a': 0.8, 'b': 0.8, 'g': 0.8, 'r': 0.8}
        self.logged = logged if logged is not None else []


def NTDGroup(paths=None, params="TopDown"):
    if paths is None:
        paths2 = [TextureData()]
    else:
        paths2 = []
        for path in paths:
            paths2.append(TextureData(path))
    return (params, paths2)


class TextureData(defJSONEncode):
    def __init__(self, path=None):
        if path is None:
            asset_path = {'Image': 'simple_com.svg'}
        elif type(path) is tuple:
            asset_path = {'XcfLayer': {'file_path': path[0], 'layer': path[1]}}
        elif type(path) is str:
            asset_path = {'Image': path}
        else:
            asset_path = path
        self.asset_path = asset_path


class Combater(defJSONEncode):
    def __init__(self, name=None, faction=0, statuses=None, texture_data=None, tick=0, ingame=True,
                 location=None, parts=None, spacing=0, command=None, detection=None, concealment=None,
                 stats=None,
    ):
        self.name = name or "nameless combater"
        self.ingame = ingame
        self.faction = faction
        self.tick = tick
        self.location = location or ComLocation()
        self.parts = {'Group': (parts, spacing)} or {'Group': ([Part()], spacing)}
        self.statuses = statuses or []
        self.texture_data = texture_data or TextureData()
        self.command = command or 'Direct'
        self.detection = detection if detection is not None else [{'Range': 10000}]
        self.concealment = concealment if concealment is not None else [{'Simple': ()}]
        self.stats = stats or {}


class ActVec(defJSONEncode):
    def __init__(self, possibilities=None, active=None):
        self.possibilities = possibilities or []
        self.active = active or []


class Status(defJSONEncode):
    def __init__(self, name=None, actions=None, substatuses=None):
        self.name = name or "nameless status"
        self.actions = actions or []
        self.substatuses = substatuses or ActVec()


class ActionTargetting(defJSONEncode):
    def __init__(self, min_distance=None, max_distance=None, friendly=False,
                 hostile=False, oneself=False, terrain=False, impassable=False,
                 nosolid=False,
    ):
        self.min_distance = min_distance
        self.max_distance = max_distance
        self.friendly = friendly
        self.hostile = hostile
        self.oneself = oneself
        self.nosolid = nosolid
        self.terrain = terrain
        self.impassable = impassable


class Action(defJSONEncode):
    def __init__(self, name=None, sound_effects=None, source_effects=None, target_effects=None):
        self.name = name or "nameless action"
        self.sound_effects = sound_effects or []
        self.source_effects = source_effects or []
        self.target_effects = target_effects or []


class Coordinate(defJSONEncode):
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class Position(defJSONEncode):
    def __init__(self, coord=None, dir=None):
        self.coord = coord or Coordinate()
        self.dir = dir or 'XY'


ComLocation = Position


class Stat(defJSONEncode):
    def __init__(self, value=10, max=None, min=0):
        self.value = value
        self.max = max or self.value
        self.min = min


class Part(defJSONEncode):
    def __init__(self, name=None, hp=OptNone(), size=OptNone()):
        self.name = name or "unnamed part"
        self.hp = hp
        self.size = size


MapLocation = Coordinate
Tick = int
Mil = int
HpPts = int
usize = int # todo


simple_com_namedtexture = ("simple_com.svg")
simple_com_2_namedtexture = ("simple_com_2.svg")
simple_com_2b_namedtexture = ("simple_com_2b.svg")
simple_com_3_namedtexture = ("simple_com_3.svg")
simple_com_blobs_namedtexture = ("simple_com_blobs.svg")


class MapElement(defJSONEncode):
    def __init__(self, name, ticks, protection_behind, protection_inside, texture_data, detection=None, concealment=None, stats=None):
        self.name = name or "defname"
        self.ticks = ticks
        self.protection_behind = protection_behind
        self.protection_inside = protection_inside
        self.texture_data = texture_data
        self.detection = detection if detection is not None else []
        self.concealment = concealment if concealment is not None else []
        self.stats = stats if stats is not None else {}


class Color(defJSONEncode):
    def __init__(self, r=1.0, g=1.0, b=1.0, a=1.0):
        self.r = r
        self.g = g
        self.b = b
        self.a = a

FACTION_COLOR_TRANSPARENCY = 0.8


def action_target_effect_combater(action_targeting, vec):
    return {'Combater': (action_targeting, vec)}
def action_target_effect_terrain(action_targeting, vec):
    return {'Terrain': (action_targeting, vec)}


def sourcetickdiff(ticks):
    return {'TickDiff': {'Number': ticks}}

def sourcessss(name, status):
    return {"SetSingleSubStatus": [name, status]}

def combrstt():
    return {"RotateSourceToTarget": []}

def sctlcombrstt():
    return {"SourceCombaterTargetLocationEffect": {"RotateSourceToTarget": []}}

def move(name: str, slowness=10, max_distance: usize = 1, sound=None) -> Action:
    return Action(
        name=name,
        sound_effects=([{
            "Play": sound
        }] if sound else []),
        source_effects=[],
        target_effects=[action_target_effect_terrain(
            ActionTargetting(terrain=True, max_distance=max_distance), [
                combrstt(),
                {
                    'Group': [
                        {
                            'TickDiff': {
                                "Calc": (
                                    "Mul",
                                    {
                                        'Target': {
                                            'TickCost': ()
                                        },
                                    },
                                    {
                                        "Number": slowness,
                                    }
                                )
                            }
                        },
                        {
                            'Teleport': ()
                        }
                    ]
                },
            ]
        )]
    )

def move_status(name: str, slowness=10, max_distance: usize = 1) -> Status:
    return Status(
        name=name,
        actions=[
            move("QMove" + name, slowness, max_distance),
        ],
        substatuses=ActVec(),
    )


def xor(a, b):
    return bool(a) != bool(b)


def diceroll(dices, sides, bonus=0):
    return {
        "DiceRoll": "{} {} {}d{}"
        .format(bonus, "-" if xor(dices < 0, sides < 0) else "+", abs(dices), abs(sides))
    }


def number(number):
    return {"Number": number}


def numbercalc(num):
    return {"NumberCalc": num}


last_man_standing = {
    "AddAIGameMaster": (
        0,
        {"LastManStanding": ()},
    ),
}

def gseffect_cloneat(comid, location, effects):
    return {
        "CloneAt": {
            "com": comid,
            "location": location,
            "effects": effects,
        }
    }


def choice_aigm(priority, tick, faction, text, choices):
    return {
        "AddAIGameMaster": (
            priority,
            {
                "Choice": {
                    "tick": tick,
                    "faction": faction,
                    "text": text,
                    "choices": choices,
                }
            }
        )
    }


def expand_map_range_combater(rang, element):
    return {
        "AddAIGameMaster": (
            10000,
            {
                "ExpandMapRangeCombater": {
                    "range": rang,
                    "element": element,
                }
            }
        )
    }


def king_of_the_hill(position, ticks):
    return {
        "AddAIGameMaster": (
            1,
            {
                "KingOfTheHill": {
                    "position": position,
                    "ticks": ticks,
                    "startcount": OptNone(),
                }
            }
        )
    }


def morale(basic, weight_basic, weight_hp, officer=None, on_fail_ticks=50):
    weights_averages = [
            ({'HpRatio1000': ()}, {'Number': weight_hp}),
            ({'Number': basic}, {'Number': weight_basic}),
    ]
    if officer:
        weights_averages.append((
            {'Calc': ("Add", {"Number": officer["base"]}, {'Calc': ("Mul", {"PartName": officer["name"]}, {"Number": officer["bonus"]})})},
            {'Number': officer["weight"]}
        ))
    return {'Morale': {
        'chance': {'WeightedAverage': weights_averages},
        'on_success': (
            [
                {'LogText': ("{com_name} succeded in morale check", "Low")},
            ],
            OptSome('Direct'),
        ),
        'on_fail': [
            [
                {'TickDiff': {'Number': on_fail_ticks}},
                {'LogText': ("{com_name} failed morale check", "High")},
            ],
            OptNone(),
        ],
    }}

def random_ai():
    return {'AI': "Random"}
