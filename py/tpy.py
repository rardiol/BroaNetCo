#!/usr/bin/env python3

 #  Copyright 2018 Ricardo Luis Souza Ardissone
 #
 #  This file is part of BroaNetCo.
 #
 #  BroaNetCo is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  BroaNetCo is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #
 #  You should have received a copy of the GNU General Public License
 #  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.


import os
import os.path
import json
import subprocess

import t1
import w18
import yasrpg
import data

def main():
    missions = (
        ("py_t1_sp", t1.sp),
        ("py_t1_mp", t1.mp),
        ("py_t1_coop", t1.coop),
        ("py_w18_sp", w18.sp),
        ("py_w18_mp", w18.mp),
        ("py_w18_obs", w18.obs),
        ("py_yasrpg_sp", yasrpg.sp),
        ("py_yasrpg_mp", yasrpg.mp),
        ("py_yasrpg_obs", yasrpg.obs),
        ("py_yasrpg_endless_sp", yasrpg.endless_sp),
        ("py_yasrpg_endless_mp", yasrpg.endless_mp),
        ("py_yasrpg_endless_obs", yasrpg.endless_obs),
    )

    try:
        os.mkdir("resources/missions")
    except FileExistsError:
        pass

    for (name, gsinit) in missions:
        with open("resources/missions/"+ name + ".bncgsinit", "wt") as f:
            print("Generating gsinit:", name)
            gsinit = gsinit()
            print("Storing gsinit")
            json.dump(gsinit, f, sort_keys=True, indent=4, cls=data.MyJSONEncoder)

    print("Finished generating python gsinits")

    # subprocess.run(["./target/release/broanetco", "-m" "/missions/t1.bncgsinit"])


if __name__ == "__main__":
    main()
