/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::default::Default;
use thiserror::Error;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[derive(Debug, Hash, PartialEq, Eq, Clone)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
/// A data structure that keeps a Vec of items and some way to determine if each is 'active' or 'inactive'
pub struct ActVecGen<T, Cond>(Vec<(T, Cond)>);

impl<T, Cond> Default for ActVecGen<T, Cond> {
    fn default() -> Self {
        Self(Default::default())
    }
}

/// A data structure that keeps a Vec of items and internally keeps track if each one is 'active' or 'inactive'
pub type ActVec<T> = ActVecGen<T, bool>;

impl<T, Cond> ActVecGen<T, Cond> {
    /// Get the ith possibility without checking if it's active
    pub fn nocheck_get(&self, i: usize) -> Option<&T> {
        self.0.get(i).map(|(t, _a)| t)
    }
    /// Get the ith possibility without checking if it's active
    pub fn nocheck_get_mut(&mut self, i: usize) -> Option<&mut T> {
        self.0.get_mut(i).map(|(t, _a)| t)
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord, Debug, Error)]
pub enum GetError {
    #[error("Element not found in the ActVecGen")]
    NotFound,
    #[error("Element is inactive")]
    Inactive,
}

impl<T, Cond> ActVecGen<T, Cond> {
    /// Gets all elements that are active
    pub fn actives<Input: Copy>(&self, input: Input) -> impl Iterator<Item = &T>
    where
        Cond: Condition<Input>,
    {
        self.0
            .iter()
            .filter_map(move |(t, cond)| if cond.is_active(input) { Some(t) } else { None })
    }

    /// Get the ith possibility
    pub fn get<Input>(&self, i: usize, input: Input) -> Result<&T, GetError>
    where
        Cond: Condition<Input>,
    {
        self.0
            .get(i)
            .ok_or(GetError::NotFound)
            .and_then(|(t, cond)| {
                if cond.is_active(input) {
                    Ok((t, cond))
                } else {
                    Err(GetError::Inactive)
                }
            })
            .map(|(t, _a)| t)
    }
    /// Get the ith possibility
    pub fn get_mut<Input>(&mut self, i: usize, input: Input) -> Result<&mut T, GetError>
    where
        Cond: Condition<Input>,
    {
        self.0
            .get_mut(i)
            .ok_or(GetError::NotFound)
            .and_then(|(t, cond)| {
                if cond.is_active(input) {
                    Ok((t, cond))
                } else {
                    Err(GetError::Inactive)
                }
            })
            .map(|(t, _a)| t)
    }
}

impl<T> ActVec<T> {
    /// Activate one element
    pub fn activate(&mut self, el: usize) {
        self.0[el].1 = true;
    }
    /// Disable one element
    pub fn disable(&mut self, el: usize) {
        self.0[el].1 = false;
    }
    /// Disable all elements
    pub fn disable_all(&mut self) {
        for (_, act) in &mut self.0 {
            *act = false;
        }
    }
    /// Set the single element passed as an argument as the only active element
    pub fn disable_all_and_activate_single(&mut self, el: usize) {
        self.disable_all();
        self.activate(el);
    }
}

impl<T, Cond> From<ActVecGen<T, Cond>> for Vec<(T, Cond)> {
    fn from(actvec: ActVecGen<T, Cond>) -> Self {
        actvec.0
    }
}

impl<T, Cond> From<Vec<(T, Cond)>> for ActVecGen<T, Cond> {
    fn from(vec: Vec<(T, Cond)>) -> Self {
        Self(vec)
    }
}

pub trait Condition<Input> {
    fn is_active(&self, input: Input) -> bool;
}

impl Condition<()> for bool {
    fn is_active(&self, (): ()) -> bool {
        *self
    }
}
