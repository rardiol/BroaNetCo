/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use io::Write;
use std::fmt::Debug;
use std::hash::Hash;
use std::marker::Send;
use std::path::{Path, PathBuf};
use std::sync::mpsc;
use std::{fs, io, iter, ops, thread};

use thiserror::Error;

use serde::de::DeserializeOwned;
use serde::ser::Serialize;

#[macro_use]
extern crate log as debug_log;

pub const SQRT_3: f64 = 1.7320508075688772;

pub trait OptionTrait<T> {
    /// If the Option is Some(v) returns Ok(v)
    /// If the Option is None compute f and if it returns Ok(v2) insert v2 into the Option and return Ok(&mut v2)
    /// Else return the Err from f
    fn get_or_insert_with_result<F, E>(&mut self, f: F) -> Result<&mut T, E>
    where
        F: FnOnce() -> Result<T, E>;
}

impl<T> OptionTrait<T> for Option<T> {
    fn get_or_insert_with_result<F, E>(&mut self, f: F) -> Result<&mut T, E>
    where
        F: FnOnce() -> Result<T, E>,
    {
        if let Some(ref mut i) = *self {
            Ok(i)
        } else {
            match f() {
                Ok(ret) => {
                    *self = Some(ret);
                    Ok(self.as_mut().unwrap())
                }
                Err(err) => Err(err),
            }
        }
    }
}

pub fn crc32<T: Hash>(t: &T) -> u32 {
    let mut s = crc32fast::Hasher::new();
    t.hash(&mut s);
    s.finalize()
}

/// Weighted average/mean over an iterator of (Value, Weight)
pub fn weighted_average<
    IT,
    VALUE: num::Zero
        + std::ops::Mul<WEIGHT, Output = VALUE>
        + std::ops::Div<WEIGHT, Output = OUTPUT>
        + Copy,
    WEIGHT: num::Zero + std::ops::Add + Copy,
    OUTPUT,
>(
    it: IT,
) -> OUTPUT
where
    IT: IntoIterator<Item = (VALUE, WEIGHT)>,
{
    let (sum_value, sum_weight) = it.into_iter().fold(
        (VALUE::zero(), WEIGHT::zero()),
        |(sum_value, sum_weight), (value, weight)| {
            (sum_value + value * weight, sum_weight + weight)
        },
    );
    sum_value / sum_weight
}

pub fn iter_result_convert<
    IN,
    OUT,
    IT: iter::IntoIterator<Item = IN>,
    F: ops::FnMut(IN) -> Result<OUT, ERR>,
    OUTIT: iter::FromIterator<OUT>,
    ERR,
>(
    iter: IT,
    f: F,
) -> Result<OUTIT, ERR> {
    iter.into_iter().map(f).collect()
}

#[derive(Debug, Clone)]
pub enum LoggerMessage<MESSAGE> {
    Message(MESSAGE),
    Flush,
}

/// Starts logger thread and returns a channel to control it
pub fn start_logger_thread<
    INIT: 'static + Serialize + Debug + Send,
    MESSAGE: 'static + Serialize + Debug + Send,
>(
    pathbuf: PathBuf,
    gsinit: INIT,
) -> (
    mpsc::Sender<LoggerMessage<MESSAGE>>,
    thread::JoinHandle<Result<(), io::Error>>,
) {
    let (th_sender, th_receiver) = mpsc::channel::<LoggerMessage<MESSAGE>>();

    let handle = thread::Builder::new()
        .name("logger".to_owned())
        .spawn(move || {
            debug!("Starting logger thread");
            let mut buffer = fs::File::create(pathbuf)?;
            debug!("Opened file: {:?}", buffer);
            buffer.write_all(b"[\n")?;
            serde_json::to_writer_pretty(&mut buffer, &gsinit)?;
            buffer.write_all(b"\n,\n[")?;
            let mut first = true;
            while let Ok(msg) = th_receiver.recv() {
                use LoggerMessage::*;
                debug!("Replay Logging: {:?}", msg);
                match msg {
                    Message(msg) => {
                        if first {
                            first = false;
                        } else {
                            buffer.write_all(b"\n,\n")?;
                        }
                        serde_json::to_writer_pretty(&mut buffer, &msg)?;
                    }
                    Flush => buffer.sync_data()?,
                }
            }
            buffer.write_all(b"\n]\n]")?;
            debug!("Ending logger thread");
            Ok(())
        })
        .unwrap();

    (th_sender, handle)
}

#[derive(Debug, Error)]
pub enum LoadLogError {
    #[error(transparent)]
    IO(#[from] io::Error),
    #[error(transparent)]
    SerdeJson(#[from] serde_json::error::Error),
}

/// Starts logger thread and returns a channel to control it
pub fn load_log<INIT, MESSAGE>(path: &Path) -> Result<(INIT, Vec<MESSAGE>), LoadLogError>
where
    (INIT, Vec<MESSAGE>): DeserializeOwned,
{
    let file = fs::File::open(path)?;
    let reader = io::BufReader::new(file);
    Ok(serde_json::from_reader(reader)?)
}
