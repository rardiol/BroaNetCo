/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use super::*;

use crosschan::SendError;
use tungstenite as tung;

use serde::de::DeserializeOwned;
use serde::Serialize;

pub(crate) type ConnectionJoinHandles = (
    JoinHandle<Result<(), serde_json::Error>>,
    JoinHandle<Result<(), serde_json::Error>>,
    JoinHandle<AResult<Option<tung::protocol::CloseFrame<'static>>>>,
);

#[derive(Debug)]
pub struct Connection<In, Out> {
    sender: crosschan::Sender<In>,
    pub receiver: crosschan::Receiver<Out>,
    sender_ws: crosschan::Sender<tung::Message>,
    // TODO: use this
    join_handles: ConnectionJoinHandles,
}

impl<In, Out> Connection<In, Out>
where
    In: 'static + Debug + Serialize + Send,
    Out: 'static + Debug + DeserializeOwned + Send,
{
    pub fn send(&self, message: In) -> Result<(), SendError<In>> {
        self.sender.send(message)
    }
    pub fn new<Stream: 'static + Sync + Send + io::Read + io::Write>(
        client: tung::WebSocket<Stream>,
        conn_name: String,
    ) -> io::Result<Self> {
        let (sender, receiver, sender_ws, join_handles) = connection(client, conn_name)?;
        Ok(Self {
            sender,
            receiver,
            sender_ws,
            join_handles,
        })
    }
}

pub fn connection<In, Out, Stream: 'static + Sync + Send + io::Read + io::Write>(
    client: tung::WebSocket<Stream>,
    conn_name: String,
) -> io::Result<(
    crossbeam_channel::Sender<In>,
    crossbeam_channel::Receiver<Out>,
    crossbeam_channel::Sender<tung::Message>,
    ConnectionJoinHandles,
)>
where
    In: 'static + Debug + Serialize + Send,
    Out: 'static + Debug + DeserializeOwned + Send,
{
    let (th_send_serializer, th_recv_serializer) = crossbeam_channel::unbounded();
    let (th_send_sender, th_recv_sender) = crossbeam_channel::unbounded();
    let (th_send_deserializer, th_recv_deserializer) = crossbeam_channel::unbounded();
    let (th_send_message, th_recv_message) = crossbeam_channel::unbounded();

    let conn_ser_handle = connection_serializer_thread(
        conn_name.clone(),
        th_recv_serializer,
        th_send_sender.clone(),
    )?;
    let conn_handle = connection_thread(
        conn_name.clone(),
        th_recv_sender,
        th_send_deserializer,
        client,
    )?;
    let conn_deser_handle =
        connection_deserializer_thread(conn_name, th_recv_deserializer, th_send_message)?;

    Ok((
        th_send_serializer,
        th_recv_message,
        th_send_sender,
        (conn_ser_handle, conn_deser_handle, conn_handle),
    ))
}

/// Runs a thread that serializes all messages received on the channel
pub fn connection_serializer_thread<In: 'static + Debug + Serialize + Send>(
    thread_name: String,
    th_recv_serializer: crossbeam_channel::Receiver<In>,
    th_send_sender: crossbeam_channel::Sender<tung::Message>,
) -> io::Result<thread::JoinHandle<Result<(), serde_json::error::Error>>> {
    thread::Builder::new()
        .name(format!("{} serializer", thread_name))
        .spawn(move || {
            debug!("{} serializer: launching", thread_name);
            for message in th_recv_serializer.iter() {
                trace!("{} serializer {:?}", thread_name, message);
                match serde_json::to_string(&message) {
                    Ok(serialized_msg) => {
                        let ws_msg_to_send = tung::Message::Text(serialized_msg);
                        if th_send_sender.send(ws_msg_to_send).is_err() {
                            debug!("{} serializer: closing due to th_send_sender", thread_name);
                            return Ok(());
                        }
                    }
                    Err(err) => {
                        error!(
                            "{} serializer: failed to serialize {:?}, error {}",
                            thread_name, message, err
                        );
                        return Err(err);
                    }
                }
            }
            debug!(
                "{} serializer: closing due to th_recv_serializer",
                thread_name
            );
            Ok(())
        })
}

/// Runs a thread that deserializes all messages received on the channel
pub fn connection_deserializer_thread<Out: 'static + Debug + DeserializeOwned + Send>(
    thread_name: String,
    th_recv_deserializer: crossbeam_channel::Receiver<String>,
    th_send_gamemessage: crossbeam_channel::Sender<Out>,
) -> io::Result<thread::JoinHandle<Result<(), serde_json::error::Error>>> {
    thread::Builder::new()
        .name(format!("{} deserializer", thread_name))
        .spawn(move || {
            debug!("{} deserializer: launching", thread_name);
            for game_message in th_recv_deserializer.iter() {
                trace!(
                    "{} deserializer deserializing {:?}",
                    thread_name,
                    game_message
                );
                let jd = &mut serde_json::Deserializer::from_str(&game_message);

                match serde_ignored::deserialize(jd, |path| {
                    warn!(
                        "Ignoring unexpect path when deserializing game message: {}",
                        path.to_string()
                    )
                }) {
                    Ok(deserialized_msg) => {
                        if th_send_gamemessage.send(deserialized_msg).is_err() {
                            debug!(
                                "{} deserializer: closing due to th_send_gamemessage",
                                thread_name
                            );
                            return Ok(());
                        }
                    }
                    Err(err) => {
                        error!(
                            "{} deserializer: failed to deserialize {:?}, error {}",
                            thread_name, game_message, err
                        );
                        return Err(err);
                    }
                }
            }
            debug!(
                "{} deserializer: closing due to th_recv_deserializer",
                thread_name
            );
            Ok(())
        })
}

/// Runs a thread to own the websocket connection and split it
/// Option 2 of https://github.com/snapview/tungstenite-rs/issues/171#issuecomment-757899992
pub fn connection_thread<Stream: 'static + Sync + Send + io::Read + io::Write>(
    thread_name: String,
    th_recv_sender: crosschan::Receiver<tung::Message>,
    th_send_deserializer: crosschan::Sender<String>,
    mut connection: tung::WebSocket<Stream>,
) -> io::Result<
    //    thread::JoinHandle<()>,
    thread::JoinHandle<AResult<Option<tung::protocol::CloseFrame<'static>>>>,
> {
    thread::Builder::new()
        .name(format!("{} connection", thread_name))
        .spawn(move || {
            debug!("{} connection: launching", thread_name);
            let mut close_frame = None;
            loop {
                loop {
                    match connection.read_message() {
                        Ok(tung::Message::Text(payload)) => {
                            debug!("Text message to {} receiver, {}", thread_name, payload);
                            if th_send_deserializer.send(payload).is_err() {
                                debug!(
                                    "{} receiver: closing due to th_send_deserializer",
                                    thread_name
                                );
                                connection.close(None)?
                            }
                        }
                        Ok(tung::Message::Binary(_)) => {
                            error!("{} receiver: Unexpected binary message", thread_name);
                            return Err(anyhow::anyhow!(
                                "{} receiver: Unexpected binary message",
                                thread_name
                            ));
                        }
                        Ok(tung::Message::Ping(data)) => {
                            debug!("Ping to {} receiver, {:?}", thread_name, data);
                        }
                        Ok(tung::Message::Pong(data)) => {
                            debug!("Pong to {} receiver, {:?}", thread_name, data);
                        }
                        Ok(tung::Message::Close(r)) => {
                            debug!("{} receiver: close message with data {:?}", thread_name, r);
                            close_frame = r.clone();
                            connection.close(r)?
                        }
                        Err(tung::error::Error::Io(io_err))
                            if io_err.kind() == io::ErrorKind::WouldBlock =>
                        {
                            break
                        }
                        Err(tung::error::Error::ConnectionClosed) => {
                            debug!("Recv ConnectionClosed in {} connection", thread_name);
                            return Ok(close_frame);
                        }
                        Err(err) => {
                            error!("{} receiver: connection error: {}", thread_name, err);
                            anyhow::bail!(err)
                        }
                    }
                }

                match th_recv_sender.try_recv() {
                    Ok(message) => {
                        debug!(
                            "Sending message from {} connection, {:?}",
                            thread_name, message
                        );
                        loop {
                            match connection.write_message(message.clone()) {
                                Ok(()) => break,
                                Err(tung::error::Error::Io(io_err))
                                    if io_err.kind() == io::ErrorKind::WouldBlock =>
                                {
                                    std::thread::sleep(std::time::Duration::from_millis(10));
                                }
                                Err(tung::error::Error::ConnectionClosed) => {
                                    debug!("Send ConnectionClosed in {} connection", thread_name);
                                    return Ok(close_frame);
                                }
                                Err(err) => {
                                    error!("{} sender: error while sending: {:?}", err, message);
                                    anyhow::bail!(err);
                                }
                            }
                        }
                    }
                    Err(crosschan::TryRecvError::Empty) => {
                        std::thread::sleep(std::time::Duration::from_millis(10))
                    }
                    Err(crosschan::TryRecvError::Disconnected) => {
                        debug!(
                            "{} connection: closing due to th_recv_connection",
                            thread_name
                        );
                        connection.close(None)?
                    }
                }
            }
        })
}
