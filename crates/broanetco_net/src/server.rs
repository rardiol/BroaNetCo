/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::collections::HashMap;
use std::marker::Send;
use std::sync::Arc;
use std::{io, net, thread};
use thread::JoinHandle;

use rand::prelude::*;

use crosschan::select;

use tungstenite as tung;

use crate::game::init::GSInit;
use crate::game::{FactionID, GS};
use crate::game::{GRng, Void};

use crate::util;
use crate::util::*;
use crate::*;

pub type ConnectionID = usize;

/// Connection side of a link between a faction thread and a network connection thread
type FactionConnectionChannels = (
    crosschan::Sender<GameMessage>,
    crosschan::Receiver<FGameMessage>,
);

#[derive(Debug)]
struct FactionConnections {
    connections: HashMap<FactionID, crosschan::Receiver<FactionConnectionChannels>>,
}

#[derive(Debug, Error)]
enum TakeConnectionError {
    #[error("Faction {0} not available for connection")]
    FactionNotAvailable(FactionID),
    #[error("Faction {0} connection has already been assigned")]
    FactionAlreadyAssigned(FactionID),
    #[error("Faction {0} thread has disconnected")]
    FactionThreadDisconnect(FactionID),
}

impl FactionConnections {
    /// Get all available connections ids
    pub fn get_available(&self) -> Vec<FactionID> {
        self.connections
            .iter()
            .filter_map(
                |(facid, chan)| {
                    if !chan.is_empty() {
                        Some(*facid)
                    } else {
                        None
                    }
                },
            )
            .collect()
    }
    /// Tries to take a connection
    pub fn take_connection(
        &self,
        facid: FactionID,
    ) -> Result<FactionConnectionChannels, TakeConnectionError> {
        if let Some(channel) = self.connections.get(&facid) {
            match channel.try_recv() {
                Ok(channels) => Ok(channels),
                Err(crosschan::TryRecvError::Disconnected) => {
                    Err(TakeConnectionError::FactionThreadDisconnect(facid))
                }
                Err(crosschan::TryRecvError::Empty) => {
                    Err(TakeConnectionError::FactionAlreadyAssigned(facid))
                }
            }
        } else {
            Err(TakeConnectionError::FactionNotAvailable(facid))
        }
    }
    pub fn new(
        th_game_sender: crosschan::Sender<(FactionID, GameMessage)>,
        factions: Vec<FactionID>,
    ) -> (Self, Vec<crosschan::Sender<FGameMessage>>) {
        let (faction_conn_client, th_game_senders) = factions
            .into_iter()
            .map(|faction| {
                let (th_fgame_sender, th_fgame_receiver) = crossbeam_channel::unbounded();
                (
                    (
                        faction,
                        // TODO: unwrap and joinhandle
                        faction_thread(faction, th_game_sender.clone(), th_fgame_receiver)
                            .unwrap()
                            .1,
                    ),
                    th_fgame_sender,
                )
            })
            .unzip();

        debug!(
            "Generate server thread channels: {:?}",
            (&faction_conn_client)
        );
        (
            Self {
                connections: faction_conn_client,
            },
            th_game_senders,
        )
    }
}

pub fn server<
    RNG: 'static + rand::RngCore + Send,
    RLC: 'static + game::replay::ReplayLoggerControlTrait + Clone + Send,
>(
    address: &str,
    port: u16,
    factions: Vec<FactionID>,
    gsinit: GSInit,
    main_thread: thread::Thread,
    replay_logger: Option<RLC>,
    rng: RNG,
) -> AResult<(JoinHandle<()>, JoinHandle<AResult<()>>)> {
    let server = net::TcpListener::bind((address, port))?;
    info!("start server for factions: {:?}", factions);

    let (th_game_sender, th_game_receiver) = crosschan::unbounded();

    let (available, th_game_senders) = FactionConnections::new(th_game_sender, factions);
    let available = Arc::new(available);

    let game_server_handle = game_server(
        gsinit.clone(),
        th_game_senders,
        th_game_receiver,
        replay_logger,
        rng,
    )
    .context("Failed to launch game server")?;

    // TODO: end the main server thread when the game ends or fails
    let main_server_handle = thread::Builder::new()
        .name("main server".to_string())
        .spawn(move || {
            main_thread.unpark();
            for i in 0.. {
                let gsinit = gsinit.clone();
                info!("Server waiting for connection {}", i);
                let request = server.accept();
                match connection_thread(i, request, gsinit, Arc::clone(&available)) {
                    Ok(_connection_handle) => (), // TODO: do something with connection handle
                    Err(err) => error!("Failed to open connection {} with error {:?}", i, err),
                }
            }
        })
        .context("Failed to launch main server thread")?;
    Ok((main_server_handle, game_server_handle))
}

fn game_server<
    RNG: 'static + rand::RngCore + Send,
    RLC: 'static + game::replay::ReplayLoggerControlTrait + Clone + Send,
>(
    gsinit: GSInit,
    th_send_players: Vec<crosschan::Sender<FGameMessage>>,
    th_recv_players: crosschan::Receiver<(FactionID, GameMessage)>,
    replay_logger: Option<RLC>,
    mut rng: RNG,
) -> io::Result<thread::JoinHandle<AResult<()>>> {
    thread::Builder::new()
        .name("server gameplay".to_string())
        .spawn(move || {
            let mut gs: GS<NamedTextureDataTransfer> = gsinit.into();

            gs.update::<Void, _>(&mut None, None, replay_logger.clone());

            loop {
                match th_recv_players.recv() {
                    Ok((faction_id, GameMessage::GSApply(gsapply))) => {
                        // Set all of the client PRNGs with a seed from the server CSPRNG

                        info!("From {:?} applying: {:?}", faction_id, &gsapply);

                        let mut seed: <GRng as SeedableRng>::Seed = Default::default();
                        rng.fill(&mut seed);

                        debug!("Reseeding: {:?}", &seed);

                        let seed_gsapply = GodGSApply::SetRNG(seed);

                        gs.god_gsapply::<Void, _>(
                            seed_gsapply.clone(),
                            &None,
                            &mut None,
                            &replay_logger,
                        );
                        gs.gsapply::<Void, _>(&gsapply, &None, &mut None, &replay_logger);
                        gs.update::<Void, _>(&mut None, None, replay_logger.clone());

                        let seed_message = FGameMessage::GodGSApply { msg: seed_gsapply };
                        let gsapply_message = FGameMessage::GSApply {
                            origin: faction_id,
                            msg: gsapply.clone(),
                        };
                        let sync_message = FGameMessage::SyncChecksum();

                        debug!("game server sending to player threads");
                        for th_sender in &th_send_players {
                            trace!("game server: sending to {:?}", th_sender);
                            th_sender
                                .send(seed_message.clone())
                                .context("Failed to send seed_message")?;
                            th_sender
                                .send(gsapply_message.clone())
                                .context("Failed to send gsapply_message")?;
                            th_sender
                                .send(sync_message.clone())
                                .context("Failed to send sync_message")?;
                            trace!("game server: finished ending to {:?}", th_sender);
                        }
                    }
                    Ok((facid, GameMessage::SyncChecksumEcho(client_hash))) => {
                        let server_hash = gs.crc32();
                        if client_hash == server_hash {
                            debug!("synchashecho for {:?}, hash: {:?}", facid, client_hash);
                        } else {
                            warn!(
                                "synchashecho fail for {:?}, client: {:?}, server: {:?}",
                                facid, client_hash, server_hash
                            );
                        }
                    }
                    Ok((facid, unk)) => {
                        panic!("Unexpected GameMessage from {:?}: {:?}", facid, unk)
                    }
                    Err(err) => panic!("Error in server gameplay server from {:?}", err),
                }
            }
        })
}

/// A thread for each non-local player
/// Is separate from the connection threads and should survive between network disconnects
/// and avoid panicking. Sends a new connection over its return channel and if it disconnects
/// sends another one
fn faction_thread(
    facid: FactionID,
    th_faction2game_sender: crosschan::Sender<(FactionID, GameMessage)>,
    th_game2faction_receiver: crosschan::Receiver<FGameMessage>,
) -> io::Result<(
    thread::JoinHandle<()>,
    crosschan::Receiver<FactionConnectionChannels>,
)> {
    // Bounded as there is no reason for there to be more than one connection in the queue
    let (th_create_faction_connection_sender, th_create_faction_connection_receiver) =
        crosschan::bounded(1);

    let thread_name = format!("faction {}", facid);
    let handle = thread::Builder::new()
        .name(thread_name.clone())
        .spawn(move || {
            // Stores messages that should be sent on reconnections
            let mut messages_backup: Vec<FGameMessage> = Vec::new();
            loop {
                let (th_faction2connection_sender, th_faction2connection_receiver) =
                    crosschan::unbounded();
                let (th_connection2faction_sender, th_connection2faction_receiver) =
                    crosschan::unbounded();

                for message in &messages_backup {
                    th_faction2connection_sender.send(message.clone()).expect("failed to send to local unbounded channel, this should not happen");
                }

                debug!("thread {}: waiting for connection", thread_name);
                if th_create_faction_connection_sender
                    .send((th_connection2faction_sender, th_faction2connection_receiver)).is_err()
                {
                    error!("thread {}: failed to send new connection", thread_name);
                    return;
                }
                debug!("thread {}: connected", thread_name);
                // Simply pass along messages up and down until a disconnect
                // stores messages passed down (to connection) for reconnects
                loop {
                    select!{
                        recv(th_game2faction_receiver) -> msg => {
                            match msg {
                                Ok(msg) => {
                                    messages_backup.push(msg.clone());
                                    debug!("thread {}: passing down {:?}", thread_name, msg);
                                    match th_faction2connection_sender.send(msg) {
                                        Ok(()) => (),
                                        Err(_msg) => {
                                            error!("thread {}: disconnected from connection sender", thread_name);
                                            break;
                                        }
                                    }
                                }
                                Err(crosschan::RecvError) => {
                                    error!("thread {}: game server disconnected", thread_name);
                                    return;
                                }
                            }
                        }
                        recv(th_connection2faction_receiver) -> msg => {
                            match msg {
                                Ok(msg) => {
                                    debug!("thread {}: passing up {:?}", thread_name, msg);
                                    match th_faction2game_sender.send((facid, msg)) {
                                        Ok(()) => (),
                                        Err(_msg) => {
                                            error!("thread {}: game server disconnected", thread_name);
                                            return;
                                        }
                                    }
                                }
                                Err(crosschan::RecvError) => {
                                    error!("thread {}: disconnected from connection receiver", thread_name);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        })?;
    Ok((handle, th_create_faction_connection_receiver))
}

fn connection_thread(
    id: ConnectionID,
    request: io::Result<(net::TcpStream, net::SocketAddr)>,
    gsinit: GSInit,
    available: Arc<FactionConnections>,
) -> io::Result<thread::JoinHandle<AResult<()>>> {
    info!("new connection {:?}", available);
    thread::Builder::new()
        .name(format!("Connection {}", id))
        .spawn::<_, AResult<()>>(move || {
            let (thread_name_base, join_handles) = {
                let (tcp_stream, socket_addr) = request?;
                tcp_stream.set_nonblocking(true)?;
                // TODO: protocol check https://github.com/snapview/tungstenite-rs/issues/145#issuecomment-713581499
                /*
                if !request.protocols().contains(&PROTOCOL_NAME.to_string()) {
                    request.reject().map_err(|x| x.1)?;
                    anyhow::bail!("Server refused connection {} due to protocol error", id);
                }
                */

                let mut attempt = tung::accept(tcp_stream);
                let client = loop {
                    match attempt {
                        Ok(stream) => break stream,
                        Err(tung::HandshakeError::Interrupted(midhandshake)) => {
                            std::thread::sleep(std::time::Duration::from_millis(10));
                            attempt = midhandshake.handshake();
                        }
                        Err(tung::HandshakeError::Failure(err)) => anyhow::bail!(err),
                    }
                };

                info!("Server connection {} starting from {}", id, socket_addr);

                let thread_name_base = format!("ser cid:{}", id);

                //////////////////////////////////////////////////
                //         Connection-------------              //
                //         ^         \            \             //
                // th_gamemessage   th_serialize   \            //
                //       /              v           v           //
                // Deserializer      Serializer1    Serializer2 //
                //       ^               |          /           //
                //th_deserializer  ---->th_sender<--            //
                //       |        /      v                      //
                //    Receiver----      Sender                  //
                //       ^               v                      //
                //   ws_receiver      ws_sender                 //
                //////////////////////////////////////////////////

                let (th_send_serializer, th_recv_gamemessage, th_send_sender, join_handles) =
                    util::connection(client, thread_name_base.clone())
                        .context("Failed to create connection helper threads")?;

                th_send_serializer
                    .send(FGameMessage::GSInit(gsinit))
                    .context("Failed to send GSInit to client")?;

                // TODO: what happens on empty available ?
                // Faction assignment
                let (facid, (th_connection2faction_sender, th_connection2faction_receiver)) = loop {
                    th_send_serializer
                        .send(FGameMessage::FactionOffer {
                            factions: available.get_available(),
                        })
                        .context("Failed to send FactionOffer")?;
                    match th_recv_gamemessage.recv().with_context(|| {
                        let errmsg = "Receiver thread channel crashed, aborting faction assignment"
                            .to_owned();
                        error!("{}", errmsg);
                        errmsg
                    })? {
                        GameMessage::FactionRequest { faction } => {
                            match available.take_connection(faction) {
                                Ok(faction_conn) => {
                                    info!("Assigning faction {} to connection {}", faction, id);
                                    th_send_serializer
                                        .send(FGameMessage::FactionAssign { faction })
                                        .context("Failed to send FactionAssign message")?;
                                    break (faction, faction_conn);
                                }
                                Err(err @ TakeConnectionError::FactionNotAvailable(_)) => {
                                    warn!("connection {} faction assignment error: {}", id, err);
                                }
                                Err(err @ TakeConnectionError::FactionAlreadyAssigned(_)) => {
                                    warn!("connection {} faction assignment error: {}", id, err);
                                }
                                Err(err @ TakeConnectionError::FactionThreadDisconnect(_)) => {
                                    error!("connection {} faction assignment error: {}", id, err);
                                    anyhow::bail!(err);
                                }
                            }
                        }
                        msg => {
                            anyhow::bail!(
                                "Client sent unexpected {:?} during faction assignment",
                                msg
                            );
                        }
                    }
                };

                let thread_name_base = {
                    let mut t = thread_name_base;
                    t.push_str(&format!(", facid: {}", facid));
                    t
                };

                {
                    connection_serializer_thread(
                        thread_name_base.clone(),
                        th_connection2faction_receiver,
                        th_send_sender,
                    )
                    .context("Failed to create connection serializer thread")?;
                    for message in th_recv_gamemessage.iter() {
                        // Starts a new thread that connects the messages from other connections to this
                        debug!(
                            "thread {} connection: received {:?}",
                            thread_name_base, message
                        );
                        match message {
                            a @ GameMessage::GSApply(_) => {
                                th_connection2faction_sender
                                    .send(a)
                                    .context("Failed to send to game server")?;
                            }
                            a @ GameMessage::SyncChecksumEcho(_) => {
                                th_connection2faction_sender
                                    .send(a)
                                    .context("Failed to send to game server")?;
                            }
                            _ => anyhow::bail!(
                                "Client sent unexpected GameMessage during gameplay, {:?}",
                                message
                            ),
                        }
                    }
                    debug!(
                        "thread {} connection: closing due to th_recv_gamemessage",
                        thread_name_base
                    );
                };
                (thread_name_base, join_handles)
            };
            let (a, b, c) = join_handles;
            debug!("thread {} connection: joining", thread_name_base);
            a.join().unwrap()?;
            b.join().unwrap()?;
            c.join().unwrap()?;
            debug!("thread {} connection: all ok", thread_name_base);
            Ok(())
        })
}
