/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

#[macro_use]
extern crate log as debug_log;
#[macro_use]
extern crate serde_derive;

extern crate broanetco_game as game;
extern crate broanetco_util;

use crate::game::init::GSInit;
use crate::game::{FactionID, GSApply, GSChecksum, GodGSApply, NamedTextureDataTransfer};

use std::fmt::Debug;
use std::{io, thread};
use thread::JoinHandle;

use anyhow::anyhow;
use anyhow::Context as AContext;
use anyhow::Result as AResult;
use thiserror::Error;

use crossbeam_channel as crosschan;
use either::*;

// TODO: use async websocket?

/// Game client
pub mod client;
/// Game server/host
pub mod server;
/// General networking utils
mod util;

pub use client::Client;

/// The type of messages that clients send
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum GameMessage {
    FactionRequest {
        faction: FactionID,
    },
    GSApply(GSApply),
    /// Answers a request for a (part)hash of its GS
    SyncChecksumEcho(GSChecksum),
}

/// The type of message that the server sends
/// Currently this identifies the original sender
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum FGameMessage {
    GSInit(GSInit),
    // TODO: state transitions change valid messages at specific moments
    FactionOffer {
        factions: Vec<FactionID>,
    },
    FactionAssign {
        faction: FactionID,
    },
    // TODO: still needed?
    GSApply {
        origin: FactionID,
        msg: GSApply,
    },
    GodGSApply {
        msg: GodGSApply<NamedTextureDataTransfer>,
    },
    /// Ask for the client to send a (part)hash of its GS
    SyncChecksum(),
}

#[allow(dead_code)]
const PROTOCOL_NAME: &str = concat!(env!("CARGO_PKG_NAME"), "-", env!("CARGO_PKG_VERSION"));
