/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use super::*;

use std::net::TcpStream;

use crossbeam_channel::SendError;

use tungstenite as tung;
use url::Url;

use crate::game::init::GSInit;
use crate::game::{FactionID, GSChecksum};

#[derive(Debug)]
pub struct Client<State>(util::Connection<GameMessage, FGameMessage>, State);

// Type states
use self::states::*;
pub mod states {
    #[derive(Debug)]
    pub struct Start;
    #[derive(Debug)]
    pub struct FactionAssign;
    #[derive(Debug)]
    pub struct Game;
}

impl Client<Start> {
    /// Create a new client connection to the specified server and port
    pub fn new(address: &str, port: u16) -> AResult<Self> {
        let url: Url = {
            let mut url = Url::parse(&("ws://".to_string() + address))?;
            url.set_port(Some(port))
                .map_err(|()| anyhow!("failed to set port"))?;
            url
        };

        //TODO: Add retry with time interval
        let stream = TcpStream::connect((address, port))?;
        stream.set_nonblocking(true)?;
        let mut attempt = tung::client(url, stream);
        let client = loop {
            match attempt {
                Ok(stream) => break stream,
                Err(tung::HandshakeError::Interrupted(midhandshake)) => {
                    std::thread::sleep(std::time::Duration::from_millis(10));
                    attempt = midhandshake.handshake();
                }
                Err(tung::HandshakeError::Failure(err)) => anyhow::bail!(err),
            }
        }; // TODO: ws subprotocol

        Ok(Self(
            util::Connection::new(client.0, "client".to_owned())?,
            Start,
        ))
    }
}

impl Client<Start> {
    /// Expects and processes a GSInit from the server
    pub fn receiver_gs(self) -> AResult<(Client<FactionAssign>, GSInit)> {
        let fgame_message = self.0.receiver.recv()?;
        match fgame_message {
            FGameMessage::GSInit(gsinit) => Ok((Client(self.0, FactionAssign), gsinit)),
            other => anyhow::bail!("Expected GSInit, got: {:?}", other),
        }
    }
}

impl Client<FactionAssign> {
    /// Negotiates for a faction assignment from the server
    pub fn get_assigned_faction(
        self,
        faction: Option<FactionID>,
    ) -> AResult<(Client<Game>, FactionID)> {
        loop {
            let fgame_message = self.0.receiver.recv()?;
            match fgame_message {
                FGameMessage::FactionOffer { factions } => {
                    let faction = match faction {
                        Some(faction) if factions.contains(&faction) => faction,
                        None if !factions.is_empty() => factions[0],
                        Some(_) | None => {
                            let errmsg = format!(
                                "get_assigned_faction failed, requested: {:?}, available {:?}",
                                faction, factions
                            );
                            error!("{}", errmsg);
                            anyhow::bail!(errmsg)
                        }
                    };
                    self.0.send(GameMessage::FactionRequest { faction })?;
                }
                // TODO: reject undesired faction
                FGameMessage::FactionAssign { faction } => {
                    return Ok((Client(self.0, Game), faction))
                }
                other => anyhow::bail!("Expected faction assignment, got: {:?}", other),
            }
        }
    }
}

impl Client<Game> {
    // TODO: multiple messages at a time?
    /// Checks if the server sent a FGameMessage::GSApply and returns it
    pub fn process_msgs<F: FnOnce() -> GSChecksum>(
        &self,
        checksum: F,
    ) -> AResult<Option<Either<(FactionID, GSApply), GodGSApply<NamedTextureDataTransfer>>>> {
        match self.0.receiver.try_recv() {
            Ok(FGameMessage::GSApply { origin, msg }) => Ok(Some(Left((origin, msg)))),
            Ok(FGameMessage::GodGSApply { msg }) => Ok(Some(Right(msg))),
            Ok(FGameMessage::SyncChecksum()) => {
                self.send_sync_echo(checksum())?;
                Ok(None)
            }
            Ok(other) => anyhow::bail!("Expected GSApply, got: {:?}", other),
            Err(crossbeam_channel::TryRecvError::Empty) => Ok(None),
            Err(crossbeam_channel::TryRecvError::Disconnected) => {
                anyhow::bail!("process_msgs: channel closed unexpectedly")
            }
        }
    }

    pub fn send_gsa(&self, msg: GSApply) -> Result<(), SendError<GameMessage>> {
        self.0.send(GameMessage::GSApply(msg))
    }

    fn send_sync_echo(&self, msg: GSChecksum) -> Result<(), SendError<GameMessage>> {
        self.0.send(GameMessage::SyncChecksumEcho(msg))
    }
}
