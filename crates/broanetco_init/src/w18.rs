/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::convert::TryInto;

use crate::helpers::*;

// TODO: what happens when a musket holding unit only has officers remaining

/*
*/

fn morale(
    base: usize,
    weight_basic: usize,
    weight_hp: usize,
    officers: Vec<(String, usize, usize, usize)>, // Part name, base, bonus, weight
) -> Command {
    Command::Direct // TODO
}

fn cannon() -> CombaterBuilder {
    /* TODO
    div
    150
    bncl::number::Combater::PartCond(bncl::condition::ConditionDet
    )),
    */
    let cannon_status = {
        let status_name = "cannon";
        let load_action = |name, target| Action {
            name,
            keycode: Some('r'),
            sound_effects: vec![effect::Sound::Play("wood2".to_owned())],
            source_effects: vec![
                effect::Combater::TickDiff(bncnumber!(dice "20 + 3d10")),
                effect::Combater::SetSingleSubStatus((status_name.to_owned(), vec![]), target),
            ],
            ..Default::default()
        };
        let loaded_status = |name, range, damage, multiplier| Status {
            name,
            actions: vec![Action {
                name: "cannon-fire".to_owned(),
                keycode: Some('f'),
                sound_effects: vec![effect::Sound::Play("Explosion-LS100155".to_owned())],
                source_effects: vec![
                    effect::Combater::TickDiff(bncnumber!(dice "20 + 3d10")),
                    effect::Combater::SetSingleSubStatus((status_name.to_owned(), vec![]), 0),
                ],
                target_effects: vec![ActionTargetEffect::Combater(
                    attack_action_target(range),
                    vec![effect::CombaterCombater::Repeated(
                        Box::new(effect::CombaterCombater::OnePartToMissablePartSizeNormal {
                            source_filter: bnccondition!(condition::PartCombater::Source(
                                condition::Part::Name("part-cannon".to_owned())
                            )),
                            stddev: bncnumber!(250),
                            hit: Some(Box::new(effect::PartPart::Target(effect::Part::HpDiff(
                                damage,
                            )))),
                            miss: None,
                        }),
                        multiplier,
                    )],
                )],
                ..Default::default()
            }],
            ..Default::default()
        };
        Status {
            name: status_name.to_owned(),
            substatuses: vec![
                (
                    Status {
                        name: "unloaded".to_owned(),
                        actions: vec![
                            load_action("cannon-load-roundshot".to_owned(), 1),
                            load_action("cannon-load-grapeshot".to_owned(), 2),
                        ],
                        ..Default::default()
                    },
                    true,
                ),
                (
                    loaded_status(
                        "loaded-roundshot".to_owned(),
                        6,
                        bncnumber!(dice "-250-10d15"),
                        bncnumber!(1),
                    ),
                    false,
                ),
                (
                    loaded_status(
                        "loaded-grapeshot".to_owned(),
                        2,
                        bncnumber!(dice "-50-2d15"),
                        bncnumber!(10),
                    ),
                    false,
                ),
            ]
            .into(),
            ..Default::default()
        }
    };

    let melee_status = Status {
        name: "cannon-melee-status".to_owned(),
        actions: vec![pp_attack_action(
            "melee".to_owned(),
            Some('b'),
            bncnumber!(dice "10 + 3d10"),
            vec![
                (
                    bncnumber!(dice "0 - 1d10"),
                    condition::Part::Name("officer".to_owned()),
                ),
                (
                    bncnumber!(dice "0 - 1d6"),
                    condition::Part::Name("gunner".to_owned()),
                ),
                (
                    bncnumber!(dice "0 - 1d4"),
                    condition::Part::Name("bugler".to_owned()),
                ),
            ],
        )],
        ..Default::default()
    };

    let part_officer = Part {
        name: "officer".to_owned(),
        hp: Some(Stat::new_max(30)),
        size: Some(30),
    };
    let part_cannon = Part {
        name: "part-cannon".to_owned(),
        hp: None,
        size: None,
    };
    let part_gunner = Part {
        name: "gunner".to_owned(),
        hp: Some(Stat::new_max(30)),
        size: Some(30),
    };
    let part_bugler = Part {
        name: "bugler".to_owned(),
        hp: Some(Stat::new_max(30)),
        size: Some(30),
    };

    let battery = itertools::repeat_n(part_officer, 1)
        .chain(itertools::repeat_n(part_cannon, 1))
        .chain(itertools::repeat_n(part_gunner, 3))
        .chain(itertools::repeat_n(part_bugler, 1));

    let mut ret = CombaterBuilder::default();
    ret.statuses(vec![
        los_view_status(3, 0),
        simple_view_status(),
        cannon_status,
        melee_status,
        move_status_stamina(20, 1, 10, 4, 10, 10),
    ])
    .command(morale(
        970,
        100,
        150,
        vec![("officer".to_owned(), 500, 600, 200)],
    ))
    // TODO: separate the two batteries
    .parts(PartGroup::Group(
        battery.clone().chain(battery).collect(),
        15,
    ))
    .edata_insert((stamina_id(), Stat::new_max(1000)))
    .texture_data((TextureParams::TopDown, vec![textures::cannon()]));
    ret
}

fn light_infantry() -> CombaterBuilder {
    let rifle_status = {
        let status_name = "rifle";
        Status {
            name: status_name.to_owned(),
            substatuses: vec![
                (
                    Status {
                        name: "rifle-loaded".to_owned(),
                        actions: vec![Action {
                            name: "rifle-fire".to_owned(),
                            keycode: Some('f'),
                            sound_effects: vec![effect::Sound::Play("rifle_3".to_owned())],
                            source_effects: vec![
                                effect::Combater::TickDiff(bncnumber!(dice "10 + 1d10")),
                                effect::Combater::SetSingleSubStatus(
                                    (status_name.to_owned(), vec![]),
                                    1,
                                ),
                            ],
                            target_effects: vec![ActionTargetEffect::Combater(
                                attack_action_target(4),
                                vec![effect::CombaterCombater::OnePartToMissablePartSizeNormal {
                                    source_filter: bnccondition!(condition::PartCombater::Source(
                                        condition::Part::Name("light-infantry".to_owned())
                                    )),
                                    stddev: bncnumber!(150),
                                    hit: Some(Box::new(effect::PartPart::Target(
                                        effect::Part::HpDiff(bncnumber!(dice "-25-1d15")),
                                    ))),
                                    miss: None,
                                }],
                            )],
                        }],
                        ..Default::default()
                    },
                    true,
                ),
                (
                    Status {
                        name: "rifle-unloaded".to_owned(),
                        actions: vec![Action {
                            name: "musket-reload".to_owned(),
                            keycode: Some('r'),
                            sound_effects: vec![effect::Sound::Play("wood2".to_owned())],
                            source_effects: vec![
                                effect::Combater::TickDiff(bncnumber!(dice "10 + 1d10")),
                                effect::Combater::SetSingleSubStatus(
                                    (status_name.to_owned(), vec![]),
                                    0,
                                ),
                            ],
                            ..Default::default()
                        }],
                        ..Default::default()
                    },
                    false,
                ),
            ]
            .into(),
            ..Default::default()
        }
    };
    let melee_status = Status {
        name: "lightinfantry-melee-status".to_owned(),
        actions: vec![pp_attack_action(
            "melee".to_owned(),
            Some('b'),
            bncnumber!(dice "10 + 3d10"),
            vec![
                (
                    bncnumber!(dice "0 - 1d10"),
                    condition::Part::Name("officer".to_owned()),
                ),
                (
                    bncnumber!(dice "0 - 1d8"),
                    condition::Part::Name("light-infantry".to_owned()),
                ),
            ],
        )],
        ..Default::default()
    };

    let part_officer = Part {
        name: "officer".to_owned(),
        hp: Some(Stat::new_max(30)),
        size: Some(30),
    };
    let part_lightinf = Part {
        name: "light-infantry".to_owned(),
        hp: Some(Stat::new_max(30)),
        size: Some(30),
    };

    let mut ret = CombaterBuilder::default();
    ret.statuses(vec![
        los_view_status(3, 0),
        simple_view_status(),
        melee_status,
        rifle_status,
        move_status_stamina(20, 1, 10, 4, 10, 10),
    ])
    .command(morale(
        970,
        100,
        150,
        vec![("officer".to_owned(), 500, 600, 200)],
    ))
    .parts(PartGroup::Group(
        itertools::repeat_n(part_officer, 1)
            .chain(itertools::repeat_n(part_lightinf, 9))
            .collect(),
        30,
    ))
    .edata_insert((stamina_id(), Stat::new_max(1000)))
    .texture_data((TextureParams::TopDown, vec![textures::light_infantry()]));
    ret
}

fn line_infantry() -> CombaterBuilder {
    let musket_status = {
        let status_name = "musket";
        Status {
            name: status_name.to_owned(),
            substatuses: vec![
                (
                    Status {
                        name: "musket-loaded".to_owned(),
                        actions: vec![Action {
                            name: "musket-fire".to_owned(),
                            keycode: Some('f'),
                            sound_effects: vec![effect::Sound::Play("rifle_3".to_owned())],
                            source_effects: vec![
                                effect::Combater::TickDiff(bncnumber!(dice "10 + 1d10")),
                                effect::Combater::SetSingleSubStatus(
                                    (status_name.to_owned(), vec![]),
                                    1,
                                ),
                            ],
                            target_effects: vec![ActionTargetEffect::Combater(
                                attack_action_target(3),
                                vec![effect::CombaterCombater::OnePartToMissablePartSizeNormal {
                                    source_filter: bnccondition!(condition::PartCombater::Source(
                                        condition::Part::Name("line-infantry".to_owned())
                                    )),
                                    stddev: bncnumber!(200),
                                    hit: Some(Box::new(effect::PartPart::Target(
                                        effect::Part::HpDiff(bncnumber!(dice "-20-1d20")),
                                    ))),
                                    miss: None,
                                }],
                            )],
                        }],
                        ..Default::default()
                    },
                    true,
                ),
                (
                    Status {
                        name: "musket-unloaded".to_owned(),
                        actions: vec![Action {
                            name: "musket-reload".to_owned(),
                            keycode: Some('r'),
                            sound_effects: vec![effect::Sound::Play("wood2".to_owned())],
                            source_effects: vec![
                                effect::Combater::TickDiff(bncnumber!(dice "20+3d10")),
                                effect::Combater::SetSingleSubStatus(
                                    (status_name.to_owned(), vec![]),
                                    0,
                                ),
                            ],
                            ..Default::default()
                        }],
                        ..Default::default()
                    },
                    false,
                ),
            ]
            .into(),
            ..Default::default()
        }
    };
    let bayonet_status = Status {
        name: "lineinfantry-bayonet-status".to_owned(),
        actions: vec![pp_attack_action(
            "bayonet".to_owned(),
            Some('b'),
            bncnumber!(dice "10 + 3d10"),
            vec![
                (
                    bncnumber!(dice "0 - 1d10"),
                    condition::Part::Name("officer".to_owned()),
                ),
                (
                    bncnumber!(dice "0 - 1d10"),
                    condition::Part::Name("line-infantry".to_owned()),
                ),
            ],
        )],
        ..Default::default()
    };

    let part_officer = Part {
        name: "officer".to_owned(),
        hp: Some(Stat::new_max(30)),
        size: Some(30),
    };
    let part_lineinf = Part {
        name: "line-infantry".to_owned(),
        hp: Some(Stat::new_max(30)),
        size: Some(30),
    };

    let mut ret = CombaterBuilder::default();
    ret.statuses(vec![
        los_view_status(3, 0),
        simple_view_status(),
        bayonet_status,
        musket_status,
        move_status_stamina(20, 1, 10, 4, 10, 10),
    ])
    .command(morale(
        970,
        100,
        150,
        vec![("officer".to_owned(), 500, 600, 200)],
    ))
    .parts(PartGroup::Group(
        itertools::repeat_n(part_officer, 1)
            .chain(itertools::repeat_n(part_lineinf, 19))
            .collect(),
        10,
    ))
    .edata_insert((stamina_id(), Stat::new_max(1000)))
    .texture_data((TextureParams::TopDown, vec![textures::line_infantry()]));
    ret
}

fn cavalry() -> CombaterBuilder {
    let part_officer = Part {
        name: "officer".to_owned(),
        hp: Some(Stat::new_max(40)),
        size: Some(50),
    };
    let part_cavalry = Part {
        name: "cavalryman".to_owned(),
        hp: Some(Stat::new_max(40)),
        size: Some(50),
    };
    let sword_status = Status {
        name: "cavalry-sword-status".to_owned(),
        actions: vec![pp_attack_action(
            "attack".to_owned(),
            Some('a'),
            number::Random::DiceRoll("20 + 4d4".to_string()).into_calc(),
            vec![
                (
                    number::Random::DiceRoll("0 - 1d10".to_string()).into_calc(),
                    condition::Part::Name("officer".to_owned()),
                ),
                (
                    number::Random::DiceRoll("0 - 1d10".to_string()).into_calc(),
                    condition::Part::Name("cavalry".to_owned()),
                ),
            ],
        )],
        ..Default::default()
    };
    let mut ret = CombaterBuilder::default();
    ret.statuses(vec![
        los_view_status(3, 0),
        simple_view_status(),
        sword_status,
        move_status_stamina(10, 1, 5, 4, 10, 10),
    ])
    .command(morale(
        970,
        100,
        150,
        vec![("officer".to_owned(), 500, 600, 200)],
    ))
    .parts(PartGroup::Group(
        itertools::repeat_n(part_officer, 1)
            .chain(itertools::repeat_n(part_cavalry, 9))
            .collect(),
        25,
    ))
    .edata_insert((stamina_id(), Stat::new_max(1000)))
    .texture_data((TextureParams::TopDown, vec![textures::cavalry()]));
    ret
}

fn move_status_stamina(
    walking_speed: Tick,
    walking_cost: Tick,
    running_speed: Tick,
    running_cost: Tick,
    rest_value: Tick,
    rest_cost: Tick,
) -> Status {
    let status_name = "moving";
    let walking_status = Status {
        name: "walking".to_owned(),
        actions: vec![setsubstatus_action(
            "run".to_owned(),
            Some('v'),
            (status_name.to_owned(), vec![]),
            1,
            "starts-running".to_string(),
            None,
        )],
        conditional_substatuses: stamina_move(walking_speed, walking_cost),
        ..Default::default()
    };
    let running_status = Status {
        name: "running".to_owned(),
        actions: vec![setsubstatus_action(
            "slow-down".to_owned(),
            Some('x'),
            (status_name.to_owned(), vec![]),
            0,
            "stops-running".to_string(),
            None,
        )],
        conditional_substatuses: stamina_move(running_speed, running_cost),
        ..Default::default()
    };

    Status {
        name: status_name.to_owned(),
        actions: vec![rest_action(bncnumber!(rest_value), bncnumber!(rest_cost))],
        substatuses: vec![(walking_status, true), (running_status, false)].into(),
        ..Default::default()
    }
}

mod textures {
    use super::*;
    pub fn cavalry() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image("simple_com.svg".into()))
    }
    pub fn cannon() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image("simple_com_3.svg".into()))
    }
    pub fn line_infantry() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image("simple_com_2.svg".into()))
    }
    pub fn light_infantry() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image("simple_com_2b.svg".into()))
    }
}

pub fn hostile_ai() -> FactionType {
    FactionType::AI(AI::Random(Random::Basic))
}

pub fn sp(tiledmap: tiled::Map) -> GSInit {
    gen(tiledmap, FactionType::Player, hostile_ai())
}
pub fn sprev(tiledmap: tiled::Map) -> GSInit {
    gen(tiledmap, hostile_ai(), FactionType::Player)
}
pub fn mp(tiledmap: tiled::Map) -> GSInit {
    gen(tiledmap, FactionType::Player, FactionType::Player)
}
pub fn obs(tiledmap: tiled::Map) -> GSInit {
    gen(tiledmap, hostile_ai(), hostile_ai())
}

const FACTION_SOUTH: FactionID = FactionID(0);
const FACTION_NORTH: FactionID = FactionID(1);

fn gen(tiledmap: tiled::Map, fac_south_type: FactionType, fac_north_type: FactionType) -> GSInit {
    let models_map = hashmap![
        "line_infantry".to_owned() => line_infantry(),
        "light_infantry".to_owned() => light_infantry(),
        "cavalry".to_owned() => cavalry(),
        "cannon".to_owned() => cannon(),
    ];

    let (map, start_coms, aigm) = super::tiledload::process_from_model(&tiledmap, &models_map);

    let start_coms = start_coms
        .into_iter()
        .map(|x| GodGSApply::AddCombater(x.build().unwrap()));

    let mut ggsa: Vec<GodGSApply<NamedTextureDataTransfer>> = map.into();

    ggsa.extend(aigm.into_iter().map(|x| GodGSApply::AddAIGameMaster(0, x)));

    ggsa.extend(vec![
        GodGSApply::SetName("W18".to_string()),
        /* TODO
        GodGSApply::AddAIGameMaster(
            0,
            AIGameMaster::KingOfTheHill {
                valid_for: btreeset![FACTION_SOUTH, FACTION_NORTH],
                position: super::tiledload::tiled_matrix_coord_to_hex(19, 19),
                ticks: 0,
                startcount: None,
            },
        ),
        */
        GodGSApply::AddAIGameMaster(
            1,
            AIGameMaster::LastManStanding {
                valid_for: btreeset![FACTION_SOUTH, FACTION_NORTH],
            },
        ),
        /* TODO
        GodGSApply::AddAIGameMaster(
            10,
            AIGameMaster::ExpandMapRangeCombater{
                2,
                todo!(),
                }
            ),
        */
        /* TODO
        GodGSApply::AddAIGameMaster(
            20,
            AIGameMaster::Choice(
                100,
                factions::south,
                "choice-reinforcements",
                vec![
                    ("West", effect::GS::Location(effect::Location::CloneAt(reinforcement_id, todo!()))),
                    ("East", effect::GS::Location(effect::Location::CloneAt(reinforcement_id, todo!()))),
                ],
            )
        ),
        */
        GodGSApply::AddFaction(
            Faction::new(
                "South".to_string(),
                FactionColor {
                    r: 1.0_f32.try_into().unwrap(),
                    g: 0.0_f32.try_into().unwrap(),
                    b: 0.0_f32.try_into().unwrap(),
                    a: FACTION_COLOR_TRANSPARENCY.try_into().unwrap(),
                },
                fac_south_type,
            )
            .set_see_entire_map(),
        ),
        GodGSApply::AddFaction(
            Faction::new(
                "North".to_string(),
                FactionColor {
                    r: 0.0_f32.try_into().unwrap(),
                    g: 0.0_f32.try_into().unwrap(),
                    b: 1.0_f32.try_into().unwrap(),
                    a: FACTION_COLOR_TRANSPARENCY.try_into().unwrap(),
                },
                fac_north_type,
            )
            .set_see_entire_map(),
        ),
    ]);

    ggsa.extend(start_coms);

    ggsa.into()
}
