/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::convert::TryInto;

use num::Zero;

use crate::helpers::*;
use crate::*;

fn merge_into_status() -> Status {
    Status {
        name: "Split Party".to_owned(),
        actions: vec![Action {
            name: "Merge back".to_owned(),
            keycode: Some('m'),
            target_effects: vec![ActionTargetEffect::Combater(
                vec![
                    (
                        condition::CombaterCombater::NotSame.into(),
                        ActionTargetingCombaterError::Oneself,
                    ),
                    (
                        condition::CombaterCombater::SameFaction.into(),
                        ActionTargetingCombaterError::Hostile,
                    ),
                    max_range_acttarg_cc(1),
                ],
                vec![effect::CombaterCombater::TransferPart(bnccondition!(true))],
            )],
            ..Default::default()
        }],
        ..Default::default()
    }
}

fn split_off_status(model: CombaterID, partname: String) -> Status {
    Status {
        name: "Scout party".to_owned(),
        conditional_substatuses: vec![(
            Status {
                name: "- may split".to_owned(),
                actions: vec![Action {
                    name: "Split".to_owned(),
                    keycode: Some('s'),
                    target_effects: vec![ActionTargetEffect::Terrain(
                        moveable_acttarg(),
                        vec![effect::CombaterLocation::CloneAtTarget(
                            model,
                            vec![effect::CombaterCombater::TransferPart(bnccondition!(
                                and,
                                (condition::Part::Live),
                                (condition::Part::Name(partname.clone())),
                            ))],
                        )],
                    )],
                    ..Default::default()
                }],
                ..Default::default()
            },
            bnccondition!(
                <,
                bncnumber!(0),
                number::Combater::PartCond(
                    bnccondition!(
                        and,
                        (condition::Part::Live),
                        (condition::Part::Name(partname.clone())),
                    )
                ),
            ),
        )]
        .into(),
        ..Default::default()
    }
}

fn town_status() -> Status {
    let is_inside_town = bnccondition!(condition::Combater::Location(condition::Location::Name(
        "Town".to_owned()
    )));
    let is_outside_town = bnccondition!(
        not,
        condition::Combater::Location(condition::Location::Name("Town".to_owned()))
    );
    let outside_status_path = vec![Right(0)];
    let moving_status_path = vec![Right(0), Left(1)];

    // TODO: stamina costs of moving in and out of town
    // TODO: chance to fail for sneaking in and out
    let sneak_into_town = Action {
        name: "Sneak into town".to_owned(),
        keycode: Some('s'),
        sound_effects: Default::default(),
        source_effects: vec![effect::Combater::TickDiff(bncnumber!(dice "20+2d10"))],
        target_effects: vec![ActionTargetEffect::Terrain(
            vec![
                max_range_acttarg(1),
                is_empty_acttarg(),
                (
                    condition::CombaterLocation::Target(condition::Location::Name(
                        "Town".to_owned(),
                    ))
                    .into(),
                    ActionTargetingLocationError::Impassable, // TODO: more ATLEs
                ),
            ],
            vec![ROTATESTT, effect::CombaterLocation::Teleport()],
        )],
    };
    let sneak_out_town = Action {
        name: "Sneak out town".to_owned(),
        keycode: Some('s'),
        sound_effects: Default::default(),
        source_effects: vec![effect::Combater::SetSingleSubStatus(
            ("Stance".to_owned(), outside_status_path.clone()),
            1,
        )],
        target_effects: vec![ActionTargetEffect::Terrain(
            moveable_acttarg(),
            move_effect(1),
        )],
    };

    let camping_status = Status {
        name: "- Camping".to_owned(),
        actions: vec![
            setsubstatus_action(
                "VRaise camp".to_owned(),
                Some('v'),
                ("Stance".to_owned(), outside_status_path.clone()),
                1,
                "{com_name} took {ticks} to raise camp".to_string(),
                Some(number::Random::DiceRoll("30 + 3d10".to_owned()).into_calc()),
            ),
            rest_action(bncnumber!(dice "50+5d10"), bncnumber!(10)),
        ],
        texture_data: (TextureParams::Simple, vec![textures::tent()]),
        ..Default::default()
    };

    let walking_status = Status {
        name: " - Walking".to_owned(),
        actions: vec![setsubstatus_action(
            "VRun!".to_owned(),
            Some('v'),
            ("Stance".to_owned(), moving_status_path.clone()),
            1,
            "{com_name} starts running".to_string(),
            None,
        )],
        conditional_substatuses: stamina_move(10, 1),
        ..Default::default()
    };
    let running_status = Status {
        name: " - Running".to_owned(),
        actions: vec![setsubstatus_action(
            "XSlow down".to_owned(),
            Some('x'),
            ("Stance".to_owned(), moving_status_path),
            0,
            "{com_name} stops running".to_string(),
            None,
        )],
        conditional_substatuses: stamina_move(5, 4),
        ..Default::default()
    };

    let moving_status = Status {
        name: "- Moving".to_owned(),
        actions: vec![
            rest_action(bncnumber!(dice "25+5d5"), bncnumber!(10)),
            sneak_into_town,
            setsubstatus_action(
                "CSet camp".to_owned(),
                Some('c'),
                ("Stance".to_owned(), outside_status_path.clone()),
                0,
                "{com_name} took {ticks} to set camp".to_string(),
                Some(bncnumber!(dice "15 + 1d10")),
            ),
        ],
        substatuses: vec![(walking_status, true), (running_status, false)].into(),
        ..Default::default()
    };

    let outside_status = Status {
        name: "- Outside".to_owned(),
        actions: vec![pp_attack_action(
            "Attack".to_owned(),
            Some('a'),
            number::Random::DiceRoll("20 + 4d4".to_string()).into_calc(),
            vec![
                (
                    number::Random::DiceRoll("0 - 3d10".to_string()).into_calc(),
                    condition::Part::Name("Quester".to_owned()),
                ),
                (
                    number::Random::DiceRoll("0 - 1d10".to_string()).into_calc(),
                    condition::Part::Name("Scout".to_owned()),
                ),
                (
                    number::Random::DiceRoll("0 - 2d10".to_string()).into_calc(),
                    condition::Part::Name("Warrior".to_owned()),
                ),
                (
                    number::Random::DiceRoll("0 - 1d10".to_string()).into_calc(),
                    condition::Part::Name("Archer".to_owned()),
                ),
            ],
        )],
        substatuses: vec![(camping_status, true), (moving_status, false)].into(),
        concealment: vec![Concealment::Simple(), Concealment::LineOfSight(0)],
        ..Default::default()
    };

    let inside_town_status = Status {
        name: "- Inside town".to_owned(),
        actions: vec![
            sneak_out_town,
            rest_action(bncnumber!(dice "75+8d10"), bncnumber!(10)),
        ],
        ..Default::default()
    };

    Status {
        name: "Stance".to_owned(),
        conditional_substatuses: vec![
            (outside_status, is_outside_town),
            (inside_town_status, is_inside_town),
        ]
        .into(),
        detection: vec![Detection::Range(1), Detection::LineOfSight(3)],
        ..Default::default()
    }
}

mod textures {
    use super::*;
    pub fn guard() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image(
            "wesnoth/data/core/images/units/human-loyalists/spearman.png".into(),
        ))
    }
    pub fn wolf() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image(
            "wesnoth/data/core/images/units/monsters/wolf.png".into(),
        ))
    }
    pub fn scout() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image(
            "wesnoth/data/core/images/units/human-peasants/woodsman.png".into(),
        ))
    }
    pub fn party() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image(
            "wesnoth/data/core/images/units/human-magi/mage.png".into(),
        ))
    }
    pub fn quester() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image(
            "wesnoth/data/core/images/units/human-magi/mage.png".into(),
        ))
    }
    pub fn tent() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image(
            "wesnoth/data/core/images/terrain/castle/encampment/tent.png".into(),
        ))
    }
}

mod mapstat {
    use super::*;

    pub fn wolf() -> MapElementEDataIntID {
        MapElementEDataIntID::new("wolf".into())
    }
    pub fn fire() -> MapElementEDataRatioID {
        MapElementEDataRatioID::new("fire".into())
    }
}

pub fn guard_patrol_builder(guards: usize, officers: usize) -> CombaterBuilder {
    let part_officer = Part {
        name: "Officer".to_owned(),
        hp: Some(Stat::new_max(100)),
        size: Some(50),
    };
    let part_guard = Part {
        name: "Guard".to_owned(),
        hp: Some(Stat::new_max(10)),
        size: Some(50),
    };
    let parts =
        itertools::repeat_n(part_officer, officers).chain(itertools::repeat_n(part_guard, guards));
    let mut ret = CombaterBuilder::default();
    ret.parts(PartGroup::Group(parts.collect(), 0))
        .statuses(vec![
            Status {
                name: "Swords".to_owned(),
                actions: vec![pp_attack_action(
                    "Attack".to_owned(),
                    Some('a'),
                    number::Random::DiceRoll("20 + 4d4".to_string()).into_calc(),
                    vec![
                        (
                            number::Random::DiceRoll("0 - 2d10".to_string()).into_calc(),
                            condition::Part::Name("Guard".to_owned()),
                        ),
                        (
                            number::Random::DiceRoll("0 - 1d10".to_string()).into_calc(),
                            condition::Part::Name("Officer".to_owned()),
                        ),
                    ],
                )],
                ..Default::default()
            },
            move_status(20),
            view_status(1),
            los_view_status(2, 0),
        ])
        .faction(FACTION_HOSTILE)
        .texture_data((TextureParams::Simple, vec![textures::guard()]));
    ret
}

fn wolf_pack_builder(wolves: usize) -> CombaterBuilder {
    let wolf_part = Part {
        name: "Wolf".to_owned(),
        hp: Some(Stat::new_max(5)),
        size: Some(400),
    };
    let mut ret = CombaterBuilder::default();
    ret.statuses(vec![
        Status {
            name: "Mouth".to_owned(),
            actions: vec![pp_attack_action(
                "Bite".to_owned(),
                Some('b'),
                number::Random::DiceRoll("10 + 2d4".to_string()).into_calc(),
                vec![(
                    number::Random::DiceRoll("0 - 1d10".to_string()).into_calc(),
                    condition::Part::Name("Wolf".to_owned()),
                )],
            )],
            ..Default::default()
        },
        move_status(20),
        view_status(1),
        los_view_status(1, 1),
    ])
    .parts(PartGroup::Group(vec![wolf_part; wolves], 0))
    .faction(FACTION_HOSTILE)
    .command(Command::AI(AI::Random(Random::MapStat(bncnumber!(
        number::Location::GetStatInt(mapstat::wolf())
    )))))
    .texture_data((TextureParams::Simple, vec![textures::wolf()]));
    ret
}

pub fn hostile_ai() -> FactionType {
    FactionType::AI(AI::Random(Random::Basic))
}

pub fn sp(tiledmap: tiled::Map) -> GSInit {
    gen(tiledmap, FactionType::Player, hostile_ai())
}
pub fn mp(tiledmap: tiled::Map) -> GSInit {
    gen(tiledmap, FactionType::Player, FactionType::Player)
}
pub fn obs(tiledmap: tiled::Map) -> GSInit {
    gen(tiledmap, hostile_ai(), hostile_ai())
}

const FACTION_MAIN: FactionID = FactionID(0);
const FACTION_HOSTILE: FactionID = FactionID(1);

fn gen(tiledmap: tiled::Map, fac_main_type: FactionType, fac_hostile_type: FactionType) -> GSInit {
    let fire_namedtexture: NamedTextureDataTransfer = NamedTextureDataTransfer::new(
        NTDAssetPath::Image("wesnoth/data/core/images/scenery/flames01.png".into()),
    );
    let ntlava: NamedTextureDataTransfer = NamedTextureDataTransfer::new(NTDAssetPath::Image(
        "wesnoth/data/core/images/terrain/unwalkable/lava-tile.png".into(),
    ));

    let scouts_id = CombaterID::new(0);

    let fire_map_element = game::map::MapElement {
        name: "fire".to_owned(),
        texture_data: vec![fire_namedtexture],
        ..Default::default()
    };

    let lava_map_element = game::map::MapElementBuilder::default()
        .name("lava".to_owned())
        .texture_data(vec![ntlava])
        .stats_insert((mapstat::fire(), mil_chances(0)))
        .build()
        .unwrap();

    let models_map = {
        let quester_builder = {
            let mut ret = CombaterBuilder::default();
            ret.statuses(vec![town_status()])
                .parts(PartGroup::Group(
                    vec![Part {
                        name: "Player".to_owned(),
                        hp: Some(Stat::new_max(100)),
                        size: Some(10),
                    }],
                    0,
                ))
                .edata_insert((stamina_id(), Stat::new_max(1000)))
                .tick(5)
                .faction(FACTION_MAIN)
                .texture_data((TextureParams::Simple, vec![textures::quester()]));
            ret
        };
        let party_builder = {
            let mut ret = CombaterBuilder::default();
            ret.statuses(vec![
                town_status(),
                // TODO: splitting and town?
                split_off_status(scouts_id, "Scout".to_owned()),
            ])
            .parts(PartGroup::Group(
                vec![
                    Part {
                        name: "Quester".to_owned(),
                        hp: Some(Stat::new_max(200)),
                        size: Some(10),
                    },
                    Part {
                        name: "Warrior".to_owned(),
                        hp: Some(Stat::new_max(100)),
                        size: Some(10),
                    },
                    Part {
                        name: "Warrior".to_owned(),
                        hp: Some(Stat::new_max(100)),
                        size: Some(10),
                    },
                    Part {
                        name: "Archer".to_owned(),
                        hp: Some(Stat::new_max(70)),
                        size: Some(10),
                    },
                    Part {
                        name: "Archer".to_owned(),
                        hp: Some(Stat::new_max(70)),
                        size: Some(10),
                    },
                    Part {
                        name: "Scout".to_owned(),
                        hp: Some(Stat::new_max(80)),
                        size: Some(10),
                    },
                    Part {
                        name: "Scout".to_owned(),
                        hp: Some(Stat::new_max(80)),
                        size: Some(10),
                    },
                ],
                0,
            ))
            .edata_insert((stamina_id(), Stat::new_max(1000)))
            .tick(5)
            .faction(FACTION_MAIN)
            .texture_data((TextureParams::Simple, vec![textures::party()]));
            ret
        };

        hashmap![
            "quester".to_owned() => quester_builder,
            "guard".to_owned() => guard_patrol_builder(1, 0),
            "wolf".to_owned() => wolf_pack_builder(1),
            "wolf_pack".to_owned() => wolf_pack_builder(3),
            "party".to_owned() => party_builder,
            "guard_patrol".to_owned() => guard_patrol_builder(2, 1),
        ]
    };

    let (mut map, start_coms, aigm) = super::tiledload::process_from_model(&tiledmap, &models_map);

    map.set_default_mapstat(mapstat::wolf(), 0);
    map.set_default_mapstat(mapstat::fire(), Chances::zero());

    let start_coms = start_coms
        .into_iter()
        .map(|x| GodGSApply::AddCombater(x.build().unwrap()));

    let mut ggsa: Vec<GodGSApply<NamedTextureDataTransfer>> = map.into();

    ggsa.extend(aigm.into_iter().map(|x| GodGSApply::AddAIGameMaster(0, x)));

    ggsa.extend(vec![
        GodGSApply::SetName("TPTA".to_string()),
        GodGSApply::AddCombater(
            CombaterBuilder::default()
                .name("Scouts".to_owned())
                .parts(PartGroup::Group(vec![], 0))
                .statuses(vec![
                    move_status(5),
                    los_view_status(4, 2),
                    view_status(1),
                    merge_into_status(),
                ])
                .faction(FACTION_MAIN)
                .command(Command::Direct)
                .texture_data((TextureParams::Simple, vec![textures::scout()]))
                .location(ComLocation {
                    dir: hex2d::Direction::XY,
                    coord: MAP_ORIGIN,
                })
                .tick(0)
                .ingame(false)
                .build()
                .unwrap(),
        ),
        GodGSApply::AddAIGameMaster(
            1,
            AIGameMaster::LastManStanding {
                valid_for: btreeset![FACTION_HOSTILE],
            },
        ),
        GodGSApply::AddAIGameMaster(
            100,
            AIGameMaster::MapSpreader {
                next_tick: 200,
                interval: 40,
                spreader_name: "fire".to_string(),
                odds: bncnumber!(mul number::Location::GetStatRatio(mapstat::fire()), Chances::new(1, 5)),
                new_element: fire_map_element.clone(),
                overtextures: true,
                range: 1,
            },
        ),
        GodGSApply::AddAIGameMaster(
            101,
            AIGameMaster::MapSpreader {
                next_tick: 200,
                interval: 40,
                spreader_name: "fire".to_string(),
                odds: bncnumber!(mul number::Location::GetStatRatio(mapstat::fire()), Chances::new(1, 50)),
                new_element: fire_map_element,
                overtextures: true,
                range: 2,
            },
        ),
        GodGSApply::AddAIGameMaster(
            110,
            AIGameMaster::MapSpreader {
                next_tick: 300,
                interval: 40,
                spreader_name: "lava".to_string(),
                odds: bncnumber!(mil_chances(100)),
                new_element: lava_map_element,
                overtextures: false,
                range: 1,
            },
        ),
        GodGSApply::AddAIGameMaster(
            109,
            AIGameMaster::CombaterEffectOnTerrain {
                map_element_name: "fire".to_string(),
                effect: effect::Combater::Kill(),
            },
        ),
        GodGSApply::AddAIGameMaster(
            119,
            AIGameMaster::CombaterEffectOnTerrain {
                map_element_name: "lava".to_string(),
                effect: effect::Combater::Kill(),
            },
        ),
        GodGSApply::AddAIGameMaster(200, AIGameMaster::RevealMap { range: 4 }),
        GodGSApply::AddFaction(Faction::new(
            "Player".to_string(),
            FactionColor {
                r: 1.0_f32.try_into().unwrap(),
                g: 0.0_f32.try_into().unwrap(),
                b: 0.0_f32.try_into().unwrap(),
                a: FACTION_COLOR_TRANSPARENCY.try_into().unwrap(),
            },
            fac_main_type,
        )),
        GodGSApply::AddFaction(Faction::new(
            "Hostiles".to_string(),
            FactionColor {
                r: 0.0_f32.try_into().unwrap(),
                g: 0.0_f32.try_into().unwrap(),
                b: 1.0_f32.try_into().unwrap(),
                a: FACTION_COLOR_TRANSPARENCY.try_into().unwrap(),
            },
            fac_hostile_type,
        )),
    ]);

    ggsa.extend(start_coms);

    ggsa.into()
}
