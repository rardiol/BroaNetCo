/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use crate::game::init::*;
use crate::game::test1;

use std::path::Path;

use crate::game::*;

use test1::gen;

pub use test1::coop as mp_test1_coop;
pub use test1::mp as mp_test1;
pub use test1::sp as sp_test1;

pub use self::tcoop as mp_test2_coop;
pub use self::tmp as mp_test2;
pub use self::tsp as sp_test2;

fn map() -> game::map::Map<NamedTextureDataTransfer> {
    super::tiledload::load_from_model(Path::new("resources/tiled/t1.tmx"), &Default::default()).0
}

pub fn tsp() -> GSInit {
    gen(
        FactionType::Player,
        FactionType::AI(AI::Random(Random::Basic)),
        FactionType::AI(AI::Random(Random::Basic)),
        Some(map()),
    )
}
pub fn tmp() -> GSInit {
    gen(
        FactionType::Player,
        FactionType::Player,
        FactionType::Player,
        Some(map()),
    )
}
pub fn tcoop() -> GSInit {
    gen(
        FactionType::Player,
        FactionType::Player,
        FactionType::AI(AI::Random(Random::Basic)),
        Some(map()),
    )
}
