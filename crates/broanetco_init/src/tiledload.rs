/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

//! .tmx tiled map editor loader
//!
//! Supports hexagonal maps with staggering axis set to X (flat top)
//!
//! Tile layers. Tileset properties are converted to MapElement fields as serde json strings
//! Fields for each MapElement can be set in any layer, but higher layer overwrite lower layers
//! Exception to images, which are added to the images of lower layers
//! Other key names that are ints are stored as EData i32
//! Other key names that are floats are stored EData Chances (Ratio)
//!
//! Object layers are converted accordingly to an optional model map into CombaterBuilder.
//! Snap to grid needs to be on
//! properties:
//! model = string
//! faction = int

use std::collections::HashMap;
use std::convert::TryInto;
use std::path;
use std::path::{Path, PathBuf};

use game::hex2d::Direction;
use num::Integer;

use crate::game::map::*;
use crate::game::*;
use ::tiled::*;

pub type ModelID = String;
pub type Model = HashMap<CombaterModelID, CombaterBuilder<NamedTextureDataTransfer>>;
pub type CombaterModelID = String;

pub fn maps_with_model(mut path: PathBuf) -> Vec<(ModelID, PathBuf, tiled::Map)> {
    let mut ret = Vec::new();
    path.push("tiled/");
    info!("tiled directory: {:?}", path);
    for filename in path.read_dir().expect("Error opening tiled directory") {
        let filename = filename.expect("Error reading tiled directory");
        if filename.file_name() == std::ffi::OsStr::new(".gitignore") {
            continue;
        }
        if filename.path().extension() != Some(std::ffi::OsStr::new("tmx")) {
            warn!("Invalid extension for tiled {:?}", filename);
            continue;
        }
        info!("found tiled file: {:?}", filename.path());

        let map = ::tiled::parse_file(&filename.path()).unwrap();

        if let Some(model) = map.properties.get("model") {
            if let tiled::PropertyValue::StringValue(model) = model {
                ret.push((
                    model.clone(),
                    filename.path().file_stem().unwrap().into(),
                    map,
                ))
            } else {
                error!("map model should be string for {:?}", filename);
            }
        } else {
            info!("found tiled map without model: {:?}", filename);
        }
    }
    ret
}

/// Converts from path relative to .tmx file to asset path
///
/// Assumes relative path starting with ../assets/
fn convertfind(relpath: &str) -> PathBuf {
    let mut path = Path::new(relpath).components();
    assert_eq!(path.next(), Some(path::Component::ParentDir));
    assert_eq!(
        path.next(),
        Some(path::Component::Normal("assets".as_ref()))
    );
    path.collect()
}

// https://www.redblobgames.com/grids/hexagons/#coordinates

// function cube_to_oddr(cube):
//     var col = cube.x + (cube.z - (cube.z&1)) / 2
//     var row = cube.z
//     return OffsetCoord(col, row)

#[allow(dead_code)]
pub fn map_location_to_tiled(cube: MapLocation) -> (i32, i32) {
    let col = cube.x + (cube.z() - (cube.z() & 1)) / 2;
    let row = cube.z();
    (col, row)
}

// function oddr_to_cube(hex):
// var x = hex.col - (hex.row - (hex.row&1)) / 2
//     var z = hex.row
//     var y = -x-z
//     return Cube(x, y, z)

/// Converts tiled coord to hex2d
pub fn tiled_matrix_coord_to_hex(column: usize, row: usize) -> MapLocation {
    let column: i32 = column.try_into().unwrap();
    let row: i32 = row.try_into().unwrap();
    let x = column - (row - (row & 1)) / 2;
    let z = row;
    let y = -x - z + column / 2;

    MapLocation { x, y }
}

// TODO: magic numbers
/// Converts tiled object map positions to hex2d, assuming snap to grid is on
fn tiled_object_location_to_maplocation(x: f32, y: f32, tile_size: f32) -> MapLocation {
    let column: usize = <usize as num::NumCast>::from((x / (tile_size * 0.75)).round()).unwrap();
    let y = if column.is_even() {
        y
    } else {
        y - (tile_size / 2.0)
    };
    let row: usize = <usize as num::NumCast>::from((y / tile_size).round()).unwrap();
    tiled_matrix_coord_to_hex(column, row)
}

pub fn load_from_model(
    path: &Path,
    models: &Model,
) -> (
    crate::game::map::Map<NamedTextureDataTransfer>,
    Vec<CombaterBuilder<NamedTextureDataTransfer>>,
    Vec<crate::game::AIGameMaster<NamedTextureDataTransfer>>,
) {
    let map = ::tiled::parse_file(path).unwrap();
    process_from_model(&map, models)
}

/// Takes a path to a tiled.tmx file, return a Map
pub fn process_from_model(
    map: &tiled::Map,
    models: &Model,
) -> (
    crate::game::map::Map<NamedTextureDataTransfer>,
    Vec<CombaterBuilder<NamedTextureDataTransfer>>,
    Vec<crate::game::AIGameMaster<NamedTextureDataTransfer>>,
) {
    use PropertyValue::*;
    debug!("{:?}", map);

    let mut buildermap: HashMap<MapLocation, MapElementBuilder<NamedTextureDataTransfer>> =
        Default::default();
    let mut aigm: Vec<crate::game::AIGameMaster<NamedTextureDataTransfer>> = Default::default();

    let tile_size = map.tile_width;
    assert_eq!(map.tile_width, map.tile_height);
    assert_eq!(map.orientation, Orientation::Hexagonal);

    for layer in &map.layers {
        match &layer.tiles {
            LayerData::Finite(lines) => {
                // Vec<Vec<T>> iterator with column and line
                for ((colnum, linenum), LayerTile { gid, .. }) in lines
                    .iter()
                    .enumerate()
                    .map(|(linenum, line)| {
                        line.iter()
                            .enumerate()
                            .map(move |(colnum, element)| ((colnum, linenum), element))
                    })
                    .flatten()
                {
                    // Ignores missing tileset (nothing there)
                    if let Some(tileset) = map.get_tileset_by_gid(*gid) {
                        let map_location = tiled_matrix_coord_to_hex(colnum, linenum);
                        // Get the base map_element which will be extended by this layer
                        let map_element = buildermap.entry(map_location).or_default();
                        debug!(
                            "processing Tiled({:?})-Hex2d({:?})",
                            (colnum, linenum),
                            map_location
                        );

                        // Extend texture_data with the images
                        if layer.visible {
                            for el in
                                tileset.images.iter().map(|::tiled::Image { source, .. }| {
                                    NamedTextureDataTransfer::new(NTDAssetPath::Image(convertfind(
                                        source,
                                    )))
                                })
                            {
                                map_element.texture_data_extend(el);
                            }
                        }

                        // Load other MapElement fields from the tileset properties
                        for (key, value) in tileset.properties.iter() {
                            let known_keys = [
                                "name",
                                "protection_inside",
                                "protection_behind",
                                "detection",
                                "concealment",
                                "ticks",
                                "path_concealment",
                                "self_concealment",
                                "koth_factions",
                            ];
                            match (key, value) {
                                (key, StringValue(string)) if key == known_keys[0] => {
                                    map_element.name(string.clone());
                                }
                                (key, StringValue(string)) if key == known_keys[1] => {
                                    map_element.protection_inside(
                                        serde_json::from_str(string).unwrap_or_else(|_| {
                                            panic!(
                                                "Failed to parse tileset property at {:?}",
                                                (colnum, linenum)
                                            )
                                        }),
                                    );
                                }
                                (key, StringValue(string)) if key == known_keys[2] => {
                                    map_element.protection_behind(
                                        serde_json::from_str(string).unwrap_or_else(|_| {
                                            panic!(
                                                "Failed to parse tileset property at {:?}",
                                                (colnum, linenum)
                                            )
                                        }),
                                    );
                                }
                                (key, StringValue(string)) if key == known_keys[3] => {
                                    map_element.detection(
                                        serde_json::from_str(string).unwrap_or_else(|_| {
                                            panic!(
                                                "Failed to parse tileset property at {:?}",
                                                (colnum, linenum)
                                            )
                                        }),
                                    );
                                }
                                (key, StringValue(string)) if key == known_keys[4] => {
                                    map_element.concealment(
                                        serde_json::from_str(string).unwrap_or_else(|_| {
                                            panic!(
                                                "Failed to parse tileset property at {:?}",
                                                (colnum, linenum)
                                            )
                                        }),
                                    );
                                }
                                (key, StringValue(string)) if key == known_keys[5] => {
                                    map_element.ticks(serde_json::from_str(string).unwrap_or_else(
                                        |_| {
                                            panic!(
                                                "Failed to parse tileset property at {:?}",
                                                (colnum, linenum)
                                            )
                                        },
                                    ));
                                }
                                (key, StringValue(string)) if key == known_keys[6] => {
                                    map_element.path_concealment(
                                        serde_json::from_str(string).unwrap_or_else(|_| {
                                            panic!(
                                                "Failed to parse tileset property at {:?}",
                                                (colnum, linenum)
                                            )
                                        }),
                                    );
                                }
                                (key, StringValue(string)) if key == known_keys[7] => {
                                    map_element.self_concealment(
                                        serde_json::from_str(string).unwrap_or_else(|_| {
                                            panic!(
                                                "Failed to parse tileset property at {:?}",
                                                (colnum, linenum)
                                            )
                                        }),
                                    );
                                }
                                (key, StringValue(string)) if key == known_keys[8] => {
                                    aigm.push(AIGameMaster::KingOfTheHill {
                                        valid_for: serde_json::from_str(string).unwrap_or_else(
                                            |_| {
                                                panic!(
                                                    "Failed to parse tileset property at {:?}",
                                                    (colnum, linenum)
                                                )
                                            },
                                        ),
                                        position: map_location,
                                        ticks: 0,
                                        startcount: None,
                                    })
                                }
                                (key, wrongvaluetype) if known_keys.iter().any(|e| e == key) => {
                                    error!(
                                        "key {:?} is of incorrect value type {:?}",
                                        key, wrongvaluetype
                                    )
                                }
                                (key, IntValue(int)) => {
                                    map_element.stats_insert((
                                        MapElementEDataIntID::new(key.clone()),
                                        *int,
                                    ));
                                }
                                (key, FloatValue(fratio)) => {
                                    map_element.stats_insert((
                                        MapElementEDataRatioID::new(key.clone()),
                                        Chances::approximate_float(*fratio)
                                            .expect("failed to convert float"),
                                    ));
                                }
                                (key, _) => warn!("Unknown key/value: {:?}", key),
                            }
                        }
                    }
                }
            }
            _ => unimplemented!("Only finite tiled maps supported"),
        };
    }

    let mut combaters: Vec<CombaterBuilder<NamedTextureDataTransfer>> = Default::default();
    for object_group in &map.object_groups {
        let group_faction = object_group.properties.get("faction").map(|fid| {
            if let IntValue(int) = fid {
                FactionID(
                    (*int)
                        .try_into()
                        .expect("faction has to valid faction number"),
                )
            } else {
                panic!("faction property has to be int");
            }
        });
        for object in &object_group.objects {
            debug!("tiled object: {:?}", object);
            let properties = &object.properties;
            let mut model: CombaterBuilder<NamedTextureDataTransfer> = properties
                .get("model")
                .map(|property| {
                    if let StringValue(model_name) = property {
                        model_name
                    } else {
                        panic!("model property has to be string")
                    }
                })
                .map(|model_name| {
                    models
                        .get(model_name)
                        .unwrap_or_else(|| panic!("model not found: {}", model_name))
                        .clone()
                })
                .unwrap_or_else(Default::default);

            model.name(object.name.clone());

            let dir = properties
                .get("direction")
                .map(|property| {
                    if let StringValue(string) = property {
                        string
                    } else {
                        panic!("property has to be string")
                    }
                })
                .map(|dirstr| {
                    // json needs quotes around it
                    let dirstr = format!("\"{}\"", dirstr);
                    serde_json::from_str(&dirstr).expect("failed to parse direction")
                })
                .unwrap_or(Direction::XY);
            model.location(ComLocation {
                coord: tiled_object_location_to_maplocation(object.x, object.y, tile_size as f32),
                dir,
            });

            if let Some(group_faction) = group_faction {
                model.faction(group_faction);
            } else {
                if let Some(property) = properties.get("faction") {
                    if let IntValue(int) = property {
                        model.faction(FactionID(
                            (*int)
                                .try_into()
                                .expect("faction has to valid faction number"),
                        ));
                    } else {
                        error!("faction property has to be int");
                    }
                }
            }
            // TODO: properties

            combaters.push(model.clone());
        }
    }

    let mapret = buildermap
        .into_iter()
        .map(|(loc, elbuilder)| (loc, elbuilder.build().unwrap()))
        .collect();
    debug!("tmx converted to {:?}", mapret);
    debug!("tmx generated {:?} coms", combaters.len());
    (mapret, combaters, aigm)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_tiled_to_hex() {
        assert_eq!(tiled_matrix_coord_to_hex(0, 0), MapLocation { x: 0, y: 0 });
        assert_eq!(
            tiled_matrix_coord_to_hex(1, 4),
            MapLocation { x: -1, y: -3 }
        );
    }

    #[test]
    fn test_tiled_object_to_hex() {
        assert_eq!(
            tiled_object_location_to_maplocation(0.0, 0.0, 64.0),
            tiled_matrix_coord_to_hex(0, 0)
        );
        assert_eq!(
            tiled_object_location_to_maplocation(64.0, 0.0, 64.0),
            tiled_matrix_coord_to_hex(1, 0)
        );
        assert_eq!(
            tiled_object_location_to_maplocation(352.0, 144.0, 64.0),
            tiled_matrix_coord_to_hex(5, 3)
        );
        assert_eq!(
            tiled_object_location_to_maplocation(72.0, 216.0, 72.0),
            tiled_matrix_coord_to_hex(1, 4)
        );
    }
}
