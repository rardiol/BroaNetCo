pub(crate) use crate::game::init::*;
pub(crate) use crate::game::map::*;
pub(crate) use crate::game::stat::Stat;
pub(crate) use crate::game::test1::helpers::*;
pub(crate) use crate::game::*;

pub(crate) type CombaterBuilder = game::CombaterBuilder<NamedTextureDataTransfer>;
pub(crate) type Status = game::Status<NamedTextureDataTransfer>;

pub(crate) mod textures {
    use super::*;
    pub fn exhausted() -> NamedTextureDataTransfer {
        NamedTextureDataTransfer::new(NTDAssetPath::Image("down_arrow_left.svg".into()))
    }
}

pub(crate) fn simple_view_status() -> Status {
    Status {
        detection: vec![Detection::Range(1)],
        concealment: vec![Concealment::Simple()],
        ..Default::default()
    }
}

pub(crate) fn los_view_status(detection: i32, concealment: i32) -> Status {
    Status {
        detection: vec![Detection::LineOfSight(detection)],
        concealment: vec![Concealment::LineOfSight(concealment)],
        ..Default::default()
    }
}

pub(crate) fn move_effect(speed: i32) -> Vec<SCTLE> {
    vec![
        ROTATESTT,
        effect::CombaterLocation::Group(vec![
            effect::CombaterLocation::TickDiff(bncnumber!(
                mul
                speed,
                (number::CombaterLocation::Target(number::Location::TickCost(),))
            )),
            effect::CombaterLocation::Teleport(),
        ]),
    ]
}

pub(crate) fn move_effect_stamina(speed: i32, stamina_cost_mul: i32) -> Vec<SCTLE> {
    vec![
        ROTATESTT,
        effect::CombaterLocation::Group(vec![
            effect::CombaterLocation::TickDiff(bncnumber!(
                mul
                speed,
                (number::CombaterLocation::Target(number::Location::TickCost(),))
            )),
            effect::CombaterLocation::SourceEDataStatDiff(
                stamina_id(),
                bncnumber!(
                    sub
                    0,
                    bncnumber!(
                        mul
                        stamina_cost_mul,
                        (number::CombaterLocation::Target(number::Location::TickCost(),))
                    )
                ),
            ),
            effect::CombaterLocation::Teleport(),
        ]),
    ]
}

pub(crate) fn stamina_move(
    speed: i32,
    stamina_cost_mul: i32,
) -> ConditionalSubStatusVec<NamedTextureDataTransfer> {
    vec![
        (
            Status {
                name: " - Exhausted".to_owned(),
                actions: Default::default(),
                texture_data: (TextureParams::Simple, vec![textures::exhausted()]),
                ..Default::default()
            },
            is_exhausted(),
        ),
        (
            Status {
                name: "".to_owned(),
                actions: vec![move_action_stamina(speed, stamina_cost_mul)],
                ..Default::default()
            },
            is_not_exhausted(),
        ),
    ]
    .into()
}

pub(crate) fn is_exhausted() -> condition::ConditionDet<condition::Combater> {
    bnccondition!(
        <,
        number::Combater::GetEDataStat(stamina_id()),
        20,
    )
}

pub(crate) fn is_not_exhausted() -> condition::ConditionDet<condition::Combater> {
    bnccondition!(
        >=,
        number::Combater::GetEDataStat(stamina_id()),
        20,
    )
}
pub(crate) fn move_action(speed: i32) -> Action {
    Action {
        name: "QMove".to_owned(),
        keycode: Some('q'),
        sound_effects: vec![],
        source_effects: vec![],
        target_effects: vec![ActionTargetEffect::Terrain(
            moveable_acttarg(),
            move_effect(speed),
        )],
    }
}

pub(crate) fn move_action_stamina(speed: i32, stamina_cost_mul: i32) -> Action {
    Action {
        name: "QMove".to_owned(),
        keycode: Some('q'),
        sound_effects: vec![],
        source_effects: vec![],
        target_effects: vec![ActionTargetEffect::Terrain(
            moveable_acttarg(),
            move_effect_stamina(speed, stamina_cost_mul),
        )],
    }
}

pub(crate) fn stamina_id() -> CombaterEDataStatID {
    CombaterEDataStatID::new("stamina".to_owned())
}

pub(crate) fn rest_action(
    diff: number::Calc<number::Combater, i32>,
    tick_cost: number::Calc<number::Combater, Tick>,
) -> Action {
    Action {
        name: "ZRest".to_owned(),
        keycode: Some('z'),
        sound_effects: vec![],
        source_effects: vec![
            effect::Combater::TickDiff(tick_cost),
            effect::Combater::EDataStatDiff(stamina_id(), diff),
        ],
        target_effects: vec![],
    }
}

pub(crate) fn move_status(speed: i32) -> Status {
    Status {
        name: "Movement".to_owned(),
        actions: vec![move_action(speed)],
        ..Default::default()
    }
}

#[allow(dead_code)]
pub(crate) fn attack_action(
    name: String,
    tick_cost: number::Calc<number::Combater, Tick>,
    damage: number::Calc<number::Combater, HpPts>,
) -> Action {
    Action {
        name,
        keycode: None,
        sound_effects: vec![],
        source_effects: vec![effect::Combater::TickDiff(tick_cost)],
        target_effects: vec![ActionTargetEffect::Combater(
            melee_acttarg(),
            vec![
                effect::CombaterCombater::Group(vec![
                    effect::CombaterCombater::Target(
                        effect::Combater::HpDiff(damage)
                    );
                    1
                ]),
                SCTCROTATESTT,
            ],
        )],
    }
}

pub(crate) fn pp_attack_action(
    name: String,
    keycode: Option<char>,
    tick_cost: number::Calc<number::Combater, Tick>,
    attacks: Vec<(number::Calc<number::Part, HpPts>, condition::Part)>,
) -> Action {
    let mut effs = vec![SCTCROTATESTT];
    effs.extend(attacks.into_iter().map(|(damage, attacker_filter)| {
        effect::CombaterCombater::OnePartToRandomPart {
            source_filter: bnccondition!(and, attacker_filter, (condition::Part::Live),),
            target_filter: bnccondition!(condition::Part::Attackable),
            target_weights: bncnumber!(1), // TODO
            effect: effect::PartPart::Target(effect::Part::HpDiff(damage)),
        }
    }));
    Action {
        name,
        keycode,
        sound_effects: vec![],
        source_effects: vec![effect::Combater::TickDiff(tick_cost)],
        target_effects: vec![ActionTargetEffect::Combater(melee_acttarg(), effs)],
    }
}

pub(crate) fn setsubstatus_action(
    name: String,
    keycode: Option<char>,
    substatusid: FullSubStatusID,
    substatusactid: SubStatusActID,
    loglinefmt: effect::FmtString,
    tick_cost: Option<number::Calc<number::Combater, Tick>>,
) -> Action {
    let effects = vec![effect::Combater::LogLine(
        loglinefmt,
        Box::new(if let Some(tick_cost) = tick_cost {
            effect::Combater::Group(vec![
                effect::Combater::TickDiff(tick_cost),
                effect::Combater::SetSingleSubStatus(substatusid, substatusactid),
            ])
        } else {
            effect::Combater::SetSingleSubStatus(substatusid, substatusactid)
        }),
        true,
    )];
    Action {
        name,
        keycode,
        source_effects: effects,
        ..Default::default()
    }
}
