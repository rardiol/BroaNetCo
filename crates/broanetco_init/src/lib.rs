/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

#[macro_use]
extern crate log as debug_log;
#[macro_use]
extern crate counted_array;
#[macro_use]
extern crate maplit;

extern crate broanetco_actvec as act_vec;
extern crate broanetco_game as game;
extern crate broanetco_util as util;

use std::fmt::Debug;
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;

use crate::game::init::*;
use crate::game::GodGSApply;

/// Default test game
mod gsfn;
/// Helper for game initialization
mod helpers;
/// Tiled map editor related
mod tiledload;
/// TPTA game
mod tpta;
/// W18 game
mod w18;

// TODO: rethink this
#[derive(Debug, Clone)]
pub enum Mission {
    Deterministic {
        name: &'static str,
        gsinit: fn() -> GSInit,
    },
    #[allow(dead_code)]
    Random {
        name: &'static str,
        gsinit: fn(Option<Seed>) -> GSInit,
    },
    Stored {
        path: PathBuf,
        gsinit: GSInit,
    },
    // TODO: fn should take &map
    TiledModel {
        name: String,
        map: tiled::Map,
        gsinit: fn(tiled::Map) -> GSInit,
    },
}

impl Mission {
    pub fn name(&self) -> &str {
        match *self {
            Self::Deterministic { name, .. } | Self::Random { name, .. } => name,
            Self::TiledModel { ref name, .. } => name,
            Self::Stored { ref path, .. } => path.file_stem().unwrap().to_str().unwrap(),
        }
    }
    pub fn gsinit(&self) -> GSInit {
        let mut gsi = match *self {
            Self::Deterministic { gsinit, .. } => gsinit(),
            Self::Random { gsinit, .. } => gsinit(None),
            Self::Stored { ref gsinit, .. } => gsinit.clone(),
            Self::TiledModel {
                ref map, gsinit, ..
            } => gsinit(map.clone()),
        };
        gsi.push_ggsa(GodGSApply::SetRNG(rand::random::<Seed>()));
        gsi
    }
}

// TODO: don't deserialize every time?
pub fn missions(path: PathBuf) -> Vec<Mission> {
    let mut v = CODED_MISSIONS.to_vec();
    v.extend(missions_gsinit(path.clone()));
    v.extend(missions_tiled(path));
    v
}

pub fn missions_gsinit(mut path: PathBuf) -> Vec<Mission> {
    let mut v = Vec::new();
    path.push("missions/");
    info!("missions directory: {:?}", path);
    for mission_filename in path.read_dir().expect("Error opening mission directory") {
        let mission_filename = mission_filename.expect("Error reading mission directory");
        if mission_filename.file_name() == std::ffi::OsStr::new(".gitignore") {
            continue;
        }
        if mission_filename.path().extension() != Some(std::ffi::OsStr::new("bncgsinit")) {
            warn!("Invalid extension for bncgsinit {:?}", mission_filename);
            continue;
        }
        info!("found mission file: {:?}", mission_filename.path());

        let jd = &mut serde_json::Deserializer::from_reader(BufReader::new(
            File::open(mission_filename.path()).expect("failed to open mission file"),
        ));

        let gsinit = match serde_ignored::deserialize(jd, |path| {
            warn!(
                "Ignoring unexpect path when deserializing mission: {}",
                path.to_string()
            )
        }) {
            Ok(gsinit) => gsinit,
            Err(err) => {
                error!(
                    "Error {} while deserializing mission file: {:?}",
                    err, mission_filename
                );
                continue;
            }
        };
        v.push(Mission::Stored {
            path: mission_filename.path(),
            gsinit,
        });
    }
    v
}

pub fn missions_tiled(path: PathBuf) -> Vec<Mission> {
    let maps_with_model = tiledload::maps_with_model(path);

    maps_with_model
        .into_iter()
        .filter_map(|(modelid, filename, tiledmap)| match modelid.as_ref() {
            "tpta" => {
                let basename =
                    String::from("tiled_") + filename.to_str().expect("tmx filename invalid str");
                let spname = basename.clone() + "_sp";
                let mpname = basename.clone() + "_mp";
                let obsname = basename.clone() + "_obs";
                // TODO: pointless cloning
                Some(vec![
                    Mission::TiledModel {
                        name: spname,
                        map: tiledmap.clone(),
                        gsinit: tpta::sp,
                    },
                    Mission::TiledModel {
                        name: mpname,
                        map: tiledmap.clone(),
                        gsinit: tpta::mp,
                    },
                    Mission::TiledModel {
                        name: obsname,
                        map: tiledmap,
                        gsinit: tpta::obs,
                    },
                ])
            }
            "w18" => {
                let basename =
                    String::from("tiled_") + filename.to_str().expect("tmx filename invalid str");
                let spname = basename.clone() + "_sp";
                let sprevname = basename.clone() + "_sprev";
                let mpname = basename.clone() + "_mp";
                let obsname = basename.clone() + "_obs";
                Some(vec![
                    Mission::TiledModel {
                        name: spname,
                        map: tiledmap.clone(),
                        gsinit: w18::sp,
                    },
                    Mission::TiledModel {
                        name: sprevname,
                        map: tiledmap.clone(),
                        gsinit: w18::sprev,
                    },
                    Mission::TiledModel {
                        name: mpname,
                        map: tiledmap.clone(),
                        gsinit: w18::mp,
                    },
                    Mission::TiledModel {
                        name: obsname,
                        map: tiledmap,
                        gsinit: w18::obs,
                    },
                ])
            }
            a => {
                error!("tiled model not found: {:?}", a);
                None
            }
        })
        .flatten()
        .collect()
}

counted_array!(pub const CODED_MISSIONS: [Mission; _] = [
    Mission::Deterministic {
        name: "rs_test1_sp",
        gsinit: gsfn::sp_test1,
    },
    Mission::Deterministic {
        name: "rs_test1_coop",
        gsinit: gsfn::mp_test1_coop,
    },
    Mission::Deterministic {
        name: "rs_test1_mp",
        gsinit: gsfn::mp_test1,
    },
    Mission::Deterministic {
        name: "rs_test2_sp",
        gsinit: gsfn::sp_test2,
    },
    Mission::Deterministic {
        name: "rs_test2_coop",
        gsinit: gsfn::mp_test2_coop,
    },
    Mission::Deterministic {
        name: "rs_test2_mp",
        gsinit: gsfn::mp_test2,
    },
]);

pub fn missions_names(path: PathBuf) -> Vec<String> {
    let mut missions = missions(path)
        .iter()
        .map(|x| x.name().to_owned())
        .collect::<Vec<_>>();
    missions.sort();
    missions
}
