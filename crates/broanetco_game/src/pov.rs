/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use super::*;

use std::cell::RefCell;

#[derive(Clone, Debug)]
pub struct GSPov<'a, NTD> {
    gs: &'a GS<NTD>,
    pub pov: FactionID,
    combaters: VecMap<&'a Combater<NTD>>,
    // TODO: an alternative to RefCell?
    fac_can_see_map_cache: RefCell<BTreeMap<MapLocation, bool>>,
    fac_can_see_cache: RefCell<BTreeMap<(FactionID, CombaterID), bool>>,
}

impl<'a, NTD: NTDTrait> GSPov<'a, NTD> {
    pub fn new(gs: &'a GS<NTD>, pov: FactionID) -> Self {
        GSPov {
            gs,
            pov,
            combaters: gs
                .combaters()
                .filter(move |(comid, _com)| gs.fac_can_see(pov, *comid))
                .map(|(CombaterID(id), com)| (id, com))
                .collect(),
            fac_can_see_map_cache: Default::default(),
            fac_can_see_cache: Default::default(),
        }
    }
}

impl<NTD: NTDTrait> GSBaseTrait for GSPov<'_, NTD>
where
    GS<NTD>: GSTrait<NTD = NTD>,
{
    type NTD = NTD;
    fn tick(&self) -> Tick {
        self.gs.tick()
    }
    fn get_map(&self) -> &Map<Self::NTD> {
        self.gs.fac_map(self.pov)
    }
    fn get_playing(&self) -> &PlayingID {
        self.gs.get_playing()
    }
    fn get_log(
        &self,
    ) -> Box<dyn DoubleEndedIterator<Item = (CombatLogEntryID, &CombatLogEntry)> + '_> {
        Box::new(
            self.gs
                .get_log()
                .filter(move |(cleid, _cle)| self.fac(self.pov).logged.contains(cleid)),
        )
    }

    fn com(&self, id: CombaterID) -> &Combater<Self::NTD> {
        let CombaterID(id2) = id;
        if let Some(com) = self.combaters.get(id2) {
            return com;
        }
        warn!(
            "GSPoV with unseen {:?} ({:?}) from pov {:?} ({:?})",
            id,
            self.gs.com(id).name,
            self.pov,
            self.gs.fac(self.pov).name,
        );
        self.gs.com(id)
    }
    fn fac(&self, FactionID(id): FactionID) -> &Faction<Self::NTD> {
        &self.gs.factions[id]
    }

    fn combaters<'s>(
        &'s self,
    ) -> Box<dyn DoubleEndedIterator<Item = (CombaterID, &'s Combater<Self::NTD>)> + 's> {
        Box::new(
            self.combaters
                .iter()
                .map(|(comid, com)| (CombaterID(comid), *com)),
        )
    }
    fn factions<'s>(
        &'s self,
    ) -> Box<dyn DoubleEndedIterator<Item = (FactionID, &'s Faction<Self::NTD>)> + 's> {
        self.gs.factions()
    }
}

impl<'g, NTD: NTDTrait> GSViewTrait for GSPov<'g, NTD> {
    /// Cached
    fn fac_can_see_map(&self, source: FactionID, target: MapLocation) -> bool
    where
        Self: Sized,
    {
        if source != self.pov {
            trace!("fac_can_see_map: uncached call");
            return self.fac_can_see_map_nocache(source, target);
        }
        *self
            .fac_can_see_map_cache
            .borrow_mut()
            .entry(target)
            .or_insert_with(|| {
                trace!("fac_can_see_map: yet uncached call");
                self.fac_can_see_map_nocache(source, target)
            })
    }

    /// Cached
    fn fac_can_see(&self, source: FactionID, target: CombaterID) -> bool
    where
        Self: Sized,
    {
        *self
            .fac_can_see_cache
            .borrow_mut()
            .entry((source, target))
            .or_insert_with(|| self.fac_can_see_nocache(source, target))
    }
}

#[derive(Clone, Debug)]
pub struct GSComPov<'a, NTD> {
    gs: &'a GS<NTD>,
    pov: CombaterID,
}

impl<'a, NTD> GSComPov<'a, NTD> {
    pub fn new(gs: &'a GS<NTD>, pov: CombaterID) -> Self {
        GSComPov { gs, pov }
    }
}

impl<NTD: NTDTrait> GSBaseTrait for GSComPov<'_, NTD>
where
    GS<NTD>: GSTrait<NTD = NTD>,
{
    type NTD = NTD;
    fn tick(&self) -> Tick {
        self.gs.tick()
    }
    fn get_map(&self) -> &Map<NTD> {
        self.gs.fac_map(self.com(self.pov).faction)
    }
    fn get_playing(&self) -> &PlayingID {
        self.gs.get_playing()
    }
    // TODO: filter for log events that were seen
    fn get_log(
        &self,
    ) -> Box<dyn DoubleEndedIterator<Item = (CombatLogEntryID, &CombatLogEntry)> + '_> {
        Box::new(self.gs.get_log())
    }

    fn com(&self, id: CombaterID) -> &Combater<NTD> {
        if !self.gs.com_can_see(self.pov, id) {
            warn!(
                "GSPoV with unseen {:?} ({:?}) from pov {:?} ({:?})",
                id,
                self.gs.com(id).name,
                self.pov,
                self.gs.com(self.pov).name,
            );
        }
        self.gs.com(id)
    }
    fn fac(&self, FactionID(id): FactionID) -> &Faction<NTD> {
        &self.gs.factions[id]
    }

    fn combaters<'s>(
        &'s self,
    ) -> Box<dyn DoubleEndedIterator<Item = (CombaterID, &'s Combater<NTD>)> + 's> {
        Box::new(
            self.gs
                .combaters()
                .filter(move |(comid, _com)| self.gs.com_can_see(self.pov, *comid)),
        )
    }
    fn factions<'s>(
        &'s self,
    ) -> Box<dyn DoubleEndedIterator<Item = (FactionID, &'s Faction<NTD>)> + 's> {
        self.gs.factions()
    }
}
