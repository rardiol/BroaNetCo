/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::convert::TryInto;

use hex2d::Direction;
use maplit::btreeset;

use crate::act_vec::ActVec;
use crate::bncl::number;
use crate::map::{ComLocation, Map, MapElement, MapLocation};
use crate::stat::Stat;
use crate::FactionID;
use crate::*;

pub mod helpers {
    use super::*;

    pub fn max_range_acttarg(
        max_range: Distance,
    ) -> (
        condition::ConditionDet<condition::CombaterLocation>,
        ActionTargetingLocationError,
    ) {
        (
            max_range_condition(max_range),
            ActionTargetingLocationError::MaxRange,
        )
    }

    pub fn max_range_acttarg_cc(
        max_range: Distance,
    ) -> (
        condition::ConditionDet<condition::CombaterCombater>,
        ActionTargetingCombaterError,
    ) {
        (
            max_range_condition_cc(max_range),
            ActionTargetingCombaterError::Location(ActionTargetingLocationError::MaxRange),
        )
    }

    pub fn max_range_condition(
        max_range: Distance,
    ) -> condition::ConditionDet<condition::CombaterLocation> {
        bnccondition!(
            <=,
            number::CombaterLocation::Distance,
            max_range,
        )
    }

    pub fn max_range_condition_cc(
        max_range: Distance,
    ) -> condition::ConditionDet<condition::CombaterCombater> {
        bnccondition!(
            <=,
            number::CombaterCombater::CombaterLocation(number::CombaterLocation::Distance),
            max_range,
        )
    }

    pub fn attack_action_target(max_range: Distance) -> ActionTargetingCombater {
        vec![
            (
                max_range_condition_cc(max_range),
                ActionTargetingCombaterError::Location(ActionTargetingLocationError::MaxRange),
            ),
            (
                condition::CombaterCombater::IsHostile.into(),
                ActionTargetingCombaterError::Friendly,
            ),
        ]
    }

    pub fn melee_acttarg() -> ActionTargetingCombater {
        attack_action_target(1)
    }

    pub fn is_empty_acttarg() -> (
        condition::ConditionDet<condition::CombaterLocation>,
        ActionTargetingLocationError,
    ) {
        (
            condition::CombaterLocation::Target(condition::Location::IsEmpty).into(),
            ActionTargetingLocationError::Occupied,
        )
    }

    pub fn is_passable_acttarg() -> (
        condition::ConditionDet<condition::CombaterLocation>,
        ActionTargetingLocationError,
    ) {
        (
            condition::CombaterLocation::Target(condition::Location::IsPassable).into(),
            ActionTargetingLocationError::Impassable,
        )
    }

    pub fn moveable_acttarg() -> ActionTargetingLocation {
        vec![
            max_range_acttarg(1),
            is_empty_acttarg(),
            is_passable_acttarg(),
        ]
    }

    pub const ROTATESTT: effect::CombaterLocation =
        effect::CombaterLocation::RotateSourceToTarget();
    pub const SCTCROTATESTT: effect::CombaterCombater = effect::CombaterCombater::CombaterLocation(
        effect::CombaterLocation::RotateSourceToTarget(),
    );

    pub const FACTION_COLOR_TRANSPARENCY: f32 = 0.8;

    pub type SCTCE = effect::CombaterCombater;
    pub type SCTLE = effect::CombaterLocation;

    pub fn view_status<NTD>(range: i32) -> Status<NTD> {
        Status {
            detection: vec![Detection::Range(range)],
            concealment: vec![Concealment::Simple()],
            ..Default::default()
        }
    }
}

use self::helpers::*;

pub fn sp() -> GSInit {
    gen(
        FactionType::Player,
        FactionType::AI(AI::Random(Random::Basic)),
        FactionType::AI(AI::Random(Random::Basic)),
        None,
    )
}
pub fn mp() -> GSInit {
    gen(
        FactionType::Player,
        FactionType::Player,
        FactionType::Player,
        None,
    )
}
pub fn coop() -> GSInit {
    gen(
        FactionType::Player,
        FactionType::Player,
        FactionType::AI(AI::Random(Random::Basic)),
        None,
    )
}

pub fn gen(
    fac1type: FactionType,
    fac2type: FactionType,
    fac3type: FactionType,
    map: Option<Map<NamedTextureDataTransfer>>,
) -> GSInit {
    let simple_com_namedtexture =
        NamedTextureDataTransfer::new(NTDAssetPath::Image("simple_com.svg".into()));
    let map_wall_namedtexture =
        NamedTextureDataTransfer::new(NTDAssetPath::Image("map_wall.svg".into()));
    let faction_p0 = FactionID(0);
    let faction_p1 = FactionID(1);
    let faction_p2 = FactionID(2);
    let map_floor_texture =
        NamedTextureDataTransfer::new(NTDAssetPath::Image("map_floor.svg".into()));

    let (map, (pos0, pos1, pos2)) = if let Some(map) = map {
        (
            map,
            (
                ComLocation {
                    coord: MapLocation { x: 0, y: 0 },
                    dir: Direction::YZ,
                },
                ComLocation {
                    coord: MapLocation { x: 0, y: -2 },
                    dir: Direction::XZ,
                },
                ComLocation {
                    coord: MapLocation { x: 2, y: -2 },
                    dir: Direction::XZ,
                },
            ),
        )
    } else {
        (
            Map::new_range(
                3,
                &MapElement {
                    name: "def".to_owned(),
                    ticks: Some(10),
                    protection_behind: Some(mil_chances(100)),
                    protection_inside: Some(mil_chances(100)),
                    texture_data: vec![map_floor_texture],
                    ..Default::default()
                },
            ),
            (
                ComLocation {
                    coord: MapLocation { x: 0, y: 2 },
                    dir: Direction::YZ,
                },
                ComLocation {
                    coord: MapLocation { x: 2, y: 0 },
                    dir: Direction::XZ,
                },
                ComLocation {
                    coord: MapLocation { x: 0, y: 0 },
                    dir: Direction::XZ,
                },
            ),
        )
    };
    let move_status = Status {
        name: "Movement".to_owned(),
        actions: vec![Action {
            name: "QMove".to_owned(),
            keycode: Some('q'),
            sound_effects: vec![],
            source_effects: vec![],
            target_effects: vec![ActionTargetEffect::Terrain(
                moveable_acttarg(),
                vec![
                    ROTATESTT,
                    effect::CombaterLocation::Group(vec![
                        effect::CombaterLocation::TickDiff(bncnumber!(
                            mul
                            10,
                            (number::CombaterLocation::Target(number::Location::TickCost()))
                        )),
                        effect::CombaterLocation::Teleport(),
                    ]),
                ],
            )],
        }],
        ..Default::default()
    };

    let mut ggsa: Vec<GodGSApply<NamedTextureDataTransfer>> = map.into();

    ggsa.extend(vec![
        GodGSApply::AddAIGameMaster(
            0,
            AIGameMaster::LastManStanding {
                valid_for: btreeset![faction_p0, faction_p1, faction_p2,],
            },
        ),
        GodGSApply::AddAIGameMaster(200, AIGameMaster::RevealMap { range: 4 }),
        GodGSApply::AddFaction(Faction::new(
            "Player".to_string(),
            FactionColor {
                r: 1.0_f32.try_into().unwrap(),
                g: 0.0_f32.try_into().unwrap(),
                b: 0.0_f32.try_into().unwrap(),
                a: FACTION_COLOR_TRANSPARENCY.try_into().unwrap(),
            },
            fac1type,
        )),
        GodGSApply::AddFaction(Faction::new(
            "Hostiles".to_string(),
            FactionColor {
                r: 0.0_f32.try_into().unwrap(),
                g: 0.0_f32.try_into().unwrap(),
                b: 1.0_f32.try_into().unwrap(),
                a: FACTION_COLOR_TRANSPARENCY.try_into().unwrap(),
            },
            fac2type,
        )),
        GodGSApply::AddFaction(Faction::new(
            "fac3".to_string(),
            FactionColor {
                r: 0.0_f32.try_into().unwrap(),
                g: 1.0_f32.try_into().unwrap(),
                b: 0.0_f32.try_into().unwrap(),
                a: FACTION_COLOR_TRANSPARENCY.try_into().unwrap(),
            },
            fac3type,
        )),
        GodGSApply::SetMapElement {
            location: MapLocation { x: 1, y: 0 },
            element: MapElement {
                name: "wall".to_owned(),
                ticks: None,
                protection_behind: Some(mil_chances(80)),
                protection_inside: Some(mil_chances(150)),
                texture_data: vec![map_wall_namedtexture.clone()],
                ..Default::default()
            },
        },
        GodGSApply::AddCombater(
            CombaterBuilder::default()
                .name("player".to_string())
                .statuses(vec![
                    view_status(1),
                    Status {
                        name: "Musket".to_owned(),
                        actions: vec![Action {
                            name: "Bayonet".to_owned(),
                            keycode: Some('b'),
                            sound_effects: vec![],
                            source_effects: vec![effect::Combater::TickDiff(3.into())],
                            target_effects: vec![
                                (ActionTargetEffect::Combater(
                                    melee_acttarg(),
                                    vec![
                                        effect::CombaterCombater::Target(effect::Combater::HpDiff(
                                            number::Calc::Value(-4),
                                        )),
                                        SCTCROTATESTT,
                                    ],
                                )),
                            ],
                        }],
                        substatuses: ActVec::from(vec![
                            (
                                Status {
                                    name: " (Loaded)".to_owned(),
                                    actions: vec![Action {
                                        name: "Fire".to_owned(),
                                        keycode: Some('f'),
                                        sound_effects: vec![effect::Sound::Play(
                                            "Explosion-LS100155".to_owned(),
                                        )],
                                        source_effects: vec![
                                            effect::Combater::TickDiff(5.into()),
                                            effect::Combater::SetSingleSubStatus(
                                                ("Musket".to_owned(), vec![]),
                                                1,
                                            ),
                                        ],
                                        target_effects: vec![ActionTargetEffect::Combater(
                                            attack_action_target(3),
                                            vec![
                                                effect::CombaterCombater::Group(vec![
                                                        effect::CombaterCombater::Target(
                                                            effect::Combater::HpDiff(
                                                                number::Calc::Value(-6)
                                                            )
                                                        );
                                                        1
                                                    ]),
                                                SCTCROTATESTT,
                                            ],
                                        )],
                                    }],
                                    ..Default::default()
                                },
                                true,
                            ),
                            (
                                Status {
                                    name: " (Unloaded)".to_owned(),
                                    actions: vec![Action {
                                        name: "Reload".to_owned(),
                                        keycode: Some('r'),
                                        sound_effects: vec![],
                                        source_effects: vec![
                                            effect::Combater::TickDiff(7.into()),
                                            effect::Combater::SetSingleSubStatus(
                                                ("Musket".to_owned(), vec![]),
                                                0,
                                            ),
                                        ],
                                        target_effects: vec![],
                                    }],
                                    ..Default::default()
                                },
                                false,
                            ),
                        ]),
                        ..Default::default()
                    },
                    move_status.clone(),
                ])
                .parts(PartGroup::Group(
                    vec![Part {
                        name: "psold".to_owned(),
                        hp: Some(Stat::new_max(13)),
                        size: Some(700),
                    }],
                    0,
                ))
                .location(pos0)
                .tick(12)
                .faction(faction_p0)
                .texture_data((Default::default(), vec![simple_com_namedtexture.clone()]))
                .build()
                .unwrap(),
        ),
        GodGSApply::AddCombater(
            CombaterBuilder::default()
                .name("enemy".to_string())
                .statuses(vec![
                    view_status(1),
                    Status {
                        name: "Sword".to_owned(),
                        actions: vec![Action {
                            name: "Attack".to_owned(),
                            keycode: Some('a'),
                            sound_effects: vec![],
                            source_effects: vec![effect::Combater::TickDiff(4.into())],
                            target_effects: vec![
                                (ActionTargetEffect::Combater(
                                    melee_acttarg(),
                                    vec![
                                        effect::CombaterCombater::Group(vec![
                                                effect::CombaterCombater::Target(
                                                    effect::Combater::HpDiff(
                                                        number::Calc::Value(-5)
                                                    )
                                                );
                                                1
                                            ]),
                                        SCTCROTATESTT,
                                    ],
                                )),
                            ],
                        }],
                        ..Default::default()
                    },
                    move_status.clone(),
                ])
                .parts(PartGroup::Group(
                    vec![Part {
                        name: "esold".to_owned(),
                        hp: Some(Stat::new_max(12)),
                        size: Some(700),
                    }],
                    0,
                ))
                .location(pos1)
                .tick(6)
                .faction(faction_p1)
                .texture_data((Default::default(), vec![simple_com_namedtexture.clone()]))
                .build()
                .unwrap(),
        ),
        GodGSApply::AddCombater(
            CombaterBuilder::default()
                .name("fac3en".to_string())
                .statuses(vec![
                    view_status(1),
                    Status {
                        name: "Sword".to_owned(),
                        actions: vec![Action {
                            name: "Attack".to_owned(),
                            keycode: Some('a'),
                            sound_effects: vec![],
                            source_effects: vec![effect::Combater::TickDiff(4.into())],
                            target_effects: vec![ActionTargetEffect::Combater(
                                melee_acttarg(),
                                vec![
                                    effect::CombaterCombater::Group(vec![
                                            effect::CombaterCombater::Target(
                                                effect::Combater::HpDiff(
                                                    bncnumber!(-5)
                                                )
                                            );
                                            1
                                        ]),
                                    SCTCROTATESTT,
                                ],
                            )],
                        }],
                        ..Default::default()
                    },
                    move_status.clone(),
                ])
                .parts(PartGroup::Group(
                    vec![Part {
                        name: "esold".to_owned(),
                        hp: Some(Stat::new_max(12)),
                        size: Some(700),
                    }],
                    0,
                ))
                .location(pos2)
                .tick(8)
                .faction(faction_p2)
                .texture_data((Default::default(), vec![simple_com_namedtexture.clone()]))
                .build()
                .unwrap(),
        ),
    ]);
    ggsa.into()
}
