/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::fmt::Debug;
use std::hash::Hash;
use std::marker::Sized;

use super::*;

#[derive(Debug, Clone, Hash, Serialize, Deserialize)]
pub enum Detection {
    /// Simple range of sight, ignores the rest
    Range(i32),
    /// Adds distance to concealment
    LineOfSight(i32),
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize)]
pub enum Concealment {
    Simple(),
    LineOfSight(i32),
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize)]
pub enum MapDetection {
    /// Simple bonus
    LineOfSight(i32),
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize)]
pub enum MapConcealment {
    /// Simple bonus
    LineOfSight(i32),
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize)]
pub enum MapPathConcealment {
    /// Simple bonus
    LineOfSight(i32),
}

pub const DEFAULT_CONCEALMENTS: [Concealment; 2] =
    [Concealment::Simple(), Concealment::LineOfSight(0)];

impl<NTD: NTDTrait> GSViewTrait for GS<NTD> {}

impl<'g, NTD: NTDTrait> GSViewTrait for GSComPov<'g, NTD> {}

// TODO: integrate this better with GSViewTrait
/// More efficient than
pub fn com_can_see_maps<'a, IT: 'a + Iterator<Item = MapLocation>, GST: GSViewTrait>(
    gs: &'a GST,
    source: CombaterID,
    targets: IT,
) -> impl Iterator<Item = MapLocation> + 'a {
    let Combater {
        location: location_source,
        ..
    } = gs.com(source);
    let detection = gs.com_detections(source);
    targets.filter(move |location_target| {
        gs.can_see_location(
            location_source.coord,
            &detection,
            *location_target,
            &gs.get_map()[*location_target].self_concealment,
        )
    })
}

pub trait GSViewTrait: GSBaseTrait {
    fn com_concealments(&self, comid: CombaterID) -> Vec<Concealment>
    where
        Self: Sized,
    {
        // TODO: cloneless
        self.com(comid)
            .active_statuses(self, comid)
            .flat_map(|status| status.concealment.clone())
            .collect()
    }

    fn com_detections(&self, comid: CombaterID) -> Vec<Detection>
    where
        Self: Sized,
    {
        self.com(comid)
            .active_statuses(self, comid)
            .flat_map(|status| status.detection.clone())
            .collect()
    }

    /// Could source see a Combater at the target with given concealment?
    fn com_can_see_location(
        &self,
        source: CombaterID,
        location_target: MapLocation,
        concealment: &[Concealment],
    ) -> bool
    where
        Self: Sized,
    {
        let Combater {
            location: location_source,
            ..
        } = self.com(source);
        let detection = self.com_detections(source);
        self.can_see_location(
            location_source.coord,
            &detection,
            location_target,
            concealment,
        )
    }

    /// Could source see a Combater at the target with given concealment?
    fn can_see_location(
        &self,
        location_source: MapLocation,
        detection: &[Detection],
        location_target: MapLocation,
        concealment: &[Concealment],
    ) -> bool
    where
        Self: Sized,
    {
        let map_detection = &self.get_map()[location_source].detection;
        let map_concealment = &self.get_map()[location_target].concealment;
        let distance = location_source.distance(location_target);
        trace!(
            "can_see: -- {:?} det: {:?}, target on: {:?} conc: {:?}",
            location_source,
            detection,
            location_target,
            concealment
        );
        for detection in detection {
            for concealment in concealment {
                trace!(
                    "com_can_see pair: det: {:?}, conc: {:?}",
                    detection,
                    concealment
                );
                match (detection, concealment) {
                    (Detection::Range(range), Concealment::Simple()) => {
                        let result = *range >= distance;
                        trace!(
                            "com_can_see Range-LoS: res:{:?} -- distance:{:?}",
                            result,
                            distance
                        );
                        if result {
                            return true;
                        }
                    }
                    (Detection::LineOfSight(detection), Concealment::LineOfSight(concealment)) => {
                        let mut detection: i32 = *detection;
                        let mut concealment: i32 = *concealment;
                        for map_detection in map_detection {
                            match map_detection {
                                MapDetection::LineOfSight(bonus) => detection += bonus,
                            }
                        }
                        // Just the elements in the path, excluding source and target
                        let mut path_iter = location_source
                            .line_to_iter(location_target)
                            .skip(1)
                            .peekable();
                        while let Some(map_element_in_path) = path_iter.next() {
                            if path_iter.peek().is_none() {
                                break;
                            }
                            if !self.get_map().contains_coord(map_element_in_path) {
                                // tiles outside of the map are assumed to be view blockers
                                // FIXME: this can be confusing on the map borders, since
                                // line_to_iter has no edge detection
                                // example: (o=outside map, f=flatland, 1 and 2 are coms)
                                //  o 1
                                // o o f
                                //  o 2
                                break;
                            }
                            for map_path_concealment in
                                &self.get_map()[map_element_in_path].path_concealment
                            {
                                match map_path_concealment {
                                    MapPathConcealment::LineOfSight(bonus) => concealment += bonus,
                                }
                            }
                        }
                        for map_concealment in map_concealment {
                            match map_concealment {
                                MapConcealment::LineOfSight(bonus) => concealment += bonus,
                            }
                        }
                        let result = detection >= concealment + distance;
                        trace!(
                            "com_can_see LoS: res:{:?} -- detection: {:?}, concealment: {:?}, distance:{:?}",
                            detection,
                            concealment,
                            result,
                            distance
                        );
                        if result {
                            return true;
                        }
                    }
                    (_detection, _concealment) => (),
                }
            }
        }
        false
    }

    /// Can source see the target?
    fn com_can_see_map(&self, source: CombaterID, target: MapLocation) -> bool
    where
        Self: Sized,
    {
        trace!(
            "com_can_see_map: -- source:{:?}, target:{:?}",
            source,
            target,
        );
        self.com_can_see_location(source, target, &self.get_map()[target].self_concealment)
    }

    /// Can source see the target?
    fn com_can_see(&self, source: CombaterID, target: CombaterID) -> bool
    where
        Self: Sized,
    {
        assert!(
            self.get_map()
                .get(self.com(source).location.coord)
                .is_some(),
            "source not in known map"
        );
        if source == target {
            return true;
        }
        let Combater {
            location: location_target,
            ..
        } = self.com(target);
        trace!("com_can_see: -- source:{:?}, target:{:?}", source, target,);
        self.com_can_see_location(
            source,
            location_target.coord,
            &self.com_concealments(target),
        )
    }
    /// Could the source see a target at location with the given concealment?
    fn fac_can_see_location(
        &self,
        source: FactionID,
        target: MapLocation,
        concealment: &[Concealment],
    ) -> bool
    where
        Self: Sized,
    {
        for (comid, _com) in self
            .combaters()
            .filter(|(_comid, com)| com.faction == source)
        {
            if self.com_can_see_location(comid, target, concealment) {
                return true;
            }
        }
        false
    }

    /// Can the source see the target map location?
    /// i.e. can any of source's combaters see the target?
    fn fac_can_see_map_nocache(&self, source: FactionID, target: MapLocation) -> bool
    where
        Self: Sized,
    {
        for (comid, _com) in self
            .combaters()
            .filter(|(_comid, com)| com.faction == source)
        {
            if self.com_can_see_map(comid, target) {
                return true;
            }
        }
        false
    }

    /// Can the source see the target map location?
    /// i.e. can any of source's combaters see the target?
    fn fac_can_see_map(&self, source: FactionID, target: MapLocation) -> bool
    where
        Self: Sized,
    {
        self.fac_can_see_map_nocache(source, target)
    }

    /// Can the source see the target?
    /// i.e. can any of source's combaters see the target?
    fn fac_can_see_nocache(&self, source: FactionID, target: CombaterID) -> bool
    where
        Self: Sized,
    {
        assert!(
            self.get_map()
                .get(self.com(target).location.coord)
                .is_some(),
            "target not in known map"
        );
        if !self.fac_can_see_map_nocache(source, self.com(target).location.coord) {
            debug!("fac_can_see_nocache: can't see where the com is standing");
            return false;
        }
        for (comid, _com) in self
            .combaters()
            .filter(|(_comid, com)| com.faction == source)
        {
            if self.com_can_see(comid, target) {
                return true;
            }
        }
        false
    }

    /// Can the source see the target?
    /// i.e. can any of source's combaters see the target?
    fn fac_can_see(&self, source: FactionID, target: CombaterID) -> bool
    where
        Self: Sized,
    {
        self.fac_can_see_nocache(source, target)
    }
}
