/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

#[macro_use]
extern crate derive_builder;
#[macro_use]
extern crate log as debug_log;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate derivative;
#[macro_use]
extern crate derive_new;
#[macro_use]
extern crate const_fn;

extern crate broanetco_actvec as act_vec;
extern crate broanetco_util as util;

pub extern crate hex2d;
/// For using None as a DrawCacheTrait or ReplayLoggerControlTrait
pub use void::Void;

use std::collections::{BTreeMap, BTreeSet, VecDeque};
use std::convert::{TryFrom, TryInto};
use std::fmt::Debug;
use std::hash::Hash;
use std::{fmt, ops};

use hex2d::Direction;

use itertools::Itertools;
use num::rational::Rational32;
use ordered_float::{FloatIsNan, NotNan};
use rand::{Rng, SeedableRng};
use rand_xorshift::XorShiftRng;
pub use toodee::{TooDee, TooDeeMap};
use vec_map::VecMap;

pub use either::{Either, Left, Right};

/// GS - action related things
mod action;
/// AIs
pub mod ai;
/// Combater related
mod combater;
/// Faction related
pub mod faction;
/// Effects and numbers
#[macro_use]
pub mod bncl;
/// Extra Data store
pub mod edata;
/// GSInit
pub mod init;
/// Logging system
mod log;
/// Game Map
pub mod map;
/// AI GameMasters system
mod master;
/// Limited GS views
mod pov;
/// Game replay logging
pub mod replay;
/// Sound system
pub mod sound;
/// Stat struct
pub mod stat;
/// A test GSInit
pub mod test1;
/// Paths to textures without textures, NTD
pub mod texture;
/// Visibility system
mod view;

pub use self::ai::*;
pub use self::combater::*;
pub use self::faction::*;
use self::init::*;
use self::map::{Map, MapElement, MapLocation};
use self::sound::SoundControl;
pub use self::texture::*;
pub use crate::action::*;
use crate::bncl::condition::{ConditionTrait, RandomConditionTrait};
pub use crate::log::*;
pub use crate::master::*;
pub use crate::pov::*;
use crate::util::*;
pub use crate::view::*;
use bncl::effect::Effect;
pub use bncl::*;
pub use edata::EDataID;
use replay::{ReplayLoggerControlTrait, ReplayLoggerMessage};

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize, new)]
pub struct CombaterID(usize);

/// Basic unit of distance (hexagonal taxicab)
pub type Distance = i32;

/// Basic unit of time
pub type Tick = i32;
/// x/1000
pub type Mil = i32;
pub type Chances = Rational32;
pub fn mil_chances(input: Mil) -> Chances {
    Chances::new(input, 1000)
}

pub type UISHint = Option<CombaterID>;

/// Identifier of a Fluent message
pub type MessageI18N = String;
pub type IdentifierI18N = String;
pub type ArgI18N = Either<String, i32>;
/// Convert to FluentArgs
pub type ArgsI18N = Vec<(IdentifierI18N, ArgI18N)>;
/// All needed for translation
pub type TextI18N = (MessageI18N, ArgsI18N);

#[derive(Debug, Clone, Derivative, Serialize, Deserialize)]
#[derivative(Hash)]
#[derivative(Default(new = "true", bound = ""))]
pub struct GS<NTD> {
    /// Name of the game. Used to set the window name and get translation file
    pub name: String,
    pub playing: PlayingID,
    tick: Tick,
    factions: Vec<Faction<NTD>>,
    combaters: Vec<Combater<NTD>>,
    pub map: Map<NTD>,
    // TODO: hash the rng too
    #[derivative(Hash = "ignore")]
    #[derivative(Default(value = "GRng::seed_from_u64(0)"))]
    rng: GRng,
    log: Vec<CombatLogEntry>,
    // TODO: other possibilities
    timed_effects: BTreeMap<Tick, VecDeque<(CombaterID, effect::Combater)>>,
    ai_game_masters: BTreeMap<AIGMPriority, AIGameMaster<NTD>>,
}

pub type GRng = XorShiftRng;

impl fmt::Display for CombaterID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let CombaterID(p) = *self;
        write!(f, "{}", p)
    }
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize, Derivative)]
#[derivative(Default(bound = ""))]
/// Whose turn to play is it
pub enum PlayingID {
    /// Faction can play actively with the given combater.
    Faction(CombaterID),
    /// Faction combater may act, but may do so automatically
    FactionAuto(CombaterID),
    /// Between factions acting, GS updating by itself
    #[derivative(Default)]
    Interrupt,
    /// Shows a choice that a certain faction must answer
    Choice {
        faction: FactionID,
        text: String,
        choices: Vec<(String, effect::GS)>,
    },
    /// Game over
    Winner(FactionID),
}

impl PlayingID {
    pub fn is_interrupt(&self) -> bool {
        matches!(self, PlayingID::Interrupt)
    }
}

impl fmt::Display for PlayingID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let ret = match *self {
            Self::Faction(p) => format!("Playing: Combater {}", p),
            Self::Choice { faction, .. } => format!("Making a choice: Player {}", faction),
            Self::FactionAuto(p) => format!("Processing: Combater {}", p),
            Self::Winner(p) => format!("Winner: Player {}", p),
            Self::Interrupt => "Interrupt".to_owned(),
        };
        f.write_str(&ret)
    }
}

pub type HpPts = i32;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct ColorWrap {
    /// Red component
    pub r: NotNan<f32>,
    /// Green component
    pub g: NotNan<f32>,
    /// Blue component
    pub b: NotNan<f32>,
    /// Alpha component
    pub a: NotNan<f32>,
}

impl From<ColorWrap> for (f32, f32, f32, f32) {
    fn from(cl: ColorWrap) -> (f32, f32, f32, f32) {
        (
            cl.r.into_inner(),
            cl.g.into_inner(),
            cl.b.into_inner(),
            cl.a.into_inner(),
        )
    }
}

impl TryFrom<(f32, f32, f32, f32)> for ColorWrap {
    type Error = FloatIsNan;
    fn try_from((r, g, b, a): (f32, f32, f32, f32)) -> Result<Self, FloatIsNan> {
        Ok(Self {
            r: r.try_into()?,
            g: g.try_into()?,
            b: b.try_into()?,
            a: a.try_into()?,
        })
    }
}

impl ColorWrap {
    pub fn dissolve(self) -> (f32, f32, f32, f32) {
        self.into()
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum GodGSApply<NTD> {
    AddCombater(Combater<NTD>),
    AddFaction(Faction<NTD>),
    AddAIGameMaster(AIGMPriority, AIGameMaster<NTD>),
    SetMapElement {
        location: MapLocation,
        element: MapElement<NTD>,
    },
    SetRNG(Seed),
    SetPlayingID(PlayingID),
    SetTick(Tick),
    SetName(String),
    GSEffect(effect::GS),
    GSApply(GSApply),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum GSApply {
    Action {
        source: CombaterID,
        action_id: ActionID,
        targets: Vec<ActionTarget>,
    },
    Choice(usize),
    SkipTick {
        source: CombaterID,
        amount: Tick,
    },
    EndFactionTurn,
}

impl<NTD: NTDTrait> GSBaseTrait for GS<NTD> {
    type NTD = NTD;
    fn tick(&self) -> Tick {
        self.tick
    }

    fn get_map(&self) -> &Map<NTD> {
        &self.map
    }
    fn get_playing(&self) -> &PlayingID {
        &self.playing
    }

    // Access Combater and Faction
    fn com(&self, CombaterID(id): CombaterID) -> &Combater<NTD> {
        &self.combaters[id]
    }
    fn fac(&self, FactionID(id): FactionID) -> &Faction<NTD> {
        &self.factions[id]
    }

    ///////////////////////////////////////////////
    // Combater helpers
    /// Get all combaters with IDs (ingame only)
    fn combaters<'s>(
        &'s self,
    ) -> Box<dyn DoubleEndedIterator<Item = (CombaterID, &'s Combater<NTD>)> + 's> {
        Box::new(
            self.combaters
                .iter()
                .enumerate()
                .map(|(comid, com)| (CombaterID(comid), com))
                .filter(|(_comid, com)| com.ingame),
        )
    }

    ///////////////////////////////////////////////
    // Faction helpers
    /// Get all factions with IDs
    fn factions<'s>(
        &'s self,
    ) -> Box<dyn DoubleEndedIterator<Item = (FactionID, &'s Faction<NTD>)> + 's> {
        Box::new(
            self.factions
                .iter()
                .enumerate()
                .map(|(facid, fac)| (FactionID(facid), fac)),
        )
    }

    //////////////////////////////////////////////
    // logging
    fn get_log(
        &self,
    ) -> Box<dyn DoubleEndedIterator<Item = (CombatLogEntryID, &CombatLogEntry)> + '_> {
        Box::new(self.log.iter().enumerate())
    }
}

impl<NTD: NTDTrait> GSTrait for GS<NTD> {}

impl<'g, NTD: NTDTrait> GSTrait for GSPov<'g, NTD> {}

impl<'g, NTD: NTDTrait> GSTrait for GSComPov<'g, NTD> {}

impl<NTD: NTDTrait> GS<NTD> {
    // Access Combater and Faction
    pub fn com_m(&mut self, CombaterID(id): CombaterID) -> &mut Combater<NTD> {
        &mut self.combaters[id]
    }
    // Access Combater and Faction
    pub fn fac_m(&mut self, FactionID(id): FactionID) -> &mut Faction<NTD> {
        &mut self.factions[id]
    }

    ///////////////////////////////////////////////
    // Adding faction and combater
    /// Adds a faction and returns its ID
    pub fn add_faction(&mut self, faction_data: Faction<NTD>) -> FactionID {
        self.factions.push(faction_data);
        FactionID(self.factions.len() - 1)
    }
    /// Adds a combater and returns its ID
    pub fn add_combater(&mut self, combater_data: Combater<NTD>) -> CombaterID {
        self.combaters.push(combater_data);
        CombaterID(self.combaters.len() - 1)
    }
    /// Removes a combater
    /// Just sets ingame off
    pub fn remove_combater(&mut self, combater: CombaterID) {
        self.com_m(combater).ingame = false;
    }

    ///////////////////////////////////////////////
    // Combater helpers

    ///////////////////////////////////////////////
    // Faction helpers

    fn fac_map(&self, id: FactionID) -> &Map<NTD> {
        self.fac(id).map.as_ref().unwrap_or(&self.map)
    }

    /// Checks if faction is "alive"
    /// Currently a faction is alive iff it has a combater ingame
    pub fn faction_is_alive(&self, faction: FactionID) -> bool {
        for (_comid, com) in self.combaters() {
            if com.faction == faction {
                return true;
            }
        }
        false
    }

    /// Gets factions that are alive
    fn alive_factions(&self) -> impl DoubleEndedIterator<Item = (FactionID, &Faction<NTD>)> {
        self.factions()
            .filter(move |(facid, _)| self.faction_is_alive(*facid))
    }

    /// Gets all factions played by humans
    pub fn player_factions(&self) -> Vec<FactionID> {
        self.factions()
            .filter(|(_faction_id, faction_data)| !faction_data.faction_type.is_ai())
            .map(|(facid, _)| facid)
            .collect()
    }

    ///////////////////////////////////////////////
    // Turn management

    pub fn play_order(&self) -> Vec<(CombaterID, &Combater<NTD>)> {
        self.combaters()
            .sorted_by_key(|(_comid, com)| com.tick)
            .collect()
    }

    pub fn after_gsapply_adjust(&mut self) {
        match self.playing {
            PlayingID::Faction(_facid) | PlayingID::FactionAuto(_facid) => {
                self.playing = PlayingID::Interrupt
            }
            PlayingID::Winner(..) | PlayingID::Interrupt | PlayingID::Choice { .. } => (),
        }
    }

    /// Handles GS.ai_game_masters.
    /// Returns true when nothing has been done and phase can end
    pub fn phase_ai_game_masters<DRAWCACHE: DrawCacheTrait>(
        &mut self,
        limit_tick: Tick,
        drawcache: &mut Option<&mut DRAWCACHE>,
        sound_control: &Option<SoundControl>,
    ) -> bool {
        trace!("phase_ai_game_masters");
        let keys: Vec<AIGMPriority> = self.ai_game_masters.keys().copied().collect();
        for key in keys {
            trace!("running aigamemaster {:?}", key);
            if !self.run_ai_game_master(key, limit_tick, drawcache, sound_control) {
                debug!("AIGM with priority {:?} has done something", key);
                return false;
            }
        }
        true
    }

    /// Handles GS.timed_effects.
    /// Returns true when nothing has been done and phase can end
    pub fn phase_target_effects<DRAWCACHE: DrawCacheTrait>(
        &mut self,
        limit_tick: Tick,
        drawcache: &mut Option<&mut DRAWCACHE>,
        _sound_control: &Option<SoundControl>,
    ) -> bool {
        trace!("phase_target_effects");
        if let Some((tick, targeteffects)) = self.timed_effects.iter_mut().next() {
            let tick = *tick;
            if tick <= limit_tick {
                // TODO: use effect log
                trace!("phase_target_effects: at tick {:?}", tick);
                self.tick = tick;
                if let Some((target, effect)) = targeteffects.pop_front() {
                    debug!(
                        "phase_target_effects: applying target effect {:?}-{:?}",
                        target, effect
                    );
                    effect.apply(self, target);
                    if let Some(ref mut drawcache) = drawcache {
                        drawcache.reset_game();
                    }
                    false
                } else {
                    // All (target, effects) for this tick have been done,
                    // so remove the entry
                    assert!(self.timed_effects.remove(&tick).unwrap().is_empty());
                    false
                }
            } else {
                // Next timed effect is after next combater acts
                true
            }
        } else {
            // timed_effect is empty, nothing to do
            true
        }
    }

    /// Handles combaters acting
    /// Returns true when everything here is done and therefore update should end
    /// This happens either when external input is needed or the game is locked in the current state
    pub fn phase_com_actions<DRAWCACHE: DrawCacheTrait, RLC: ReplayLoggerControlTrait>(
        &mut self,
        drawcache: &mut Option<&mut DRAWCACHE>,
        sound_control: &Option<SoundControl>,
        _replay_logger: &Option<RLC>,
    ) -> bool {
        // AI and GSapply and actions
        let gsapply = match self.playing {
            PlayingID::Winner { .. } => {
                trace!("play_ai winner");
                return true;
            }
            PlayingID::Interrupt => {
                trace!("play_ai interrupt");
                // TODO: make this configurable or an AIGM

                let (next_comid, _next_com) = self.next_com_to_play();

                self.playing = PlayingID::FactionAuto(next_comid);

                if let Some(ref mut drawcache) = drawcache {
                    drawcache.reset_game();
                }
                return false;
            }
            PlayingID::Choice {
                faction,
                ref choices,
                ..
            } => {
                trace!("play_ai choice");
                match self.fac(faction).faction_type {
                    FactionType::Player => return true,
                    FactionType::AI(AI::Deterministic(Deterministic::Nop)) => GSApply::Choice(0),
                    FactionType::AI(AI::Random(_)) => {
                        GSApply::Choice(self.rng.gen_range(0..choices.len()))
                    }
                }
            }
            PlayingID::FactionAuto(comid) => {
                match self.command(self.com(comid).command.clone(), comid) {
                    Some(gsa) => gsa,
                    None => return false,
                }
            }
            PlayingID::Faction(comid) => {
                let facid = self.com(comid).faction;
                match self.fac(facid).faction_type.clone() {
                    FactionType::AI(ai) => {
                        match ai {
                            AI::Deterministic(ai) => {
                                let pov = GSPov::new(self, facid);
                                ai.run(&pov, comid)
                            }
                            AI::Random(ai) => {
                                // TODO: cloning the rng twice will result in two ais doing the
                                // same thing
                                let mut rng = GRng::from_rng(&mut self.rng).unwrap();
                                let pov = GSPov::new(self, facid);
                                ai.run(&pov, comid, &mut rng)
                            }
                        }
                    }
                    FactionType::Player => {
                        trace!("play_ai player");
                        return true;
                    }
                }
            }
        };
        debug!("play_ai action {:?}", gsapply);
        self.gsapply::<_, Void>(&gsapply, &sound_control.clone(), drawcache, &None);
        false
    }

    pub fn command(&mut self, command: Command, comid: CombaterID) -> Option<GSApply> {
        match command {
            Command::Direct => {
                self.playing = PlayingID::Faction(comid);
                None
            }
            Command::AI(ai) => Some(
                // TODO: pick gspov or gscompov
                match ai {
                    AI::Deterministic(ai) => {
                        let pov = GSComPov::new(self, comid);
                        ai.run(&pov, comid)
                    }
                    AI::Random(ai) => {
                        // TODO: cloning the rng twice will result in two ais doing the
                        // same thing
                        let mut rng = GRng::from_rng(&mut self.rng).unwrap();
                        let pov = GSComPov::new(self, comid);
                        ai.run(&pov, comid, &mut rng)
                    }
                },
            ),
            Command::SingleTarget {
                target,
                attack_action,
                on_loss_of_target,
            } => {
                let mut rng = GRng::from_rng(&mut self.rng).unwrap();
                let facid = self.com(comid).faction;
                if self.com(target).ingame && self.fac_can_see(facid, target) {
                    Some(ai::single_target_ai(
                        &GSPov::new(self, facid),
                        &mut rng,
                        comid,
                        target,
                        attack_action,
                    ))
                } else {
                    match on_loss_of_target {
                        CommandSingleTargetOnLossOfTarget::Die => {
                            self.remove_combater(comid);
                            None
                        }
                        CommandSingleTargetOnLossOfTarget::ChangeCommand(new_command) => {
                            self.com_m(comid).command = *new_command;
                            None
                        }
                    }
                }
            }
            Command::Condition {
                condition,
                on_success,
                on_fail,
            } => {
                let (effects, new_command) = if condition.eval_gs_rng(self, comid) {
                    on_success
                } else {
                    on_fail
                };
                for effect in effects {
                    effect.apply(self, comid);
                }
                if let Some(new_command) = new_command {
                    self.command(*new_command, comid)
                } else {
                    None
                }
            }
        }
    }

    // TODO: specify clearly the turn system both local and networked

    /// Does update needs to run(and caches may be invalidated?)
    pub fn needs_update(&self) -> bool {
        !matches!(
            self.playing,
            PlayingID::Faction(_) | PlayingID::Choice { .. }
        )
    }

    pub fn update<DRAWCACHE: DrawCacheTrait, RLC: ReplayLoggerControlTrait>(
        &mut self,
        drawcache: &mut Option<&mut DRAWCACHE>,
        sound_control: Option<SoundControl>,
        //TODO : clarify when and what needs to go to replay logger
        replay_logger: Option<RLC>,
    ) {
        trace!("update start");
        loop {
            trace!("update loop: {:?}", self.playing);

            // TODO: only reset drawcache once

            //////////////////////////////////////////
            // AIGameMaster Phase
            if !self.phase_ai_game_masters(
                self.next_com_to_play().1.tick,
                drawcache,
                &sound_control,
            ) {
                continue;
            }

            trace!("update end aigamemaster: {:?}", self.playing);

            //////////////////////////////////////////
            // Timed Effects Phase
            if !self.phase_target_effects(self.next_com_to_play().1.tick, drawcache, &sound_control)
            {
                continue;
            }

            trace!("update end timed_effects: {:?}", self.playing);

            //////////////////////////////////////////
            // An unit will play
            self.tick = self.next_com_to_play().1.tick;
            if self.phase_com_actions(drawcache, &sound_control, &replay_logger) {
                break;
            } else {
                continue;
            }
        }
        trace!("update end");
    }

    ///////////////////////////////////////////////
    // (God)GSApply
    /// Applies a GodGSApply
    pub fn god_gsapply<DRAWCACHE: DrawCacheTrait, RLC: ReplayLoggerControlTrait>(
        &mut self,
        msg: GodGSApply<NTD>,
        sound_control: &Option<SoundControl>,
        drawcache: &mut Option<&mut DRAWCACHE>,
        replay_logger: &Option<RLC>,
    ) {
        info!("Applying GodGSApply: {:?}", msg);
        if let Some(replay_logger) = replay_logger {
            replay_logger
                .send(ReplayLoggerMessage::GodGSApply(
                    msg.clone().try_into_params(&mut ()).unwrap(),
                )) // TODO: no unwrap
                .unwrap()
        }
        if let Some(ref mut drawcache) = drawcache {
            drawcache.reset_game();
        }
        match msg {
            GodGSApply::AddCombater(combater) => {
                let _ = self.add_combater(combater);
            }
            GodGSApply::AddFaction(faction) => {
                let _ = self.add_faction(faction);
            }
            GodGSApply::SetMapElement { location, element } => {
                self.map.insert(location, element);
            }
            GodGSApply::AddAIGameMaster(priority, aigm) => {
                if let Some(previous) = self.ai_game_masters.insert(priority, aigm) {
                    warn!("aigm at {:?} overwritten: {:?}", priority, previous);
                }
            }
            GodGSApply::SetRNG(seed) => {
                self.rng = GRng::from_seed(seed);
            }
            GodGSApply::SetPlayingID(playing_id) => {
                self.playing = playing_id;
            }
            GodGSApply::GSEffect(effect) => {
                let _effect_log = effect.apply(self, ());
                // TODO: consider what to do with this efflog
            }
            GodGSApply::SetTick(tick) => {
                self.tick = tick;
            }
            GodGSApply::SetName(name) => {
                self.name = name;
            }
            GodGSApply::GSApply(gsapply) => {
                self.gsapply::<_, void::Void>(&gsapply, sound_control, drawcache, &None)
            }
        }
    }

    /// Applies a GSApply
    pub fn gsapply<DRAWCACHE: DrawCacheTrait, RLC: ReplayLoggerControlTrait>(
        &mut self,
        msg: &GSApply,
        sound_control: &Option<SoundControl>,
        drawcache: &mut Option<&mut DRAWCACHE>,
        replay_logger: &Option<RLC>,
    ) {
        info!("Applying GSApply: {:?}", msg);
        if let Some(replay_logger) = replay_logger {
            replay_logger
                .send(ReplayLoggerMessage::GSApply(msg.clone()))
                .unwrap()
        }
        debug!("RNG status: {:?}", self.rng);
        if let Some(drawcache) = drawcache {
            drawcache.reset_game();
        }
        match (&self.playing, msg) {
            (PlayingID::Faction(_facid), _) | (PlayingID::FactionAuto(_facid), _) => (),
            (PlayingID::Choice { .. }, GSApply::Choice { .. }) => (),
            _ => warn!("gsapply when no faction may act"),
        }
        let log_msg = match *msg {
            GSApply::Action {
                source,
                action_id,
                ref targets,
            } => self.do_combater_action(source, action_id, targets.clone(), sound_control),
            GSApply::Choice(choice) => self.do_choice(choice),
            GSApply::SkipTick { source, amount } => self.do_skip_tick(source, amount),
            GSApply::EndFactionTurn => self.do_end_faction_turn(),
        };

        info!("GameLog: {:?}", log_msg);
        self.push_log(log_msg);

        self.after_gsapply_adjust();
    }
}

pub type GSChecksum = [u32; 9];

impl<NTD: std::hash::Hash> GS<NTD> {
    ///////////////////////////////////////////////
    // Utils
    pub fn crc32(&self) -> GSChecksum {
        [
            crc32(&self),
            crc32(&self.playing),
            crc32(&self.tick),
            crc32(&self.factions),
            crc32(&self.combaters),
            crc32(&self.map),
            //           crc32(&self.rng),
            crc32(&self.log),
            crc32(&self.timed_effects),
            crc32(&self.ai_game_masters),
        ]
    }
}

pub trait GSBaseTrait {
    type NTD: NTDTrait;
    fn com(&self, id: CombaterID) -> &Combater<Self::NTD>;
    fn fac(&self, id: FactionID) -> &Faction<Self::NTD>;

    // https://stackoverflow.com/questions/39482131/is-it-possible-to-use-impl-trait-as-a-functions-return-type-in-a-trait-defini#39490692
    fn combaters<'s>(
        &'s self,
    ) -> Box<dyn DoubleEndedIterator<Item = (CombaterID, &'s Combater<Self::NTD>)> + 's>;
    fn factions<'s>(
        &'s self,
    ) -> Box<dyn DoubleEndedIterator<Item = (FactionID, &'s Faction<Self::NTD>)> + 's>;

    fn tick(&self) -> Tick;
    fn get_log(
        &self,
    ) -> Box<dyn DoubleEndedIterator<Item = (CombatLogEntryID, &CombatLogEntry)> + '_>;
    fn get_map(&self) -> &Map<Self::NTD>;
    fn get_playing(&self) -> &PlayingID;

    // TODO: multiple noncolliding units in same space
    /// Find combater by location
    fn combater_at_location(
        &self,
        location: MapLocation,
    ) -> Option<(CombaterID, &Combater<Self::NTD>)> {
        self.combaters()
            .find(|(_comid, com)| com.location.coord == location)
    }

    /// Gets if faction1 is friendly towards faction2
    /// Currently only true if equal
    fn friendly_factions(&self, faction1: FactionID, faction2: FactionID) -> bool {
        faction1 == faction2
    }
}

pub trait GSTrait: GSActionTrait {
    // TODO: what if there's two simultaneous combaters?
    // TODO: fix this for timed_effects and other time issues
    fn next_com_to_play(&self) -> (CombaterID, &Combater<Self::NTD>) {
        let (com_id, com) = self
            .combaters()
            .min_by_key(|&(_comid, com)| com.tick)
            .unwrap();
        (com_id, com)
    }

    /// Checks if the com can be played by its faction now
    fn com_can_act(&self, comid: CombaterID) -> bool {
        if let PlayingID::Faction(playing_comid) = self.get_playing() {
            if *playing_comid == comid {
                return true;
            }
        }
        false
    }

    fn play_order(&self) -> Vec<(CombaterID, &Combater<Self::NTD>)> {
        self.combaters()
            .sorted_by_key(|(_comid, com)| com.tick)
            .collect()
    }
}

fn ninmil_rng<RNG: rand::RngCore>(chance: Chances, rng: &mut RNG) -> bool {
    trace!("ninmil_rng: chance: {}", chance);
    let (num, den) = chance.into();
    assert!(den > 0);
    // rand panics in the first two cases
    let ret = if num < 0 {
        false
    } else if num > den {
        true
    } else {
        rng.gen_ratio(num.try_into().unwrap(), den.try_into().unwrap())
    };
    trace!("ninmil_rng: ret: {}", ret);
    ret
}

fn ninmil<NTD>(chance: Chances, gs: &mut GS<NTD>) -> bool {
    ninmil_rng(chance, &mut gs.rng)
}

pub trait DrawCacheTrait {
    fn reset_game(&mut self);
}

impl DrawCacheTrait for void::Void {
    fn reset_game(&mut self) {
        unreachable!()
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    use crate::GS;
    type GST = GS<NamedTextureDataTransfer>;

    #[test]
    fn test_gs_gen() {
        test_gs();
    }

    pub fn test_gs() -> GST {
        test1::sp().into()
    }

    #[test]
    fn move_test() {
        use crate::map::MapLocation;
        use hex2d::Direction;

        let mut gs = test_gs();

        assert!(gs
            .combater_at_location(MapLocation { x: 0, y: 0 })
            .is_some());
        assert!(gs
            .combater_at_location(MapLocation { x: 1, y: -1 })
            .is_none());

        let msg = gs.move_combater_direction(
            gs.combater_at_location(MapLocation { x: 0, y: 0 })
                .unwrap()
                .0,
            Direction::XY,
        );
        gs.gsapply::<void::Void, void::Void>(&msg.unwrap(), &None, &mut None, &None);

        assert!(gs
            .combater_at_location(MapLocation { x: 0, y: 0 })
            .is_none());
        assert!(gs
            .combater_at_location(MapLocation { x: 1, y: -1 })
            .is_some());
    }
}
