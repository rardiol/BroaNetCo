/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use super::texture::*;
use super::*;
use crate::{GodGSApply, MapConcealment, MapDetection, MapPathConcealment, Tick};

use std::collections::BTreeMap;
use std::iter;

use derive_more::{Display, From, Into};
use hex2d::{Coordinate, Direction, Position};
use toodee::TooDeeOps;

pub type MapLocation = Coordinate<i32>;
pub type ComLocation = Position<i32>;

pub const MAP_ORIGIN: MapLocation = MapLocation { x: 0, y: 0 };

#[derive(
    PartialOrd, Ord, PartialEq, Eq, Debug, Clone, Hash, Serialize, Deserialize, new, Display,
)]
pub struct MapElementEDataIntID(String);
#[derive(
    PartialOrd, Ord, PartialEq, Eq, Debug, Clone, Hash, Serialize, Deserialize, new, Display,
)]
pub struct MapElementEDataRatioID(String);

pub type MapElementEDataInt = i32;

#[derive(Debug, Clone, Hash, Serialize, Deserialize, Default)]
pub struct MapElementDataStore(
    pub BTreeMap<MapElementEDataIntID, MapElementEDataInt>,
    pub BTreeMap<MapElementEDataRatioID, Rational32>,
);

impl EDataID for MapElementEDataIntID {
    type Store = MapElementDataStore;
    type Value = MapElementEDataInt;
    fn get_store(store: &Self::Store) -> &BTreeMap<Self, Self::Value> {
        &store.0
    }
    fn get_store_mut(store: &mut Self::Store) -> &mut BTreeMap<Self, Self::Value> {
        &mut store.0
    }
}

impl EDataID for MapElementEDataRatioID {
    type Store = MapElementDataStore;
    type Value = Chances;
    fn get_store(store: &Self::Store) -> &BTreeMap<Self, Self::Value> {
        &store.1
    }
    fn get_store_mut(store: &mut Self::Store) -> &mut BTreeMap<Self, Self::Value> {
        &mut store.1
    }
}

impl std::iter::Extend<(MapElementEDataIntID, MapElementEDataInt)> for MapElementDataStore {
    fn extend<T>(&mut self, iter: T)
    where
        T: IntoIterator<Item = (MapElementEDataIntID, MapElementEDataInt)>,
    {
        self.0.extend(iter)
    }
}

impl std::iter::Extend<(MapElementEDataRatioID, Chances)> for MapElementDataStore {
    fn extend<T>(&mut self, iter: T)
    where
        T: IntoIterator<Item = (MapElementEDataRatioID, Chances)>,
    {
        self.1.extend(iter)
    }
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize, Derivative, Builder, Into, From)]
#[builder(default)]
#[derivative(Default(bound = ""))]
pub struct MapElement<NTD> {
    pub name: MessageI18N,
    pub ticks: Option<Tick>,
    pub protection_inside: Option<Chances>,
    pub protection_behind: Option<Chances>,
    pub detection: Vec<MapDetection>,
    pub concealment: Vec<MapConcealment>,
    pub path_concealment: Vec<MapPathConcealment>,
    #[builder(default = "DEFAULT_CONCEALMENTS.to_vec()")]
    #[derivative(Default(value = "DEFAULT_CONCEALMENTS.to_vec()"))]
    pub self_concealment: Vec<Concealment>,
    #[builder(setter(each = "texture_data_extend"))]
    pub texture_data: MapNTDGroup<NTD>,
    #[builder(default)]
    #[builder(setter(each = "stats_insert"))]
    pub store: MapElementDataStore,
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize, Derivative, Into, From)]
#[derivative(Default(bound = ""))]
pub struct Map<NTD> {
    offset_row: usize,
    offset_col: usize,
    grid: TooDeeMap<MapElement<NTD>>,
}

/*
impl<NTD> Default for Map<NTD> {
    fn default() -> Self {
        Self(Default::default())
    }
}
*/

impl<NTD> MapElement<NTD> {
    pub fn is_passable(&self) -> bool {
        self.ticks.is_some()
    }
}

impl<NTD: NTDTrait> Map<NTD> {
    /// Create a map with element in a range around MAP_ORIGIN
    pub fn new_range(size: i32, element: &MapElement<NTD>) -> Self {
        let mut map = Self::default();
        for location in MAP_ORIGIN.range_iter(size) {
            map.insert(location, element.clone());
        }
        map
    }
    pub fn get(&self, coord: MapLocation) -> Option<&MapElement<NTD>> {
        self.grid.map_get(self.maplocation_to_toodee(coord)?)
    }
    pub fn contains_coord(&self, coord: MapLocation) -> bool {
        if let Some(coord) = self.maplocation_to_toodee(coord) {
            self.grid.map_contains_key(coord)
        } else {
            false
        }
    }
    fn maplocation_to_toodee(&self, coord: MapLocation) -> Option<toodee::Coordinate> {
        let row = coord.y + self.offset_row as i32;
        let col = coord.x + self.offset_col as i32;
        Some((col.try_into().ok()?, row.try_into().ok()?))
    }
    /// Adjusts the map until the coord is valid
    fn maplocation_to_toodee_mut(&mut self, coord: MapLocation) -> toodee::Coordinate {
        trace!("maplocation_to_toodee_mut {:?}", coord);
        let row = loop {
            let row = coord.y + self.offset_row as i32;
            match row.try_into() {
                Ok(row) => break row,
                Err(_err) => {
                    trace!("maplocation_to_toodee_mut orow {:?}", self.offset_row);
                    self.offset_row += 1;
                    self.grid.insert_row(0, vec![None; self.grid.num_cols()]);
                }
            }
        };
        let col = loop {
            let col = coord.x + self.offset_col as i32;
            match col.try_into() {
                Ok(col) => break col,
                Err(_err) => {
                    trace!("maplocation_to_toodee_mut ocol {:?}", self.offset_col);
                    self.offset_col += 1;
                    self.grid.insert_col(0, vec![None; self.grid.num_rows()]);
                }
            }
        };
        trace!("maplocation_to_toodee_mut res {:?}", (col, row));
        (col, row)
    }
    fn toodee_to_maplocation(&self, (col, row): toodee::Coordinate) -> MapLocation {
        MapLocation {
            x: col as i32 - self.offset_col as i32,
            y: row as i32 - self.offset_row as i32,
        }
    }
    fn toodee_enumerate_to_maplocation(&self, i: usize) -> MapLocation {
        self.toodee_to_maplocation((i % self.grid.num_cols(), i / self.grid.num_cols()))
        //self.data.get_unchecked(coord.1 * self.num_cols + coord.0)
    }
    /* TODO
    pub fn coords(&self) -> btree_map::Keys<MapLocation, MapElement<NTD>> {
        self.grid.keys()
    }
     */
    pub fn iter(&self) -> impl Iterator<Item = (MapLocation, &MapElement<NTD>)> {
        (&self.grid)
            .into_iter()
            .enumerate()
            .filter_map(|(i, el)| Some((i, (*el).as_ref()?)))
            .map(move |(i, el)| (self.toodee_enumerate_to_maplocation(i), el))
    }
    /*
    pub fn iter_mut(&mut self) -> impl Iterator<Item = (MapLocation, &mut MapElement<NTD>)> + '_ {
        (&mut self.grid)
            .into_iter()
            .enumerate()
            .filter_map(|(i, el)| Some((i, (*el).as_mut()?)))
            .map(move |(i, el)| (self.toodee_enumerate_to_maplocation(i), el))
    }
    */
    pub fn iter_mut_elements(&mut self) -> impl Iterator<Item = &mut MapElement<NTD>> {
        self.grid.map_values_mut()
    }
    pub fn insert(&mut self, location: MapLocation, element: MapElement<NTD>) {
        trace!("insert {:?}", location);
        let location = self.maplocation_to_toodee_mut(location);
        self.grid.map_insert(location, element)
    }
    pub fn set_default_mapstat<LEDataID: EDataID<Store = MapElementDataStore>>(
        &mut self,
        mapstat: LEDataID,
        default: LEDataID::Value,
    ) {
        for el in self.iter_mut_elements() {
            mapstat
                .clone()
                .entry(&mut el.store)
                .or_insert_with(|| default.clone());
        }
    }
    pub fn debug(&self) {
        println!(
            "offset_row: {}, offset_col: {}",
            self.offset_row, self.offset_col
        );
        for row in self.grid.rows() {
            println!(
                "{:?}",
                row.iter()
                    .map(|x| x.as_ref().map_or("", |xx| &xx.name))
                    .collect::<Vec<_>>()
            );
        }
    }
}

impl<NTD: NTDTrait> std::ops::Index<MapLocation> for Map<NTD> {
    type Output = MapElement<NTD>;

    fn index(&self, coord: MapLocation) -> &Self::Output {
        self.get(coord)
            .unwrap_or_else(|| panic!("coord {:?} not in map", coord))
    }
}

/* TODO
#[derive(Debug, Clone)]
pub struct IntoIter<Element>(toodee::IntoIterTooDee<Option<Element>>, usize, usize);

impl<Element> iter::Iterator for IntoIter<Element> {
    type Item = (MapLocation, Element);
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.0.next() {
                Some(Some(el)) => break Some((coord, el)),
                Some(None) => continue,
                None => break None,
            }
        }
    }
}

impl<NTD: NTDTrait> IntoIterator for Map<NTD> {
    type Item = <Self::IntoIter as iter::Iterator>::Item;
    type IntoIter = IntoIter<MapElement<NTD>>;

    fn into_iter(self) -> Self::IntoIter {
        IntoIter(self.grid.into_iter(), 0, self.grid.cols())
    }
}
 */

impl<NTD: NTDTrait> From<Map<NTD>> for Vec<GodGSApply<NTD>> {
    fn from(map: Map<NTD>) -> Vec<GodGSApply<NTD>> {
        map.iter()
            .map(|(location, element)| GodGSApply::SetMapElement {
                location,
                element: element.to_owned(),
            })
            .collect()
    }
}

impl<NTD: NTDTrait> iter::FromIterator<(MapLocation, MapElement<NTD>)> for Map<NTD> {
    fn from_iter<I: IntoIterator<Item = (MapLocation, MapElement<NTD>)>>(iter: I) -> Self {
        let mut map = Self::default();
        for (coord, el) in iter {
            map.insert(coord, el);
        }
        map
    }
}

pub mod util {
    use crate::map::*;

    use hex2d::Integer;

    // TODO: merge into hex2d?
    #[allow(dead_code)]
    pub fn dislocate<T: Integer>(pos: Position<T>, dir: Direction) -> Position<T> {
        Position {
            coord: pos.coord + dir,
            dir: pos.dir,
        }
    }
    #[allow(dead_code)]
    pub fn rotadd<T: Integer>(pos: Position<T>, dir: Direction) -> Position<T> {
        Position {
            coord: pos.coord + dir,
            dir,
        }
    }
}

#[test]
mod test {
    use super::*;

    fn insertname(map: &mut Map<NamedTextureDataTransfer>, coord: MapLocation) {
        map.insert(
            coord,
            MapElementBuilder::default()
                .name(format!("{:?}", coord))
                .build()
                .unwrap(),
        );
    }

    fn checkname(map: &Map<NamedTextureDataTransfer>, coord: MapLocation) -> bool {
        if let Some(el) = map.get(coord) {
            el.name == format!("{:?}", coord)
        } else {
            false
        }
    }

    fn checkname2(map: &Map<NamedTextureDataTransfer>, coord: MapLocation) -> bool {
        map[coord].name == format!("{:?}", coord)
    }

    fn insertcheckname(map: &mut Map<NamedTextureDataTransfer>, coord: MapLocation) -> bool {
        insertname(map, coord);
        checkname2(map, coord)
    }

    #[test]
    fn test() {
        let mut map: Map<NamedTextureDataTransfer> = Default::default();

        assert!(insertcheckname(&mut map, MapLocation { x: 0, y: 0 }));
        assert!(insertcheckname(&mut map, MapLocation { x: 5, y: 5 }));
        assert!(insertcheckname(&mut map, MapLocation { x: 0, y: 2 }));
        assert!(insertcheckname(&mut map, MapLocation { x: 1, y: -3 }));
        assert!(insertcheckname(&mut map, MapLocation { x: -4, y: -4 }));
        assert!(insertcheckname(&mut map, MapLocation { x: 1, y: 3 }));

        assert!(checkname2(&mut map, MapLocation { x: 0, y: 0 }));
        assert!(checkname2(&mut map, MapLocation { x: 5, y: 5 }));
        assert!(checkname2(&mut map, MapLocation { x: 0, y: 2 }));
        assert!(checkname2(&mut map, MapLocation { x: 1, y: -3 }));
        assert!(checkname2(&mut map, MapLocation { x: -4, y: -4 }));
        assert!(checkname2(&mut map, MapLocation { x: 1, y: 3 }));

        let v = map
            .iter()
            .map(|(coord, el)| (format!("{:?}", coord), el.name.to_owned()))
            .collect::<Vec<_>>();

        assert_eq!(v.len(), 6);

        for (a, b) in v {
            assert_eq!(a, b);
        }
    }
}
