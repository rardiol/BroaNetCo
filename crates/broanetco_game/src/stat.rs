/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use num::rational;
use std::{fmt, ops};

#[derive(Copy, Clone, Debug, Default, Hash, PartialEq, Eq, Serialize, Deserialize)]
/// A clamped number with max and min
pub struct Stat<T> {
    value: T,
    max: T,
    // TODO: allow Optional min/max for overkilling and overheal
    min: T,
}

impl<T: PartialOrd + ops::Sub<Output = T> + ops::Add<Output = T> + From<i32> + Copy> Stat<T> {
    pub fn value(&self) -> T {
        self.value
    }
    pub fn max(&self) -> T {
        self.max
    }
    #[allow(dead_code)]
    pub fn min(&self) -> T {
        self.min
    }
    /// Create new at value/value
    pub fn new_max(value: T) -> Self {
        Self::new(value, value)
    }
    /// Create new at value/max
    pub fn new(value: T, max: T) -> Self {
        Self::new_min(value, max, T::from(0))
    }
    /// Create new at min/value/max
    pub fn new_min(value: T, max: T, min: T) -> Self {
        assert!(value >= min);
        assert!(max >= value);
        Self { value, max, min }
    }
    /// Returns the effected and the remainder effect
    /// (Excess, Effective Change)
    pub fn diff(&mut self, diff: T) -> (T, T) {
        let init = self.value;
        self.value = num::clamp(self.value + diff, self.min, self.max);

        (
            diff + init - self.value, // excess
            self.value - init,        // effective change
        )
    }
}

impl<T: Copy + num::ToPrimitive + num::Integer + num::bigint::ToBigInt> Stat<T> {
    pub fn ratio(&self) -> rational::Ratio<T> {
        rational::Ratio::new(self.value, self.max)
    }
}

impl<T: fmt::Display + num::Zero> fmt::Display for Stat<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.min.is_zero() {
            write!(f, "({}/{})", self.value, self.max)
        } else {
            write!(f, "({}/{}/{})", self.min, self.value, self.max)
        }
    }
}

impl<T: ops::Add<Output = T>> ops::Add for Stat<T> {
    type Output = Self;

    fn add(self, other: Self) -> Self::Output {
        Self {
            value: self.value + other.value,
            max: self.max + other.max,
            min: self.min + other.min,
        }
    }
}

impl<T: ops::AddAssign + Copy> ops::AddAssign for Stat<T> {
    fn add_assign(&mut self, other: Self) {
        self.value += other.value;
        self.max += other.max;
        self.min += other.min;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new() {
        assert_eq!(Stat::new_max(5), Stat::new(5, 5));
        assert_eq!(Stat::new(5, 6), Stat::new_min(5, 6, 0));
    }

    #[test]
    fn ratio() {
        use num::ToPrimitive;
        let ratio: f32 = Stat::new(4, 10).ratio().to_f32().unwrap();
        assert!((0.4f32 - ratio).abs() < 0.0001);
    }

    #[test]
    fn display() {
        assert_eq!(format!("{}", Stat::new(4, 10)), "(4/10)".to_owned());
        assert_eq!(
            format!("{}", Stat::new_min(4, 10, 2)),
            "(2/4/10)".to_owned()
        );
    }

    #[test]
    fn add() {
        let stat1 = Stat::new(2, 10);
        let stat2 = Stat::new(4, 5);
        let stat3 = Stat::new_min(1, 1, 1);
        assert_eq!(Stat::new_min(7, 16, 1), stat1 + stat2 + stat3);
    }

    #[test]
    fn addassign() {
        let mut stat1 = Stat::new(2, 10);
        let stat2 = Stat::new(4, 5);
        stat1 += stat2;
        assert_eq!(Stat::new(6, 15), stat1);
    }

    #[test]
    fn diff() {
        let mut stat1 = Stat::new_min(1, 10, -1);

        assert_eq!((1, 10, -1), (stat1.value(), stat1.max(), stat1.min()));

        assert_eq!((0, -2), stat1.diff(-2));
        assert_eq!(-1, stat1.value());

        assert_eq!((-3, 0), stat1.diff(-3));
        assert_eq!(-1, stat1.value());

        assert_eq!((0, 4), stat1.diff(4));
        assert_eq!(3, stat1.value());

        assert_eq!((-1, -4), stat1.diff(-5));
        assert_eq!(-1, stat1.value());

        assert_eq!((1, 11), stat1.diff(12));
        assert_eq!(10, stat1.value());

        assert_eq!((10, 10, -1), (stat1.value(), stat1.max(), stat1.min()));
    }
}
