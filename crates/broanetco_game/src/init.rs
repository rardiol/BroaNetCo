/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use super::*;
use crate::replay::{ReplayLog, ReplayLoggerMessage};

pub type Seed = [u8; 16];

// TODO: rethink. Enum, Option, ...?
/// All data needed to initialize a GS with a specific map, factions and so on
#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum GSInit<NTD = NamedTextureDataTransfer> {
    // TODO:    GSGGSA(Option<GS<NTD>>, Vec<GodGSApply<NTD>>),
    GGSA(Vec<GodGSApply<NTD>>),
    ReplayLog(Box<ReplayLog<NTD>>),
}

impl<NTD: NTDTrait> GSInit<NTD> {
    pub fn push_ggsa(&mut self, ggsa: GodGSApply<NTD>) {
        match self {
            Self::GGSA(vec) => vec.push(ggsa),
            Self::ReplayLog(log) => (*log).1.push(ReplayLoggerMessage::GodGSApply(ggsa)),
        }
    }
}

impl<NTD: NTDTrait> From<GSInit<NTD>> for GS<NTD> {
    /// Creates a GS from `GSInit` data
    fn from(gsinit: GSInit<NTD>) -> Self {
        use GSInit::*;
        match gsinit {
            GGSA(god_gsapply) => {
                let mut gs: GS<NTD> = Default::default();
                for god_gsapply in god_gsapply {
                    gs.god_gsapply::<void::Void, void::Void>(god_gsapply, &None, &mut None, &None);
                }
                gs
            }
            ReplayLog(log) => {
                let (gsinit, vec) = *log;
                let mut gs: GS<NTD> = gsinit.into();
                gs.update::<Void, Void>(&mut None, None, None);
                for apply in vec {
                    match apply {
                        ReplayLoggerMessage::GSApply(gsa) => {
                            gs.gsapply::<void::Void, void::Void>(&gsa, &None, &mut None, &None)
                        }
                        ReplayLoggerMessage::GodGSApply(ggsa) => {
                            gs.god_gsapply::<void::Void, void::Void>(ggsa, &None, &mut None, &None)
                        }
                    }
                    gs.update::<Void, Void>(&mut None, None, None);
                }
                gs
            }
        }
    }
}

impl<NTD: NTDTrait> From<ReplayLog<NTD>> for GSInit<NTD> {
    fn from(log: ReplayLog<NTD>) -> Self {
        Self::ReplayLog(Box::new(log))
    }
}

impl<NTD: NTDTrait> From<Vec<GodGSApply<NTD>>> for GSInit<NTD> {
    fn from(vec: Vec<GodGSApply<NTD>>) -> Self {
        Self::GGSA(vec)
    }
}
