/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::marker::Sized;

use hex2d::Direction;

use super::*;
use crate::effect::Effect;

impl<NTD: NTDTrait> GSActionTrait for GS<NTD> {}

impl<'g, NTD: NTDTrait> GSActionTrait for GSPov<'g, NTD> {}

impl<'g, NTD: NTDTrait> GSActionTrait for GSComPov<'g, NTD> {}

pub trait GSActionTrait: GSViewTrait {
    // TODO: use proper selection of teffg
    fn valid_targeting(
        &self,
        source: CombaterID,
        action_id: ActionID,
        position: usize,
        target: ActionTarget,
    ) -> Result<(), ActionTargetingCombaterError>
    where
        Self: std::marker::Sized,
    {
        trace!(
            "valid_targeting: {:?} pos {:?} from {:?} to {:?}",
            action_id,
            position,
            source,
            target
        );
        let source_combater = self.com(source);

        let targeting_info = &source_combater
            .actions(self, source)
            .find(|(taction_id, _)| *taction_id == action_id)
            .expect("Action not found")
            .1;
        trace!("{:?}", targeting_info);

        match (
            &targeting_info
                .target_effects
                .get(position)
                .expect("Action with no such target"),
            target,
        ) {
            (ActionTargetEffect::Combater(a, _effects), ActionTarget::Combater(target)) => {
                for (test, err) in a {
                    if !test.eval(self, (source, target)) {
                        return Err(*err);
                    }
                }
            }
            (ActionTargetEffect::Terrain(a, _effects), ActionTarget::Terrain(target)) => {
                for (test, err) in a {
                    if !test.eval(self, (source, target)) {
                        // TODO: no wrapping
                        return Err(ActionTargetingCombaterError::Location(*err));
                    }
                }
            }
            (_, _) => panic!("valid_targeting: invalid type of target"),
        }
        Ok(())
    }

    fn combater_action(
        &self,
        source: CombaterID,
        action_id: ActionID,
        targets: Vec<ActionTarget>,
        // TODO: more specific errors
    ) -> Result<GSApply, Option<ActionTargetingCombaterError>>
    where
        Self: Sized,
    {
        let source_combater = self.com(source);
        assert!(source_combater.ingame);

        debug!(
            "combater_action: t: {:?}, s: [{:?}] {:?}",
            targets, source, source_combater
        );

        let action = &source_combater
            .actions(self, source)
            .find(|(taction_id, _)| *taction_id == action_id)
            .expect("Action not found")
            .1;

        // Not enough targets
        if action.target_effects.len() != targets.len() {
            return Err(None);
        }

        for (i, target) in targets.clone().into_iter().enumerate() {
            match target {
                ActionTarget::Combater(target) => {
                    self.com(target);
                }
                ActionTarget::Terrain(target) => {
                    assert!(self.get_map().contains_coord(target));
                }
            }
            if let Err(err) = self.valid_targeting(source, action_id, i, target) {
                debug!("combater_action: fail due to {:?}", err);
                return Err(Some(err));
            }
        }

        Ok(GSApply::Action {
            source,
            action_id,
            targets,
        })
    }

    // TODO: result
    fn move_combater_direction(&self, source: CombaterID, direction: Direction) -> Option<GSApply>
    where
        Self: Sized,
    {
        let mover_combater = &self.com(source);

        assert!(mover_combater.ingame);

        let start_location = mover_combater.location;
        let new_location = start_location.coord + direction;
        if !self.get_map().contains_coord(new_location) {
            return None;
        }

        // TODO: find the action in a better way
        let keycode = 'Q';
        let (action_id, _action_name) = mover_combater
            .actions(self, source)
            .find(|(_, x)| x.name.starts_with(keycode))?;
        self.combater_action(source, action_id, vec![ActionTarget::Terrain(new_location)])
            .ok()
    }

    fn end_faction_turn(&self) -> GSApply {
        // TODO: games where skipping is not allowed
        GSApply::EndFactionTurn
    }

    fn choice(&self, choice: usize) -> Option<GSApply> {
        if let PlayingID::Choice { ref choices, .. } = self.get_playing() {
            if choices.get(choice).is_some() {
                Some(GSApply::Choice(choice))
            } else {
                None
            }
        } else {
            warn!("called choice outside of Playing::Choice");
            None
        }
    }
}

impl<NTD: NTDTrait> GS<NTD> {
    /// Returns None if the action is not valid in the real GS, but valid from the
    /// PoV of the faction that did it
    pub(super) fn do_combater_action(
        &mut self,
        source: CombaterID,
        action_id: ActionID,
        targets: Vec<ActionTarget>,
        sound_control: &Option<SoundControl>,
    ) -> CombatLogEntry {
        let action = self
            .com(source)
            .actions(self, source)
            .find(|(taction_id, _)| *taction_id == action_id)
            .unwrap()
            .1
            .clone();
        assert!(self
            .combater_action(source, action_id, targets.clone())
            .is_ok());

        // TODO: check if its their turn

        // TODO: Check from the PoV of the sender and do something else
        // For example, trying to teleport somewhere that is occupied by
        // and invisible unit
        if let Err(err) = self.combater_action(source, action_id, targets.clone()) {
            debug!(
                "do_combater_action valid_targeting(god) invalid due to {:?}",
                err
            );
            // return Err?
            todo!()
        }

        // TODO: no sound for miss
        if let Some(sound_control) = sound_control {
            for effect::Sound::Play(sound_id) in action.sound_effects {
                sound_control.send(sound_id).unwrap();
            }
        }

        let mut effect_log = EffectLog::new();
        let mut targets_log = Vec::new();
        for seff in action.source_effects {
            // TODO: this doesn't make sense, source may always default to combater
            effect_log += seff.apply(self, source);
        }
        for (i, atefg) in action.target_effects.iter().enumerate() {
            let target = targets[i];
            targets_log.push(target);
            match atefg {
                ActionTargetEffect::Combater(_targeting, teffg) => {
                    if let ActionTarget::Combater(target) = target {
                        for teff in teffg {
                            effect_log += teff.apply(self, (source, target));
                        }
                    } else {
                        panic!("action target doesn't matches action");
                    }
                }
                ActionTargetEffect::Terrain(_targeting, teffg) => {
                    if let ActionTarget::Terrain(target) = target {
                        for teff in teffg {
                            effect_log += teff.apply(self, (source, target));
                        }
                    } else {
                        panic!("action target doesn't matches action");
                    }
                }
            }
        }

        // TODO: better log, show:
        // Show this on the UI, not just log?
        CombatLogEntry::Action {
            source,
            action_name: action.name,
            targets: targets_log,
            effect_log,
        }
    }

    pub(super) fn do_skip_tick(&mut self, mover: CombaterID, amount: Tick) -> CombatLogEntry {
        let mover_combater = self.com_m(mover);
        mover_combater.tick += amount;
        CombatLogEntry::SkipTick(mover, amount, CombatLogEntryPriority::Middle)
    }

    pub(super) fn do_end_faction_turn(&mut self) -> CombatLogEntry {
        self.end_faction_turn();
        debug! {"Ending faction {} turn", self.playing}

        match self.playing {
            PlayingID::Faction(_facid) | PlayingID::FactionAuto(_facid) => {
                let vo = self.play_order();
                let comid_playing = vo[0].0;
                let new_tick = vo[1].1.tick + 1;
                let com_playing = self.com_m(comid_playing);
                com_playing.tick = new_tick;
                CombatLogEntry::EndFactionTurn(comid_playing, CombatLogEntryPriority::Middle)
            }
            _ => panic!("ended non playing turn"),
        }
    }

    pub(super) fn do_choice(&mut self, choice: usize) -> CombatLogEntry {
        if let PlayingID::Choice {
            choices,
            faction,
            text,
        } = self.playing.clone()
        {
            debug!("do_choice: {:?} {:?}", choice, choices);
            let choice = choices.get(choice).expect("invalid choice");
            choice.1.apply(self, ());
            self.playing = PlayingID::Interrupt;
            CombatLogEntry::Choice {
                choice: choice.0.clone(),
                faction,
                text,
                priority: CombatLogEntryPriority::High,
            }
        } else {
            panic!("called do_choice when no choice offered")
        }
    }
}
