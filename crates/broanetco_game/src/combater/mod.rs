/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fmt::Debug;
use std::hash::Hash;

use self::map::ComLocation;
use self::stat::Stat;
use super::*;
use crate::act_vec::*;

use derive_more::{Display, From, Into};
use either::{Either, Left, Right};

use condition::ConditionTrait;

pub type ActionID = usize;
pub type StatusID = MessageI18N;
pub type FullSubStatusID = (StatusID, SubStatusPath);
// TODO: A better solution?
pub type SubStatusPath = Vec<SubStatusID>;
pub type SubStatusID = Either<SubStatusActID, SubStatusCondID>;
pub type SubStatusActID = usize;
pub type SubStatusCondID = usize;
pub type FullPartID = (CombaterID, PartID);

pub type CombaterEDataStatType = i32;
pub type CombaterEDataStat = Stat<CombaterEDataStatType>;
#[derive(
    PartialOrd, Ord, PartialEq, Eq, Debug, Clone, Hash, Serialize, Deserialize, new, Display,
)]
pub struct CombaterEDataStatID(String);

#[derive(Debug, Clone, Hash, Default, Serialize, Deserialize)]
pub struct CombaterEDataStore(pub BTreeMap<CombaterEDataStatID, CombaterEDataStat>);

impl EDataID for CombaterEDataStatID {
    type Store = CombaterEDataStore;
    type Value = CombaterEDataStat;
    fn get_store(store: &Self::Store) -> &BTreeMap<Self, Self::Value> {
        &store.0
    }
    fn get_store_mut(store: &mut Self::Store) -> &mut BTreeMap<Self, Self::Value> {
        &mut store.0
    }
}

impl std::iter::Extend<(CombaterEDataStatID, CombaterEDataStat)> for CombaterEDataStore {
    fn extend<T>(&mut self, iter: T)
    where
        T: IntoIterator<Item = (CombaterEDataStatID, CombaterEDataStat)>,
    {
        self.0.extend(iter)
    }
}

/// Things related to Part
mod part;

pub use self::part::*;

#[derive(Debug, Clone, Hash, Serialize, Deserialize, Builder, Into, From)]
/// An ingame unit
pub struct Combater<NTD> {
    pub name: MessageI18N,
    #[builder(default)]
    pub statuses: Vec<Status<NTD>>,
    pub parts: PartGroup,
    pub location: ComLocation,
    #[builder(default)]
    /// When the unit will be allowed to act next
    pub tick: i32,
    pub faction: FactionID,
    #[builder(default = "true")]
    pub ingame: bool,
    pub texture_data: NTDGroup<NTD>,
    // TODO: rethink this
    #[builder(default)]
    pub command: Command,
    #[builder(default)]
    #[builder(setter(each = "edata_insert"))]
    pub edata: CombaterEDataStore,
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize, Derivative)]
#[derivative(Default)]
/// Type of control the unit is under
pub enum Command {
    /// Always under direct control of faction player
    #[derivative(Default)]
    Direct,
    /// Use the specified AI
    AI(AI),
    Condition {
        condition: condition::Condition<condition::Combater>,
        on_success: (Vec<effect::Combater>, Option<Box<Command>>),
        on_fail: (Vec<effect::Combater>, Option<Box<Command>>),
    },
    /// Tries to do attack_action to target, if not possible go in its direction
    SingleTarget {
        target: CombaterID,
        attack_action: ActionID,
        on_loss_of_target: CommandSingleTargetOnLossOfTarget,
    },
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize)]
/// What can happen when a Command::SingleTarget loses its target
pub enum CommandSingleTargetOnLossOfTarget {
    ChangeCommand(Box<Command>),
    Die,
}

pub type ConditionalSubStatusVec<NTD> =
    ActVecGen<Status<NTD>, condition::ConditionDet<condition::Combater>>;

#[derive(Clone, Copy, Debug, Hash, Serialize, Deserialize, Into, From)]
pub struct Pair<A, B>(A, B);

impl<GST: GSViewTrait> Condition<Pair<&GST, CombaterID>>
    for condition::ConditionDet<condition::Combater>
{
    fn is_active(&self, Pair(gs, comid): Pair<&GST, CombaterID>) -> bool {
        self.eval(gs, comid)
    }
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize, Derivative, Into, From)]
#[derivative(Default(bound = ""))]
pub struct Status<NTD> {
    pub name: MessageI18N,
    pub substatuses: ActVec<Status<NTD>>,
    pub conditional_substatuses: ConditionalSubStatusVec<NTD>,
    pub actions: Vec<Action>,
    pub detection: Vec<Detection>,
    pub concealment: Vec<Concealment>,
    // TODO: make this derived from parts?
    pub texture_data: NTDGroup<NTD>,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy, Serialize, Deserialize)]
/// Reasons a test against an ActionTargeting may fail
pub enum ActionTargetingCombaterError {
    Location(ActionTargetingLocationError),
    Friendly,
    Hostile,
    Oneself,
    NoSolid,
    NoTarget,
}

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy, Serialize, Deserialize)]
/// Reasons a test against an ActionTargeting may fail
pub enum ActionTargetingLocationError {
    MinRange,
    MaxRange,
    Occupied,
    Impassable,
}

impl<NTD: NTDTrait> Combater<NTD> {
    pub fn active_statuses<'a, GST: GSViewTrait<NTD = NTD>>(
        &'a self,
        gs: &'a GST,
        comid: CombaterID,
    ) -> impl Iterator<Item = &'a Status<NTD>> + 'a {
        self.statuses
            .iter()
            .flat_map(move |x| x.active_statuses(gs, comid))
    }
    pub fn actions<'a, GST: GSActionTrait<NTD = NTD>>(
        &'a self,
        gs: &'a GST,
        comid: CombaterID,
    ) -> impl Iterator<Item = (ActionID, &'a Action)> + 'a {
        self.statuses
            .iter()
            .flat_map(move |x| x.all_actions(gs, comid))
            .enumerate()
    }
    pub fn hp(&self) -> Stat<HpPts> {
        let cur = self
            .parts_live()
            .filter_map(|(_pid, p)| p.hp)
            .map(|x| x.value())
            .sum();
        let max = self
            .parts()
            .filter_map(|(_pid, p)| p.hp)
            .map(|x| x.max())
            .sum();
        Stat::new(cur, max)
    }
    pub fn hp_diff(&mut self, d: HpPts) -> HpPts {
        debug!("hp_diff {}", d);
        let mut remaining_diff = d;
        for (_partid, part) in self.parts_mut() {
            // Skips if part has no hp
            if let Some(hp) = &mut part.hp {
                let (excess, done) = hp.diff(remaining_diff);
                remaining_diff -= done;
                trace!(
                    "hp_diff diff round: excess: {}, done: {}, remaining: {}",
                    excess,
                    done,
                    remaining_diff
                );
                if remaining_diff == 0 {
                    break;
                }
            }
        }
        debug!("hp_diff remaining_diff: {}", remaining_diff);
        remaining_diff
    }
    pub fn hp_diff_single_target(&mut self, d: HpPts, partid: PartID) -> HpPts {
        debug!("hp_diff_single_target {} {}", d, partid);
        if let Some(ref mut hp) = self.parts.part_mut(partid).hp {
            hp.diff(d).0
        } else {
            panic!("called hp_diff_single_targe with a hp-less part")
        }
    }
}

#[derive(Debug, Copy, Clone, Hash, Serialize, Deserialize)]
pub enum ActionTarget {
    Combater(CombaterID),
    Terrain(MapLocation),
}

impl ActionTarget {
    pub fn location<NTD: NTDTrait, GST: GSBaseTrait>(&self, gs: &GST) -> MapLocation {
        match self {
            Self::Combater(comid) => gs.com(*comid).location.coord,
            Self::Terrain(loc) => *loc,
        }
    }
}

pub type ActionTargetingCombater = Vec<(
    condition::ConditionDet<condition::CombaterCombater>,
    ActionTargetingCombaterError,
)>;
pub type ActionTargetingLocation = Vec<(
    condition::ConditionDet<condition::CombaterLocation>,
    ActionTargetingLocationError,
)>;

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum ActionTargetEffect {
    Combater(ActionTargetingCombater, Vec<effect::CombaterCombater>),
    Terrain(ActionTargetingLocation, Vec<effect::CombaterLocation>),
}

#[derive(Clone, Debug, Default, Hash, Serialize, Deserialize, Builder)]
pub struct Action {
    pub name: MessageI18N,
    pub keycode: Option<char>,
    #[builder(default)]
    pub sound_effects: Vec<effect::Sound>,
    #[builder(default)]
    pub source_effects: Vec<effect::Combater>,
    #[builder(default)]
    pub target_effects: Vec<ActionTargetEffect>,
}

impl<NTD: NTDTrait> Status<NTD> {
    // Recursive so it has to be dyn
    pub fn active_statuses<'a, GST: GSViewTrait<NTD = NTD>>(
        &'a self,
        gs: &'a GST,
        comid: CombaterID,
    ) -> Box<dyn Iterator<Item = &'a Status<NTD>> + 'a> {
        Box::new(
            std::iter::once(self)
                .chain(
                    self.substatuses
                        .actives(())
                        .flat_map(move |x| x.active_statuses(gs, comid)),
                )
                .chain(
                    self.conditional_substatuses
                        .actives((gs, comid).into())
                        .flat_map(move |x| x.active_statuses(gs, comid)),
                ),
        )
    }

    pub fn full_name<GST: GSViewTrait<NTD = NTD>>(&self, gs: &GST, comid: CombaterID) -> String {
        self.active_statuses(gs, comid)
            .map(|x| x.name.clone())
            .collect()
    }

    pub fn all_actions<'a, GST: GSActionTrait<NTD = NTD>>(
        &'a self,
        gs: &'a GST,
        comid: CombaterID,
    ) -> Box<dyn Iterator<Item = &'a Action> + 'a> {
        Box::new(
            self.active_statuses(gs, comid)
                .flat_map(|x| x.actions.iter()),
        )
    }

    pub fn substatus_mut(&mut self, path: &[SubStatusID]) -> &mut Status<NTD> {
        let mut target = self;
        for path in path {
            target = match path {
                Left(ssactid) => target
                    .substatuses
                    .nocheck_get_mut(*ssactid)
                    .expect("invalid SubStatusPathA"),
                Right(sscondid) => target
                    .conditional_substatuses
                    .nocheck_get_mut(*sscondid)
                    .expect("invalid SubStatusPathC"),
            }
        }
        target
    }
}
