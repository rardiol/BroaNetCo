/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use super::*;
use std::convert::TryInto;
use std::{iter, slice};

pub type PartID = usize;
pub type PartSize = Mil;

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum PartGroup {
    /// Parts and spacing between them
    Group(Vec<Part>, PartSize),
}

#[derive(Clone, Debug, Default, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub struct Part {
    pub name: MessageI18N,
    pub hp: Option<Stat<HpPts>>,
    pub size: Option<PartSize>,
}

impl<NTD: Debug> Combater<NTD> {
    pub fn parts(&self) -> iter::Enumerate<slice::Iter<'_, Part>> {
        self.parts.parts()
    }
    pub fn parts_mut(&mut self) -> iter::Enumerate<slice::IterMut<'_, Part>> {
        self.parts.parts_mut()
    }
    pub fn parts_live(&self) -> impl DoubleEndedIterator<Item = (PartID, &Part)> + '_ {
        self.parts().filter(|(_, x)| x.is_alive())
    }
    pub fn remove_part(&mut self, partid: PartID) -> Part {
        let PartGroup::Group(vec, _psize) = &mut self.parts;
        vec.remove(partid)
    }
    pub fn insert_part(&mut self, part: Part) {
        let PartGroup::Group(vec, _psize) = &mut self.parts;
        vec.push(part)
    }

    // TODO: faster alternatives to distribute?
}

impl PartGroup {
    #[allow(dead_code)]
    pub fn part(&self, id: PartID) -> &Part {
        let PartGroup::Group(ref vec, _) = self;
        &vec[id]
    }

    pub fn part_mut(&mut self, id: PartID) -> &mut Part {
        let PartGroup::Group(ref mut vec, _) = self;
        &mut vec[id]
    }

    pub fn parts(&self) -> iter::Enumerate<slice::Iter<'_, Part>> {
        let PartGroup::Group(vec, _) = self;
        vec.iter().enumerate()
    }

    pub fn parts_mut(&mut self) -> iter::Enumerate<slice::IterMut<'_, Part>> {
        let PartGroup::Group(ref mut vec, _) = self;
        vec.iter_mut().enumerate()
    }

    /// Array containing which part is that position, total size of target, and total size of actual targets
    pub fn distribute(&self) -> (Box<[Option<PartID>; 1000]>, PartSize, PartSize) {
        let PartGroup::Group(_, spacing) = self;
        let spacing: usize = (*spacing).try_into().unwrap();
        let mut array = [None; 1000];
        let mut pos: usize = 0;
        let mut total_target = 0;
        for (partid, part) in self.parts().filter(|(_partid, part)| part.is_alive()) {
            if let Some(partsize) = part.size {
                total_target += partsize;
                for _ in 0..partsize {
                    array[pos] = Some(partid);
                    pos += 1;
                }
                pos += spacing;
            } else {
                continue;
            }
        }
        if pos > 0 {
            pos -= spacing;
        }
        (Box::new(array), pos.try_into().unwrap(), total_target)
    }
}

impl Part {
    pub fn is_alive(&self) -> bool {
        self.hp.map_or(true, |x| x.value().is_positive())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn distribute0() {
        let correct = [None; 1000];

        let partgroup = PartGroup::Group(
            vec![Part {
                size: None,
                ..Default::default()
            }],
            2,
        );

        let (a, b, c) = partgroup.distribute();
        assert_eq!((*a, b, c), (correct, 0, 0));
    }

    #[test]
    fn distribute1() {
        let mut correct = [None; 1000];
        correct[0] = Some(0);
        correct[1] = Some(0);

        let partgroup = PartGroup::Group(
            vec![Part {
                size: Some(2),
                ..Default::default()
            }],
            4,
        );

        let (a, b, c) = partgroup.distribute();
        assert_eq!((*a, b, c), (correct, 2, 2));
    }

    #[test]
    fn distribute2() {
        let mut correct = [None; 1000];
        correct[0] = Some(1);
        correct[1] = Some(1);
        correct[5] = Some(4);
        correct[9] = Some(5);
        correct[10] = Some(5);
        correct[11] = Some(5);
        correct[12] = Some(5);

        let partgroup = PartGroup::Group(
            vec![
                Part {
                    size: None,
                    ..Default::default()
                },
                Part {
                    size: Some(2),
                    ..Default::default()
                },
                Part {
                    size: None,
                    ..Default::default()
                },
                Part {
                    size: Some(5),
                    hp: Some(Stat::new(0, 5)),
                    ..Default::default()
                },
                Part {
                    size: Some(1),
                    ..Default::default()
                },
                Part {
                    size: Some(4),
                    ..Default::default()
                },
            ],
            3,
        );

        let (a, b, c) = partgroup.distribute();
        assert_eq!((*a, b, c), (correct, 13, 7));
    }
}
