/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use rand::prelude::*;

use super::*;
use crate::bncl::number::RngTrait as NumberRngTrait;
use std::collections::BTreeSet;

use AIGameMaster::*;

/// Priority and identity number for AIGameMaster
/// Lower runs first
/// Needs to be unique
pub type AIGMPriority = i32;

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum AIGameMaster<NTD> {
    ////////////////////////
    // Win conditions
    /// Ends the game when only one faction left
    LastManStanding { valid_for: BTreeSet<FactionID> },
    /// Ends the game if a faction keeps a combater on a position for ticks uninterrupted
    KingOfTheHill {
        valid_for: BTreeSet<FactionID>,
        position: MapLocation,
        ticks: Tick,
        startcount: Option<(FactionID, Tick)>,
    },

    ////////////////////////
    // Spawning map elements and combaters
    /// Spawns combater at next_tick, then next_tick+interval, and so on
    /// Picks randomly from positions, filtering out positions that are occupied or cannot be walked on
    Spawn {
        positions: Vec<MapLocation>,
        combater: CombaterID,
        next_tick: Tick,
        interval: Tick,
    },
    /// If a combater is within range of a empty map position, add element there
    ExpandMapRangeCombater {
        range: Distance,
        element: MapElement<NTD>,
    },
    /// Spreads a MapElement
    MapSpreader {
        next_tick: Tick,
        interval: Tick,
        /// MapElement name from which the element spreads
        spreader_name: String,
        range: Distance,
        new_element: MapElement<NTD>,
        /// If true, put the old map element's textures under the new
        overtextures: bool,
        odds: number::Calc<number::Location, Chances>,
    },
    /// Set playing to Choice at tick
    Choice {
        tick: Tick,
        faction: FactionID,
        text: String,
        choices: Vec<(String, effect::GS)>,
    },
    CombaterEffectOnTerrain {
        map_element_name: String,
        effect: effect::Combater,
    },

    /// Reveals MapElements that are in view to Factions
    RevealMap { range: Distance },
}

impl<NTD: NTDTrait> GS<NTD> {
    /// Returns true when nothing has been done and phase can end
    pub fn run_ai_game_master<DRAWCACHE: DrawCacheTrait>(
        &mut self,
        id: AIGMPriority,
        limit_tick: Tick,
        drawcache: &mut Option<&mut DRAWCACHE>,
        sound_control: &Option<SoundControl>,
    ) -> bool {
        // TODO: game logging
        let ai_game_master = self.ai_game_masters.remove(&id).unwrap();
        trace!(
            "run_ai_game_master: {:?} {:?} {:?}",
            id,
            limit_tick,
            ai_game_master
        );
        let mut rng = self.rng.clone();
        let (optaigm, god_gsapply) = ai_game_master.run(self, limit_tick, &mut rng);
        self.rng = rng;
        if let Some(aigm) = optaigm {
            self.ai_game_masters.insert(id, aigm);
        }
        let mut ret = true;
        for god_gsapply in god_gsapply {
            // TODO: replay_logger?
            // TODO: cases in which the AIGM mutates itself but returns []?
            ret = false;
            self.god_gsapply::<_, void::Void>(god_gsapply, sound_control, drawcache, &None);
        }
        ret
    }
}

impl<NTD: NTDTrait> AIGameMaster<NTD> {
    pub fn run<RNG: Rng>(
        mut self,
        gs: &GS<NTD>,
        limit_tick: Tick,
        rng: &mut RNG,
    ) -> (Option<AIGameMaster<NTD>>, Vec<GodGSApply<NTD>>) {
        match &mut self {
            LastManStanding { valid_for } => {
                if !gs.playing.is_interrupt() {
                    return (Some(self), Vec::new());
                }
                let alive_factions = gs.alive_factions().map(|x| x.0).collect::<Vec<_>>();

                if let Some(potential_winner) = alive_factions.get(0) {
                    if alive_factions.get(1).is_some() {
                        debug!("LastManStanding: multiple factions alive");
                        (Some(self), Vec::new())
                    } else if !valid_for.contains(potential_winner) {
                        debug!("LastManStanding: potential_winner cannot win by LastManStanding");
                        (Some(self), Vec::new())
                    } else {
                        info!("LastManStanding: winner: {:?}", potential_winner);
                        (
                            Some(self),
                            vec![GodGSApply::SetPlayingID(PlayingID::Winner(
                                *potential_winner,
                            ))],
                        )
                    }
                } else {
                    // TODO
                    warn!("LastManStanding: no one left");
                    (Some(self), Vec::new())
                }
            }
            KingOfTheHill {
                valid_for,
                position,
                ticks,
                startcount,
            } => {
                if !gs.playing.is_interrupt() {
                    return (Some(self), Vec::new());
                }

                let tick = gs.tick();

                if let Some((_comid, com)) = gs.combater_at_location(*position) {
                    let com_faction = com.faction;
                    if valid_for.contains(&com_faction) {
                        if let Some((faction, start_tick)) = startcount {
                            if com_faction == *faction {
                                if tick >= *start_tick + *ticks {
                                    // Victory
                                    (
                                        Some(self),
                                        vec![GodGSApply::SetPlayingID(PlayingID::Winner(
                                            com_faction,
                                        ))],
                                    )
                                } else {
                                    // Counting towards victory
                                    (Some(self), Vec::new())
                                }
                            } else {
                                // Victory point changed owners
                                *startcount = Some((com_faction, tick));
                                (Some(self), Vec::new())
                            }
                        } else {
                            // Someone just got on the victory point
                            *startcount = Some((com_faction, tick));
                            (Some(self), Vec::new())
                        }
                    } else {
                        // Faction controlling victory point cannot win this way
                        *startcount = None;
                        (Some(self), Vec::new())
                    }
                } else {
                    // Nobody on the victory point
                    *startcount = None;
                    (Some(self), Vec::new())
                }
            }
            Spawn {
                positions,
                combater,
                next_tick,
                interval,
            } => {
                if *next_tick > limit_tick {
                    return (Some(self), Vec::new());
                }

                let mut ret = vec![GodGSApply::SetTick(*next_tick)];

                let valid_positions = positions
                    .iter()
                    .filter(|coord| gs.combater_at_location(**coord).is_none())
                    .filter(|coord| gs.map[**coord].ticks.is_some())
                    .collect::<Vec<_>>();
                if let Some(new_coord) = valid_positions.choose(rng) {
                    *next_tick += *interval;
                    ret.push(GodGSApply::GSEffect(effect::GS::Location(
                        effect::Location::CloneAt(*combater, Default::default()),
                        **new_coord,
                    )));
                } else {
                    warn!("Can't spawn combater");
                }
                (Some(self), ret)
            }
            ExpandMapRangeCombater { range, element } => {
                let vec = gs
                    .combaters()
                    .map(|(_comid, com)| com.location.coord)
                    .map(|com_coord| com_coord.range_iter(*range))
                    .flatten()
                    .filter(|map_coord| !gs.map.contains_coord(*map_coord))
                    .inspect(|map_coord| {
                        debug!(
                            "ExpandMapRangeCombater: Adding map element at {:?}",
                            map_coord
                        )
                    })
                    .map(|map_coord| GodGSApply::SetMapElement {
                        location: map_coord,
                        element: element.clone(),
                    })
                    .collect();
                (Some(self), vec)
            }
            MapSpreader {
                interval,
                next_tick,
                spreader_name,
                odds,
                overtextures,
                new_element,
                range,
            } => {
                if *next_tick > limit_tick {
                    return (Some(self), Vec::new());
                }
                *next_tick += *interval;

                let mut ret = vec![GodGSApply::SetTick(*next_tick)];
                // If a element is in range of a spreader multiple times, odds are checked
                // multiple times
                ret.extend(
                    gs.map
                        .iter()
                        .filter(|(_loc, el)| el.name == *spreader_name)
                        .map(|(loc, _el)| loc.range_iter(*range))
                        .flatten()
                        .filter(|location| gs.map.contains_coord(*location))
                        .filter(|loc| gs.map[*loc].name != *spreader_name)
                        .filter(|loc| ninmil_rng(odds.value_rng(gs, rng, *loc), rng))
                        .map(|location| GodGSApply::SetMapElement {
                            location,
                            element: MapElement {
                                texture_data: if *overtextures {
                                    let mut ret = gs.map[location].texture_data.clone();
                                    ret.extend(new_element.texture_data.clone());
                                    ret
                                } else {
                                    new_element.texture_data.clone()
                                },
                                ..new_element.clone()
                            },
                        })
                        .inspect(|ggsa| debug!("MapSpreader: {:?}", ggsa)),
                );
                (Some(self), ret)
            }
            Choice {
                tick,
                faction,
                text,
                choices,
            } => {
                if *tick > limit_tick {
                    return (Some(self), Vec::new());
                }
                if !gs.playing.is_interrupt() {
                    return (Some(self), Vec::new());
                }

                let vec = vec![GodGSApply::SetPlayingID(PlayingID::Choice {
                    faction: *faction,
                    text: text.clone(),
                    choices: choices.clone(),
                })];
                (None, vec)
            }
            CombaterEffectOnTerrain {
                map_element_name,
                effect,
            } => {
                let ret = gs
                    .combaters()
                    .filter(|(_comid, com)| gs.map[com.location.coord].name == *map_element_name)
                    .map(|(comid, _com)| {
                        GodGSApply::GSEffect(effect::GS::Combater(effect.clone(), comid))
                    })
                    .collect();
                (Some(self), ret)
            }
            RevealMap { range } => {
                let range = *range;
                let changes = gs
                    .factions()
                    .inspect(|(facid, _fac)| {
                        trace!("RevealMap for {:?}", facid);
                    })
                    .filter(|(_facid, fac)| fac.map.is_some())
                    .flat_map(|(facid, fac)| {
                        gs.combaters()
                            .filter(move |(_comid, com)| com.faction == facid)
                            .flat_map(move |(comid, com)| {
                                com_can_see_maps(
                                    gs,
                                    comid,
                                    com.location
                                        .coord
                                        .range_iter(range)
                                        .filter(move |coord| gs.map.contains_coord(*coord))
                                        .inspect(|coord| {
                                            trace!("RevealMap range in {:?}", coord);
                                        })
                                        .filter(move |coord| {
                                            fac.map.as_ref().unwrap().get(*coord).map(|el| &el.name)
                                                != Some(&gs.map[*coord].name)
                                        }) // TODO: check if they are different, for when the map changes after being revealed
                                        .inspect(|coord| {
                                            trace!("RevealMap not in sight in {:?}", coord);
                                        }),
                                )
                            })
                            .inspect(|coord| {
                                trace!("RevealMap effect in {:?}", coord);
                            })
                            .map(move |coord| {
                                GodGSApply::GSEffect(effect::GS::Faction(
                                    effect::Faction::RevealMap(coord),
                                    facid,
                                ))
                            })
                    })
                    .collect::<Vec<_>>();
                log!(
                    if changes.is_empty() {
                        ::log::Level::Trace
                    } else {
                        ::log::Level::Debug
                    },
                    "RevealMap: reveal {:?}",
                    changes
                );
                (Some(self), changes)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_lms() {
        use super::*;
        use crate::tests;
        use PlayingID::*;

        let mut gs = tests::test_gs();

        let (comid1, comid2, comid3) = (CombaterID(0), CombaterID(1), CombaterID(2));
        let (_facid1, _facid2, facid3) = (FactionID(0), FactionID(1), FactionID(2));

        assert!(gs.playing.is_interrupt());
        assert!(gs.run_ai_game_master::<void::Void>(0, 0, &mut None, &None));

        gs.com_m(comid1).ingame = false;
        assert!(gs.run_ai_game_master::<void::Void>(0, 0, &mut None, &None));
        assert!(gs.playing.is_interrupt());

        gs.com_m(comid2).ingame = false;
        assert!(!gs.run_ai_game_master::<void::Void>(0, 0, &mut None, &None));
        if let Winner(facid) = gs.playing {
            if facid != facid3 {
                panic!("wrong winner: {:?}", facid);
            }
        } else {
            panic!("no winner")
        }

        assert!(gs.run_ai_game_master::<void::Void>(0, 0, &mut None, &None));

        gs.com_m(comid3).ingame = false;
        assert!(gs.run_ai_game_master::<void::Void>(0, 0, &mut None, &None));
        if let Winner(facid) = gs.playing {
            if facid != facid3 {
                panic!("wrong winner: {:?}", facid);
            }
        } else {
            panic!("no winner")
        }

        gs.playing = Interrupt;

        assert!(gs.run_ai_game_master::<void::Void>(0, 0, &mut None, &None));
        assert!(gs.playing.is_interrupt());
    }

    #[test]
    // TODO: more thorough testing
    fn test_spawn() {
        use super::*;
        use crate::tests;

        let mut gs = tests::test_gs();

        let (comid1, _comid2, _comid3) = (CombaterID(0), CombaterID(1), CombaterID(2));
        let (_facid1, _facid2, _facid3) = (FactionID(0), FactionID(1), FactionID(2));

        let aigms_priority = 1;

        gs.god_gsapply::<void::Void, void::Void>(
            GodGSApply::AddAIGameMaster(
                1,
                AIGameMaster::Spawn {
                    positions: vec![MapLocation { x: -1, y: -1 }],
                    combater: comid1,
                    next_tick: 10,
                    interval: 30,
                },
            ),
            &None,
            &mut None,
            &None,
        );

        assert_eq!(gs.tick(), 0);
        assert!(gs
            .combater_at_location(MapLocation { x: -1, y: -1 })
            .is_none());

        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority, 5, &mut None, &None));
        assert_eq!(gs.tick(), 0);
        assert!(gs
            .combater_at_location(MapLocation { x: -1, y: -1 })
            .is_none());

        assert!(!gs.run_ai_game_master::<void::Void>(aigms_priority, 15, &mut None, &None));
        assert_eq!(gs.tick(), 10);
        assert!(gs
            .combater_at_location(MapLocation { x: -1, y: -1 })
            .is_some());

        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority, 20, &mut None, &None));
        gs.com_m(
            gs.combater_at_location(MapLocation { x: -1, y: -1 })
                .unwrap()
                .0,
        )
        .ingame = false;

        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority, 25, &mut None, &None));
        assert_eq!(gs.tick(), 10);
        assert!(gs
            .combater_at_location(MapLocation { x: -1, y: -1 })
            .is_none());

        assert!(!gs.run_ai_game_master::<void::Void>(aigms_priority, 50, &mut None, &None));
        assert!(gs
            .combater_at_location(MapLocation { x: -1, y: -1 })
            .is_some());

        assert_eq!(gs.tick(), 40);
    }

    #[test]
    fn test_emrc() {
        use super::*;
        use crate::tests;

        let mut gs = tests::test_gs();

        let (comid1, comid2, _comid3) = (CombaterID(0), CombaterID(1), CombaterID(2));
        let (_facid1, _facid2, _facid3) = (FactionID(0), FactionID(1), FactionID(2));

        let aigms_priority_1 = 13;
        let aigms_priority_2 = 14;

        let mapelementname_1 = "testemcr1".to_owned();
        let mapelementname_2 = "testemcr2".to_owned();

        let mapelement1 = MapElement {
            name: mapelementname_1.clone(),
            ..Default::default()
        };

        let mapelement2 = MapElement {
            name: mapelementname_2.clone(),
            ..Default::default()
        };

        gs.god_gsapply::<void::Void, void::Void>(
            GodGSApply::AddAIGameMaster(
                aigms_priority_1,
                AIGameMaster::ExpandMapRangeCombater {
                    range: 0,
                    element: mapelement1,
                },
            ),
            &None,
            &mut None,
            &None,
        );
        gs.god_gsapply::<void::Void, void::Void>(
            GodGSApply::AddAIGameMaster(
                aigms_priority_2,
                AIGameMaster::ExpandMapRangeCombater {
                    range: 1,
                    element: mapelement2,
                },
            ),
            &None,
            &mut None,
            &None,
        );

        let target1 = MapLocation { x: -5, y: 6 };
        let target1_d1 = target1 + MapLocation { x: 1, y: 0 };
        let target2 = MapLocation { x: 10, y: 2 };
        let target2_d1 = target2 + MapLocation { x: 1, y: 0 };
        let target3 = MapLocation { x: -53, y: 0 };
        assert!(!gs.map.contains_coord(target1));
        assert!(!gs.map.contains_coord(target2));
        assert!(!gs.map.contains_coord(target1_d1));
        assert!(!gs.map.contains_coord(target2_d1));
        assert!(!gs.map.contains_coord(target3));
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_1, 50, &mut None, &None));
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_2, 50, &mut None, &None));
        assert!(!gs.map.contains_coord(target1));
        assert!(!gs.map.contains_coord(target2));
        assert!(!gs.map.contains_coord(target1_d1));
        assert!(!gs.map.contains_coord(target2_d1));
        assert!(!gs.map.contains_coord(target3));

        gs.com_m(comid1).location.coord = target1;
        assert!(!gs.run_ai_game_master::<void::Void>(aigms_priority_1, 50, &mut None, &None));
        assert_eq!(gs.map[target1].name, mapelementname_1,);
        assert!(!gs.map.contains_coord(target2));
        assert!(!gs.map.contains_coord(target1_d1));
        assert!(!gs.map.contains_coord(target2_d1));
        assert!(!gs.map.contains_coord(target3));
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_1, 50, &mut None, &None));
        assert_eq!(gs.map[target1].name, mapelementname_1,);
        assert!(!gs.map.contains_coord(target2));
        assert!(!gs.map.contains_coord(target1_d1));
        assert!(!gs.map.contains_coord(target2_d1));
        assert!(!gs.map.contains_coord(target3));

        assert!(!gs.run_ai_game_master::<void::Void>(aigms_priority_2, 50, &mut None, &None));
        assert_eq!(gs.map[target1].name, mapelementname_1,);
        assert!(!gs.map.contains_coord(target2));
        assert_eq!(gs.map[target1_d1].name, mapelementname_2,);
        assert!(!gs.map.contains_coord(target2_d1));
        assert!(!gs.map.contains_coord(target3));
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_1, 50, &mut None, &None));
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_2, 50, &mut None, &None));

        gs.com_m(comid2).location.coord = target2;
        assert!(!gs.run_ai_game_master::<void::Void>(aigms_priority_2, 50, &mut None, &None));
        assert_eq!(gs.map[target1].name, mapelementname_1,);
        assert_eq!(gs.map[target2].name, mapelementname_2,);
        assert_eq!(gs.map[target1_d1].name, mapelementname_2,);
        assert_eq!(gs.map[target2_d1].name, mapelementname_2,);
        assert!(!gs.map.contains_coord(target3));
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_1, 50, &mut None, &None));
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_2, 50, &mut None, &None));
        assert_eq!(gs.map[target1].name, mapelementname_1,);
        assert_eq!(gs.map[target2].name, mapelementname_2,);
        assert_eq!(gs.map[target1_d1].name, mapelementname_2,);
        assert_eq!(gs.map[target2_d1].name, mapelementname_2,);
        assert!(!gs.map.contains_coord(target3));

        gs.com_m(comid2).location.coord = target3;
        assert!(!gs.run_ai_game_master::<void::Void>(aigms_priority_1, 50, &mut None, &None));
        assert_eq!(gs.map[target3].name, mapelementname_1,);
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_1, 50, &mut None, &None));
    }

    #[test]
    #[should_panic]
    fn test_choice_panic1() {
        test_choice_base(true, false);
    }

    #[test]
    #[should_panic]
    fn test_choice_panic2() {
        test_choice_base(false, true);
    }

    #[test]
    fn test_choice() {
        test_choice_base(false, false);
    }

    fn test_choice_base(mischoice_panic: bool, overuse_panic: bool) {
        use super::*;
        use crate::tests;

        let mut gs = tests::test_gs();

        let (_comid1, _comid2, _comid3) = (CombaterID(0), CombaterID(1), CombaterID(2));
        let (facid1, _facid2, _facid3) = (FactionID(0), FactionID(1), FactionID(2));

        let aigms_priority_1 = 10;

        gs.god_gsapply::<void::Void, void::Void>(
            GodGSApply::AddAIGameMaster(
                aigms_priority_1,
                AIGameMaster::Choice {
                    tick: 40,
                    faction: facid1,
                    text: "what".to_owned(),
                    choices: vec![
                        (
                            "pickleft".to_owned(),
                            effect::GS::LogText("left".to_owned(), CombatLogEntryPriority::Middle),
                        ),
                        (
                            "pickright".to_owned(),
                            effect::GS::LogText("right".to_owned(), CombatLogEntryPriority::Middle),
                        ),
                    ],
                },
            ),
            &None,
            &mut None,
            &None,
        );

        assert!(gs.playing.is_interrupt());
        assert!(gs.run_ai_game_master::<void::Void>(aigms_priority_1, 30, &mut None, &None));
        assert!(gs.playing.is_interrupt());

        assert!(!gs.run_ai_game_master::<void::Void>(aigms_priority_1, 50, &mut None, &None));
        if mischoice_panic {
            gs.gsapply::<void::Void, void::Void>(&GSApply::Choice(4), &None, &mut None, &mut None);
        } else {
            gs.gsapply::<void::Void, void::Void>(&GSApply::Choice(1), &None, &mut None, &mut None);
            assert!(gs.playing.is_interrupt());
            if let CombatLogEntry::Text(string, CombatLogEntryPriority::Middle) = &gs.log[0] {
                if string != "right" {
                    panic!("wrong choice: {:?}", string);
                }
            } else {
                panic!("no log")
            }

            if overuse_panic {
                assert!(gs.run_ai_game_master::<void::Void>(
                    aigms_priority_1,
                    50,
                    &mut None,
                    &None
                ));
            }
        }
    }
}
// TODO: test tick changes from AIGM running
