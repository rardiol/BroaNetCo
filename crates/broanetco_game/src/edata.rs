/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::cmp::Ord;
use std::collections::btree_map;
use std::collections::BTreeMap;
use std::fmt::Debug;
use std::marker::Sized;

pub trait EDataID: Debug + Clone + Sized + Ord {
    type Store;
    type Value: Debug + Clone;
    fn get_store(store: &Self::Store) -> &BTreeMap<Self, Self::Value>;
    fn get_store_mut(store: &mut Self::Store) -> &mut BTreeMap<Self, Self::Value>;

    fn get<'s>(&'s self, store: &'s Self::Store) -> Option<&'s Self::Value> {
        Self::get_store(store).get(self)
    }
    fn get_mut<'s>(&'s self, store: &'s mut Self::Store) -> Option<&'s mut Self::Value> {
        Self::get_store_mut(store).get_mut(self)
    }
    fn entry(self, store: &mut Self::Store) -> btree_map::Entry<Self, Self::Value> {
        Self::get_store_mut(store).entry(self)
    }
    fn insert(self, store: &mut Self::Store, value: Self::Value) -> Option<Self::Value> {
        Self::get_store_mut(store).insert(self, value)
    }
}
