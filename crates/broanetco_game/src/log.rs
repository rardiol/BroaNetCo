/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fmt::Debug;

use hex2d::Direction;

use super::*;

pub type CombatLogEntryID = usize;

// TODO: formatted log, not string(to show on map, movements and attacks)
#[derive(Debug, Clone, Hash, Serialize, Deserialize)]
pub enum CombatLogEntry {
    Text(TextI18N, CombatLogEntryPriority),
    /// TODO: use this again
    Move {
        source: CombaterID,
        direction: Direction,
        ticks: Tick,
    },
    Action {
        source: CombaterID,
        action_name: MessageI18N,
        targets: Vec<ActionTarget>,
        effect_log: EffectLog,
    },
    SkipTick(CombaterID, Tick, CombatLogEntryPriority),
    EndFactionTurn(CombaterID, CombatLogEntryPriority),
    Choice {
        choice: MessageI18N,
        faction: FactionID,
        text: MessageI18N,
        priority: CombatLogEntryPriority,
    },
}

impl CombatLogEntry {
    #[const_fn("1.46")]
    pub const fn priority(&self) -> CombatLogEntryPriority {
        match *self {
            Self::Text(_, clep)
            | Self::SkipTick(_, _, clep)
            | Self::EndFactionTurn(_, clep)
            | Self::Choice { priority: clep, .. } => clep,
            Self::Move { .. } => CombatLogEntryPriority::Low,
            Self::Action { .. } => CombatLogEntryPriority::Middle,
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Serialize, Deserialize)]
pub enum CombatLogEntryPriority {
    Low,
    Middle,
    High,
}

impl<NTD: NTDTrait> GS<NTD> {
    // TODO: should be checked for fac_can_see at beginning of action too,
    // for example a move out of visual range
    pub fn push_log(&mut self, el: CombatLogEntry) -> CombatLogEntryID {
        info!("Pushing to log: {:?}", el);
        let factions_can_see = self
            .factions()
            .filter(|(facid, _fac)| {
                use CombatLogEntry as CLE;
                match &el {
                    CLE::Text(_, _) | CLE::Choice { .. } => true,
                    CLE::Move { source, .. }
                    | CLE::Action { source, .. }
                    | CLE::SkipTick(source, _, _)
                    | CLE::EndFactionTurn(source, _)
                        if self.fac_can_see(*facid, *source) =>
                    {
                        true
                    }
                    // TODO: multitarget
                    // TODO: terrain target can see
                    CLE::Action { targets, .. } => {
                        if let Some(ActionTarget::Combater(tcomid)) = targets.get(0) {
                            self.fac_can_see(*facid, *tcomid)
                        } else {
                            false
                        }
                    }
                    CLE::Move { .. } | CLE::SkipTick(_, _, _) | CLE::EndFactionTurn(_, _) => false,
                }
            })
            .map(|(facid, _fac)| facid)
            .collect::<Vec<_>>();
        self.log.push(el);
        let id = self.log.len() - 1;
        for facid in factions_can_see {
            self.fac_m(facid).logged.insert(id);
        }
        id
    }
}

#[derive(
    Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Serialize, Deserialize, Derivative,
)]
#[derivative(Default(new = "true"))]
pub struct EffectLog {
    pub hp_diff: HpPts,
    pub ticks: Tick,
    pub hits: usize,
    pub misses: usize,
    pub text: Vec<TextI18N>,
}

impl EffectLog {
    pub fn hp_diff(mut self, amount: HpPts) -> Self {
        self.hp_diff += amount;
        self
    }
    pub fn ticks(mut self, amount: Tick) -> Self {
        self.ticks += amount;
        self
    }
    pub fn hit(mut self) -> Self {
        self.hits += 1;
        self
    }
    pub fn miss(mut self) -> Self {
        self.misses += 1;
        self
    }
    pub fn push_line(mut self, text: TextI18N) -> Self {
        self.text.push(text);
        self
    }
}

impl ops::AddAssign for EffectLog {
    fn add_assign(&mut self, other: Self) {
        self.hp_diff += other.hp_diff;
        self.ticks += other.ticks;
        self.misses += other.misses;
        self.hits += other.hits;
        self.text.extend(other.text);
    }
}
