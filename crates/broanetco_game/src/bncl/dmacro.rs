/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

#[macro_export]
macro_rules! bncnumber {
    ($val:literal) => {
        number::Calc::Value($val)
    };
    (add $a:expr, $b:expr) => {
        number::Calc::Operation(
            number::Operation::Add,
            Box::new(bncnumber!($a)),
            Box::new(bncnumber!($b)),
        )
    };
    (mul $a:expr, $b:expr) => {
        number::Calc::Operation(
            number::Operation::Mul,
            Box::new(bncnumber!($a)),
            Box::new(bncnumber!($b)),
        )
    };
    (sub $a:expr, $b:expr) => {
        number::Calc::Operation(
            number::Operation::Sub,
            Box::new(bncnumber!($a)),
            Box::new(bncnumber!($b)),
        )
    };
    (div $a:expr, $b:expr) => {
        number::Calc::Operation(
            number::Operation::Div,
            Box::new(bncnumber!($a)),
            Box::new(bncnumber!($b)),
        )
    };
    (dice $val:literal) => {
        number::Random::DiceRoll($val.to_owned()).into_calc()
    };
    ($def:expr) => {
        $def.into()
    };
}

#[macro_export]
macro_rules! bnccondition {
    (<=, $a:expr, $b:expr,) => {
        condition::Condition2::Numeric(condition::Numeric::LET, (bncnumber!($a)), (bncnumber!($b)))
            .into()
    };
    (<, $a:expr, $b:expr,) => {
        condition::Condition2::Numeric(condition::Numeric::LT, (bncnumber!($a)), (bncnumber!($b)))
            .into()
    };
    (>=, $a:expr, $b:expr,) => {
        condition::Condition2::Numeric(condition::Numeric::GET, (bncnumber!($a)), (bncnumber!($b)))
            .into()
    };
    (>, $a:expr, $b:expr,) => {
        condition::Condition2::Numeric(condition::Numeric::GT, (bncnumber!($a)), (bncnumber!($b)))
            .into()
    };
    (==, $a:expr, $b:expr,) => {
        condition::Condition2::Numeric(condition::Numeric::EQ, (bncnumber!($a)), (bncnumber!($b)))
            .into()
    };
    (!=, $a:expr, $b:expr,) => {
        condition::Condition2::Numeric(condition::Numeric::NEQ, (bncnumber!($a)), (bncnumber!($b)))
            .into()
    };
    (not, $a:expr) => {
        condition::Condition2::Not(Box::new(bnccondition!($a))).into()
    };
    (and, $a:expr, $b:expr,) => {
        condition::Condition2::Logical(
            condition::Logical::And,
            Box::new(bnccondition!($a)),
            Box::new(bnccondition!($b)),
        )
        .into()
    };
    (nand, $a:expr, $b:expr,) => {
        condition::Condition2::Logical(
            condition::Logical::Nand,
            Box::new(bnccondition!($a)),
            Box::new(bnccondition!($b)),
        )
        .into()
    };
    (or, $a:expr, $b:expr,) => {
        condition::Condition2::Logical(
            condition::Logical::Or,
            Box::new(bnccondition!($a)),
            Box::new(bnccondition!($b)),
        )
        .into()
    };
    (nor, $a:expr, $b:expr,) => {
        condition::Condition2::Logical(
            condition::Logical::Nor,
            Box::new(bnccondition!($a)),
            Box::new(bnccondition!($b)),
        )
        .into()
    };
    ($def:expr) => {
        $def.into()
    };
}
