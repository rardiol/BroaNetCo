/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use super::number::RngTrait as NumberRngTrait;
use super::number::Trait as NumberTrait;
use crate::map::*;
use crate::*;

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum CombaterCombater {
    Source(Combater),
    Target(Combater),
    CombaterLocation(CombaterLocation),

    IsHostile,
    SameFaction,
    /// Are source and target different?
    NotSame,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum CombaterLocation {
    Source(Combater),
    Target(Location),
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum GS {}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Location {
    Name(String),
    IsEmpty,
    IsPassable,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Part {
    Name(String),
    Live,
    /// Is live and has hp
    Attackable,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum PartPart {
    Source(Part),
    Target(Part),
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum PartCombater {
    Source(Part),
    Target(Combater),
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Combater {
    Location(Location),
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Faction {}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Numeric {
    /// LessEqualThan
    LET,
    /// LessThan
    LT,
    /// GreaterThan
    GT,
    /// GreaterEqualThan
    GET,
    /// Equal
    EQ,
    /// NotEqual
    NEQ,
}

impl Numeric {
    pub fn value<T: std::cmp::PartialOrd + std::cmp::PartialEq>(&self, a: T, b: T) -> bool {
        match self {
            Numeric::LET => a <= b,
            Numeric::LT => a < b,
            Numeric::GT => a > b,
            Numeric::GET => a >= b,
            Numeric::EQ => a == b,
            Numeric::NEQ => a != b,
        }
    }
}

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Logical {
    And,
    Or,
    Nand,
    Nor,
}

impl Logical {
    pub fn value(&self, a: bool, b: bool) -> bool {
        match self {
            Self::And => a && b,
            Self::Or => a || b,
            Self::Nand => !(a && b),
            Self::Nor => !(a || b),
        }
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Condition2<Data: ConditionTrait, RngData, Number>
where
    <Data as ConditionTrait>::Number:
        Debug + Clone + std::hash::Hash + std::cmp::Eq + serde::Serialize,
    for<'dee> <Data as bncl::condition::ConditionTrait>::Number: serde::Deserialize<'dee>,
{
    Value(bool),
    Data(Data),
    Chances(RngData),
    Not(Box<Condition2<Data, RngData, Number>>),
    Numeric(Numeric, Number, Number),
    Logical(
        Logical,
        Box<Condition2<Data, RngData, Number>>,
        Box<Condition2<Data, RngData, Number>>,
    ),
}

pub type ConditionDet<Data> =
    Condition2<Data, void::Void, number::Calc<<Data as ConditionTrait>::Number, i32, void::Void>>;
pub type Condition<Data> = Condition2<
    Data,
    number::Calc<<Data as ConditionTrait>::Number, Chances>,
    number::Calc<<Data as ConditionTrait>::Number, i32>,
>;

impl<Data: ConditionTrait, RngData, Number> From<Data> for Condition2<Data, RngData, Number>
where
    <Data as ConditionTrait>::Number:
        Debug + Clone + std::hash::Hash + std::cmp::Eq + serde::Serialize,
    for<'dee> <Data as ConditionTrait>::Number: serde::Deserialize<'dee>,
{
    fn from(o: Data) -> Self {
        Self::Data(o)
    }
}

impl<Data: ConditionTrait, RngData, Number> From<bool> for Condition2<Data, RngData, Number>
where
    <Data as ConditionTrait>::Number:
        Debug + Clone + std::hash::Hash + std::cmp::Eq + serde::Serialize,
    for<'dee> <Data as ConditionTrait>::Number: serde::Deserialize<'dee>,
{
    fn from(o: bool) -> Self {
        Self::Value(o)
    }
}

impl ConditionTrait for CombaterCombater {
    type Targets = (CombaterID, CombaterID);
    type Number = number::CombaterCombater;

    fn eval<GST: GSViewTrait>(&self, gs: &GST, (source, target): Self::Targets) -> bool {
        let source_com = gs.com(source);
        let target_com = gs.com(target);
        match self {
            Self::Source(a) => a.eval(gs, source),
            Self::Target(a) => a.eval(gs, target),
            Self::CombaterLocation(a) => a.eval(gs, (source, target_com.location.coord)),
            Self::SameFaction => source_com.faction == target_com.faction,
            Self::NotSame => source != target,
            Self::IsHostile => !gs.friendly_factions(source_com.faction, target_com.faction),
        }
    }
}

impl ConditionTrait for CombaterLocation {
    type Targets = (CombaterID, MapLocation);
    type Number = number::CombaterLocation;

    fn eval<GST: GSViewTrait>(&self, gs: &GST, (source, target): Self::Targets) -> bool {
        match self {
            Self::Source(a) => a.eval(gs, source),
            Self::Target(a) => a.eval(gs, target),
        }
    }
}

impl ConditionTrait for Combater {
    type Targets = CombaterID;
    type Number = number::Combater;

    fn eval<GST: GSViewTrait>(&self, gs: &GST, target: Self::Targets) -> bool {
        match self {
            Self::Location(c) => c.eval(gs, gs.com(target).location.coord),
        }
    }
}

impl ConditionTrait for Location {
    type Targets = MapLocation;
    type Number = number::Location;

    fn eval<GST: GSViewTrait>(&self, gs: &GST, target: Self::Targets) -> bool {
        let element = &gs.get_map()[target];
        match self {
            Self::Name(ref name) => element.name == *name,
            Self::IsEmpty => gs.combater_at_location(target).is_none(),
            Self::IsPassable => element.is_passable(),
        }
    }
}

impl ConditionTrait for Part {
    type Targets = FullPartID;
    type Number = number::Part;

    fn eval<GST: GSViewTrait>(&self, gs: &GST, (comid, partid): Self::Targets) -> bool {
        match self {
            Self::Name(ref name) => gs.com(comid).parts.part(partid).name == *name,
            Self::Live => gs.com(comid).parts.part(partid).is_alive(),
            Self::Attackable => {
                let part = gs.com(comid).parts.part(partid);
                part.is_alive() && part.hp.is_some()
            }
        }
    }
}

impl ConditionTrait for PartCombater {
    type Targets = (FullPartID, CombaterID);
    type Number = number::PartCombater;

    fn eval<GST: GSViewTrait>(
        &self,
        gs: &GST,
        ((s_comid, s_paid), t_comid): Self::Targets,
    ) -> bool {
        match self {
            Self::Source(ref x) => x.eval(gs, (s_comid, s_paid)),
            Self::Target(ref x) => x.eval(gs, t_comid),
        }
    }
}

impl ConditionTrait for PartPart {
    type Targets = (FullPartID, FullPartID);
    type Number = number::PartPart;

    fn eval<GST: GSViewTrait>(
        &self,
        gs: &GST,
        ((s_comid, s_paid), (t_comid, t_paid)): Self::Targets,
    ) -> bool {
        match self {
            Self::Source(ref x) => x.eval(gs, (s_comid, s_paid)),
            Self::Target(ref x) => x.eval(gs, (t_comid, t_paid)),
        }
    }
}

impl ConditionTrait for Faction {
    type Targets = FactionID;
    type Number = number::Faction;

    fn eval<GST: GSViewTrait>(&self, _gs: &GST, _target: Self::Targets) -> bool {
        unreachable!()
    }
}

impl ConditionTrait for GS {
    type Targets = ();
    type Number = number::Value<i32>; // TODO:??

    fn eval<GST: GSViewTrait>(&self, _gs: &GST, _target: Self::Targets) -> bool {
        unreachable!()
    }
}

impl<Data: ConditionTrait> ConditionTrait for ConditionDet<Data>
where
    // Number calc Target ==
    number::Calc<<Data as ConditionTrait>::Number, i32, void::Void>:
        number::Trait<i32, Targets = Data::Targets>,
    number::Calc<<Data as ConditionTrait>::Number, Chances, void::Void>:
        number::Trait<Chances, Targets = Data::Targets>,
    <Data as ConditionTrait>::Targets: Copy,
    // Data derives
    Data::Number: Debug + Clone + std::hash::Hash + std::cmp::Eq + serde::Serialize,
    for<'dee> <Data as ConditionTrait>::Number: serde::Deserialize<'dee>,
{
    type Targets = Data::Targets;
    type Number = number::Location;

    fn eval<GST: GSViewTrait>(&self, gs: &GST, targets: Self::Targets) -> bool {
        match self {
            Self::Value(val) => *val,
            Self::Data(data) => data.eval(gs, targets),
            Self::Chances(void) => void::unreachable(*void),
            Self::Not(ref cond) => !cond.eval(gs, targets),
            Self::Numeric(op, a, b) => {
                let a = a.value(gs, targets);
                let b = b.value(gs, targets);
                op.value(a, b)
            }
            Self::Logical(op, a, b) => {
                let a = a.eval(gs, targets);
                let b = b.eval(gs, targets);
                op.value(a, b)
            }
        }
    }
}

impl<Data: ConditionTrait> RandomConditionTrait for Condition<Data>
where
    // Number calc Target ==
    number::Calc<<Data as ConditionTrait>::Number, i32>:
        number::RngTrait<i32, Targets = Data::Targets>,
    number::Calc<<Data as ConditionTrait>::Number, Chances>:
        number::RngTrait<Chances, Targets = Data::Targets>,
    <Data as ConditionTrait>::Targets: Copy,
    // Data derives
    Data::Number: Debug + Clone + std::hash::Hash + std::cmp::Eq + serde::Serialize,
    for<'dee> <Data as ConditionTrait>::Number: serde::Deserialize<'dee>,
{
    type Targets = Data::Targets;
    type Number = Data::Number;

    fn eval_rng<RNG: Rng, GST: GSViewTrait>(
        &self,
        gs: &GST,
        rng: &mut RNG,
        targets: Self::Targets,
    ) -> bool {
        match self {
            Self::Value(val) => *val,
            Self::Data(data) => data.eval(gs, targets),
            Self::Not(ref cond) => !cond.eval_rng(gs, rng, targets),
            Self::Numeric(op, a, b) => {
                let a = a.value_rng(gs, rng, targets);
                let b = b.value_rng(gs, rng, targets);
                op.value(a, b)
            }
            Self::Logical(op, a, b) => {
                let a = a.eval_rng(gs, rng, targets);
                let b = b.eval_rng(gs, rng, targets);
                op.value(a, b)
            }
            Self::Chances(chances) => ninmil_rng(chances.value_rng(gs, rng, targets), rng),
        }
    }
}

pub trait ConditionTrait {
    type Targets;
    type Number;

    fn eval<GST: GSViewTrait>(&self, gs: &GST, targets: Self::Targets) -> bool;
}

pub trait RandomConditionTrait {
    type Targets;
    type Number;

    /// Calculates the value of the EffectNumber
    fn eval_rng<RNG: Rng, GST: GSViewTrait>(
        &self,
        gs: &GST,
        rng: &mut RNG,
        targets: Self::Targets,
    ) -> bool;

    /// Wraps .value using the GS's RNG
    fn eval_gs_rng<NTD: NTDTrait>(&self, gs: &mut crate::GS<NTD>, targets: Self::Targets) -> bool {
        let mut rng = gs.rng.clone();
        let ret = self.eval_rng(gs, &mut rng, targets);
        gs.rng = rng;
        ret
    }
}
