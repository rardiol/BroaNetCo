/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fmt::Debug;
use std::hash::Hash;
use std::ops;

use crate::map::*;
use crate::util::*;
use crate::*;

use num::traits::ToPrimitive;

pub trait MyNumTrait:
    num::Zero
    + Debug
    + Copy
    + PartialOrd
    + num::FromPrimitive
    + ops::Add<Output = Self>
    + ops::Sub<Output = Self>
    + ops::Mul<Output = Self>
    + ops::Div<Output = Self>
    + RatioConvertTrait<Chances>
{
}

impl MyNumTrait for usize {}
impl MyNumTrait for i32 {}
impl MyNumTrait for Rational32 {}

#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Operation {
    Add,
    Sub,
    Mul,
    Div,
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum CombaterCombater {
    CombaterLocation(CombaterLocation),
    Source(Combater),
    Target(Combater),
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum CombaterLocation {
    Source(Combater),
    Target(Location),

    Distance,
}

impl From<CombaterLocation> for CombaterCombater {
    fn from(i: CombaterLocation) -> Self {
        Self::CombaterLocation(i)
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Combater {
    Location(Location),

    /// Counts alive parts
    Part(),
    /// Counts parts matching condition
    PartCond(condition::ConditionDet<condition::Part>),
    HpRatio(),
    GetEDataStat(CombaterEDataStatID),
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Location {
    TickCost(),
    GetStatInt(MapElementEDataIntID),
    GetStatRatio(MapElementEDataRatioID),
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Part {}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum PartPart {
    Source(Part),
    Target(Part),
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum PartCombater {
    Source(Part),
    Target(Combater),
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Faction {}

impl From<Location> for Combater {
    fn from(i: Location) -> Self {
        Self::Location(i)
    }
}

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub struct Value<T>(pub T);

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Random {
    // TODO: use upstream Roller
    // - Needs extra derives
    // - A bit moot until Roller does preparsing
    // see: https://github.com/Geobert/caith/blob/master/src/lib.rs#L25
    // TODO: use something like format!
    /// Use caith dice roll format, with no repeated rolls
    /// see: https://github.com/Geobert/caith/blob/master/README.md
    DiceRoll(String),
}

pub type CalcDet<Data, T> = Calc<Data, T, Void>;

#[derive(Clone, Debug, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Calc<Data, T, DataRng = Random> {
    Value(T),
    Data(Data),
    DataRng(DataRng),
    Operation(
        Operation,
        Box<Calc<Data, T, DataRng>>,
        Box<Calc<Data, T, DataRng>>,
    ),
    WeightedAverage(Vec<(Calc<Data, T, DataRng>, Calc<Data, T, DataRng>)>),
    Tick(),
}

impl<T, Data: Trait<T>, DataRng> From<Data> for Calc<Data, T, DataRng> {
    fn from(data: Data) -> Self {
        Self::Data(data)
    }
}

impl<Data, DataRng> From<i32> for Calc<Data, i32, DataRng> {
    fn from(value: i32) -> Self {
        Self::Value(value)
    }
}

impl<Data, DataRng> From<usize> for Calc<Data, usize, DataRng> {
    fn from(value: usize) -> Self {
        Self::Value(value)
    }
}

impl<Data, DataRng> From<Chances> for Calc<Data, Chances, DataRng> {
    fn from(value: Chances) -> Self {
        Self::Value(value)
    }
}

impl Random {
    pub fn into_calc<T, Data>(self) -> Calc<Data, T, Random> {
        Calc::DataRng(self)
    }
}

pub trait Trait<T> {
    type Targets;
    /// Calculates the value of the EffectNumber
    fn value<GST: GSViewTrait>(&self, gs: &GST, targets: Self::Targets) -> T
    where
        Self::Targets: Copy;
}

pub trait RngTrait<T> {
    type Targets;
    /// Calculates the value of the EffectNumber
    fn value_rng<RNG: Rng, GST: GSViewTrait>(
        &self,
        gs: &GST,
        rng: &mut RNG,
        targets: Self::Targets,
    ) -> T
    where
        Self::Targets: Copy;

    /// Wraps .value using the GS's RNG
    fn value_gs_rng<NTD: NTDTrait>(&self, gs: &mut GS<NTD>, targets: Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        let mut rng = gs.rng.clone();
        let ret = self.value_rng(gs, &mut rng, targets);
        gs.rng = rng;
        ret
    }
}

impl Operation {
    pub fn calc<T1, T2, T3>(&self, x: T1, y: T2) -> T3
    where
        T1: ops::Add<T2, Output = T3>
            + ops::Sub<T2, Output = T3>
            + ops::Mul<T2, Output = T3>
            + ops::Div<T2, Output = T3>,
    {
        match self {
            Self::Add => x + y,
            Self::Sub => x - y,
            Self::Mul => x * y,
            Self::Div => x / y,
        }
    }
}

impl Random {
    fn value_rng<T: MyNumTrait, RNG: rand::Rng>(&self, rng: &mut RNG) -> T {
        let ret = match self {
            Self::DiceRoll(caith_str) => {
                // TODO: logging
                // TODO: EffectLog?
                T::from_i64(
                    caith::Roller::new(caith_str)
                        .expect("failed to parse as caith dice roll")
                        .roll_with(rng)
                        .expect("failed caith dice roll")
                        .as_single()
                        .expect("expected single result caith roll")
                        .get_total(),
                )
                .unwrap()
            }
        };
        trace!("Random.value = {:?}", ret);
        ret
    }
}

impl<T, Data: Trait<T>> Trait<T> for Calc<Data, T, Void>
where
    T: MyNumTrait,
{
    type Targets = Data::Targets;

    fn value<GST: GSViewTrait>(&self, gs: &GST, targets: Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        let ret = match self {
            Self::Value(n) => *n,
            Self::Data(data) => data.value(gs, targets),
            Self::DataRng(void) => void::unreachable(*void),
            Self::Operation(op, ref v1, ref v2) => {
                op.calc(v1.value(gs, targets), v2.value(gs, targets))
            }
            Self::Tick() => T::from_i32(gs.tick()).expect("Invalid number conversion"),
            Self::WeightedAverage(ref vec) => weighted_average(
                vec.iter()
                    .map(|(value, weight)| (value.value(gs, targets), weight.value(gs, targets))),
            ),
        };
        trace!("NumberCalc.value = {:?}", ret);
        ret
    }
}

impl<T: MyNumTrait> RngTrait<T> for Value<T> {
    type Targets = ();

    fn value_rng<RNG: Rng, GST: GSViewTrait>(
        &self,
        _gs: &GST,
        _rng: &mut RNG,
        _targets: Self::Targets,
    ) -> T
    where
        Self::Targets: Copy,
    {
        self.0
    }
}

impl<Data: Trait<T>, T> RngTrait<T> for Calc<Data, T, Random>
where
    T: MyNumTrait,
{
    type Targets = Data::Targets;

    fn value_rng<RNG: Rng, GST: GSViewTrait>(
        &self,
        gs: &GST,
        rng: &mut RNG,
        targets: Self::Targets,
    ) -> T
    where
        Self::Targets: Copy,
    {
        let ret = match self {
            Self::Value(n) => *n,
            Self::Data(data) => data.value(gs, targets),
            Self::DataRng(data) => data.value_rng(rng),
            Self::Operation(op, ref v1, ref v2) => op.calc(
                v1.value_rng(gs, rng, targets),
                v2.value_rng(gs, rng, targets),
            ),
            Self::Tick() => T::from_i32(gs.tick()).expect("Invalid number conversion"),
            Self::WeightedAverage(ref vec) => {
                weighted_average(vec.iter().map(|(value, weight)| {
                    (
                        value.value_rng(gs, rng, targets),
                        weight.value_rng(gs, rng, targets),
                    )
                }))
            }
        };
        trace!("NumberCalc.value = {:?}", ret);
        ret
    }
}

impl<T> Trait<T> for Value<T>
where
    T: MyNumTrait,
{
    type Targets = ();

    fn value<GST: GSViewTrait>(&self, _gs: &GST, _target: Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        self.0
    }
}

impl<T> Trait<T> for Location
where
    T: MyNumTrait,
{
    type Targets = MapLocation;

    fn value<GST: GSViewTrait>(&self, gs: &GST, target: Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        let ret = match *self {
            Self::TickCost() => T::from_i32(gs.get_map()[target].ticks.expect("Tick"))
                .expect("invalid number conversion"),
            Self::GetStatRatio(ref statid) => T::ratio_convert(
                *statid
                    .get(&gs.get_map()[target].store)
                    .expect("missing map element stat"),
            ),
            Self::GetStatInt(ref statid) => T::from_i32(
                *statid
                    .get(&gs.get_map()[target].store)
                    .expect("missing map element stat"),
            )
            .unwrap(),
        };
        trace!("LocationEffectNumber.value = {:?}", ret);
        ret
    }
}

impl<T> Trait<T> for Combater
where
    T: MyNumTrait,
{
    type Targets = CombaterID;

    fn value<GST: GSViewTrait>(&self, gs: &GST, target: Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        let ret = match *self {
            Self::Location(ref subnum) => subnum.value(gs, gs.com(target).location.coord),
            Self::Part() => T::from_usize(gs.com(target).parts_live().count()).unwrap(),
            Self::PartCond(ref cond) => T::from_usize(
                gs.com(target)
                    .parts()
                    .filter(|(partid, _part)| cond.eval(gs, (target, *partid)))
                    .count(),
            )
            .unwrap(),
            Self::HpRatio() => T::ratio_convert(gs.com(target).hp().ratio()),
            Self::GetEDataStat(ref statid) => T::from_i32(
                statid
                    .get(&gs.com(target).edata)
                    .expect("Missing stat")
                    .value(),
            )
            .unwrap(),
        };
        trace!("EffectNumber.value = {:?}", ret);
        ret
    }
}

impl<T> Trait<T> for CombaterCombater
where
    T: MyNumTrait,
{
    type Targets = (CombaterID, CombaterID);

    fn value<GST: GSViewTrait>(&self, gs: &GST, (source, target): Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        let ret = match *self {
            Self::Source(ref sten) => sten.value(gs, source),
            Self::Target(ref sten) => sten.value(gs, target),
            Self::CombaterLocation(ref sten) => {
                sten.value(gs, (source, gs.com(target).location.coord))
            }
        };
        trace!("SourceTargetEffectNumber.value = {:?}", ret);
        ret
    }
}

impl<T> Trait<T> for CombaterLocation
where
    T: MyNumTrait,
{
    type Targets = (CombaterID, MapLocation);

    fn value<GST: GSViewTrait>(&self, gs: &GST, (source, target): Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        let ret = match *self {
            Self::Source(ref en) => en.value(gs, source),
            Self::Target(ref en) => en.value(gs, target),
            Self::Distance => T::from_i32(gs.com(source).location.coord.distance(target)).unwrap(),
        };
        trace!("CombaterLocation.value = {:?}", ret);
        ret
    }
}

impl<T> Trait<T> for Faction
where
    T: MyNumTrait,
{
    type Targets = FactionID;

    fn value<GST: GSViewTrait>(&self, _gs: &GST, _target: Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        unreachable!()
    }
}

impl<T> Trait<T> for Part
where
    T: MyNumTrait,
{
    type Targets = FullPartID;

    fn value<GST: GSViewTrait>(&self, _gs: &GST, (_comid, _partid): Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        unreachable!()
    }
}

impl<T> Trait<T> for PartPart
where
    T: MyNumTrait,
{
    type Targets = (FullPartID, FullPartID);

    fn value<GST: GSViewTrait>(
        &self,
        gs: &GST,
        ((s_comid, s_paid), (t_comid, t_paid)): Self::Targets,
    ) -> T
    where
        Self::Targets: Copy,
    {
        match self {
            Self::Source(ref x) => x.value(gs, (s_comid, s_paid)),
            Self::Target(ref x) => x.value(gs, (t_comid, t_paid)),
        }
    }
}

impl<T> Trait<T> for PartCombater
where
    T: MyNumTrait,
{
    type Targets = (FullPartID, CombaterID);

    fn value<GST: GSViewTrait>(&self, gs: &GST, ((s_comid, s_paid), t_comid): Self::Targets) -> T
    where
        Self::Targets: Copy,
    {
        match self {
            Self::Source(ref x) => x.value(gs, (s_comid, s_paid)),
            Self::Target(ref x) => x.value(gs, t_comid),
        }
    }
}

pub trait RatioConvertTrait<IN> {
    fn ratio_convert(inp: IN) -> Self;
}

impl<T: std::marker::Sized> RatioConvertTrait<T> for T {
    fn ratio_convert(inp: T) -> Self {
        inp
    }
}

// TODO: remove this somehow or make only explicit conversions
impl RatioConvertTrait<Rational32> for Mil {
    fn ratio_convert(inp: Rational32) -> Self {
        warn!("ratio_convert: convert from ratio to i32 by assuming it's a Mil");
        (inp * 1000).to_i32().unwrap()
    }
}

impl RatioConvertTrait<Rational32> for usize {
    fn ratio_convert(inp: Rational32) -> usize {
        warn!("ratio_convert: convert from ratio to usize by pretending to be Mil");
        (inp * 1000).to_usize().unwrap()
    }
}
