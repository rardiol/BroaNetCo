/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use super::condition::ConditionTrait;
use super::condition::RandomConditionTrait;
use super::number::RngTrait as RngNumberTrait;
use super::number::Trait as NumberTrait;

use num::ToPrimitive;
use rand::distributions::weighted::WeightedError;
use rand::prelude::*;

use crate::sound::SoundID;
use crate::*;

pub type FmtString = String;
pub type RandomWeight = i32;

// TODO: use some macro to generate repeated things

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum Sound {
    Play(SoundID),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum Part {
    Group(Vec<Part>),
    Combater(Combater),
    HpDiff(number::Calc<<Part as Effect>::Number, HpPts>),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum PartPart {
    Source(Part),
    Target(Part),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum PartCombater {
    Source(Part),
    Target(Combater),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum CombaterLocation {
    LogLine(FmtString, Box<CombaterLocation>, bool),
    Source(Combater),
    Target(Location),
    Group(Vec<CombaterLocation>),
    Repeated(
        Box<CombaterLocation>,
        number::Calc<<CombaterLocation as Effect>::Number, usize>,
    ),
    RotateSourceToTarget(),
    /// Applies effect on a line from source + mindistance to target
    LineOfEffect(Distance, Location),
    /// Teleport source to target
    Teleport(),
    TickDiff(number::Calc<<CombaterLocation as Effect>::Number, Tick>),
    SourceEDataStatDiff(
        CombaterEDataStatID,
        number::Calc<number::CombaterLocation, CombaterEDataStatType>,
    ),
    /// Clones a combater at the target location
    /// Effects are applied as (original_source, new_cloned)
    CloneAtTarget(CombaterID, Vec<CombaterCombater>),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum CombaterCombater {
    Source(Combater),
    Target(Combater),
    CombaterLocation(CombaterLocation),
    /// Switches source and target
    Switch(Box<CombaterCombater>),
    Group(Vec<CombaterCombater>),
    /// Repeat X times
    Repeated(
        Box<CombaterCombater>,
        number::Calc<<CombaterCombater as Effect>::Number, usize>,
    ),
    /// For each part in source matching source_filter,
    /// pick a target part using target_weights
    /// And then execute effect
    // TODO: random conditions and weights
    OnePartToRandomPart {
        source_filter: condition::ConditionDet<condition::Part>,
        target_filter: condition::ConditionDet<condition::Part>,
        target_weights: number::CalcDet<number::PartPart, RandomWeight>,
        effect: PartPart,
    },
    OnePartToMissablePartSizeNormal {
        source_filter: condition::ConditionDet<condition::PartCombater>,
        stddev: number::Calc<<PartCombater as Effect>::Number, PartSize>,
        hit: Option<Box<PartPart>>,
        miss: Option<Box<PartCombater>>,
    },
    /// Transfer parts from source to target matching condition
    TransferPart(condition::ConditionDet<condition::Part>),
    /// Miss according to the protection_behind and protection_inside
    /// of terrain between source and target
    Missable(Box<CombaterCombater>),
    /// Random if else
    IfThenElse {
        condition: <CombaterCombater as Effect>::Condition,
        success: Option<Box<CombaterCombater>>,
        failure: Option<Box<CombaterCombater>>,
    },
    /// Effects are applied as (new_cloned_source, original_target)
    CloneAtSource(CombaterID, Vec<CombaterCombater>),
    /// Set command of origin to Command::SingleTarget targeting target
    /// if Some(x), on_loss_of_target is set to x
    /// If None, it is set to current command
    SetCommandSingleTarget(ActionID, Option<CommandSingleTargetOnLossOfTarget>),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum Location {
    Group(Vec<Location>),
    /// Repeat X times
    Repeated(
        Box<Location>,
        number::Calc<<Location as Effect>::Number, usize>,
    ),
    CloneAt(CombaterID, Vec<Combater>),
    AreaOfEffect(Distance, Box<Location>),
    /// Tries finding a combater at location
    CombaterAt(Combater),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum Combater {
    /// Converts the effect's EffectLog into a EffectLog line
    /// If true, erases the numbers in the EffectLog
    LogLine(FmtString, Box<Combater>, bool),
    Location(Box<Location>),
    GS(Box<GS>),
    Group(Vec<Combater>),
    Repeated(
        Box<Combater>,
        number::Calc<<Combater as Effect>::Number, usize>,
    ),
    LogText(String, CombatLogEntryPriority),
    Timed(
        number::Calc<<Combater as Effect>::Number, Tick>,
        Box<Combater>,
    ),
    HpDiff(number::Calc<<Combater as Effect>::Number, HpPts>),
    TickDiff(number::Calc<<Combater as Effect>::Number, Tick>),
    // TODO: recursive effects with contexts
    SetSingleSubStatus(FullSubStatusID, SubStatusActID),
    /// Takes the total target size and has that as a chance to hit
    // TODO: reconsider with spacing
    MissablePartSize(Box<Combater>),
    // TODO: This assumes all parts with a size are valid targets
    // there can be a part with size but no HP and cause problems for
    // PartEffect::HpDiff
    /// Hits one of the target parts, taking into account size and accuracy
    MissablePartSizeNormal {
        stddev: number::Calc<<Combater as Effect>::Number, PartSize>,
        hit: Option<Box<Part>>,
        miss: Option<Box<Combater>>,
    },
    IfThenElse {
        condition: <Combater as Effect>::Condition,
        success: Option<Box<Combater>>,
        failure: Option<Box<Combater>>,
    },
    RandomWeighted(
        Vec<(
            Box<Combater>,
            number::Calc<<Combater as Effect>::Number, RandomWeight>,
        )>,
    ),
    SetCommand(Command),
    Kill(),
    EDataStatDiff(
        CombaterEDataStatID,
        number::Calc<number::Combater, CombaterEDataStatType>,
    ),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum Faction {
    RevealMap(MapLocation),
}

#[derive(Clone, Debug, Hash, Serialize, Deserialize)]
pub enum GS {
    LogText(String, CombatLogEntryPriority),
    CloneAt {
        com: CombaterID,
        location: MapLocation,
        effects: Vec<Combater>,
    },
    Combater(Combater, <Combater as Effect>::Targets),
    Location(Location, <Location as Effect>::Targets),
    Faction(Faction, <Faction as Effect>::Targets),
}

impl Effect for Part {
    type Targets = FullPartID;
    type Number = number::Part;
    type Condition = condition::Condition<condition::Part>;

    fn apply<NTD: NTDTrait>(
        &self,
        gs: &mut crate::GS<NTD>,
        (target, partid): Self::Targets,
    ) -> EffectLog {
        debug!("{:?}.apply on {:?}", self, target);
        match *self {
            Self::HpDiff(ref hpdiff) => {
                let hpdiff_value = hpdiff.value_gs_rng(gs, (target, partid));
                let target_com = gs.com_m(target);
                target_com.hp_diff_single_target(hpdiff_value, partid);
                if target_com.hp().value() <= 0 {
                    gs.remove_combater(target);
                }
                EffectLog::new().hp_diff(hpdiff_value).hit()
            }
            Self::Combater(ref effect) => effect.apply(gs, target),
            Self::Group(ref effects) => Self::group(gs, (target, partid), effects),
        }
    }
}

impl Effect for PartCombater {
    type Targets = ((CombaterID, PartID), CombaterID);
    type Number = number::PartCombater;
    type Condition = condition::Condition<condition::PartCombater>;

    fn apply<NTD: NTDTrait>(
        &self,
        gs: &mut crate::GS<NTD>,
        ((s_comid, s_paid), t_comid): Self::Targets,
    ) -> EffectLog {
        match self {
            Self::Source(ref effect) => effect.apply(gs, (s_comid, s_paid)),
            Self::Target(ref effect) => effect.apply(gs, t_comid),
        }
    }
}

impl Effect for PartPart {
    type Targets = ((CombaterID, PartID), (CombaterID, PartID));
    type Number = number::PartPart;
    type Condition = condition::Condition<condition::PartPart>;

    fn apply<NTD: NTDTrait>(
        &self,
        gs: &mut crate::GS<NTD>,
        ((s_comid, s_paid), (t_comid, t_paid)): Self::Targets,
    ) -> EffectLog {
        match self {
            Self::Source(ref effect) => effect.apply(gs, (s_comid, s_paid)),
            Self::Target(ref effect) => effect.apply(gs, (t_comid, t_paid)),
        }
    }
}

impl Effect for CombaterLocation {
    type Targets = (CombaterID, MapLocation);
    type Number = number::CombaterLocation;
    type Condition = condition::Condition<condition::CombaterLocation>;

    fn apply<NTD: NTDTrait>(
        &self,
        gs: &mut crate::GS<NTD>,
        (source, target): Self::Targets,
    ) -> EffectLog {
        match *self {
            Self::LogLine(ref fmtstring, ref effect, erasevalues) => Self::log_line(
                gs,
                (source, target),
                vec![
                    ("source_name".to_owned(), Left(gs.com(source).name.clone())),
                    (
                        "target_map_name".to_owned(),
                        Left(gs.map[target].name.clone()),
                    ),
                ],
                fmtstring.to_owned(),
                effect,
                erasevalues,
            ),
            Self::Source(ref effect) => effect.apply(gs, source),
            Self::Target(ref effect) => effect.apply(gs, target),
            Self::Group(ref effects) => Self::group(gs, (source, target), effects),
            Self::Repeated(ref effect, ref num_repeats) => {
                Self::repeated(gs, (source, target), effect, num_repeats)
            }
            Self::RotateSourceToTarget() => {
                if let Some(ndir) = gs.com(source).location.coord.direction_to_cw(target) {
                    gs.com_m(source).location.dir = ndir;
                }
                EffectLog::new()
            }
            Self::LineOfEffect(min_distance, ref effect) => {
                let mut log = EffectLog::new();
                let origin = gs.com(source).location.coord;
                let line = origin.line_to_iter(target);
                for point in line.skip(min_distance.try_into().unwrap()) {
                    log += effect.apply(gs, point);
                }
                log
            }
            Self::Teleport() => {
                gs.com_m(source).location.coord = target;
                EffectLog::new()
            }
            Self::CloneAtTarget(clone_id, ref effects) => Self::clone_at(
                gs,
                clone_id,
                |gs2, cloned_id| {
                    for eff in effects {
                        eff.apply(gs2, (source, cloned_id));
                    }
                },
                target,
            ),
            Self::TickDiff(ref tickdiff) => Self::tick_diff(gs, (source, target), source, tickdiff),
            // TODO: DRY
            Self::SourceEDataStatDiff(ref statid, ref value) => {
                let value = value.value_gs_rng(gs, (source, target));
                let (_effected, _remainder) = statid
                    .get_mut(&mut gs.com_m(source).edata)
                    .expect("Invalid stat")
                    .diff(value);
                EffectLog::new()
            }
        }
    }
}

impl Effect for CombaterCombater {
    type Targets = (CombaterID, CombaterID);
    type Number = number::CombaterCombater;
    type Condition = condition::Condition<condition::CombaterCombater>;

    fn apply<NTD: NTDTrait>(
        &self,
        gs: &mut crate::GS<NTD>,
        (source, target): Self::Targets,
    ) -> EffectLog {
        match *self {
            Self::CombaterLocation(ref effect) => {
                effect.apply(gs, (source, gs.com(target).location.coord))
            }
            Self::Source(ref effect) => effect.apply(gs, source),
            Self::Target(ref effect) => effect.apply(gs, target),
            Self::Switch(ref effect) => effect.apply(gs, (target, source)),
            Self::OnePartToRandomPart {
                ref source_filter,
                ref target_filter,
                ref target_weights,
                ref effect,
            } => {
                let mut effect_log = EffectLog::new();
                let mut rng = GRng::from_rng(&mut gs.rng).unwrap();
                // Get all source_parts that will be source of actions
                let source_parts = gs
                    .com(source)
                    .parts()
                    .map(|(pid, _part)| pid)
                    .filter(|pid| source_filter.eval(gs, (source, *pid)));
                // Get all target parts that can be targeted
                let target_parts = gs
                    .com(target)
                    .parts()
                    .map(|(pid, _part)| pid)
                    .filter(|pid| target_filter.eval(gs, (target, *pid)))
                    .collect::<Vec<_>>();
                // Pick the target for each filtered source_part
                for targets in source_parts
                    .into_iter()
                    .filter_map(|spid| {
                        match target_parts.choose_weighted(&mut rng, |tpid| {
                            target_weights.value(gs, ((source, spid), (target, *tpid)))
                        }) {
                            Ok(tpid) => Some(((source, spid), (target, *tpid))),
                            // In those cases, no target is to be chosen
                            Err(WeightedError::NoItem) | Err(WeightedError::AllWeightsZero) => None,
                            Err(err) => {
                                panic!("Error while executing OnePartToRandomPart: {}", err)
                            }
                        }
                    })
                    .collect::<Vec<_>>()
                {
                    // Since targeting is done at the beginning, effects that change the weights,
                    // filter or unfilter sources do not change the behaviour
                    // For example, if the targeting weight gives 0 for dead target parts, if the
                    // first sourcepart kills a target, others may still fire on it
                    effect_log += effect.apply(gs, targets)
                }
                effect_log
            }
            Self::OnePartToMissablePartSizeNormal {
                ref source_filter,
                ref stddev,
                ref hit,
                ref miss,
            } => {
                // TODO: merge common parts with OnePartToRandomPart and MissablePartSizeNormal

                // Get all source_parts that will be source of actions
                let source_parts = gs
                    .com(source)
                    .parts()
                    .map(|(pid, _part)| pid)
                    .filter(|pid| source_filter.eval(gs, ((source, *pid), target)))
                    .collect::<Vec<_>>();

                let mut effect_log = EffectLog::new();
                for source_part in source_parts {
                    let (distribution, total_distribution_size, _target_size) =
                        gs.com(target).parts.distribute();

                    let total_distribution_size_float: f64 =
                        total_distribution_size.try_into().unwrap();
                    // TODO: target a part close to the middle, not actual middle
                    // Example: x is midpoint
                    // 0____11___x___22____3
                    let midpoint = total_distribution_size_float / 2.0;

                    //TODO: rounding
                    let stddev =
                        f64::from(stddev.value_gs_rng(gs, ((source, source_part), target)));
                    let hit_position = gs.rng.sample(
                        rand_distr::Normal::new(midpoint, stddev).expect("Invalid normal distr"),
                    );

                    let target_part: &Option<PartID> =
                        if let Some(hit_position_int) = hit_position.round().to_usize() {
                            debug!("Hit position int: {:?}", hit_position_int);
                            distribution.get(hit_position_int).unwrap_or(&None)
                        } else {
                            debug!("Hit position out of usize");
                            &None
                        };

                    trace!(
                        "OnePartToMissablePartSizeNormal: ({:?} {:?}) {:?} {:?} ({:?}) {:?}",
                        total_distribution_size,
                        total_distribution_size_float,
                        midpoint,
                        stddev,
                        hit_position,
                        target_part,
                    );

                    effect_log += match target_part {
                        Some(partid) => {
                            if let Some(ref effect) = *hit {
                                effect.apply(gs, ((source, source_part), (target, *partid)))
                            } else {
                                EffectLog::new().miss()
                            }
                        }
                        None => {
                            if let Some(ref effect) = *miss {
                                effect.apply(gs, ((source, source_part), target))
                            } else {
                                EffectLog::new().miss()
                            }
                        }
                    };
                }
                effect_log
            }
            Self::TransferPart(ref filter) => {
                while let Some(pid) = gs
                    .com(source)
                    .parts()
                    .map(|(pid, _part)| pid)
                    .find(|pid| filter.eval(gs, (source, *pid)))
                {
                    let part = gs.com_m(source).remove_part(pid);
                    gs.com_m(target).insert_part(part);
                }
                /* How it would work if partids were stable
                let totransfer = gs
                    .com(source)
                    .parts()
                    .map(|(pid, _part)| pid)
                    .collect::<Vec<_>>()
                    .into_iter()
                    .filter(|pid| filter.eval_gs_rng(gs, (source, *pid)))
                    .collect::<Vec<_>>();
                for partid in totransfer {
                    let part = gs.com_m(source).remove_part(partid);
                    gs.com_m(target).insert_part(part);
                }
                */
                EffectLog::new()
            }
            Self::Missable(ref effect) => {
                let source_loc = gs.com(source).location.coord;
                let target_loc = gs.com(target).location.coord;
                let line = source_loc.line_to_iter(target_loc);
                let mut it = line.skip(1);
                let mut loc = it.next().unwrap();
                loop {
                    match it.next() {
                        // Middle hexes
                        Some(loc2) => {
                            if ninmil(gs.map[loc].protection_behind.unwrap(), gs) {
                                debug!("effect missed behind from {:?} to {:?}", source, target);
                                return EffectLog::new().miss();
                            }
                            loc = loc2;
                        }
                        // Last hex
                        None => {
                            if ninmil(gs.map[loc].protection_inside.unwrap(), gs) {
                                debug!("effect missed inside from {:?} to {:?}", source, target);
                                return EffectLog::new().miss();
                            } else {
                                break;
                            }
                        }
                    };
                }
                effect.apply(gs, (source, target))
            }
            Self::Group(ref effects) => Self::group(gs, (source, target), effects),
            Self::Repeated(ref effect, ref num_repeats) => {
                Self::repeated(gs, (source, target), effect, num_repeats)
            }
            Self::IfThenElse {
                ref condition,
                ref success,
                ref failure,
            } => Self::if_then_else(gs, (source, target), condition, success, failure),
            Self::CloneAtSource(clone_id, ref effects) => {
                let coord = gs.com(source).location.coord;
                // TODO: all combinations of the 3 of source, cloned and target
                Self::clone_at(
                    gs,
                    clone_id,
                    |gs2, cloned_id| {
                        for eff in effects {
                            eff.apply(gs2, (cloned_id, target));
                        }
                    },
                    coord,
                )
            }
            Self::SetCommandSingleTarget(attack_action, ref opt_on_loss_of_target) => {
                let on_loss_of_target = match opt_on_loss_of_target {
                    Some(on_loss_of_target) => on_loss_of_target.clone(),
                    None => CommandSingleTargetOnLossOfTarget::ChangeCommand(Box::new(
                        gs.com(source).command.clone(),
                    )),
                };
                gs.com_m(source).command = Command::SingleTarget {
                    target,
                    attack_action,
                    on_loss_of_target,
                };
                EffectLog::new()
            }
        }
    }
}

impl Effect for Location {
    type Targets = MapLocation;
    type Number = number::Location;
    type Condition = condition::Condition<condition::Location>;

    fn apply<NTD: NTDTrait>(&self, gs: &mut crate::GS<NTD>, target: Self::Targets) -> EffectLog {
        debug!("{:?}.apply on {:?}", self, target);
        match *self {
            Self::Group(ref effects) => Self::group(gs, target, effects),
            Self::Repeated(ref effect, ref num_repeats) => {
                Self::repeated(gs, target, effect, num_repeats)
            }
            Self::CloneAt(clone_id, ref effects) => Self::clone_at(
                gs,
                clone_id,
                |gs2, cloned_id| {
                    for eff in effects {
                        eff.apply(gs2, cloned_id);
                    }
                },
                target,
            ),
            Self::AreaOfEffect(distance, ref terrain_effect) => {
                let mut eff_log = EffectLog::new();
                for coord in target.range_iter(distance) {
                    eff_log += terrain_effect.apply(gs, coord);
                }
                eff_log
            }
            Self::CombaterAt(ref combater_effect) => {
                if let Some((comb_id, _)) = gs.combater_at_location(target) {
                    combater_effect.apply(gs, comb_id)
                } else {
                    EffectLog::new()
                }
            }
        }
    }
}

impl Effect for Combater {
    type Targets = CombaterID;
    type Number = number::Combater;
    type Condition = condition::Condition<condition::Combater>;

    fn apply<NTD: NTDTrait>(&self, gs: &mut crate::GS<NTD>, target: Self::Targets) -> EffectLog {
        debug!("{:?}.apply on {:?}", self, target);
        match *self {
            Self::GS(ref effects) => effects.apply(gs, ()),
            Self::Group(ref effects) => Self::group(gs, target, effects),
            Self::Repeated(ref effect, ref num_repeats) => {
                Self::repeated(gs, target, effect, num_repeats)
            }
            Self::Location(ref effect) => effect.apply(gs, gs.com(target).location.coord),
            Self::HpDiff(ref hpdiff) => {
                let hpdiff_value = hpdiff.value_gs_rng(gs, target);
                let target_com = gs.com_m(target);
                target_com.hp_diff(hpdiff_value);
                if target_com.hp().value() <= 0 {
                    gs.remove_combater(target);
                }
                EffectLog::new().hp_diff(hpdiff_value).hit()
            }
            Self::TickDiff(ref tickdiff) => Self::tick_diff(gs, target, target, tickdiff),
            Self::SetSingleSubStatus((ref status_id, ref substatus_id_path), substatus_id) => {
                let target_com = gs.com_m(target);
                target_com
                    .statuses
                    .iter_mut()
                    .find(|st| st.name == *status_id)
                    .unwrap()
                    .substatus_mut(substatus_id_path)
                    .substatuses
                    .disable_all_and_activate_single(substatus_id);
                EffectLog::new()
            }
            Self::MissablePartSize(ref effect) => {
                // TODO: partsize Ratio?
                if ninmil(mil_chances(gs.com(target).parts.distribute().2), gs) {
                    effect.apply(gs, target)
                } else {
                    EffectLog::new().miss()
                }
            }
            Self::MissablePartSizeNormal {
                ref stddev,
                ref hit,
                ref miss,
            } => {
                let (distribution, total_distribution_size, _target_size) =
                    gs.com(target).parts.distribute();

                let total_distribution_size_float: f64 =
                    total_distribution_size.try_into().unwrap();
                // TODO: target a part close to the middle, not actual middle
                // Example: x is midpoint
                // 0____11___x___22____3
                let midpoint = total_distribution_size_float / 2.0;

                //TODO: rounding
                let stddev = f64::from(stddev.value_gs_rng(gs, target));
                let hit_position = gs.rng.sample(
                    rand_distr::Normal::new(midpoint, stddev).expect("Invalid normal distr"),
                );

                let target_part: &Option<PartID> =
                    if let Some(hit_position_int) = hit_position.round().to_usize() {
                        debug!("Hit position int: {:?}", hit_position_int);
                        distribution.get(hit_position_int).unwrap_or(&None)
                    } else {
                        debug!("Hit position out of usize");
                        &None
                    };

                // TODO: targets don't reorganize instantly between attacks

                trace!(
                    "MissablePartSizeNormal: ({:?} {:?}) {:?} {:?} ({:?}) {:?}",
                    total_distribution_size,
                    total_distribution_size_float,
                    midpoint,
                    stddev,
                    hit_position,
                    target_part,
                );

                match target_part {
                    Some(partid) => {
                        if let Some(ref effect) = *hit {
                            effect.apply(gs, (target, *partid))
                        } else {
                            EffectLog::new().miss()
                        }
                    }
                    None => {
                        if let Some(ref effect) = *miss {
                            effect.apply(gs, target)
                        } else {
                            EffectLog::new().miss()
                        }
                    }
                }
            }
            Self::IfThenElse {
                ref condition,
                ref success,
                ref failure,
            } => Self::if_then_else(gs, target, condition, success, failure),
            Self::RandomWeighted(ref vec) => Self::random_weighted(gs, target, vec),
            Self::SetCommand(ref command) => {
                gs.com_m(target).command = command.clone();
                EffectLog::new()
            }
            Self::Kill() => {
                gs.remove_combater(target);
                EffectLog::new()
            }
            Self::Timed(ref time, ref effect) => {
                let tickvalue = time.value_gs_rng(gs, target);
                gs.timed_effects
                    .entry(tickvalue)
                    .or_insert_with(Default::default)
                    .push_back((target, *effect.clone()));
                EffectLog::new()
            }
            Self::EDataStatDiff(ref statid, ref value) => {
                let value = value.value_gs_rng(gs, target);
                let (_effected, _remainder) = statid
                    .get_mut(&mut gs.com_m(target).edata)
                    .expect("Invalid stat")
                    .diff(value);
                // TODO: effectlog
                EffectLog::new()
            }
            Self::LogText(ref format, priority) => Self::log_text(
                gs,
                vec![("com_name".to_owned(), Left(gs.com(target).name.clone()))],
                format.to_owned(),
                priority,
            ),
            Self::LogLine(ref format, ref effect, erasevalues) => Self::log_line(
                gs,
                target,
                vec![("com_name".to_owned(), Left(gs.com(target).name.clone()))],
                format.to_owned(),
                effect,
                erasevalues,
            ),
        }
    }
}

impl Effect for Faction {
    type Targets = FactionID;
    type Number = number::Faction;
    type Condition = condition::Condition<condition::Faction>;

    fn apply<NTD: NTDTrait>(&self, gs: &mut crate::GS<NTD>, target: Self::Targets) -> EffectLog {
        match self {
            Self::RevealMap(coord) => {
                let element = gs.get_map()[*coord].clone();
                if let Some(ref mut fac_map) = gs.fac_m(target).map {
                    fac_map.insert(*coord, element);
                }
                EffectLog::new()
            }
        }
    }
}

impl Effect for GS {
    type Targets = ();
    // TODO: ?????
    type Number = number::Value<i32>;
    type Condition = condition::Condition<condition::GS>;

    fn apply<NTD: NTDTrait>(&self, gs: &mut crate::GS<NTD>, (): Self::Targets) -> EffectLog {
        match self {
            Self::LogText(string, priority) => {
                Self::log_text(gs, vec![], string.to_owned(), *priority)
            }
            Self::CloneAt {
                com,
                location,
                effects,
            } => Self::clone_at(
                gs,
                *com,
                |gs2, cloned_id| {
                    for eff in effects {
                        eff.apply(gs2, cloned_id);
                    }
                },
                *location,
            ),
            Self::Location(eff, target) => eff.apply(gs, *target),
            Self::Combater(eff, target) => eff.apply(gs, *target),
            Self::Faction(eff, target) => eff.apply(gs, *target),
        }
    }
}

pub trait Effect {
    type Targets;
    type Number;
    type Condition;

    fn apply<NTD: NTDTrait>(&self, gs: &mut crate::GS<NTD>, targets: Self::Targets) -> EffectLog;

    fn if_then_else<NTD: NTDTrait>(
        gs: &mut crate::GS<NTD>,
        targets: Self::Targets,
        condition: &Self::Condition,
        success: &Option<Box<Self>>,
        failure: &Option<Box<Self>>,
    ) -> EffectLog
    where
        Self::Condition: condition::RandomConditionTrait<Targets = Self::Targets>,
        Self::Targets: Copy,
    {
        let effect: &Option<Box<Self>> = if condition.eval_gs_rng(gs, targets) {
            success
        } else {
            failure
        };
        // TODO: new/miss/etc
        if let Some(ref effect) = *effect {
            effect.apply(gs, targets)
        } else {
            EffectLog::new().miss()
        }
    }

    fn random_weighted<NTD: NTDTrait>(
        gs: &mut crate::GS<NTD>,
        targets: Self::Targets,
        items: &[(Box<Self>, number::Calc<Self::Number, RandomWeight>)],
    ) -> EffectLog
    where
        number::Calc<Self::Number, RandomWeight>:
            number::RngTrait<RandomWeight, Targets = Self::Targets>,
        Self::Targets: Copy,
    {
        use rand::distributions::weighted::WeightedIndex;

        let dist =
            WeightedIndex::new(items.iter().map(|item| item.1.value_gs_rng(gs, targets))).unwrap();
        items[dist.sample(&mut gs.rng)].0.apply(gs, targets)
    }

    fn clone_at<NTD: NTDTrait, F>(
        gs: &mut crate::GS<NTD>,
        clone_id: CombaterID,
        clone_effects: F,
        coord: MapLocation,
    ) -> EffectLog
    where
        Self::Targets: Copy,
        Self: Sized,
        F: Fn(&mut crate::GS<NTD>, CombaterID),
    {
        let comb = (*gs.com(clone_id)).clone();
        let new_comb_id = gs.add_combater(comb);

        // Now fix the cloning

        // Set new tick time
        gs.com_m(new_comb_id).tick = gs.tick();

        // put new one ingame
        gs.com_m(new_comb_id).ingame = true;

        // Change position
        gs.com_m(new_comb_id).location.coord = coord;

        clone_effects(gs, new_comb_id);

        // TODO: logging
        EffectLog::new()
    }

    fn group<NTD: NTDTrait>(
        gs: &mut crate::GS<NTD>,
        targets: Self::Targets,
        effects: &[Self],
    ) -> EffectLog
    where
        Self::Targets: Copy,
        Self: Sized,
    {
        let mut effect_log = EffectLog::new();
        for effect in effects {
            effect_log += effect.apply(gs, targets);
        }
        effect_log
    }

    fn tick_diff<NTD: NTDTrait>(
        gs: &mut crate::GS<NTD>,
        number_targets: Self::Targets,
        tick_diff_target: CombaterID,
        tick_diff: &number::Calc<Self::Number, Tick>,
    ) -> EffectLog
    where
        number::Calc<Self::Number, Tick>: number::RngTrait<Tick, Targets = Self::Targets>,
        Self::Targets: Copy,
    {
        let value = tick_diff.value_gs_rng(gs, number_targets);
        let target_com = gs.com_m(tick_diff_target);
        target_com.tick += value;
        EffectLog::new().ticks(value)
    }
    fn repeated<NTD: NTDTrait>(
        gs: &mut crate::GS<NTD>,
        targets: Self::Targets,
        effect: &Self,
        repeats: &number::Calc<Self::Number, usize>,
    ) -> EffectLog
    where
        number::Calc<Self::Number, usize>: number::RngTrait<usize, Targets = Self::Targets>,
        Self::Targets: Copy,
    {
        let mut effect_log = EffectLog::new();
        for _ in 0..repeats.value_gs_rng(gs, targets) {
            effect_log += effect.apply(gs, targets);
        }
        effect_log
    }
    fn log_line<NTD: NTDTrait>(
        gs: &mut crate::GS<NTD>,
        targets: Self::Targets,
        mut args: ArgsI18N,
        format: MessageI18N,
        effect: &Self,
        erasevalues: bool,
    ) -> EffectLog
    where
        number::Calc<Self::Number, i32>: number::RngTrait<i32, Targets = Self::Targets>,
        Self::Targets: Copy,
    {
        let mut effect_log = effect.apply(gs, targets);
        args.push(("hp_diff".to_owned(), Right(effect_log.hp_diff)));
        args.push(("ticks".to_owned(), Right(effect_log.ticks)));
        args.push((
            "hits".to_owned(),
            Right(effect_log.hits.try_into().unwrap()),
        ));
        args.push((
            "misses".to_owned(),
            Right(effect_log.misses.try_into().unwrap()),
        ));
        effect_log.text.push((format, args));
        if erasevalues {
            effect_log.hp_diff = 0;
            effect_log.ticks = 0;
            effect_log.hits = 0;
            effect_log.misses = 0;
        }
        effect_log
    }
    fn log_text<NTD: NTDTrait>(
        gs: &mut crate::GS<NTD>,
        args: ArgsI18N,
        format: MessageI18N,
        priority: CombatLogEntryPriority,
    ) -> EffectLog
    where
        number::Calc<Self::Number, i32>: number::RngTrait<i32, Targets = Self::Targets>,
        Self::Targets: Copy,
    {
        gs.push_log(CombatLogEntry::Text((format, args), priority));
        EffectLog::new()
    }
}
