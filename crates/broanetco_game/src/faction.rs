/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use super::*;

use derive_more::{From, Into};

pub type FactionColor = ColorWrap;
#[derive(Clone, Copy, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct FactionID(pub usize);

impl fmt::Display for FactionID {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let FactionID(p) = *self;
        write!(f, "{}", p)
    }
}

#[derive(Debug, Clone, Hash, Serialize, Deserialize, Into, From, new)]
pub struct Faction<NTD> {
    pub name: MessageI18N,
    pub color: FactionColor,
    pub faction_type: FactionType,
    #[new(default)]
    pub logged: BTreeSet<CombatLogEntryID>,
    #[new(value = "Some(Default::default())")]
    pub map: Option<Map<NTD>>,
}

impl<NTD> Faction<NTD> {
    pub fn set_see_entire_map(mut self) -> Self {
        self.map = None;
        self
    }
}

impl FactionType {
    #[const_fn("1.46")]
    pub const fn is_ai(&self) -> bool {
        match *self {
            Self::Player => false,
            Self::AI(_) => true,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
/// Type of player for a Faction
pub enum FactionType {
    /// Human player
    Player,
    AI(AI),
}
