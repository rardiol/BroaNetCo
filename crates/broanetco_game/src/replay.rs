/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::sync::mpsc;

use crate::texture::*;
use crate::{GSApply, GodGSApply};

// TODO: add a startup message?

pub trait ReplayLoggerControlTrait {
    fn send(
        &self,
        msg: ReplayLoggerMessage,
    ) -> Result<(), mpsc::SendError<util::LoggerMessage<ReplayLoggerMessage>>>;
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum ReplayLoggerMessage<NTD = NamedTextureDataTransfer> {
    GodGSApply(GodGSApply<NTD>),
    GSApply(GSApply),
}

impl ReplayLoggerControlTrait for void::Void {
    fn send(
        &self,
        _msg: ReplayLoggerMessage,
    ) -> Result<(), mpsc::SendError<util::LoggerMessage<ReplayLoggerMessage>>> {
        unreachable!()
    }
}

//////////////////////
// default implementation of the replay logger, can use another

use std::io;
use std::path::PathBuf;
use std::thread;

use crate::init::GSInit;

#[derive(Debug, Clone)]
pub struct ReplayLoggerControl(mpsc::Sender<util::LoggerMessage<ReplayLoggerMessage>>);

pub type ReplayLog<NTD = NamedTextureDataTransfer> = (GSInit<NTD>, Vec<ReplayLoggerMessage<NTD>>);

impl ReplayLoggerControlTrait for ReplayLoggerControl {
    fn send(
        &self,
        msg: ReplayLoggerMessage,
    ) -> Result<(), mpsc::SendError<util::LoggerMessage<ReplayLoggerMessage>>> {
        self.0.send(util::LoggerMessage::Message(msg))
    }
}

impl ReplayLoggerControl {
    /// Starts the logger thread and returns a channel to control it
    pub fn new(
        pathbuf: PathBuf,
        gsinit: GSInit,
    ) -> (
        ReplayLoggerControl,
        thread::JoinHandle<Result<(), io::Error>>,
    ) {
        let (th_sender, handle) = util::start_logger_thread(pathbuf, gsinit);

        (ReplayLoggerControl(th_sender), handle)
    }

    pub fn flush(&self) -> Result<(), mpsc::SendError<util::LoggerMessage<ReplayLoggerMessage>>> {
        self.0.send(util::LoggerMessage::Flush)
    }
}
