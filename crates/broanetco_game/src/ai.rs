/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::ops::AddAssign;

use rand::distributions::uniform::SampleUniform;
use rand::distributions::weighted::WeightedError;
use rand::prelude::*;

use crate::bncl::number::RngTrait as NumberRngTrait;
use crate::*;

type AIWeight = i32;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum AI {
    Deterministic(Deterministic),
    /// Random behaviour AI
    Random(Random),
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum Deterministic {
    /// Do nothing AI
    Nop,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum Random {
    /// Attacks if it can
    /// Else walks to random enemy if it can see one
    /// Else walks randomly
    Basic,
    MapStat(number::Calc<number::Location, AIWeight>),
    Weighted {
        attack_weight: number::Calc<number::CombaterCombater, AIWeight>,
        move_enemy_weight: number::Calc<number::CombaterCombater, AIWeight>,
        move_weight: number::Calc<number::CombaterLocation, AIWeight>,
    },
}

impl Deterministic {
    pub fn run<GST: GSTrait>(&self, _gs: &GST, _comid: CombaterID) -> GSApply {
        match self {
            Self::Nop => nop_ai(),
        }
    }
}

impl Random {
    pub fn run<GST: GSTrait, RNG: rand::Rng>(
        &self,
        gs: &GST,
        comid: CombaterID,
        rng: &mut RNG,
    ) -> GSApply {
        let _facid = gs.com(comid).faction;

        match self {
            Self::Basic => random_ai(gs, comid, rng),
            Self::MapStat(ten) => {
                random_move_weights_ai(gs, comid, rng, |loc, rng| ten.value_rng(gs, rng, loc))
                    .expect("invalid ai map weight")
            }
            Self::Weighted {
                attack_weight,
                move_enemy_weight,
                move_weight,
            } => random_weights_ai(
                gs,
                comid,
                rng,
                |tcomid, rng| attack_weight.value_rng(gs, rng, (comid, tcomid)),
                |tcomid, rng| move_enemy_weight.value_rng(gs, rng, (comid, tcomid)),
                |tloc, rng| move_weight.value_rng(gs, rng, (comid, tloc)),
            )
            .expect("invalid ai map weight"),
        }
    }
}

pub const fn nop_ai() -> GSApply {
    GSApply::EndFactionTurn
}

// TODO: use a real pathing algo
//

fn valid_attacks<GST: GSTrait>(
    gs: &GST,
    comid: CombaterID,
) -> impl Iterator<Item = (GSApply, CombaterID)> + '_ {
    let _com = gs.com(comid);
    debug!("play_ai: RandomAI2 for {:?}", comid);

    // TODO: proper choosing of an action
    let attack_id = 0;
    let action_attack_id = gs
        .com(comid)
        .actions(gs, comid)
        .nth(attack_id)
        .map(|x| x.0)
        .unwrap();
    gs.combaters().filter_map(move |(tcomid, _tcom)| {
        Some((
            gs.combater_action(
                comid,
                action_attack_id,
                vec![ActionTarget::Combater(tcomid)],
            )
            .ok()?,
            tcomid,
        ))
    })
}

pub fn random_weights_ai<
    Rng: rand::Rng,
    GST: GSTrait,
    Weight: SampleUniform + for<'a> AddAssign<&'a Weight> + PartialOrd<Weight> + Clone + Default,
    FAttack: FnMut(CombaterID, &mut Rng) -> Weight,
    //TODO: FEMove: FnMut(CombaterID, MapLocation, &mut Rng) -> Weight,
    FEMove: FnMut(CombaterID, &mut Rng) -> Weight,
    FMove: FnMut(MapLocation, &mut Rng) -> Weight,
>(
    gs: &GST,
    comid: CombaterID,
    rng: &mut Rng,
    mut attack_weight: FAttack,
    mut enemy_move_weight: FEMove,
    mut map_move_weight: FMove,
) -> Result<GSApply, WeightedError> {
    let com = gs.com(comid);
    let facid = com.faction;
    debug!("play_ai: RandomAI4 for {:?}", comid);

    match valid_attacks(gs, comid)
        .map(|(gsa, tcomid)| (attack_weight(tcomid, rng), (gsa, tcomid)))
        .collect::<Vec<_>>()
        .choose_weighted(rng, |(weight, _d)| weight.clone())
    {
        Ok((_weight, (gsa, _tcomid))) => {
            return Ok(gsa.clone());
        }
        Err(WeightedError::NoItem) | Err(WeightedError::AllWeightsZero) => (),
        Err(err) => return Err(err),
    }

    match gs
        .combaters()
        .filter(|(_tcid, tcom)| !gs.friendly_factions(facid, tcom.faction))
        .flat_map(|(tcid, tcom)| {
            com.location
                .coord
                .directions_to(tcom.location.coord)
                .into_iter()
                .map(move |x| (tcid, x))
        })
        .filter_map(|(tcid, dir)| Some((gs.move_combater_direction(comid, dir)?, tcid)))
        .map(|(gsa, tcomid)| (enemy_move_weight(tcomid, rng), (gsa, tcomid)))
        .collect::<Vec<_>>()
        .choose_weighted(rng, |(weight, _d)| weight.clone())
    {
        Ok((_weight, (gsa, _tcomid))) => {
            return Ok(gsa.clone());
        }
        Err(WeightedError::NoItem) | Err(WeightedError::AllWeightsZero) => (),
        Err(err) => return Err(err),
    }

    match Direction::all()
        .iter()
        .filter_map(|dir| Some((dir, gs.move_combater_direction(comid, *dir)?)))
        .map(|(dir, gsa)| (dir, gsa, com.location.coord + *dir))
        .map(|(dir, gsa, coord)| (map_move_weight(coord, rng), (dir, gsa, coord)))
        .collect::<Vec<_>>()
        .choose_weighted(rng, |(weight, _d)| weight.clone())
    {
        Ok((_weight, (_dir, gsa, _coord))) => {
            return Ok(gsa.clone());
        }
        Err(WeightedError::NoItem) | Err(WeightedError::AllWeightsZero) => (),
        Err(err) => return Err(err),
    }

    Ok(GSApply::EndFactionTurn)
}

pub fn random_move_weights_ai<
    Rng: rand::Rng,
    GST: GSTrait,
    Weight: SampleUniform + for<'a> AddAssign<&'a Weight> + PartialOrd<Weight> + Clone + Default,
    F: FnMut(MapLocation, &mut Rng) -> Weight,
>(
    gs: &GST,
    comid: CombaterID,
    rng: &mut Rng,
    mut map_move_weight: F,
) -> Result<GSApply, WeightedError> {
    let com = gs.com(comid);
    debug!("play_ai: RandomAI3 for {:?}", comid);

    let valid_attacks = valid_attacks(gs, comid);

    if let Some((gsa, _tcomid)) = valid_attacks.choose(rng) {
        return Ok(gsa);
    }
    match Direction::all()
        .iter()
        .filter_map(|dir| Some((dir, gs.move_combater_direction(comid, *dir)?)))
        .map(|(dir, gsa)| (dir, gsa, com.location.coord + *dir))
        .map(|(dir, gsa, coord)| (map_move_weight(coord, rng), (dir, gsa, coord)))
        .collect::<Vec<_>>()
        .choose_weighted(rng, |(weight, _d)| weight.clone())
    {
        Ok((_weight, (_dir, gsa, _coord))) => Ok(gsa.clone()),
        Err(WeightedError::AllWeightsZero) | Err(WeightedError::NoItem) => {
            Ok(GSApply::EndFactionTurn)
        }
        Err(err) => Err(err),
    }
}

pub fn random_ai<Rng: rand::Rng, GST: GSTrait>(
    gs: &GST,
    comid: CombaterID,
    rng: &mut Rng,
) -> GSApply {
    let com = gs.com(comid);
    let facid = com.faction;
    debug!("play_ai: RandomAI2 for {:?}", comid);

    let valid_attacks = valid_attacks(gs, comid);

    if let Some((gsa, _tcomid)) = valid_attacks.choose(rng) {
        gsa
    } else {
        let move_targets = gs
            .combaters()
            .inspect(|x| {
                trace!("1: {:?}", x);
            })
            .filter(|(_tcid, tcom)| !gs.friendly_factions(facid, tcom.faction))
            .inspect(|x| {
                trace!("2: {:?}", x);
            })
            .flat_map(|(_tcid, tcom)| com.location.coord.directions_to(tcom.location.coord))
            .inspect(|x| {
                trace!("3: {:?}", x);
            })
            .filter_map(|x| gs.move_combater_direction(comid, x))
            .inspect(|x| {
                trace!("4: {:?}", x);
            })
            .collect::<Vec<_>>();
        trace!("valid move targets {:?}", move_targets);
        if let Some(enemy_direction_move) = move_targets.iter().choose(rng).cloned() {
            debug!("ai moving in attack direciton");
            enemy_direction_move
        } else {
            Direction::all()
                .iter()
                .filter_map(|x| gs.move_combater_direction(comid, *x))
                .choose(rng)
                .unwrap_or(GSApply::EndFactionTurn)
        }
    }
}

pub fn single_target_ai<Rng: rand::Rng, GST: GSTrait>(
    gs: &GST,
    rng: &mut Rng,
    source: CombaterID,
    target: CombaterID,
    attack_action: ActionID,
) -> GSApply {
    debug!(
        "single_target_ai: {:?} to {:?} do {:?}",
        source, target, attack_action
    );
    let source_com = gs.com(source);
    let target_com = gs.com(target);
    assert!(source_com.ingame);
    // TODO: what to do if target disappears
    assert!(target_com.ingame);

    // Check if attack possible
    // TODO: what if the target can't be seen?
    if let Ok(gsa) = gs.combater_action(source, attack_action, vec![ActionTarget::Combater(target)])
    {
        debug!("executing attack");
        return gsa;
    }

    // Check if already reached target
    if source_com.location.coord == target_com.location.coord {
        warn!("single_target_ai nothing to do");
        return GSApply::EndFactionTurn;
    }

    // Move in the correct direction
    let target_direction = source_com
        .location
        .coord
        .directions_to(target_com.location.coord)
        .into_iter()
        .inspect(|x| {
            trace!("1: {:?}", x);
        })
        .filter_map(|x| gs.move_combater_direction(source, x))
        .inspect(|x| {
            trace!("2: {:?}", x);
        })
        .choose(rng)
        .expect("No path");

    debug!("single target ai moving in attack direction");
    target_direction
}
