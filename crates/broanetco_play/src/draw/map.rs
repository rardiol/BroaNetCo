/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use game::hex2d::Direction;
use ggez::graphics;
use ggez::graphics::{DrawParam, Drawable, Text, TextFragment};
use ggez::Context;
use ggez::GameResult;

use super::super::input::uis;
use super::super::wrappers::MapElement;
use super::*;
use crate::game::map::MapLocation;
use crate::game::texture::*;
use crate::game::{
    ActionTargetingCombaterError, ActionTargetingLocationError, CombaterID, Distance, GSViewTrait,
    GS,
};
use uis::Trait as UISTrait;

type Vector2 = nalgebra::Vector2<f32>;
type Point2 = nalgebra::Point2<f32>;

// TODO: use const fn when stable when https://github.com/rust-lang/rust/issues/57241 is solved
macro_rules! this_hex_height {
    ( $x:expr ) => {
        HEX_HEIGHT - 2.0 * $x
    };
}

macro_rules! this_half_width {
    ( $x:expr ) => {
        this_hex_height!($x) / 2.0
    };
}

macro_rules! hex_polygon {
    ( $x:expr ) => {
        [
            //FlatTop
            [HEX_X_CENTER - this_half_width!($x) / 2.0, $x],
            [HEX_X_CENTER + this_half_width!($x) / 2.0, $x],
            [
                HEX_X_CENTER + this_half_width!($x),
                this_hex_height!($x) / 2.0 + $x,
            ],
            [HEX_X_CENTER + this_half_width!($x) / 2.0, HEX_HEIGHT - $x],
            [HEX_X_CENTER - this_half_width!($x) / 2.0, HEX_HEIGHT - $x],
            [
                HEX_X_CENTER - this_half_width!($x),
                this_hex_height!($x) / 2.0 + $x,
            ],
        ]
    };
}

const HEX_POLYGON: [[f32; 2]; 6] = hex_polygon!(0.0);
const SMALL_HEX_POLYGON: [[f32; 2]; 6] = hex_polygon!(8.0);

pub(super) const BORDER_HEX_POLYGON: [[f32; 2]; 14] = [
    HEX_POLYGON[0],
    HEX_POLYGON[1],
    HEX_POLYGON[2],
    HEX_POLYGON[3],
    HEX_POLYGON[4],
    HEX_POLYGON[5],
    HEX_POLYGON[0],
    SMALL_HEX_POLYGON[0],
    SMALL_HEX_POLYGON[1],
    SMALL_HEX_POLYGON[2],
    SMALL_HEX_POLYGON[3],
    SMALL_HEX_POLYGON[4],
    SMALL_HEX_POLYGON[5],
    SMALL_HEX_POLYGON[0],
];

impl DrawCache {
    fn hex_border_overlay(&mut self, ctx: &mut Context) -> GameResult<&mut Mesh> {
        self.hex_border_overlay.get_or_insert_with_result(|| {
            Mesh::new_polygon(
                ctx,
                graphics::DrawMode::fill(),
                &BORDER_HEX_POLYGON,
                COLOR_WHITE,
            )
        })
    }
    fn hex_overlay(&mut self, ctx: &mut Context) -> GameResult<&mut Mesh> {
        self.hex_overlay.get_or_insert_with_result(|| {
            Mesh::new_polygon(ctx, graphics::DrawMode::fill(), &HEX_POLYGON, COLOR_WHITE)
        })
    }
}

impl GSDrawMap for GS<NamedTextureDataFull> {}
impl<'g> GSDrawMap for GSPov<'g, NamedTextureDataFull> {
    fn draw_map_terrain_centered(
        &self,
        _font_data: &FontData,
        _interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        center: MapLocation,
        range: Distance,
        ctx: &mut Context,
        t: Vector2,
    ) -> GameResult<()> {
        let map = self.get_map();
        for coord in center.range_iter(range).filter(|x| map.contains_coord(*x)) {
            trace!("draw_map_terrain_centered: coord {:?}", coord);
            let element = &map[coord];
            MapElement::from(element).draw(coord, ctx, t)?;
            if !self.fac_can_see_map(self.pov, coord) {
                drawcache.hex_overlay(ctx)?.draw(
                    ctx,
                    DrawParam::default()
                        .dest(pair_to_point(coord.to_pixel(HEX_SPACING)) + t)
                        .color(COLOR_OUT_OF_SIGHT),
                )?;
            }
        }
        Ok(())
    }
}

pub trait GSDrawMap: GSTrait<NTD = NamedTextureDataFull> + Sized {
    fn draw_map<MUIS: UISMapDraw>(
        &self,
        uis: &MUIS,
        font_data: &FontData,
        interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        center: MapLocation,
        range: Distance,
        ctx: &mut Context,
        t: Vector2,
    ) -> GameResult<()> {
        let t: Vector2 = t - pair_to_vec2(center.to_pixel(HEX_SPACING));
        self.draw_map_terrain_centered(
            font_data,
            interface_data,
            drawcache,
            center,
            range,
            ctx,
            t,
        )?;

        // TODO: flickering
        for (comid, _com) in self
            .combaters()
            .filter(|(_comid, com)| com.location.coord.distance(center) <= range)
        {
            self.draw_combater(comid, font_data, interface_data, drawcache, ctx, t)?;
        }

        uis.draw_map(self, font_data, interface_data, drawcache, ctx, t)?;

        Ok(())
    }

    fn draw_combater(
        &self,
        comid: CombaterID,
        _font_data: &FontData,
        interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        ctx: &mut Context,
        t: Vector2,
    ) -> GameResult<()> {
        let com = self.com(comid);
        if !com.ingame {
            return Ok(());
        }
        let hex = com.location;
        let target = {
            let temp = hex.coord.to_pixel(HEX_SPACING);
            Point2::new(temp.0, temp.1) + t
        };

        for (params, texture_data) in std::iter::once(&com.texture_data)
            .chain(com.active_statuses(self, comid).map(|x| &x.texture_data))
        {
            let rot = hex.dir.to_radians_flat();
            let params = match params {
                TextureParams::TopDown => DrawParam::default()
                    .dest(target + Vector2::new(HEX_X_CENTER as f32, HEX_X_CENTER as f32))
                    .rotation(rot)
                    .offset(Point2::new(0.5, 0.5)),
                TextureParams::Simple => DrawParam::default().dest(target),
                TextureParams::LPC => {
                    // TODO: Flat top
                    // let rotation = match hex.dir {
                    //     Direction::YX | Direction::ZX => 0.1,
                    //     Direction::XZ | Direction::XY => -0.1,
                    //     _ => 0.0,
                    // };
                    // let src_line = match hex.dir {
                    //     Direction::YX | Direction::ZX => 7.0,
                    //     Direction::XZ | Direction::XY => 9.0,
                    //     Direction::YZ => 8.0,
                    //     Direction::ZY => 10.0 ,
                    // }
                    let rotation = match hex.dir {
                        Direction::ZY | Direction::ZX => rot + std::f32::consts::PI,
                        Direction::YZ | Direction::XZ => rot,
                        _ => 0.0,
                    };
                    let src_line = match hex.dir {
                        // Down
                        Direction::ZY | Direction::ZX => 10.0,
                        // Up
                        Direction::YZ | Direction::XZ => 8.0,
                        // Left
                        Direction::YX => 9.0,
                        // Right
                        Direction::XY => 11.0,
                    };
                    DrawParam::default()
                        .src(Rect::new(
                            0.0,
                            (64.0 * src_line) / 1344.0,
                            64.0 / 832.0,
                            64.0 / 1344.0,
                        ))
                        .dest(target + Vector2::new(HEX_X_CENTER as f32, HEX_X_CENTER as f32))
                        .rotation(rotation)
                        .offset(Point2::new(0.5, 0.5))
                }
            };
            for texture_data in texture_data {
                let tparams = params.scale(rescale(texture_data));
                texture_data.texture_data.draw(ctx, tparams)?;
                trace!("comb: {:?} {:?}", hex, tparams);
            }
        }
        // TODO: show hp graphically?

        let under_tex = if self.com_can_act(comid) {
            &interface_data.can_act_texture
        } else {
            &interface_data.cannot_act_texture
        };
        under_tex.draw(ctx, DrawParam::default().dest(target))?;

        // TODO: don't show for hostiles of current players, as GSPoV filters them
        // Checks if any hostile faction can see this com
        let seen_tex = if self
            .factions()
            .filter(|(facid, _fac)| !self.friendly_factions(com.faction, *facid))
            .any(|(hostilefacid, _fac)| self.fac_can_see(hostilefacid, comid))
        {
            &interface_data.is_seen_texture
        } else {
            &interface_data.is_not_seen_texture
        };
        seen_tex.draw(ctx, DrawParam::default().dest(target))?;

        drawcache.hex_border_overlay(ctx)?.draw(
            ctx,
            DrawParam::default()
                .dest(target)
                .color(self.fac(com.faction).color.dissolve().into()),
        )?;
        Ok(())
    }
    fn draw_map_terrain_centered(
        &self,
        _font_data: &FontData,
        _interface_data: &InterfaceData,
        _drawcache: &mut DrawCache,
        center: MapLocation,
        range: Distance,
        ctx: &mut Context,
        t: Vector2,
    ) -> GameResult<()> {
        let map = self.get_map();
        for coord in center.range_iter(range).filter(|x| map.contains_coord(*x)) {
            trace!("draw_map_terrain_centered: coord {:?}", coord);
            let element = &map[coord];
            MapElement::from(element).draw(coord, ctx, t)?;
        }
        Ok(())
    }
}

pub trait UISMapDraw {
    fn draw_map<GST: GSTrait>(
        &self,
        gs: &GST,
        _font_data: &FontData,
        _interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        ctx: &mut Context,
        t_map: Vector2,
    ) -> GameResult<()>;
}

impl UISMapDraw for uis::Observer {
    fn draw_map<GST: GSTrait>(
        &self,
        _gs: &GST,
        _font_data: &FontData,
        _interface_data: &InterfaceData,
        _drawcache: &mut DrawCache,
        _ctx: &mut Context,
        _t_map: Vector2,
    ) -> GameResult<()> {
        Ok(())
    }
}

impl UISMapDraw for uis::FreeMove {
    fn draw_map<GST: GSTrait>(
        &self,
        gs: &GST,
        _font_data: &FontData,
        _interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        ctx: &mut Context,
        t_map: Vector2,
    ) -> GameResult<()> {
        drawcache.hex_overlay(ctx)?.draw(
            ctx,
            DrawParam::default()
                .dest(pair_to_point(self.map_location(gs).to_pixel(HEX_SPACING)) + t_map)
                .color(COLOR_VALID_SELECTION_TARGET),
        )
    }
}

impl UISMapDraw for uis::Move {
    fn draw_map<GST: GSTrait>(
        &self,
        gs: &GST,
        _font_data: &FontData,
        _interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        ctx: &mut Context,
        t_map: Vector2,
    ) -> GameResult<()> {
        drawcache.hex_overlay(ctx)?.draw(
            ctx,
            DrawParam::default()
                .dest(pair_to_point(self.map_location(gs).to_pixel(HEX_SPACING)) + t_map)
                .color(COLOR_MOVE_ONLY_TARGET),
        )
    }
}

impl UISMapDraw for uis::Action {
    fn draw_map<GST: GSTrait>(
        &self,
        gs: &GST,
        _font_data: &FontData,
        _interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        ctx: &mut Context,
        t_map: Vector2,
    ) -> GameResult<()> {
        let source_location = self.source_location(gs);
        let source = source_location.to_pixel(HEX_SPACING);
        let target: MapLocation = self.target_location::<GST>();

        let hex_overlay = drawcache.hex_overlay(ctx)?;

        hex_overlay.draw(
            ctx,
            DrawParam::default()
                .dest(pair_to_point(source) + t_map)
                .color(COLOR_ATTACK_SOURCE),
        )?;

        let target_color = match self.action_status(gs) {
            uis::AttackStatus::OnOrigin => COLOR_ATTACK_TARGET_ON_SOURCE,
            uis::AttackStatus::ValidTarget => COLOR_ATTACK_TARGET_VALID_TARGET,
            uis::AttackStatus::InvalidTarget(_) => COLOR_ATTACK_TARGET_INVALID_TARGET,
        };

        for hex in source_location.line_to_iter(target) {
            hex_overlay.draw(
                ctx,
                DrawParam::default()
                    .dest(pair_to_point(hex.to_pixel(HEX_SPACING)) + t_map)
                    .color(COLOR_ACTION_PATH),
            )?;
        }

        let target = target.to_pixel(HEX_SPACING);
        hex_overlay.draw(
            ctx,
            DrawParam::default()
                .dest(pair_to_point(target) + t_map)
                .color(target_color),
        )?;
        Ok(())
    }
}

impl uis::Action {
    pub(super) fn action_box<GST: GSTrait>(&self, font_data: &FontData, gs: &GST) -> Vec<Text> {
        let lines = vec![match self.action_status(gs) {
            uis::AttackStatus::OnOrigin => "On origin".to_string(),
            uis::AttackStatus::ValidTarget => "Valid target".to_string(),
            uis::AttackStatus::InvalidTarget(reason) => match reason {
                ActionTargetingCombaterError::Hostile => "Cannot target hostiles".to_string(),
                ActionTargetingCombaterError::Friendly => "Cannot target friendlies".to_string(),
                ActionTargetingCombaterError::Oneself => "Cannot target yourself".to_string(),
                ActionTargetingCombaterError::NoSolid => {
                    "Cannot target unsolid targets".to_string()
                }
                ActionTargetingCombaterError::NoTarget => "Needs a target".to_string(),
                ActionTargetingCombaterError::Location(ActionTargetingLocationError::MaxRange) => {
                    "Target out of range".to_string()
                }
                ActionTargetingCombaterError::Location(ActionTargetingLocationError::MinRange) => {
                    "Target too close".to_string()
                }
                ActionTargetingCombaterError::Location(
                    ActionTargetingLocationError::Impassable,
                ) => "Cannot target impassable terrain".to_string(),
                ActionTargetingCombaterError::Location(ActionTargetingLocationError::Occupied) => {
                    "Cannot target occupied terrain".to_string()
                }
            },
        }];

        lines
            .iter()
            .map(|text| Text::new(TextFragment::from(text.clone()).font(font_data.char_cache)))
            .collect()
    }
    pub(super) fn draw_action_box<GST: GSTrait>(
        &self,
        font_data: &FontData,
        ctx: &mut Context,
        t: Vector2,
        drawcache: &mut Option<Vec<Text>>,
        gs: &GST,
    ) -> GameResult<()> {
        let lines = drawcache.get_or_insert_with(|| self.action_box(font_data, gs));
        for (i, line) in lines.iter().enumerate() {
            line.draw(
                ctx,
                DrawParam::default().dest(Point2::new(0.0, i as f32 * 20.0) + t),
            )?;
        }
        Ok(())
    }
}

fn rescale(texture_data: &NamedTextureDataFull) -> Vector2 {
    [
        HEX_HEIGHT as f32 / texture_data.texture_data.width() as f32,
        HEX_HEIGHT as f32 / texture_data.texture_data.height() as f32,
    ]
    .into()
}

impl MapElement<'_> {
    pub(super) fn draw(&self, hex: MapLocation, ctx: &mut Context, t: Vector2) -> GameResult<()> {
        let target = pair_to_point(hex.to_pixel(HEX_SPACING)) + t;
        trace!("mapelement: {:?}", hex);

        for texture_data in &self.texture_data {
            texture_data.texture_data.draw(
                ctx,
                DrawParam::default()
                    .dest(target)
                    .scale(rescale(texture_data)),
            )?
        }
        Ok(())
    }
}
