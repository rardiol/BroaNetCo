/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use ggez::graphics::{Color, DrawParam, Drawable, Text, TextFragment};
use ggez::Context;
use ggez::GameResult;
use std::borrow::Cow;
type Vector2 = nalgebra::Vector2<f32>;
type Point2 = nalgebra::Point2<f32>;
use either::*;

use super::super::input::uis;
use super::super::wrappers::*;
use crate::game;
use crate::game::ai;
use crate::game::map::MapLocation;
use crate::game::{ActionTarget, CombaterID, Command, PlayingID, AI, GS};
use crate::ClientMode;
use num::traits::ToPrimitive;

use super::*;

const LOG_LINES: usize = 5;
const PLAY_ORDER_BOX_WIDTH: f32 = 120.0;
const COM_FULL_BOX_Y_POS: f32 = 120.0;

// impl Into<Color> for FactionColor {
//     fn into(self) -> Color {
//         Color::new(
//             self.r.into_inner(),
//             self.g.into_inner(),
//             self.b.into_inner(),
//             self.a.into_inner(),
//         )
//     }
// }

//fn localize<T>(x: T, bundle: &FluentBundle) -> &str
macro_rules! localize {
    ( $bundle:expr, $x:expr ) => {{
        localize_args($bundle, $x, None)
    }};
}

fn localize_args<'a>(
    bundle: &'a FluentBundle,
    msg: &'a str,
    args: Option<&'a FluentArgs>,
) -> Cow<'a, str> {
    let mut errors = vec![];
    let ret = bundle
        .get_message(msg)
        .map(|msg| {
            bundle.format_pattern(
                msg.value().expect("message has no value"),
                args,
                &mut errors,
            )
        })
        .unwrap_or_else(|| {
            warn!("missing message from fluent_bundle {:?}", msg);
            Cow::Borrowed(msg)
        });
    for e in errors {
        error!("localize err: {:?}", e);
    }
    ret
}

/// Converts game args to FluentArgs
fn fconv(inp: &<game::ArgsI18N as std::ops::Deref>::Target) -> FluentArgs<'_> {
    let mut ret = FluentArgs::new();
    for el in inp {
        match el.1 {
            Left(ref string) => ret.set(&el.0, string.as_str()),
            Right(int) => ret.set(&el.0, int),
        };
    }
    ret
}

pub(crate) fn current_local_player<GST: GSTrait, Client>(
    playing_as: &ClientMode<Client>,
    gs: &GST,
) -> Option<FactionID> {
    match playing_as {
        ClientMode::Client { playing_as, .. } => Some(*playing_as),
        ClientMode::Hotseat => match gs.get_playing() {
            PlayingID::Faction(comid) | PlayingID::FactionAuto(comid) => {
                Some(gs.com(*comid).faction)
            }
            PlayingID::Choice { faction, .. } => Some(*faction),
            // TODO: single player?
            _ => None,
        },
    }
}

impl GSDrawInterface for GS<NamedTextureDataFull> {}
impl<'g> GSDrawInterface for GSPov<'g, NamedTextureDataFull> {}

pub trait GSDrawInterface: GSTrait<NTD = NamedTextureDataFull> + Sized {
    fn draw_log(
        &self,
        font_data: &FontData,
        fluent_bundle: &FluentBundle,
        drawcache: &mut Option<Vec<(Text, Color)>>,
        ctx: &mut Context,
        t: Vector2,
    ) -> GameResult<()> {
        // TODO: get_or_insert_with_result
        let logtexts = drawcache.get_or_insert_with(|| {
            self.get_log()
                // TODO: hide id number from player GsPov
                .map(|x| x.1)
                .rev()
                .filter(|msg| msg.priority() >= game::CombatLogEntryPriority::Middle)
                .take(LOG_LINES)
                .map(|gl| {
                    (
                        Text::new(
                            TextFragment::from(CombatLogEntry::from(gl).text(self, fluent_bundle))
                                .font(font_data.char_cache),
                        ),
                        CombatLogEntry::from(gl).color(),
                    )
                })
                .collect()
        });

        for (i, &(ref tex, color)) in logtexts.iter().enumerate() {
            tex.draw(
                ctx,
                DrawParam::default()
                    .dest(Point2::new(0.0, 0.0 + LINE_HEIGHT * i as f32) + t)
                    .color(color),
            )?;
        }
        Ok(())
    }

    fn draw_tick(
        &self,
        font_data: &FontData,
        drawcache: &mut Option<Text>,
        fluent_bundle: &FluentBundle,
        ctx: &mut Context,
        t: Vector2,
    ) -> GameResult<()> {
        let ticktext = drawcache.get_or_insert_with(|| {
            Text::new(
                TextFragment::from(
                    localize_args(
                        fluent_bundle,
                        "gs-tick-is",
                        Some(&fluent_args!("tick" => self.tick())),
                    )
                    .as_ref(),
                )
                .font(font_data.char_cache),
            )
        });
        ticktext.draw(ctx, DrawParam::default().dest(Point2::new(0.0, 0.0) + t))
    }

    fn play_order_box(
        &self,
        comid: CombaterID,
        font_data: &FontData,
        fluent_bundle: &FluentBundle,
    ) -> GameResult<(Text, Text, f32, Color)> {
        let com = self.com(comid);
        Ok((
            Text::new(
                TextFragment::from(format!(
                    "{}, {}",
                    localize!(fluent_bundle, &com.name),
                    com.faction
                ))
                .font(font_data.char_cache),
            ),
            Text::new(
                TextFragment::from(
                    localize_args(
                        fluent_bundle,
                        "com-tick-is",
                        Some(&fluent_args!("com_tick" => com.tick)),
                    )
                    .as_ref(),
                )
                .font(font_data.char_cache),
            ),
            com.hp().ratio().to_f32().unwrap(),
            self.fac(com.faction).color.dissolve().into(),
        ))
    }

    fn draw_play_order(
        &self,
        font_data: &FontData,
        fluent_bundle: &FluentBundle,
        drawcache: &mut DrawCache,
        ctx: &mut Context,
        t: Vector2,
    ) -> GameResult<()> {
        let vo = self.play_order();
        for (i, comtext) in drawcache
            .play_order
            .get_or_insert_with_result(|| {
                vo.into_iter()
                    .map(|(cid, _)| self.play_order_box(cid, font_data, fluent_bundle))
                    .collect::<GameResult<Vec<(_, _, _, _)>>>()
            })?
            .iter()
            .enumerate()
        {
            Combater::draw_play_order_box(
                ctx,
                Vector2::new(i as f32 * PLAY_ORDER_BOX_WIDTH, 0.0) + t,
                comtext,
            )?;
        }
        Ok(())
    }

    fn draw_mouse(
        ctx: &mut Context,
        mousexy: (f32, f32),
        font_data: &FontData,
        drawcache: &mut Option<(Text, Text)>,
        map_trans: Vector2,
        transform: Vector2,
    ) -> GameResult<()> {
        // TODO: fix for recentered map
        let a: GameResult<&mut (Text, Text)> = drawcache.get_or_insert_with_result(|| {
            let t = pair_to_point((mousexy.0 as f32, mousexy.1 as f32));
            let t = t - map_trans - Vector2::new(HEX_X_CENTER as f32, HEX_HEIGHT as f32 / 2.0);
            Ok((
                Text::new(
                    TextFragment::from(format!("X, Y: {:?}", mousexy)).font(font_data.char_cache),
                ),
                Text::new(
                    TextFragment::from(format!(
                        "{:?}",
                        MapLocation::from_pixel(t[0], t[1], HEX_SPACING)
                    ))
                    .font(font_data.char_cache),
                ),
            ))
        });
        let &mut (ref t1, ref t2) = a?;
        t1.draw(
            ctx,
            DrawParam::default().dest(Point2::new(0.0, 0.0) + transform),
        )?;
        t2.draw(
            ctx,
            DrawParam::default().dest(Point2::new(0.0, LINE_HEIGHT) + transform),
        )?;
        Ok(())
    }

    fn draw_status<NetClient, MUIS: UISInterfaceDraw>(
        &self,
        uis: &MUIS,
        playing_as: &ClientMode<NetClient>,
        font_data: &FontData,
        interface_data: &InterfaceData,
        fluent_bundle: &FluentBundle,
        drawcache: &mut DrawCache,
        ctx: &mut Context,
        left_text_trans: Vector2,
        right_text_trans: Vector2,
    ) -> GameResult<()> {
        let statusvec = {
            self.draw_uis_interface(
                uis,
                font_data,
                interface_data,
                drawcache,
                fluent_bundle,
                ctx,
                left_text_trans,
                right_text_trans,
            )?;
            // TODO: ? requires the type annotation, why?
            let a: GameResult<&mut Vec<(Text, Color)>> =
                drawcache.status.get_or_insert_with_result(|| {
                    let vec: Vec<(String, Color)> = match self.get_playing() {
                        PlayingID::Interrupt => {
                            vec![("Processing turn...".to_owned(), COLOR_INTERRUPT)]
                        }
                        PlayingID::Winner(winner) => {
                            vec![(format!("{} Won", self.fac(*winner).name), COLOR_WHITE)]
                        }
                        PlayingID::FactionAuto(comid) => {
                            let facid = self.com(*comid).faction;
                            vec![(
                                format!("{} Acting auto", self.fac(facid).name),
                                self.fac(facid).color.dissolve().into(),
                            )]
                        }
                        PlayingID::Choice {
                            faction,
                            text,
                            choices,
                        } => match current_local_player(playing_as, self) {
                            Some(local_player) if local_player == *faction => {
                                let faction = self.fac(*faction);
                                let mut vec = vec![
                                    (
                                        format!("Your Turn(Choice) ({})", faction.name),
                                        faction.color.dissolve().into(),
                                    ),
                                    (text.clone(), COLOR_WHITE),
                                ];
                                vec.extend(choices.iter().enumerate().map(
                                    |(number, (name, _))| {
                                        (format!("{} - {}", number, name), COLOR_WHITE)
                                    },
                                ));
                                vec
                            }
                            _ => vec![(
                                format!("Waiting for {} to make a choice", self.fac(*faction).name),
                                self.fac(*faction).color.dissolve().into(),
                            )],
                        },
                        PlayingID::Faction(comid) => {
                            let facid = self.com(*comid).faction;
                            match current_local_player(playing_as, self) {
                                Some(local_player) if local_player == facid => vec![(
                                    format!("Your Turn ({})", self.fac(facid).name),
                                    self.fac(facid).color.dissolve().into(),
                                )],
                                _ => vec![(
                                    format!("Waiting for {} to act", self.fac(facid).name),
                                    self.fac(facid).color.dissolve().into(),
                                )],
                            }
                        }
                    };

                    Ok(vec
                        .into_iter()
                        .map(|(text, color)| {
                            (
                                Text::new(TextFragment::from(text).font(font_data.char_cache)),
                                color,
                            )
                        })
                        .collect())
                });
            a?
        };

        for (i, &(ref tex, color)) in statusvec.iter().enumerate() {
            tex.draw(
                ctx,
                DrawParam::default()
                    .dest(Point2::new(0.0, 0.0 + LINE_HEIGHT * i as f32) + left_text_trans)
                    .color(color),
            )?;
        }
        Ok(())
    }

    fn draw_uis_interface<MUIS: UISInterfaceDraw>(
        &self,
        uis: &MUIS,
        font_data: &FontData,
        interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        fluent_bundle: &FluentBundle,
        ctx: &mut Context,
        left_box_trans: Vector2,
        right_box_trans: Vector2,
    ) -> GameResult<()> {
        match (uis.source_com(self), uis.target_com(self)) {
            (Some(com1), Some(com2)) if com1 == com2 => {
                self.com_draw_full_box(
                    com1,
                    font_data,
                    fluent_bundle,
                    ctx,
                    Vector2::new(0.0, COM_FULL_BOX_Y_POS) + left_box_trans,
                    &mut drawcache.source_com,
                )?;
                self.com_draw_extra_box(
                    com1,
                    font_data,
                    fluent_bundle,
                    ctx,
                    Vector2::new(0.0, COM_FULL_BOX_Y_POS) + right_box_trans,
                    &mut drawcache.target_com,
                )?;
            }
            (Some(com), None) | (None, Some(com)) => {
                self.com_draw_full_box(
                    com,
                    font_data,
                    fluent_bundle,
                    ctx,
                    Vector2::new(0.0, COM_FULL_BOX_Y_POS) + left_box_trans,
                    &mut drawcache.source_com,
                )?;
                self.com_draw_extra_box(
                    com,
                    font_data,
                    fluent_bundle,
                    ctx,
                    Vector2::new(0.0, COM_FULL_BOX_Y_POS) + right_box_trans,
                    &mut drawcache.target_com,
                )?;
            }
            (Some(com1), Some(com2)) => {
                self.com_draw_full_box(
                    com1,
                    font_data,
                    fluent_bundle,
                    ctx,
                    Vector2::new(0.0, COM_FULL_BOX_Y_POS) + left_box_trans,
                    &mut drawcache.source_com,
                )?;
                self.com_draw_full_box(
                    com2,
                    font_data,
                    fluent_bundle,
                    ctx,
                    Vector2::new(0.0, COM_FULL_BOX_Y_POS) + right_box_trans,
                    &mut drawcache.target_com,
                )?;
            }
            (None, None) => (),
        }

        uis.draw_box(
            self,
            font_data,
            interface_data,
            drawcache,
            ctx,
            left_box_trans,
            right_box_trans,
        )?;

        let main_target_loc = {
            uis.main_target()
                .right_and_then(|target| Left(self.com(target).location.coord))
                .into_inner()
        };
        let source_com_lines =
            (drawcache.source_com.as_ref().map(|x| x.len()).unwrap_or(0) + 3) as f32;
        trace!("source_com_lines = {}", source_com_lines);
        MapElement::from(&self.get_map()[main_target_loc]).draw_full_box(
            font_data,
            ctx,
            left_box_trans + Vector2::new(0.0, COM_FULL_BOX_Y_POS + source_com_lines * LINE_HEIGHT),
            &mut drawcache.target_map_element,
        )?;
        Ok(())
    }
    fn com_display_command(&self, comid: CombaterID) -> String {
        // TODO: rng should come from somewhere?
        // let mut rng = rand_xorshift::XorShiftRng::from_entropy();
        // use rand::SeedableRng;
        // use crate::game::number::RngTrait as NumberRngTrait;
        match &self.com(comid).command {
            Command::Direct => "Direct".to_owned(),
            Command::AI(AI::Deterministic(ai::Deterministic::Nop)) => "Do nothing".to_owned(),
            Command::AI(AI::Random(_)) => "Random".to_owned(),
            Command::Condition {
                condition,
                on_fail: _,
                on_success: _,
            } => format!(
                "Conditional: {:?}",
                condition /* TODO
                          "Morale: {:.1}%",
                          chance.value_rng(self, &mut rng, comid).to_f32().unwrap() * 100.0,
                          */
            ),
            Command::SingleTarget {
                target,
                attack_action: _,
                on_loss_of_target: _,
            } => format!("Targeting {:?}", self.com(*target).name),
        }
    }

    fn com_full_box(
        &self,
        comid: CombaterID,
        font_data: &FontData,

        fluent_bundle: &FluentBundle,
    ) -> Vec<Text> {
        let com = self.com(comid);
        let (_distribute, total_size, target_size) = com.parts.distribute();
        let mut lines = vec![
            format!(
                "{}, Fac {}",
                localize_args(
                    fluent_bundle,
                    "com-name",
                    Some(&fluent_args!("com_name" => com.name.clone()))
                ),
                com.faction
            ),
            format!("Tick: {}, Hp: {}", com.tick, com.hp()),
            localize!(fluent_bundle, "units").to_string(),
        ];
        for (part_name, amount) in com
            .parts_live()
            .map(|(_partid, x)| x.name.clone())
            .collect::<counter::Counter<_>>()
            .most_common_ordered()
        {
            lines.push(
                localize_args(
                    fluent_bundle,
                    "part-count",
                    Some(&fluent_args!("part_name" => part_name, "amount" => amount)),
                )
                .to_string(),
            );
        }
        lines.push(format!("Command: {:?}", self.com_display_command(comid)));
        lines.extend(vec![
            format!(
                "Width: {}, Density: {:.1}%",
                total_size,
                (target_size as f32 / total_size as f32) * 100.0
            ),
            "Statuses:".to_owned(),
            format!("Detection: {:?}", self.com_detections(comid)),
            format!("Concealment: {:?}", self.com_concealments(comid)),
        ]);
        // TODO: more complete info
        for status in &com.statuses {
            lines.push(status.full_name(self, comid).to_string());
            for action in status.all_actions(self, comid) {
                let (first_char, rest_name) = action.name.split_at(1); //TODO: char boundary
                lines.push(format!("- ({}){}", first_char, rest_name));
            }
        }

        // TODO: make showing optional
        lines.extend(
            com.edata
                .0
                .iter()
                .map(|(id, edatastat)| format!("{}: {}", id, edatastat)),
        );

        lines
            .iter()
            .map(|text| Text::new(TextFragment::from(text.clone()).font(font_data.char_cache)))
            .collect()
    }

    fn com_draw_full_box(
        &self,
        comid: CombaterID,
        font_data: &FontData,
        fluent_bundle: &FluentBundle,
        ctx: &mut Context,
        t: Vector2,
        drawcache: &mut Option<Vec<Text>>,
    ) -> GameResult<()> {
        let com = self.com(comid);
        let lines =
            drawcache.get_or_insert_with(|| self.com_full_box(comid, font_data, fluent_bundle));
        bar_horizontal(
            ctx,
            10.0,
            100.0,
            com.hp().ratio().to_f32().unwrap(),
            t,
            COLOR_GREEN,
            COLOR_WHITE,
        )?;
        for (i, line) in lines.iter().enumerate() {
            line.draw(
                ctx,
                DrawParam::default()
                    .dest(Point2::new(0.0, i as f32 * LINE_HEIGHT + LINE_HEIGHT) + t),
            )?;
        }
        Ok(())
    }

    fn com_extra_box(
        &self,
        comid: CombaterID,
        font_data: &FontData,

        fluent_bundle: &FluentBundle,
    ) -> Vec<Text> {
        let com = self.com(comid);
        let mut lines = vec![
            format!(
                "Com: {}, Fac {}",
                localize!(fluent_bundle, &com.name),
                com.faction
            ),
            "Units:".to_string(),
        ];
        lines.extend(com.parts().map(|(_partid, part)| {
            let hp = if let Some(hp) = part.hp {
                format!("HP{},", hp)
            } else {
                String::new()
            };
            let partsize = if let Some(size) = part.size {
                format!("size:{}", size)
            } else {
                String::new()
            };
            format!("{}: {}{}", part.name, hp, partsize)
        }));
        lines
            .iter()
            .map(|text| Text::new(TextFragment::from(text.clone()).font(font_data.char_cache)))
            .collect()
    }

    fn com_draw_extra_box(
        &self,
        comid: CombaterID,
        font_data: &FontData,
        fluent_bundle: &FluentBundle,
        ctx: &mut Context,
        t: Vector2,
        drawcache: &mut Option<Vec<Text>>,
    ) -> GameResult<()> {
        let lines =
            drawcache.get_or_insert_with(|| self.com_extra_box(comid, font_data, fluent_bundle));
        for (i, line) in lines.iter().enumerate() {
            line.draw(
                ctx,
                DrawParam::default()
                    .dest(Point2::new(0.0, i as f32 * LINE_HEIGHT + LINE_HEIGHT) + t),
            )?;
        }
        Ok(())
    }
}

pub trait UISInterfaceDraw: uis::Trait {
    fn draw_box<GST: GSTrait>(
        &self,
        _gs: &GST,
        _font_data: &FontData,
        _interface_data: &InterfaceData,
        _drawcache: &mut DrawCache,
        _ctx: &mut Context,
        _left_box_trans: Vector2,
        _right_box_trans: Vector2,
    ) -> GameResult<()> {
        Ok(())
    }
}

impl UISInterfaceDraw for uis::Observer {}
impl UISInterfaceDraw for uis::FreeMove {}
impl UISInterfaceDraw for uis::Move {}
impl UISInterfaceDraw for uis::Action {
    fn draw_box<GST: GSTrait>(
        &self,
        gs: &GST,
        font_data: &FontData,
        _interface_data: &InterfaceData,
        drawcache: &mut DrawCache,
        ctx: &mut Context,
        _left_box_trans: Vector2,
        right_box_trans: Vector2,
    ) -> GameResult<()> {
        self.draw_action_box(
            font_data,
            ctx,
            // TODO
            Vector2::new(-35.0, -25.0) + right_box_trans,
            &mut drawcache.action_box,
            gs,
        )

        // TODO: reenable using a game.rs calculation
        // let mut hitchance = 1.0;
        // for hex in source_location.line_to(target).iter().skip(1) {
        //     hitchance *=
        //         (1000 - gs.map[*hex].protection.unwrap()) as f32 / 1000.0;
        // }
        // Text::new(
        //     ctx,
        //     &format!("Hitchance: {:.*}%", 1, hitchance * 100.0),
        //     &font_data.char_cache,
        // )?.draw(ctx, Point2::new(0.0, 20.0) + t_text, 0.0)?;
    }
}

impl Combater<'_> {
    fn draw_play_order_box(
        ctx: &mut Context,
        t: Vector2,
        &(ref line1, ref line2, health_bar_ratio, color): &(Text, Text, f32, Color),
    ) -> GameResult<()> {
        //graphics::set_color(ctx, color)?;
        line1.draw(
            ctx,
            DrawParam::default()
                .dest(Point2::new(0.0, 0.0) + t)
                .color(color),
        )?;
        line2.draw(
            ctx,
            DrawParam::default()
                .dest(Point2::new(0.0, LINE_HEIGHT) + t)
                .color(color),
        )?;
        bar_horizontal(
            ctx,
            10.0,
            100.0,
            health_bar_ratio,
            t + Vector2::new(0.0, 40.0),
            COLOR_GREEN,
            COLOR_WHITE,
        )?;
        Ok(())
    }
}

impl MapElement<'_> {
    pub(super) fn full_box(&self, font_data: &FontData) -> Vec<Text> {
        let mut lines = vec![
            format!("Name: {}, Tick Cost: {:?}", self.name, self.ticks),
            "Protection:".to_owned(),
            format!(
                "Behind: {:?}, Inside: {:?}",
                self.protection_behind, self.protection_inside
            ),
            format!("Detection: {:?}", self.detection),
            format!("Concealment: {:?}", self.concealment),
            format!("Path Concealment: {:?}", self.path_concealment),
        ];

        // TODO: make showing optional
        lines.extend(
            self.store
                .0
                .iter()
                .map(|(id, edataint)| format!("{}: {}", id, edataint)),
        );
        lines.extend(self.store.1.iter().map(|(id, edataratio)| {
            format!("{}: {:.1}%", id, edataratio.to_f32().unwrap() * 100.0)
        }));

        lines
            .iter()
            .map(|text| Text::new(TextFragment::from(text.clone()).font(font_data.char_cache)))
            .collect()
    }
    pub(super) fn draw_full_box(
        &self,
        font_data: &FontData,
        ctx: &mut Context,
        t: Vector2,
        drawcache: &mut Option<Vec<Text>>,
    ) -> GameResult<()> {
        let lines = drawcache.get_or_insert_with(|| self.full_box(font_data));
        for (i, line) in lines.iter().enumerate() {
            line.draw(
                ctx,
                DrawParam::default().dest(Point2::new(0.0, i as f32 * LINE_HEIGHT) + t),
            )?;
        }
        Ok(())
    }
}

impl EffectLog<'_> {
    fn text(&self, fluent_bundle: &FluentBundle) -> String {
        let mut string = String::new();
        if self.hp_diff.is_positive() {
            string.push_str(&format!("healed {} pts, ", self.hp_diff))
        } else if self.hp_diff.is_negative() {
            string.push_str(&format!("damaged {} pts, ", -self.hp_diff))
        }
        if self.hits > 0 {
            string.push_str(&format!("in {} hits, ,", self.hits))
        }
        if self.misses > 0 {
            string.push_str(&format!("and {} misses, ,", self.misses))
        }
        if self.ticks > 0 {
            string.push_str(&format!("advanced {} ticks,", self.ticks))
        }
        for (msg, args) in &self.text {
            string.push_str(&format!(
                ", {}",
                localize_args(fluent_bundle, msg, Some(&fconv(args)))
            ))
        }
        string
    }
}

impl CombatLogEntry<'_> {
    pub(super) fn color(&self) -> Color {
        CombatLogEntryPriority::from(&self.priority()).color()
    }

    pub(super) fn text<GST: GSTrait>(&self, gs: &GST, fluent_bundle: &FluentBundle) -> String {
        match self.0 {
            game::CombatLogEntry::Text((msg, args), _) => {
                localize_args(fluent_bundle, msg, Some(&fconv(args))).to_string()
            }
            game::CombatLogEntry::Move {
                source,
                direction,
                ticks,
            } => format!(
                "{:?} moved {:?} in {} ticks",
                gs.com(*source).name,
                direction,
                ticks
            ),
            game::CombatLogEntry::Action {
                source,
                ref action_name,
                ref targets,
                effect_log,
            } => format!(
                "Action {} from {} to {:?}, effect: {}",
                action_name,
                gs.com(*source).name,
                targets
                    .iter()
                    .map(|x| {
                        match x {
                            ActionTarget::Combater(comb_id) => gs.com(*comb_id).name.clone(),
                            ActionTarget::Terrain(maploc) => format!("{:?}", maploc),
                        }
                    })
                    .collect::<Vec<_>>(),
                EffectLog(effect_log).text(fluent_bundle) // TODO: multiple targets
            ),
            game::CombatLogEntry::SkipTick(comid, amount, _) => {
                format!("Combater {} skipped {} ticks", gs.com(*comid).name, amount)
            }

            game::CombatLogEntry::EndFactionTurn(comid, _) => format!(
                "Skipping ticks for faction {:?}, unit {}",
                gs.com(*comid).faction,
                gs.com(*comid).name
            ),
            game::CombatLogEntry::Choice {
                choice,
                faction,
                text,
                priority: _,
            } => format!(
                "{} picked \"{}\" on \"{}\"",
                gs.fac(*faction).name,
                choice,
                text
            ),
        }
    }
}

impl CombatLogEntryPriority<'_> {
    #[const_fn("1.46")]
    pub(super) const fn color(&self) -> Color {
        match self.0 {
            game::CombatLogEntryPriority::Low => COLOR_WHITE,
            game::CombatLogEntryPriority::Middle => COLOR_GREEN,
            game::CombatLogEntryPriority::High => COLOR_RED,
        }
    }
}
