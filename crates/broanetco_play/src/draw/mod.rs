/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

/// Drawing the interface
mod interface;
/// Drawing things on the game map
mod map;

use super::*;

use std::fmt::Debug;

use ggez::graphics::{Color, DrawParam, Drawable, Font, Image, Mesh, Rect, Text};
use ggez::{graphics, timer};
use ggez::{Context, GameResult};
type Vector2 = nalgebra::Vector2<f32>;
type Point2 = nalgebra::Point2<f32>;
use either::*;

use game::hex2d::Spacing;

use self::interface::*;
use self::map::*;
use super::MainState;
use crate::game::texture::*;
use crate::game::{DrawCacheTrait, FactionID, GSBaseTrait, GSPov, GSTrait, PlayingID, GS};
use crate::input::uis;
use crate::single_player;
use crate::util::OptionTrait;
use crate::ClientMode;

pub type DrawCacheOpt = DrawCache;

impl DrawCacheTrait for DrawCacheOpt {
    fn reset_game(&mut self) {
        // TODO: show the projected play order if action taken
        debug!("DrawCache.reset_game()");
        self.play_order = None;
        self.tick = None;
        self.log = None;
        self.status = None;
        self.reset_key();
    }
}

// TODO: add polygons, without positions, so they can be repositioned
#[derive(Clone, Debug, Default)]
pub struct DrawCache {
    source_com: Option<Vec<Text>>,
    target_com: Option<Vec<Text>>,
    target_map_element: Option<Vec<Text>>,
    play_order: Option<Vec<(Text, Text, f32, Color)>>,
    tick: Option<Text>,
    log: Option<Vec<(Text, Color)>>,
    status: Option<Vec<(Text, Color)>>,
    mouse: Option<(Text, Text)>,
    action_box: Option<Vec<Text>>,

    hex_overlay: Option<Mesh>,
    hex_border_overlay: Option<Mesh>,
}

#[derive(Debug)]
pub struct FontData {
    pub char_cache: Font,
}

impl DrawCache {
    #[allow(dead_code)]
    pub fn reset_all(&mut self) {
        debug!("DrawCache.reset_all()");
        self.reset_mouse();
        self.reset_game();
    }
    pub fn reset_key(&mut self) {
        debug!("DrawCache.reset_key()");
        self.source_com = None;
        self.target_com = None;
        self.target_map_element = None;
        self.action_box = None;
    }
    pub fn reset_mouse(&mut self) {
        trace!("DrawCache.reset_mouse()");
        self.mouse = None;
    }
}

pub(self) const LINE_HEIGHT: f32 = 20.0;

pub(self) const HEX_HEIGHT: f32 = 64.0;
pub(self) const HEX_X_CENTER: f32 = HEX_HEIGHT / 2.0;
pub(self) const HEX_SPACING: Spacing = Spacing::FlatTop(HEX_HEIGHT / 2.0);

pub(self) const COLOR_TRANSPARENT: Color = Color {
    r: 0.0,
    g: 0.0,
    b: 0.0,
    a: 0.0,
};
pub(self) const COLOR_WHITE: Color = Color {
    r: 1.0,
    g: 1.0,
    b: 1.0,
    a: 1.0,
};
pub(self) const COLOR_RED: Color = Color {
    r: 1.0,
    g: 0.0,
    b: 0.0,
    a: 1.0,
};
#[allow(dead_code)]
pub(self) const COLOR_BLUE: Color = Color {
    r: 0.0,
    g: 0.0,
    b: 1.0,
    a: 1.0,
};
pub(self) const COLOR_GREEN: Color = Color {
    r: 0.0,
    g: 1.0,
    b: 0.0,
    a: 1.0,
};

pub(self) const COLOR_GREY: Color = Color {
    r: 0.5,
    g: 0.5,
    b: 0.5,
    a: 1.0,
};

pub(self) const SELECTION_TRANSPARENCY: f32 = 0.4;
pub(self) const COLOR_VALID_SELECTION_TARGET: Color = Color {
    r: 0.1,
    g: 0.8,
    b: 0.1,
    a: SELECTION_TRANSPARENCY,
};
//green
pub(self) const COLOR_MOVE_ONLY_TARGET: Color = Color {
    r: 0.3,
    g: 0.3,
    b: 0.7,
    a: SELECTION_TRANSPARENCY,
}; //dark blue

pub(self) const COLOR_ATTACK_SOURCE: Color = Color {
    r: 0.4,
    g: 0.1,
    b: 0.1,
    a: SELECTION_TRANSPARENCY,
};
pub(self) const COLOR_ATTACK_TARGET_ON_SOURCE: Color = COLOR_TRANSPARENT;
pub(self) const COLOR_ATTACK_TARGET_VALID_TARGET: Color = Color {
    r: 0.9,
    g: 0.1,
    b: 0.1,
    a: SELECTION_TRANSPARENCY,
};
//dark blue
pub(self) const COLOR_ATTACK_TARGET_INVALID_TARGET: Color = Color {
    r: 0.1,
    g: 0.1,
    b: 0.9,
    a: SELECTION_TRANSPARENCY,
}; //dark blue

pub(self) const COLOR_ACTION_PATH: Color = Color {
    r: 0.4,
    g: 0.7,
    b: 0.4,
    a: SELECTION_TRANSPARENCY,
};

pub(self) const COLOR_OUT_OF_SIGHT: Color = Color {
    r: 0.0,
    g: 0.0,
    b: 0.0,
    a: 0.6,
};

pub(self) const COLOR_INTERRUPT: Color = COLOR_GREY;

pub(self) fn pair_to_point(pair: (f32, f32)) -> Point2 {
    Point2::new(pair.0, pair.1)
}

pub(self) fn pair_to_vec2(pair: (f32, f32)) -> Vector2 {
    Vector2::new(pair.0, pair.1)
}

pub(self) fn bar_horizontal(
    ctx: &mut Context,
    h: f32,
    w: f32,
    ratio: f32,
    t: Vector2,
    color_fill: Color,
    color_back: Color,
) -> GameResult<()> {
    let x = t.x;
    let y = t.y;
    graphics::Mesh::new_rectangle(
        ctx,
        graphics::DrawMode::fill(),
        graphics::Rect {
            x,
            y,
            h,
            w: w * ratio,
        },
        color_fill,
    )?
    .draw(ctx, DrawParam::default())?;
    graphics::Mesh::new_rectangle(
        ctx,
        graphics::DrawMode::stroke(1.0),
        graphics::Rect { x, y, h, w },
        color_back,
    )?
    .draw(ctx, DrawParam::default())?;
    Ok(())
}

#[derive(Debug, Clone)]
pub enum PoVs {
    Global,
    Faction(FactionID),
    Nothing,
}

pub trait UISDraw: UISInterfaceDraw + UISMapDraw {}
impl UISDraw for uis::FreeMove {}
impl UISDraw for uis::Observer {}
impl UISDraw for uis::Move {}
impl UISDraw for uis::Action {}

impl MainState {
    pub fn get_pov(&self) -> PoVs {
        // Observer game
        if self
            .gs
            .factions()
            .all(|(_facid, fac)| fac.faction_type.is_ai())
        {
            return PoVs::Global;
        }

        // Game over
        if let PlayingID::Winner(_) = self.gs.get_playing() {
            return PoVs::Global;
        }

        // Single player game
        if let Some(sp_facid) = single_player(&self.gs) {
            return PoVs::Faction(sp_facid);
        }
        // If the game is over, always show everything
        // On a hotseat game, the current player if it's not an AI
        // On a network game, always the local player
        match &self.mode {
            ClientMode::Hotseat => match self.gs.get_playing() {
                PlayingID::Choice { faction: facid, .. }
                    if !self.gs.fac(*facid).faction_type.is_ai() =>
                {
                    PoVs::Faction(*facid)
                }
                PlayingID::Faction(comid) | PlayingID::FactionAuto(comid)
                    if !self
                        .gs
                        .fac(self.gs.com(*comid).faction)
                        .faction_type
                        .is_ai() =>
                {
                    PoVs::Faction(self.gs.com(*comid).faction)
                }
                PlayingID::Faction(_)
                | PlayingID::Choice { .. }
                | PlayingID::Interrupt
                | PlayingID::FactionAuto(_) => PoVs::Nothing,
                PlayingID::Winner(_) => PoVs::Global,
            },
            ClientMode::Client { playing_as, .. } => match self.gs.get_playing() {
                PlayingID::Faction(_)
                | PlayingID::FactionAuto(_)
                | PlayingID::Interrupt
                | PlayingID::Choice { .. } => PoVs::Faction(*playing_as),
                PlayingID::Winner(_) => PoVs::Global,
            },
        }
    }

    pub fn draw2(&mut self, ctx: &mut Context) -> GameResult<()> {
        graphics::clear(ctx, graphics::Color::BLACK);
        let (w, h) = graphics::drawable_size(ctx);
        graphics::set_screen_coordinates(ctx, Rect::new(0.0, 0.0, w, h))?;
        let translation = Vector2::new(0.0, 0.0);

        let pov = self.get_pov();
        trace!("draw2: pov {:?}", pov);
        match (self.debug_mode, self.get_pov()) {
            (false, PoVs::Faction(pov_facid)) => {
                let gspov = GSPov::new(&self.gs, pov_facid);
                match &self.uis {
                    // TODO: clean up
                    uis::Enum::Observer(uis) => gspov.draw(
                        uis,
                        &self.mode,
                        &self.font_data,
                        &self.interface_data,
                        &self.fluent_bundle,
                        &mut self.drawcache,
                        self.mouse,
                        ctx,
                        translation,
                    ),
                    uis::Enum::Interactive(uis) => match uis {
                        uis::Interactive::FreeMove(uis) => gspov.draw(
                            uis,
                            &self.mode,
                            &self.font_data,
                            &self.interface_data,
                            &self.fluent_bundle,
                            &mut self.drawcache,
                            self.mouse,
                            ctx,
                            translation,
                        ),
                        uis::Interactive::Move(uis) => gspov.draw(
                            uis,
                            &self.mode,
                            &self.font_data,
                            &self.interface_data,
                            &self.fluent_bundle,
                            &mut self.drawcache,
                            self.mouse,
                            ctx,
                            translation,
                        ),
                        uis::Interactive::Action(uis) => gspov.draw(
                            uis,
                            &self.mode,
                            &self.font_data,
                            &self.interface_data,
                            &self.fluent_bundle,
                            &mut self.drawcache,
                            self.mouse,
                            ctx,
                            translation,
                        ),
                    },
                }?;
            }
            (false, PoVs::Global) | (true, _) => {
                match &self.uis {
                    uis::Enum::Observer(uis) => self.gs.draw(
                        uis,
                        &self.mode,
                        &self.font_data,
                        &self.interface_data,
                        &self.fluent_bundle,
                        &mut self.drawcache,
                        self.mouse,
                        ctx,
                        translation,
                    ),
                    uis::Enum::Interactive(uis) => match uis {
                        uis::Interactive::FreeMove(uis) => self.gs.draw(
                            uis,
                            &self.mode,
                            &self.font_data,
                            &self.interface_data,
                            &self.fluent_bundle,
                            &mut self.drawcache,
                            self.mouse,
                            ctx,
                            translation,
                        ),
                        uis::Interactive::Move(uis) => self.gs.draw(
                            uis,
                            &self.mode,
                            &self.font_data,
                            &self.interface_data,
                            &self.fluent_bundle,
                            &mut self.drawcache,
                            self.mouse,
                            ctx,
                            translation,
                        ),
                        uis::Interactive::Action(uis) => self.gs.draw(
                            uis,
                            &self.mode,
                            &self.font_data,
                            &self.interface_data,
                            &self.fluent_bundle,
                            &mut self.drawcache,
                            self.mouse,
                            ctx,
                            translation,
                        ),
                    },
                }?;
            }
            (false, PoVs::Nothing) => {
                // TODO
            }
        }

        graphics::present(ctx)?;

        self.frames += 1;
        if (self.frames % 100) == 0 {
            info!("FPS: {}", timer::fps(ctx));
        }

        timer::yield_now();

        Ok(())
    }
}

pub type TextureData = Image;

#[allow(dead_code)]
type NTDFGroup = NTDGroup<NamedTextureDataFull>;

use shrinkwraprs::Shrinkwrap;
#[derive(Debug, Clone, Hash, Shrinkwrap)]
pub struct NamedTextureDataFull(pub NamedTextureData<TextureData>);

impl NamedTextureDataFull {
    pub fn new_textured(asset_path: NTDAssetPath, texture_data: TextureData) -> Self {
        NamedTextureDataFull(NamedTextureData::new_textured(asset_path, texture_data))
    }
}

#[derive(Debug)]
pub struct InterfaceData {
    pub can_act_texture: TextureData,
    pub cannot_act_texture: TextureData,
    pub is_seen_texture: TextureData,
    pub is_not_seen_texture: TextureData,
}

impl GSDraw for GS<NamedTextureDataFull> {}
impl<'g> GSDraw for GSPov<'g, NamedTextureDataFull> {}

pub trait GSDraw: GSDrawMap + GSDrawInterface {
    fn draw<NetClient, MUIS: UISDraw>(
        &self,
        uis: &MUIS,
        playing_as: &ClientMode<NetClient>,
        font_data: &FontData,
        interface_data: &InterfaceData,
        fluent_bundle: &FluentBundle,
        drawcache: &mut DrawCache,
        mouse: (f32, f32),
        ctx: &mut Context,
        t: Vector2,
    ) -> GameResult<()> {
        let (w, h) = graphics::drawable_size(ctx);
        let centered_trans =
            Vector2::new(w / 2.0 - HEX_HEIGHT / 2.0, h / 2.0 - HEX_HEIGHT / 2.0) + t;
        let left_text_trans = Vector2::new(0.0, 60.0) + t;
        let right_text_trans = Vector2::new(w - 190.0, 60.0) + t;
        let bottom_text_trans = Vector2::new(0.0, h - 70.0) + t;

        let map_draw_range = {
            // (MAP_DRAW_RANGE*2+1)*HEX_HEIGHT <= MAP_DRAW_HEIGHT
            // MAP_DRAW_RANGE <= ((MAP_DRAW_HEIGHT/HEX_HEIGHT)-1)/2
            // TODO: HEX_WIDTH
            // (MAP_DRAW_RANGE*2+1)*HEX_HEIGHT <= MAP_DRAW_WIDTH
            let width_max = {
                let map_draw = right_text_trans.x - (left_text_trans.x + 225.0);
                let map_max_size = (map_draw / HEX_HEIGHT).floor() as i32;
                let max = (map_max_size - 1) / 2;
                debug!(
                    "map_draw_range: width: {:?} {:?} {:?}",
                    map_draw, map_max_size, max
                );
                max
            };
            let height_max = {
                let map_draw = bottom_text_trans.y - 100.0;
                let map_max_size = (map_draw / HEX_HEIGHT).floor() as i32;
                let max = (map_max_size - 1) / 2;
                debug!(
                    "map_draw_range: height: {:?} {:?} {:?}",
                    map_draw, map_max_size, max
                );
                max
            };
            width_max.min(height_max).max(0)
        };

        debug!(
            "
drawable_size: {:?}, trans: center: {:?}, left: {:?}, right: {:?}, bottom: {:?}",
            graphics::drawable_size(ctx),
            centered_trans,
            left_text_trans,
            right_text_trans,
            bottom_text_trans
        );

        //////////////////////////////////////////
        // Draw Interface
        Self::draw_mouse(
            ctx,
            mouse,
            font_data,
            &mut drawcache.mouse,
            centered_trans,
            Vector2::new(0.0, LINE_HEIGHT),
        )?;

        self.draw_status(
            uis,
            playing_as,
            font_data,
            interface_data,
            fluent_bundle,
            drawcache,
            ctx,
            left_text_trans,
            right_text_trans,
        )?;

        self.draw_tick(font_data, &mut drawcache.tick, fluent_bundle, ctx, t)?;

        self.draw_log(
            font_data,
            fluent_bundle,
            &mut drawcache.log,
            ctx,
            t + Vector2::new(170.0, 0.0),
        )?;

        self.draw_play_order(font_data, fluent_bundle, drawcache, ctx, bottom_text_trans)?;

        //////////////////////////////////////////
        // Draw map
        let map_draw_center = {
            // TODO
            uis.main_target()
                .right_and_then(|target| Left(self.com(target).location.coord))
                .into_inner()
        };
        self.draw_map(
            uis,
            font_data,
            interface_data,
            drawcache,
            map_draw_center,
            map_draw_range,
            ctx,
            centered_trans,
        )?;
        Ok(())
    }
}
