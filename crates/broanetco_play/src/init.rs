/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use super::texture::*;
use crate::draw::{FontData, InterfaceData};
use anyhow::Result as AResult;
use ggez::graphics::Font;
use ggez::Context;
use ggez::GameResult;

/// Load interface_data
pub fn interface_data(context: &mut Context) -> AResult<InterfaceData> {
    let can_act_texture = texturedata_from_path(context, "under_o.svg".as_ref())?;
    let cannot_act_texture = texturedata_from_path(context, "under_x.svg".as_ref())?;
    let is_seen_texture = texturedata_from_path(context, "under_right_open.svg".as_ref())?;
    let is_not_seen_texture = texturedata_from_path(context, "under_right_closed.svg".as_ref())?;

    Ok(InterfaceData {
        can_act_texture,
        cannot_act_texture,
        is_seen_texture,
        is_not_seen_texture,
    })
}

pub fn font_data(context: &mut Context) -> GameResult<FontData> {
    Ok(FontData {
        char_cache: Font::new(context, "/fonts/DejaVuSerif.ttf")?,
    })
}
