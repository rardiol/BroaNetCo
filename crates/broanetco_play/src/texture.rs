/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use std::convert::TryInto;
use std::io::Read;
use std::path::{Path, PathBuf};

use std::sync::Arc;

#[cfg(feature = "xcf")]
use xcf::Xcf;

use anyhow::Context as AContext;
use anyhow::Error as AError;
use anyhow::Result as AResult;
use cached::proc_macro::cached;

use ggez::graphics::Image;
use ggez::Context;

use crate::draw::{NamedTextureDataFull, TextureData};
use crate::game::*;

// Owns the path and convert the context to a usize (generally every process should
// only load images from the single graphics thread)
#[cached(
    key = "(usize, PathBuf)",
    convert = r#"{ (context as *const Context as usize, name.to_path_buf()) }"#,
    result
)]
pub(super) fn texturedata_from_path(context: &mut Context, name: &Path) -> AResult<TextureData> {
    let path = Path::new("/assets").join(name);
    debug!("texturedata_from_path: {:?} {:?}", name, path);
    match name.extension() {
        #[cfg(feature = "xcf")]
        Some(svgext) if svgext == Path::new("svg") => {
            let svgdpi = 96.0;
            let scale = 1.0;
            let mut svgstr = String::new();
            ggez::filesystem::open(context, path)?.read_to_string(&mut svgstr)?;
            let svg = nsvg::SvgImage::parse_str(&svgstr, nsvg::Units::Pixel, svgdpi)?;
            let (width, height, vec) = svg.rasterize_to_raw_rgba(scale)?;
            Ok(Image::from_rgba8(
                context,
                width.try_into()?,
                height.try_into()?,
                &vec,
            )?)
        }
        _ => Ok(Image::new(context, path)?),
    }
}

// TODO: no need for Arc here
// Owns the Path
#[cached(key = "PathBuf", convert = r#"{ file_path.to_path_buf() }"#, result)]
#[cfg(feature = "xcf")]
pub(super) fn load_xcf(file_path: &Path) -> AResult<Arc<Xcf>> {
    // TODO: xcf errors
    // TODO: use ggez loading
    let file_path = Path::new("./resources/assets").join(file_path);
    trace!("loading from {:?}", file_path);
    let xcf = Xcf::open(&file_path).context("failed to open xcf file")?;
    debug!(
        "Loaded xcf: {:?}, layers: {:?}",
        &file_path,
        xcf.layers.iter().map(|x| &x.name).collect::<Vec<_>>()
    );
    Ok(Arc::new(xcf))
}

#[cfg(feature = "xcf")]
pub(super) fn texturedata_from_xcf_layer(
    context: &mut Context,
    file_path: PathBuf,
    layer: &str,
) -> AResult<TextureData> {
    // TODO: xcf errors
    let xcf = load_xcf(&file_path)?;
    let layer = xcf.layer(layer).context("layer not found")?;

    // TODO: xcf and ggez crates could be made to work better together
    let rgbavec = layer.raw_sub_rgba_buffer(0, 0, 64, 64);
    Ok(Image::from_rgba8(context, 64, 64, &rgbavec)?)
}

impl TryIntoParams<Context, NamedTextureDataFull> for NamedTextureDataTransfer {
    type Error = AError;
    fn try_into_params(self, context: &mut Context) -> AResult<NamedTextureDataFull> {
        let texture_data = match &self.asset_path {
            NTDAssetPath::Image(path) => texturedata_from_path(context, path)?,
            #[cfg(feature = "xcf")]
            NTDAssetPath::XcfLayer { layer, file_path } => {
                texturedata_from_xcf_layer(context, file_path.clone(), layer)?
            }
        };
        Ok(NamedTextureDataFull::new_textured(
            self.asset_path,
            texture_data,
        ))
    }
}

impl crate::game::NTDTrait for NamedTextureDataFull {}

impl crate::game::TryIntoParams<(), NamedTextureDataTransfer> for NamedTextureDataFull {
    type Error = Void;
    fn try_into_params(self, (): &mut ()) -> Result<NamedTextureDataTransfer, Self::Error> {
        Ok(NamedTextureDataTransfer::new(self.0.asset_path))
    }
}
