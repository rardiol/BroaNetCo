/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

#[macro_use]
extern crate log as debug_log;
#[macro_use]
extern crate derivative;
#[macro_use]
extern crate const_fn;

extern crate broanetco_game as game;
extern crate broanetco_net as net;
extern crate broanetco_util as util;

use crate::game::FactionID;
use std::fmt::Debug;
use std::path::{Path, PathBuf};

#[derive(Debug)]
pub enum ClientMode<NetClient> {
    Hotseat,
    Client {
        playing_as: FactionID,
        client: NetClient,
    },
}

/// Graphical drawing to the screen
pub mod draw;
/// Initialization
pub mod init;
/// Input from mouse an keyboard
pub mod input;
/// Sound control
pub mod sound;
/// Texture loading
pub mod texture;
/// Wrappers to allow impl
pub mod wrappers;

use std::io::Read;

use ggez::event::{EventLoop, KeyCode};
use ggez::input::keyboard::KeyMods;
use ggez::{conf, event, graphics};
use ggez::{Context, ContextBuilder, GameError, GameResult};

use fluent::{fluent_args, FluentArgs, FluentResource};
pub use unic_langid::LanguageIdentifier;
type FluentBundle = fluent::FluentBundle<FluentResource>;

use either::*;

use crate::draw::{DrawCacheOpt, FontData, InterfaceData, NamedTextureDataFull};

use crate::game::replay::ReplayLoggerControl;
use crate::game::{GSBaseTrait, TryIntoParams, GS};

use crate::input::uis;
use crate::sound::SoundControl;

/// ((width, height), (fullscreen, offvsync))
pub type GraphicsSettings = ((f32, f32), (bool, bool));

/// The global state for an interactive graphics game
pub struct MainState {
    gs: GS<NamedTextureDataFull>,
    uis: uis::Enum,
    font_data: FontData,
    mouse: (f32, f32),
    pub mode: ClientMode<net::Client<net::client::states::Game>>,
    frames: usize,
    sound_ch: SoundControl,
    drawcache: DrawCacheOpt,
    interface_data: InterfaceData,
    fluent_bundle: FluentBundle,
    replay_logger: ReplayLoggerControl,
    debug_mode: bool,
}

fn get_fluent_bundle(
    lang_id: LanguageIdentifier,
    ctx: &mut Context,
    game_name: &str,
) -> FluentBundle {
    let (lang_id, mut file) =
        ggez::filesystem::open(ctx, format!("/fluent/{}/common.ftl", lang_id))
            .map(|f| (lang_id.clone(), f))
            .unwrap_or_else(|err| {
                warn!(
                    "lang_id {} not found with error {:?}, using en-US",
                    err, lang_id
                );
                (
                    "en-US".parse().unwrap(),
                    ggez::filesystem::open(ctx, "/fluent/en-US/common.ftl".to_string()).unwrap(),
                )
            });
    let mut buffer = String::new();
    file.read_to_string(&mut buffer)
        .expect(".ftl file not valid UTF8");
    let common_resource = FluentResource::try_new(buffer).unwrap();
    let mut ret = FluentBundle::new(vec![lang_id.clone(), "en-US".parse().unwrap()]);
    // GGEZ doesn't support it
    ret.set_use_isolating(false);
    ret.add_resource(common_resource).unwrap();

    if !game_name.is_empty() {
        let (_lang_id, mut file) =
            ggez::filesystem::open(ctx, format!("/fluent/{}/{}.ftl", lang_id, game_name))
                .map(|f| (lang_id.clone(), f))
                .unwrap_or_else(|err| {
                    warn!(
                        "lang_id {} not found with error {:?}, using en-US",
                        err, lang_id
                    );
                    (
                        "en-US".parse().unwrap(),
                        ggez::filesystem::open(ctx, format!("/fluent/en-US/{}.ftl", game_name))
                            .unwrap(),
                    )
                });
        let mut buffer = String::new();
        file.read_to_string(&mut buffer)
            .expect(".ftl file not valid UTF8");
        let resource = FluentResource::try_new(buffer).unwrap();
        ret.add_resource_overriding(resource);
    }

    ret
}

impl MainState {
    pub fn new(
        font_data: FontData,
        gs: GS<NamedTextureDataFull>,
        sound_ch: SoundControl,
        interface_data: InterfaceData,
        replay_logger: ReplayLoggerControl,
        lang_id: LanguageIdentifier,
        ctx: &mut Context,
    ) -> Self {
        let fluent_bundle = get_fluent_bundle(lang_id, ctx, &gs.name);
        graphics::set_window_title(ctx, &format!("BroaNetCo - {}", gs.name));
        Self {
            gs,
            font_data,
            uis: Default::default(),
            mouse: (0.0, 0.0),
            mode: ClientMode::Hotseat,
            frames: 0,
            sound_ch,
            drawcache: Default::default(),
            interface_data,
            fluent_bundle,
            replay_logger,
            debug_mode: false,
        }
    }
    pub fn run(self, ctx: Context, eventloop: EventLoop<()>) -> ! {
        ggez::event::run(ctx, eventloop, self)
    }

    pub fn current_human_player(&self) -> Option<FactionID> {
        match &self.mode {
            ClientMode::Client { playing_as, .. } => Some(*playing_as),
            ClientMode::Hotseat => {
                if let Some(sp_facid) = single_player(&self.gs) {
                    return Some(sp_facid);
                }
                use game::PlayingID::*;
                match self.gs.get_playing() {
                    Faction(comid) => {
                        let facid = self.gs.com(*comid).faction;
                        if !self.gs.fac(facid).faction_type.is_ai() {
                            Some(facid)
                        } else {
                            None
                        }
                    }
                    Choice { faction, .. } => {
                        if !self.gs.fac(*faction).faction_type.is_ai() {
                            Some(*faction)
                        } else {
                            None
                        }
                    }
                    FactionAuto(_) | Winner(_) | Interrupt => None,
                }
            }
        }
    }

    fn uis_update(&self) -> uis::Enum {
        if let uis::Enum::Observer(_) = self.uis {
            if let game::PlayingID::Faction(target) = self.gs.playing {
                if let Some(human_facid) = self.current_human_player() {
                    if human_facid == self.gs.com(target).faction {
                        return uis::Enum::Interactive(uis::Interactive::Move(uis::Move {
                            target,
                        }));
                    }
                }
            }
        }
        self.uis
    }
}

fn single_player<GST: game::GSTrait>(gs: &GST) -> Option<FactionID> {
    let mut humans = gs
        .factions()
        .filter(|(_facid, fac)| !fac.faction_type.is_ai());
    if let Some((sp_facid, _sp_fac)) = humans.next() {
        if humans.next().is_none() {
            return Some(sp_facid);
        }
    }
    None
}

impl event::EventHandler<GameError> for MainState {
    fn update(&mut self, ctx: &mut Context) -> GameResult {
        trace!("update called");
        // TODO: improve uis_hint
        if self.gs.needs_update() {
            self.gs.update(
                &mut Some(&mut self.drawcache),
                Some(self.sound_ch.clone()),
                Some(self.replay_logger.clone()),
            );
            self.uis = self.uis_update();
        }
        if let ClientMode::Client { ref client, .. } = self.mode {
            loop {
                match client
                    .process_msgs(|| self.gs.crc32())
                    .expect("failed to process network message")
                {
                    Some(Left((_origin, msg))) => {
                        //TODO: use origin to check for correctness and noncheating
                        info!("gs crc32: {:?}", util::crc32(&self.gs));
                        self.gs.gsapply(
                            &msg,
                            &Some(self.sound_ch.clone()),
                            &mut Some(&mut self.drawcache),
                            &Some(self.replay_logger.clone()),
                        )
                    }
                    Some(Right(msg)) => {
                        let msg = msg.try_into_params(ctx).expect("Failed to load textures");
                        info!("gs crc32: {:?}", util::crc32(&self.gs));
                        self.gs.god_gsapply(
                            msg,
                            &Some(self.sound_ch.clone()),
                            &mut Some(&mut self.drawcache),
                            &Some(self.replay_logger.clone()),
                        )
                    }
                    None => break,
                };
                // TODO: receive a Vec of (G)GSAs so it knows when it should update in the mid
                self.gs.update(
                    &mut Some(&mut self.drawcache),
                    Some(self.sound_ch.clone()),
                    Some(self.replay_logger.clone()),
                );
                self.uis_update();
            }
        }
        Ok(())
    }
    fn draw(&mut self, ctx: &mut Context) -> GameResult {
        graphics::set_window_title(ctx, &format!("BroaNetCo - {}", self.gs.name));
        self.draw2(ctx)
    }
    fn key_down_event(
        &mut self,
        _ctx: &mut Context,
        keycode: KeyCode,
        keymod: KeyMods,
        repeat: bool,
    ) {
        self.key_down_event2(_ctx, keycode, keymod, repeat)
    }
    fn mouse_motion_event(&mut self, ctx: &mut Context, x: f32, y: f32, xrel: f32, yrel: f32) {
        self.mouse_motion_event2(ctx, x, y, xrel, yrel)
    }
    fn quit_event(&mut self, _ctx: &mut Context) -> bool {
        false
    }
}

/// Initialization of graphics
pub fn graphics_init(
    ((w, h), (fullscreen, vsync)): GraphicsSettings,
    manifest_dir: &Path,
) -> GameResult<(Context, EventLoop<()>)> {
    let fullscreen_mode = if fullscreen {
        conf::FullscreenType::Desktop
    } else {
        conf::FullscreenType::Windowed
    };
    info!(
        "Starting with options: {}x{}, fullscreen: {}, vsync: {}",
        w, h, fullscreen, vsync
    );

    let mut cb = ContextBuilder::new("BroaNetCo", "rardiol")
        .window_setup(conf::WindowSetup::default().title("BroaNetCo").vsync(vsync))
        .window_mode(
            conf::WindowMode::default()
                .dimensions(w, h)
                .resizable(true)
                .fullscreen_type(fullscreen_mode),
        );

    // We add the CARGO_MANIFEST_DIR/resources to the filesystems paths so
    // we we look in the cargo project for files.
    {
        let mut path = PathBuf::from(manifest_dir);
        path.push("resources");
        println!("Adding path {:?}", path);
        // We need this re-assignment alas, see
        // https://aturon.github.io/ownership/builders.html
        // under "Consuming builders"
        cb = cb.add_resource_path(path);
    }

    cb.build()
}
