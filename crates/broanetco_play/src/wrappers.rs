/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
 */

use shrinkwraprs::Shrinkwrap;

use super::draw::NamedTextureDataFull;
use crate::game;

#[derive(Clone, Debug, Shrinkwrap)]
pub struct Action<'a>(pub &'a game::Action);

#[derive(Debug, Shrinkwrap)]
pub struct GS<'a>(pub &'a game::GS<NamedTextureDataFull>);

#[derive(Clone, Debug, Shrinkwrap)]
pub struct MapElement<'a>(pub &'a game::map::MapElement<NamedTextureDataFull>);

#[derive(Clone, Debug, Shrinkwrap)]
pub struct Map<'a>(pub &'a game::map::Map<NamedTextureDataFull>);

#[derive(Clone, Debug, Shrinkwrap)]
pub struct CombatLogEntryPriority<'a>(pub &'a game::CombatLogEntryPriority);

#[derive(Clone, Debug, Shrinkwrap)]
pub struct EffectLog<'a>(pub &'a game::EffectLog);

#[derive(Clone, Debug, Shrinkwrap)]
pub struct CombatLogEntry<'a>(pub &'a game::CombatLogEntry);

#[derive(Clone, Debug, Shrinkwrap)]
pub struct Combater<'a>(pub &'a game::Combater<NamedTextureDataFull>);

impl<'a> From<&'a game::Action> for Action<'a> {
    fn from(other: &'a game::Action) -> Self {
        Self(other)
    }
}

impl<'a> From<&'a game::GS<NamedTextureDataFull>> for GS<'a> {
    fn from(other: &'a game::GS<NamedTextureDataFull>) -> Self {
        Self(other)
    }
}

impl<'a> From<&'a game::map::MapElement<NamedTextureDataFull>> for MapElement<'a> {
    fn from(other: &'a game::map::MapElement<NamedTextureDataFull>) -> Self {
        Self(other)
    }
}

impl<'a> From<&'a game::map::Map<NamedTextureDataFull>> for Map<'a> {
    fn from(other: &'a game::map::Map<NamedTextureDataFull>) -> Self {
        Self(other)
    }
}

impl<'a> From<&'a game::CombatLogEntryPriority> for CombatLogEntryPriority<'a> {
    fn from(other: &'a game::CombatLogEntryPriority) -> Self {
        Self(other)
    }
}

impl<'a> From<&'a game::CombatLogEntry> for CombatLogEntry<'a> {
    fn from(other: &'a game::CombatLogEntry) -> Self {
        Self(other)
    }
}

impl<'a> From<&'a game::Combater<NamedTextureDataFull>> for Combater<'a> {
    fn from(other: &'a game::Combater<NamedTextureDataFull>) -> Self {
        Self(other)
    }
}
