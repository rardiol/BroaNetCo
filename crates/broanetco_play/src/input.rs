/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

use std::fmt::Debug;

use game::hex2d::Direction;

use crate::event::KeyCode;
use either::{Either, Left, Right};
use ggez::input::keyboard::KeyMods;
use ggez::Context;

use super::wrappers::Action;
use super::MainState;
use crate::draw::PoVs;
use crate::game::map::MapLocation;
use crate::game::{
    ActionID, ActionTarget, ActionTargetEffect, ActionTargetingCombaterError, CombaterID, PlayingID,
};
use crate::game::{GSActionTrait, GSApply, GSBaseTrait, GSTrait};
use crate::ClientMode;

impl MainState {
    /// React to key presses
    pub fn key_down_event2(
        &mut self,
        ctx: &mut Context,
        keycode: KeyCode,
        keymod: KeyMods,
        repeat: bool,
    ) {
        debug!("Input: {:?}", keycode);
        debug!("UIS: {:?}", self.uis);

        self.drawcache.reset_key();

        if keycode == KeyCode::F2 {
            println!("GS: {:?}", self.gs);
            info!("GS: {:?}", self.gs);
            return;
        }

        if keycode == KeyCode::F3 {
            let crc32vec = self.gs.crc32();
            println!("GS: {:?}", crc32vec);
            info!("GS: {:?}", crc32vec);
            return;
        }

        if keycode == KeyCode::F4 {
            info!("debug mode: {:?}", self.debug_mode);
            self.debug_mode = !self.debug_mode;
            return;
        }

        if keycode == KeyCode::F10 {
            use chrono::prelude::*;
            let datetime = Local::now().naive_local().format("%Y-%m-%dT%H:%M:%S");
            let target = std::path::PathBuf::from(format!("/{}.png", datetime));
            info!("screenshoting to {:?}", target);
            let screenshot = ggez::graphics::screenshot(ctx).expect("failed to take screenshot");
            screenshot
                .encode(ctx, ggez::graphics::ImageFormat::Png, target)
                .expect("failed to save screenshot");
            return;
        }

        // Only allows interacting if it's the local player's turn and the game is running
        let msg: Option<GSApply> = match self.gs.playing {
            PlayingID::Winner(..) => return,
            PlayingID::Interrupt => return,
            PlayingID::FactionAuto(..) => return,
            PlayingID::Choice { faction, .. } => {
                match self.mode {
                    ClientMode::Client { playing_as, .. } if playing_as != faction => return,
                    ClientMode::Client { .. } | ClientMode::Hotseat
                        if !self.gs.fac(faction).faction_type.is_ai() =>
                    {
                        // Picking a choice
                        if let Some(choice) = key_code_to_number(keycode) {
                            // TODO: choices start at 1?
                            self.gs.choice(choice)
                        } else {
                            None
                        }
                    }
                    _ => return,
                }
            }
            PlayingID::Faction(comid) => {
                let facid = self.gs.com(comid).faction;
                match self.mode {
                    ClientMode::Client { playing_as, .. } if playing_as != facid => return,
                    ClientMode::Client { .. } | ClientMode::Hotseat
                        if !self.gs.fac(facid).faction_type.is_ai() =>
                    {
                        // TODO: cleanup
                        match (self.debug_mode, self.get_pov()) {
                            (false, PoVs::Faction(pov_facid)) => {
                                let gspov = game::GSPov::new(&self.gs, pov_facid);
                                match self.uis {
                                    uis::Enum::Observer(uis) => {
                                        self.uis = uis
                                            .uis_input(&gspov, ctx, keycode, keymod, repeat)
                                            .into();
                                        None
                                    }
                                    uis::Enum::Interactive(uis) => {
                                        match uis.uis_input(&gspov, ctx, keycode, keymod, repeat) {
                                            Left(new_uis) => {
                                                self.uis = new_uis.into();
                                                None
                                            }
                                            Right((new_uis, msg)) => {
                                                self.uis = new_uis.into();
                                                Some(msg)
                                            }
                                        }
                                    }
                                }
                            }
                            (false, PoVs::Global) | (true, _) => match self.uis {
                                uis::Enum::Observer(uis) => {
                                    self.uis = uis
                                        .uis_input(&self.gs, ctx, keycode, keymod, repeat)
                                        .into();
                                    None
                                }
                                uis::Enum::Interactive(uis) => {
                                    match uis.uis_input(&self.gs, ctx, keycode, keymod, repeat) {
                                        Left(new_uis) => {
                                            self.uis = new_uis.into();
                                            None
                                        }
                                        Right((new_uis, msg)) => {
                                            self.uis = new_uis.into();
                                            Some(msg)
                                        }
                                    }
                                }
                            },
                            (false, PoVs::Nothing) => None,
                        }
                    }
                    _ => return,
                }
            }
        };

        if let Some(msg) = msg {
            if let ClientMode::Client { ref client, .. } = self.mode {
                client.send_gsa(msg).unwrap();
            } else {
                self.gs.gsapply(
                    &msg,
                    &Some(self.sound_ch.clone()),
                    &mut Some(&mut self.drawcache),
                    &Some(self.replay_logger.clone()),
                );
            }
        }
    }

    /// React to mouse motion
    pub fn mouse_motion_event2(
        &mut self,
        _ctx: &mut Context,
        x: f32,
        y: f32,
        _xrel: f32,
        _yrel: f32,
    ) {
        self.mouse = (x, y);
        self.drawcache.reset_mouse();
    }
}

pub trait UISInput {
    type Targets;
    fn uis_input<GST: GSTrait>(
        self,
        gs: &GST,
        ctx: &mut Context,
        keycode: KeyCode,
        _keymod: KeyMods,
        _repeat: bool,
    ) -> Self::Targets;
}

/// Type states
pub mod uis {
    use super::*;

    #[derive(Debug, Hash, Clone, Copy, Derivative)]
    #[derivative(Default)]
    pub enum Enum {
        #[derivative(Default)]
        Observer(Observer),
        Interactive(Interactive),
    }

    #[derive(Debug, Hash, Clone, Copy)]
    pub enum Interactive {
        FreeMove(FreeMove),
        Move(Move),
        Action(Action),
    }

    impl From<Observer> for Enum {
        fn from(o: Observer) -> Self {
            Self::Observer(o)
        }
    }

    impl From<Interactive> for Enum {
        fn from(o: Interactive) -> Self {
            Self::Interactive(o)
        }
    }

    impl From<FreeMove> for Interactive {
        fn from(o: FreeMove) -> Self {
            Self::FreeMove(o)
        }
    }

    impl From<Move> for Interactive {
        fn from(o: Move) -> Self {
            Self::Move(o)
        }
    }

    impl From<Action> for Interactive {
        fn from(o: Action) -> Self {
            Self::Action(o)
        }
    }

    impl UISInput for Interactive {
        type Targets = Either<uis::Interactive, (uis::Observer, GSApply)>;
        fn uis_input<GST: GSTrait>(
            self,
            gs: &GST,
            ctx: &mut Context,
            keycode: KeyCode,
            keymod: KeyMods,
            repeat: bool,
        ) -> Self::Targets {
            match self {
                uis::Interactive::FreeMove(x) => x.uis_input(gs, ctx, keycode, keymod, repeat),
                uis::Interactive::Move(x) => x.uis_input(gs, ctx, keycode, keymod, repeat),
                uis::Interactive::Action(x) => x.uis_input(gs, ctx, keycode, keymod, repeat),
            }
        }
    }

    #[derive(Debug, Hash, Clone, Copy)]
    /// Nothing selected, on a given map position
    pub struct FreeMove {
        target: MapLocation,
    }

    #[derive(Debug, Hash, Clone, Copy, Derivative)]
    /// Nothing selected, on a given map position, can only observe
    #[derivative(Default)]
    pub struct Observer {
        #[derivative(Default(value = "MapLocation{x:0, y:0}"))]
        target: MapLocation,
    }

    #[derive(Debug, Hash, Clone, Copy)]
    /// A combater is selected, can either pick an action or do the default move
    pub struct Move {
        pub target: CombaterID,
    }

    #[derive(Debug, Hash, Clone, Copy)]
    /// A combater+action is selected, can move the action target or deselect action
    pub struct Action {
        source: CombaterID,
        action_id: ActionID,
        // TODO: multitarget
        target: MapLocation,
    }

    impl uis::Observer {
        fn move_selection<GST: GSTrait>(self, gs: &GST, move_direction: Direction) -> Self {
            let new_selection = self.target + move_direction;
            if gs.get_map().contains_coord(new_selection) {
                Self {
                    target: new_selection,
                }
            } else {
                self
            }
        }
    }

    impl uis::FreeMove {
        fn move_selection<GST: GSTrait>(self, gs: &GST, move_direction: Direction) -> Self {
            let new_selection = self.target + move_direction;
            if gs.get_map().contains_coord(new_selection) {
                Self {
                    target: new_selection,
                }
            } else {
                self
            }
        }
        fn input_confirm<GST: GSTrait>(self, gs: &GST) -> Either<Self, uis::Move> {
            match gs.combater_at_location(self.target) {
                Some((comid, _com)) if gs.com_can_act(comid) => Right(uis::Move { target: comid }),
                _ => Left(self),
            }
        }
        fn ui_end_faction_turn<GST: GSTrait>(self, gs: &GST) -> (uis::Observer, GSApply) {
            (
                uis::Observer {
                    target: self.target,
                },
                gs.end_faction_turn(),
            )
        }
    }

    impl uis::Move {
        fn move_selection<GST: GSTrait>(
            self,
            gs: &GST,
            move_direction: Direction,
        ) -> Either<(uis::Observer, GSApply), Self> {
            if let Some(msg) = gs.move_combater_direction(self.target, move_direction) {
                Left((
                    uis::Observer {
                        target: gs.com(self.target).location.coord,
                    },
                    msg,
                ))
            } else {
                Right(self)
            }
        }

        fn input_confirm<GST: GSTrait>(self, gs: &GST) -> uis::FreeMove {
            uis::FreeMove {
                target: gs.com(self.target).location.coord,
            }
        }

        fn input_keycode<GST: GSTrait>(
            self,
            gs: &GST,
            keycode: KeyCode,
        ) -> Either<Self, Either<(uis::Observer, GSApply), uis::Action>> {
            let source = self.target;
            for (action_id, action) in gs.com(source).actions(gs, source) {
                if crate::wrappers::Action::from(action).keycode() == keycode {
                    return Right(match gs.combater_action(source, action_id, vec![]) {
                        Ok(gsa) => Left((
                            uis::Observer {
                                target: gs.com(source).location.coord,
                            },
                            gsa,
                        )),
                        Err(None) => Right(uis::Action {
                            source,
                            action_id,
                            target: gs.com(source).location.coord,
                        }),
                        Err(_) => panic!("nonsense"),
                    });
                }
            }
            Left(self)
        }

        fn ui_end_faction_turn<GST: GSTrait>(self, gs: &GST) -> (uis::Observer, GSApply) {
            (
                uis::Observer {
                    target: gs.com(self.target).location.coord,
                },
                gs.end_faction_turn(),
            )
        }
    }

    impl uis::Action {
        fn move_selection<GST: GSTrait>(self, gs: &GST, move_direction: Direction) -> Self {
            let new_selection = self.target + move_direction;
            if gs.get_map().contains_coord(new_selection) {
                Self {
                    target: new_selection,
                    ..self
                }
            } else {
                self
            }
        }
        fn input_confirm<GST: GSTrait>(
            self,
            gs: &GST,
        ) -> Either<(uis::Observer, GSApply), Either<Self, uis::FreeMove>> {
            let uis::Action {
                source,
                action_id,
                target,
            } = self;
            match self.action_status(gs) {
                // TODO: actions that allow targeting origin
                uis::AttackStatus::OnOrigin => Right(Right(uis::FreeMove { target })),
                uis::AttackStatus::ValidTarget => {
                    let target = match gs
                        .com(source)
                        .actions(gs, source)
                        .find(|(taid, _)| *taid == action_id)
                        .expect("missing action")
                        .1
                        .target_effects[0]
                    {
                        ActionTargetEffect::Combater(_, _) => ActionTarget::Combater(
                            gs.combater_at_location(target)
                                .expect("Action missing a Combater target")
                                .0,
                        ),
                        ActionTargetEffect::Terrain(_, _) => ActionTarget::Terrain(target),
                    };
                    Left((
                        uis::Observer {
                            target: gs.com(source).location.coord,
                        },
                        gs.combater_action(
                            source,
                            action_id,
                            // TODO: improve handling of targeting, and multiple targets
                            vec![target],
                        )
                        .unwrap(),
                    ))
                }
                uis::AttackStatus::InvalidTarget(_err) => Right(Left(self)),
            }
        }

        fn ui_end_faction_turn<GST: GSTrait>(self, gs: &GST) -> (uis::Observer, GSApply) {
            (
                uis::Observer {
                    target: self.target,
                },
                gs.end_faction_turn(),
            )
        }
    }

    impl UISInput for uis::Observer {
        type Targets = uis::Observer;
        fn uis_input<GST: GSTrait>(
            self,
            gs: &GST,
            _ctx: &mut Context,
            keycode: KeyCode,
            _keymod: KeyMods,
            _repeat: bool,
        ) -> Self::Targets {
            match (keycode, key_code_to_direction(keycode)) {
                (_, Some(dir)) => self.move_selection(gs, dir),

                _keycode => self,
                //keycode => self.input_keycode(gs, keycode),
            }
        }
    }

    impl UISInput for uis::FreeMove {
        type Targets = Either<uis::Interactive, (uis::Observer, GSApply)>;
        fn uis_input<GST: GSTrait>(
            self,
            gs: &GST,
            _ctx: &mut Context,
            keycode: KeyCode,
            _keymod: KeyMods,
            _repeat: bool,
        ) -> Self::Targets {
            match (keycode, key_code_to_direction(keycode)) {
                (_, Some(dir)) => Left(self.move_selection(gs, dir).into()),

                (KeyCode::Key5, _) | (KeyCode::Numpad5, _) => {
                    Left(self.input_confirm(gs).either(|x| x.into(), |x| x.into()))
                }

                (KeyCode::Key0, _) | (KeyCode::Numpad0, _) => Right(self.ui_end_faction_turn(gs)),

                _keycode => Left(self.into()),
                //keycode => self.input_keycode(gs, keycode),
            }
        }
    }
    impl UISInput for uis::Move {
        type Targets = Either<uis::Interactive, (uis::Observer, GSApply)>;
        fn uis_input<GST: GSTrait>(
            self,
            gs: &GST,
            _ctx: &mut Context,
            keycode: KeyCode,
            _keymod: KeyMods,
            _repeat: bool,
        ) -> Self::Targets {
            match (keycode, key_code_to_direction(keycode)) {
                (_, Some(dir)) => self
                    .move_selection(gs, dir)
                    .either(|(a, b)| Right((a, b)), |a| Left(a.into())),

                (KeyCode::Key5, _) | (KeyCode::Numpad5, _) => Left(self.input_confirm(gs).into()),

                (KeyCode::Key0, _) | (KeyCode::Numpad0, _) => Right(self.ui_end_faction_turn(gs)),

                (keycode, _) => self.input_keycode(gs, keycode).either(
                    |mov| Left(mov.into()),
                    |o| o.either(Right, |act| Left(act.into())),
                ),
            }
        }
    }

    impl UISInput for uis::Action {
        type Targets = Either<uis::Interactive, (uis::Observer, GSApply)>;
        fn uis_input<GST: GSTrait>(
            self,
            gs: &GST,
            _ctx: &mut Context,
            keycode: KeyCode,
            _keymod: KeyMods,
            _repeat: bool,
        ) -> Self::Targets {
            match (keycode, key_code_to_direction(keycode)) {
                (_, Some(dir)) => Left(self.move_selection(gs, dir).into()),

                (KeyCode::Key5, _) | (KeyCode::Numpad5, _) => self
                    .input_confirm(gs)
                    .either(Right, |o| o.either(|a| Left(a.into()), |b| Left(b.into()))),

                (KeyCode::Key0, _) | (KeyCode::Numpad0, _) => {
                    let (a, b) = self.ui_end_faction_turn(gs);
                    Right((a, b))
                }

                _ => Left(self.into()),
                // keycode => self.input_keycode(gs, keycode),
            }
        }
    }

    pub trait Trait: Clone + Debug {
        fn main_target(&self) -> Either<MapLocation, CombaterID>;
        fn source_com<GST: GSTrait>(&self, gs: &GST) -> Option<CombaterID>;
        fn target_com<GST: GSTrait>(&self, gs: &GST) -> Option<CombaterID>;
        /// Of main_target
        fn map_location<GST: GSTrait>(&self, gs: &GST) -> MapLocation;
    }

    impl Trait for FreeMove {
        fn main_target(&self) -> Either<MapLocation, CombaterID> {
            Left(self.target)
        }
        fn source_com<GST: GSTrait>(&self, gs: &GST) -> Option<CombaterID> {
            gs.combater_at_location(self.target).map(|x| x.0)
        }
        fn target_com<GST: GSTrait>(&self, _gs: &GST) -> Option<CombaterID> {
            None
        }
        fn map_location<GST: GSTrait>(&self, _gs: &GST) -> MapLocation {
            self.target
        }
    }

    impl Trait for Observer {
        fn main_target(&self) -> Either<MapLocation, CombaterID> {
            Left(self.target)
        }
        fn source_com<GST: GSTrait>(&self, gs: &GST) -> Option<CombaterID> {
            gs.combater_at_location(self.target).map(|x| x.0)
        }
        fn target_com<GST: GSTrait>(&self, _gs: &GST) -> Option<CombaterID> {
            None
        }
        fn map_location<GST: GSTrait>(&self, _gs: &GST) -> MapLocation {
            self.target
        }
    }

    impl Trait for Move {
        fn main_target(&self) -> Either<MapLocation, CombaterID> {
            Right(self.target)
        }
        fn source_com<GST: GSTrait>(&self, _gs: &GST) -> Option<CombaterID> {
            Some(self.target)
        }
        fn target_com<GST: GSTrait>(&self, _gs: &GST) -> Option<CombaterID> {
            Some(self.target)
        }
        fn map_location<GST: GSTrait>(&self, gs: &GST) -> MapLocation {
            gs.com(self.target).location.coord
        }
    }

    /// The states the UI can show for targeting an action
    #[derive(Debug, Copy, Clone, Eq, PartialEq)]
    pub enum AttackStatus {
        OnOrigin,
        ValidTarget,
        InvalidTarget(ActionTargetingCombaterError),
    }

    impl Trait for Action {
        fn main_target(&self) -> Either<MapLocation, CombaterID> {
            Left(self.target)
        }
        fn source_com<GST: GSTrait>(&self, _gs: &GST) -> Option<CombaterID> {
            Some(self.source)
        }
        fn target_com<GST: GSTrait>(&self, gs: &GST) -> Option<CombaterID> {
            gs.combater_at_location(self.target).map(|x| x.0)
        }
        fn map_location<GST: GSTrait>(&self, _gs: &GST) -> MapLocation {
            self.target
        }
    }

    impl Action {
        pub fn source_location<GST: GSTrait>(self, gs: &GST) -> MapLocation {
            gs.com(self.source).location.coord
        }
        pub fn target_location<GST: GSTrait>(self) -> MapLocation {
            self.target
        }
    }

    /*
        // TODO: rework
        // TODO: show other's action
        pub fn update<GST: GSTrait, CLIENT>(
            &mut self,
            gs: &GST,
            client_mode: &ClientMode<CLIENT>,
            hint: game::UISHint,
        ) {
            if let Some(target) = hint {
                if let game::PlayingID::Faction(comid) = gs.get_playing() {
                    let facid = gs.com(*comid).faction;
                    if !gs.fac(facid).faction_type.is_ai() {
                        match client_mode {
                            ClientMode::Hotseat => {
                                *self = uis::Move { target };
                                return;
                            }
                            ClientMode::Client { playing_as, .. } if facid == *playing_as => {
                                *self = uis::Move { target };
                                return;
                            }
                            ClientMode::Client { .. } => (),
                        }
                    }
                }
            }
        }
    }
    *
    */

    impl Action {
        pub fn action_status<GST: GSTrait>(&self, gs: &GST) -> AttackStatus {
            let Self {
                source,
                action_id,
                target,
            } = self;
            // TODO
            let is_valid_targeting = match gs
                .com(*source)
                .actions(gs, *source)
                .find(|(tid, _)| *tid == *action_id)
                .expect("missing action")
                .1
                .target_effects[0]
            {
                ActionTargetEffect::Combater(_, _) => match gs.combater_at_location(*target) {
                    Some((tcomid, _tcom)) => {
                        gs.valid_targeting(*source, *action_id, 0, ActionTarget::Combater(tcomid))
                    }
                    None => {
                        return AttackStatus::InvalidTarget(ActionTargetingCombaterError::NoTarget)
                    }
                },
                ActionTargetEffect::Terrain(_, _) => {
                    gs.valid_targeting(*source, *action_id, 0, ActionTarget::Terrain(*target))
                }
            };
            debug!("action_status: {:?}", is_valid_targeting);
            if gs.com(*source).location.coord == *target {
                // TODO: allow self targetting
                AttackStatus::OnOrigin
            } else {
                match is_valid_targeting {
                    Ok(()) => AttackStatus::ValidTarget,
                    Err(err) => AttackStatus::InvalidTarget(err),
                }
            }
        }
    }
}

#[const_fn("1.46")]
const fn key_code_to_number(keycode: KeyCode) -> Option<usize> {
    match keycode {
        KeyCode::Key0 | KeyCode::Numpad0 => Some(0),
        KeyCode::Key1 | KeyCode::Numpad1 => Some(1),
        KeyCode::Key2 | KeyCode::Numpad2 => Some(2),
        KeyCode::Key3 | KeyCode::Numpad3 => Some(3),
        KeyCode::Key4 | KeyCode::Numpad4 => Some(4),
        KeyCode::Key5 | KeyCode::Numpad5 => Some(5),
        KeyCode::Key6 | KeyCode::Numpad6 => Some(6),
        KeyCode::Key7 | KeyCode::Numpad7 => Some(7),
        KeyCode::Key8 | KeyCode::Numpad8 => Some(8),
        KeyCode::Key9 | KeyCode::Numpad9 => Some(9),
        _ => None,
    }
}

/// Converts number keycodes to hex directions
#[const_fn("1.46")]
const fn key_code_to_direction(keycode: KeyCode) -> Option<Direction> {
    match keycode {
        KeyCode::Key8 | KeyCode::Numpad8 => Some(Direction::YZ),
        KeyCode::Key9 | KeyCode::Numpad9 => Some(Direction::XZ),
        KeyCode::Key3 | KeyCode::Numpad3 => Some(Direction::XY),
        KeyCode::Key2 | KeyCode::Numpad2 => Some(Direction::ZY),
        KeyCode::Key1 | KeyCode::Numpad1 => Some(Direction::ZX),
        KeyCode::Key7 | KeyCode::Numpad7 => Some(Direction::YX),
        _ => None,
    }
}

/// Converts lowercase ascii letters to ggez KeyCodes
#[const_fn("1.46")]
const fn char_to_key_code(ch: char) -> Option<KeyCode> {
    match ch {
        'a' => Some(KeyCode::A),
        'b' => Some(KeyCode::B),
        'c' => Some(KeyCode::C),
        'd' => Some(KeyCode::D),
        'e' => Some(KeyCode::E),
        'f' => Some(KeyCode::F),
        'g' => Some(KeyCode::G),
        'h' => Some(KeyCode::H),
        'i' => Some(KeyCode::I),
        'j' => Some(KeyCode::J),
        'k' => Some(KeyCode::K),
        'l' => Some(KeyCode::L),
        'm' => Some(KeyCode::M),
        'n' => Some(KeyCode::N),
        'o' => Some(KeyCode::O),
        'p' => Some(KeyCode::P),
        'q' => Some(KeyCode::Q),
        'r' => Some(KeyCode::R),
        's' => Some(KeyCode::S),
        't' => Some(KeyCode::T),
        'u' => Some(KeyCode::U),
        'v' => Some(KeyCode::V),
        'w' => Some(KeyCode::W),
        'x' => Some(KeyCode::X),
        'y' => Some(KeyCode::Y),
        'z' => Some(KeyCode::Z),
        _ => None,
    }
}

impl Action<'_> {
    /// Gets the KeyCode that if pressed should select this action
    /// Defaults to the first letter of the action name
    fn keycode(&self) -> KeyCode {
        char_to_key_code(
            self.0
                .keycode
                .unwrap_or_else(|| self.0.name.chars().next().unwrap().to_ascii_lowercase()),
        )
        .expect("invalid char as keycode")
    }
}

#[cfg(test)]
mod tests {

    /*
        use game;
        use game::{GSApply, GameStatus, GS};
        use draw::NamedTextureDataTransfer;
        type GST = GS<NamedTextureDataTransfer>;
        use init;
        use game::hex2d::Direction;

        fn do_thing(gs: &mut GST, thing: &mut FnMut(&mut GST) -> Option<GSApply>) {
            let msg = thing(gs).unwrap();
            gs.gsapply(&msg, None, None);
        }
        fn do_ui_thing(gs: &mut GST, thing: &mut FnMut(&mut GST) -> Option<GSApply>) {
            let no_msg = thing(gs);
            assert!(no_msg.is_none())
        }

        fn simple_move_direction(gs: &mut GST, direction: Direction) {
            do_ui_thing(gs, &mut |gs: &mut GST| gs.uis.input_confirm());
            do_thing(gs, &mut |gs: &mut GST| gs.uis.move_selection(direction));
            do_ui_thing(gs, &mut |gs: &mut GST| gs.uis.move_selection(direction));
            do_thing(gs, &mut |gs: &mut GST| gs.ui_end_faction_turn());
            do_thing(gs, &mut |gs: &mut GST| gs.ui_end_faction_turn());
        }

        fn simple_attack_direction(gs: &mut GST, direction: Direction) {
            do_ui_thing(gs, &mut |gs: &mut GST| gs.input_confirm());
            do_ui_thing(gs, &mut |gs: &mut GST| gs.input_confirm());
            do_ui_thing(gs, &mut |gs: &mut GST| gs.move_selection(direction));
            do_thing(gs, &mut |gs: &mut GST| gs.input_confirm());
            do_thing(gs, &mut |gs: &mut GST| gs.ui_end_faction_turn());
            do_thing(gs, &mut |gs: &mut GST| gs.ui_end_faction_turn());
        }

        #[test]
        fn gen_test() {
            let mut gs = init::tests::test_gs();

            do_thing(&mut gs, &mut |gs: &mut GST| gs.ui_end_faction_turn());
            do_thing(&mut gs, &mut |gs: &mut GST| gs.ui_end_faction_turn());

            simple_move_direction(&mut gs, Direction::ZX);

            for _ in 0..3 {
                simple_attack_direction(&mut gs, Direction::ZY);
            }

            simple_move_direction(&mut gs, Direction::YZ);

            for _ in 0..2 {
                simple_attack_direction(&mut gs, Direction::YZ);
            }

            do_thing(&mut gs, &mut |gs: &mut GST| gs.ui_end_faction_turn());

            assert_eq!(gs.status, GameStatus::Winner(0));
        }

        #[test]
        fn end_turn_test() {
            let mut gs = init::tests::test_gs();

            assert!(gs.tick() == 1);
            assert!(gs.playing == game::FactionID(0));

            let msg = gs.ui_end_faction_turn().unwrap();
            gs.gsapply(msg);

            assert!(gs.tick() == 1);
            assert!(gs.playing == game::FactionID(1));

            let msg = gs.ui_end_faction_turn().unwrap();
            gs.gsapply(msg);

            assert!(gs.tick() == 2);
            assert!(gs.playing == game::FactionID(0));
        }
    */
}
