/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

pub use crate::game::sound::*;

use std::collections::HashMap;
use std::sync::mpsc;
use std::thread;

use anyhow::Context as AContext;
use anyhow::Result as AResult;
use ggez::audio;
use ggez::audio::SoundSource;
use ggez::filesystem;
use ggez::Context;
use ggez::GameResult;

pub type Sounds = HashMap<SoundID, audio::Source>;

// TODO: load on demand?
// TODO: network transfer?

/// Loads all sound files in the /sounds directory
pub fn load_sounds(context: &mut Context) -> GameResult<Sounds> {
    filesystem::read_dir(context, "/sounds/")?
        .map(|x| {
            debug!("Loading: {:?}", x);
            Ok((
                x.file_stem().unwrap().to_string_lossy().into_owned(),
                audio::Source::new(context, x)?,
            ))
        })
        .collect::<GameResult<Sounds>>()
}

/// Starts the audio thread and returns a channel
/// to send sound filenames to be played
pub fn start_sound_thread(
    mut sounds: Sounds,
) -> Result<(thread::JoinHandle<AResult<()>>, mpsc::Sender<SoundID>), std::io::Error> {
    let (audio_th_sender, audio_th_receiver) = mpsc::channel::<SoundID>();

    let handle = thread::Builder::new()
        .name("audio".to_owned())
        .spawn(move || {
            debug!("Starting audio thread");
            while let Ok(requested_name) = audio_th_receiver.recv() {
                debug!("Playing: {}", requested_name);
                let sound: &mut audio::Source =
                    sounds.get_mut(&requested_name).context("Invalid sound")?;
                sound.set_volume(1.0);
                if !sound.playing() {
                    debug!("Sound already playing");
                    // TODO: rewrite withour threads or some other solution
                    //sound.play().context("Failed to play sound")?;
                }
            }
            debug!("Ending audio thread");
            Ok(())
        })?;

    Ok((handle, audio_th_sender))
}
