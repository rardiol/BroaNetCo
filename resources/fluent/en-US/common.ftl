units = unid
com-name = {comnames}
part-count = {$amount ->
 [one]-
*[many]{$amount}:
} {partnames}
