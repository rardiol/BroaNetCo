with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "rust-env";
  nativeBuildInputs = [
    rustc cargo rustfmt clippy

    pkgconfig
  ];

  buildInputs = [
    alsaLib libudev
  ];

  MYLIBS = stdenv.lib.makeLibraryPath
  [
    xorg.libX11.out
    xorg.libXcursor.out
    xorg.libXrandr.out
    xorg.libXi.out
    libglvnd.out
  ];

  shellHook = ''
    export LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$MYLIBS"
  '';

  RUST_BACKTRACE = 1;
}
