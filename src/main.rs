/*
 *  Copyright 2018 Ricardo Luis Souza Ardissone
 *
 *  This file is part of BroaNetCo.
 *
 *  BroaNetCo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  BroaNetCo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with BroaNetCo.  If not, see <http://www.gnu.org/licenses/>.
*/

#[macro_use]
extern crate log;
#[macro_use]
extern crate const_fn;

use std::mem;
use std::path::{Path, PathBuf};
use std::{path, thread};

use anyhow::Result as AResult;
use clap::arg_enum;

use structopt::StructOpt;

use rand_chacha::rand_core::SeedableRng;
use rand_chacha::ChaCha8Rng;

extern crate broanetco_actvec as act_vec;
/// Game Logic
extern crate broanetco_game as game;
extern crate broanetco_init as init;
extern crate broanetco_net as net;
extern crate broanetco_play as play;
extern crate broanetco_util as util;

use crate::game::init::GSInit;
use crate::game::replay::ReplayLoggerControl;
use crate::game::{NamedTextureDataTransfer, TryIntoParams};
use crate::play::draw::NamedTextureDataFull;
use crate::play::init::{font_data, interface_data};
use crate::play::sound::{load_sounds, start_sound_thread};
use crate::play::{graphics_init, ClientMode, GraphicsSettings, MainState};

/// Command line arguments for running the game
#[derive(StructOpt, Debug, Clone)]
#[structopt(author, about)]
pub struct Opts {
    #[structopt(
        short = "x",
        long = "width",
        default_value = "1000",
        value_name = "WIDTH"
    )]
    /// screen width
    width: f32,
    #[structopt(
        short = "y",
        long = "height",
        default_value = "750",
        value_name = "HEIGHT"
    )]
    /// screen height
    height: f32,
    #[structopt(long = "fullscreen")]
    fullscreen: bool,
    #[structopt(long = "offvsync")]
    offvsync: bool,
    #[structopt(long = "nogui")]
    nogui: bool,
    #[structopt(long = "mode", default_value = "hotseat", value_name = "MODE")]
    /// game mode, one of "client", "hotseat" , "server", "host"
    mode: Mode,
    #[structopt(long = "address", default_value = "localhost")]
    address: String,
    #[structopt(long = "port", default_value = "27451")]
    port: u16,
    #[structopt(short = "m", long = "mission", value_name = "MISSION_NAME")]
    mission: Option<String>,
    #[structopt(short = "f", long = "faction", value_name = "FACTION_NUMBER")]
    faction: Option<usize>,
    #[structopt(short = "r", long = "replay", value_name = "BNCLOG_FILE")]
    replay: Option<PathBuf>,
    #[structopt(default_value = "log.bnclog")]
    log_file: PathBuf,
    #[structopt(default_value = "server.bnclog")]
    server_log_file: PathBuf,
    #[structopt(short = "l", long = "language", value_name = "en-US")]
    language: Option<play::LanguageIdentifier>,
}

arg_enum! {
    #[derive(Clone, Copy, Eq, PartialEq, Debug)]
    pub enum Mode {
        // Local game, single player or hotseat multiplayer
        Hotseat,
        // Run a game server, but don't play locally
        Headless,
        // Run a game server and play locally
        Host,
        // Networked multiplayer client
        Client,
    }
}

impl Mode {
    #[const_fn("1.46")]
    pub const fn networked(&self) -> bool {
        match *self {
            Self::Hotseat => false,
            Self::Headless | Self::Host | Self::Client => true,
        }
    }
    pub fn log(&self, address: &str, port: u16) {
        match *self {
            Self::Hotseat => info!("Running on hotseat mode"),
            Self::Headless => info!("Running on headless mode, on port {}", port),
            Self::Host => info!("Running on host mode, on port {}", port),
            Self::Client => info!(
                "Running on client mode, on address {}, on port {}",
                address, port
            ),
        }
    }
    #[const_fn("1.46")]
    pub const fn run_server(&self) -> bool {
        match *self {
            Self::Hotseat | Self::Client => false,
            Self::Headless | Self::Host => true,
        }
    }
    #[const_fn("1.46")]
    pub const fn run_client(&self) -> bool {
        match *self {
            Self::Hotseat | Self::Headless => false,
            Self::Host | Self::Client => true,
        }
    }
    #[const_fn("1.46")]
    pub const fn run_hotseat(&self) -> bool {
        match *self {
            Self::Hotseat => true,
            Self::Headless | Self::Host | Self::Client => false,
        }
    }
    #[const_fn("1.46")]
    pub const fn run_graphics(&self) -> bool {
        match *self {
            Self::Hotseat | Self::Host | Self::Client => true,
            Self::Headless => false,
        }
    }
}

/// Gets cmdline arguments, launches threads and joins on them
///
/// Tries to avoid early panicking and makes sure replaylogger closes
/// before propagating panics
fn main() -> AResult<()> {
    if std::env::var("RUST_LOG").is_err() {
        // Default log level
        std::env::set_var("RUST_LOG", "warn,broanetco=info,broanetco_game=warn,broanetco_init=warn,broanetco_play=warn,broanetco_net=info,broanetco_util=warn,broanetco_actvec=warn");
    }

    pretty_env_logger::init_timed();

    debug!(
        "size report: GS:{:?} MapElement:{:?} Combater{:?}",
        std::mem::size_of::<game::GS::<NamedTextureDataFull>>(),
        std::mem::size_of::<game::map::MapElement::<NamedTextureDataFull>>(),
        std::mem::size_of::<game::Combater::<NamedTextureDataFull>>()
    );

    info!(
        "Starting {} {}",
        env!("CARGO_PKG_NAME"),
        env!("CARGO_PKG_VERSION")
    );

    let opts = Opts::from_args();

    let opts = if (!opts.nogui) { gui::run(opts) } else { opts };

    if opts.mode.run_server() {
        // TODO: server by default should bind to 0.0.0.0, not localhost

        opts.mode.log(&opts.address, opts.port);
        // TODO: log mission and faction

        let gsinit = get_gsinit(&opts.mission, &opts.replay)?;
        let gs: game::GS<NamedTextureDataTransfer> = gsinit.clone().into();
        let players = gs.player_factions();
        let (replay_logger, replay_logger_handle) =
            ReplayLoggerControl::new(opts.server_log_file.clone(), gsinit.clone());

        let (main_server_handle, game_server_handle) = net::server::server(
            &opts.address,
            opts.port,
            players,
            gsinit,
            thread::current(),
            Some(replay_logger.clone()),
            ChaCha8Rng::from_entropy(),
        )
        .unwrap();
        thread::park(); // wait for server to be ready

        let run_graphics_result = if opts.mode.run_graphics() {
            let (run_graphics_result, replay_logger, replay_logger_handle) = run_graphics(opts)?;
            Some((run_graphics_result, replay_logger, replay_logger_handle))
        } else {
            None
        };

        let main_server_result = main_server_handle.join();
        let game_server_result = game_server_handle.join();
        mem::drop(replay_logger);
        let replay_logger_result = replay_logger_handle.join();
        if let Some((
            run_graphics_result,
            run_graphics_replay_logger,
            run_graphics_replay_logger_handle,
        )) = run_graphics_result
        {
            mem::drop(run_graphics_replay_logger);
            run_graphics_replay_logger_handle.join().unwrap().unwrap();
            run_graphics_result.unwrap();
        }
        replay_logger_result.unwrap().unwrap();
        main_server_result.unwrap();
        game_server_result.unwrap().unwrap();
    } else if opts.mode.run_graphics() {
        let (run_graphics_result, replay_logger, replay_logger_handle) = run_graphics(opts)?;
        mem::drop(replay_logger);
        replay_logger_handle.join().unwrap().unwrap();
        run_graphics_result.unwrap();
    };
    Ok(())
}

const fn load_graphics_settings(opts: &Opts) -> GraphicsSettings {
    ((opts.width, opts.height), (opts.fullscreen, !opts.offvsync))
}

/// Run interactive graphical game
fn run_graphics(
    opts: Opts,
) -> AResult<(
    AResult<()>,
    ReplayLoggerControl,
    thread::JoinHandle<Result<(), std::io::Error>>,
)> {
    let graphics_settings = load_graphics_settings(&opts);
    let langid = get_langid(&opts);

    if opts.mode.run_hotseat() {
        let gsinit = get_gsinit(&opts.mission, &opts.replay)?;
        let (replay_logger, replay_logger_handle) =
            ReplayLoggerControl::new(opts.log_file, gsinit.clone());
        let replay_logger_clone = replay_logger.clone();
        let res: AResult<()> = {
            let manifest_dir = Path::new(env!("CARGO_MANIFEST_DIR"));
            let (mut ctx, eventloop) = graphics_init(graphics_settings, manifest_dir)?;

            let font_data = font_data(&mut ctx)?;
            let (_sound_handle, sound_ch) = start_sound_thread(load_sounds(&mut ctx)?)?;

            let ms = {
                info!("Loading textures");
                let gsinit = gsinit.try_into_params(&mut ctx)?;
                info!("Finished loading textures");
                let gs: game::GS<NamedTextureDataFull> = gsinit.into();
                MainState::new(
                    font_data,
                    gs,
                    sound_ch,
                    interface_data(&mut ctx).expect("Failed to load interface data"),
                    replay_logger_clone,
                    langid,
                    &mut ctx,
                )
            };
            ms.run(ctx, eventloop)
        };
        Ok((
            res.map_err(|gr| gr.into()),
            replay_logger,
            replay_logger_handle,
        ))
    } else if opts.mode.run_client() {
        let client = net::client::Client::new(&opts.address, opts.port)?;
        let (client, gsinit) = client.receiver_gs()?;
        let (replay_logger, replay_logger_handle) =
            ReplayLoggerControl::new(opts.log_file.clone(), gsinit.clone());
        let replay_logger_clone = replay_logger.clone();
        let res = {
            let manifest_dir = Path::new(env!("CARGO_MANIFEST_DIR"));
            let (mut ctx, eventloop) = graphics_init(graphics_settings, manifest_dir)?;

            let font_data = font_data(&mut ctx)?;
            let (_sound_handle, sound_ch) = start_sound_thread(load_sounds(&mut ctx)?)?;

            let ms = {
                info!("Loading textures");
                let gsinit = gsinit.try_into_params(&mut ctx)?;
                info!("Finished loading textures");
                debug!("Ws client gsinit: {:?}", gsinit);
                let gs: game::GS<NamedTextureDataFull> = gsinit.into();
                let mut ms = MainState::new(
                    font_data,
                    gs,
                    sound_ch,
                    interface_data(&mut ctx).expect("Failed to load interface data"),
                    replay_logger_clone,
                    langid,
                    &mut ctx,
                );

                let (client, playing_as) =
                    client.get_assigned_faction(opts.faction.map(game::FactionID))?;
                info!("Playing as: {:?}", playing_as);

                ms.mode = ClientMode::Client { playing_as, client };
                ms
            };
            ms.run(ctx, eventloop)
        };
        Ok((res, replay_logger, replay_logger_handle))
    } else {
        panic!("Invalid mode"); // TODO: refactor
    }
}

/// Prints valid mission names
fn print_missions(missions_names: &[String]) {
    for name in missions_names {
        println!("{}", name);
    }
}

fn get_langid(opts: &Opts) -> play::LanguageIdentifier {
    opts.language
        .clone()
        .unwrap_or_else(|| "en-US".parse().unwrap()) // TODO: langneg and envvars
}

fn get_path() -> PathBuf {
    let mut path: PathBuf;
    {
        let manifest_dir = env!("CARGO_MANIFEST_DIR");
        path = path::PathBuf::from(manifest_dir);
        path.push("resources");
        println!("Adding path {:?}", path);
    }
    path
}

/// Get a GSInit from a mission name
fn get_gsinit(mission: &Option<String>, replay: &Option<PathBuf>) -> AResult<GSInit> {
    if let Some(replay_file) = replay {
        return Ok(util::load_log(replay_file)
            .expect("failed to read bnclog file")
            .into());
    }
    let path = get_path();
    match *mission {
        Some(ref mission) => match init::missions(path.clone())
            .into_iter()
            .find(|x| x.name() == mission)
        {
            Some(mission) => {
                let gsinit = mission.gsinit();
                debug!("Selected gsinit: {:?}", gsinit);
                Ok(gsinit)
            }
            None => {
                println!("Mission unavailable: {}", mission);
                println!("Available missions:");
                print_missions(&init::missions_names(path));
                anyhow::bail!("Mission unavailable")
            }
        },
        None => {
            println!("No mission selected");
            println!("Available missions:");
            print_missions(&init::missions_names(path));
            anyhow::bail!("No mission selected")
        }
    }
}

mod gui {
    use crate::{get_path, Mode, Opts};
    use std::sync::mpsc;

    /*
        use ggez::{
            event,
            graphics::{self, Color},
            ContextBuilder, GameError, GameResult,
        };
        use ggez_egui::EguiBackend;

        pub fn run(opts: Opts) -> Opts {
            let (sender, receiver) = mpsc::sync_channel(1);
            let (ctx, event_loop) = ContextBuilder::new("BroaNetCoL", "Ricardo Ardissone")
                .build()
                .unwrap();

            let launcher_ui = LauncherUI::new(opts, init::missions_names(get_path()), sender);

            debug!("Launching BNCL");
            event::run(ctx, event_loop, launcher_ui);
            debug!("Waiting for game opts");
            let ret = receiver.recv().unwrap();
            debug!("Received game opts");
            ret
        }

        struct LauncherUI {
            egui_backend: EguiBackend,
            opts: Opts,
            available_missions: Vec<String>,
            sender: mpsc::SyncSender<Opts>,
        }

        impl LauncherUI {
            fn new(
                opts: Opts,
                available_missions: Vec<String>,
                sender: mpsc::SyncSender<Opts>,
            ) -> Self {
                Self {
                    egui_backend: EguiBackend::default(),
                    opts,
                    available_missions,
                    sender,
                }
            }
        }

        impl event::EventHandler<GameError> for LauncherUI {
            fn update(&mut self, ctx: &mut ggez::Context) -> Result<(), GameError> {
                trace!("update");
                let mut opts = &mut self.opts;
                let mut mode = opts.mode;
                let mut mission: String = opts.mission.get_or_insert(self.available_missions[0].clone()).to_string();
                let egui_ctx = self.egui_backend.get_context();

                egui::CentralPanel::default().show(&egui_ctx, |ui| {
                    for tmode in [Mode::Hotseat, Mode::Headless, Mode::Host, Mode::Client] {
                        ui.selectable_value(&mut mode, tmode, tmode.to_string());
                    }
                    for tmission in self.available_missions.iter() {
                        ui.selectable_value(&mut mission, tmission.clone(), tmission);
                    }
                    egui::warn_if_debug_build(ui);
                    if ui.button("Launch").clicked() {
                        self.sender.send(self.opts.clone()).unwrap();
                        event::quit(ctx);
                    }
                });
                self.opts.mode = mode;
                self.opts.mission = Some(mission);

                Ok(())
            }

            fn draw(&mut self, ctx: &mut ggez::Context) -> Result<(), GameError> {
                graphics::clear(ctx, Color::BLACK);
                graphics::draw(ctx, &self.egui_backend, ([0.0, 0.0],))?;
                graphics::present(ctx)
            }

            fn mouse_button_down_event(
                &mut self,
                _ctx: &mut ggez::Context,
                button: event::MouseButton,
                _x: f32,
                _y: f32,
            ) {
                self.egui_backend.input.mouse_button_down_event(button);
            }

            fn mouse_button_up_event(
                &mut self,
                _ctx: &mut ggez::Context,
                button: event::MouseButton,
                _x: f32,
                _y: f32,
            ) {
                self.egui_backend.input.mouse_button_up_event(button);
            }

            fn mouse_motion_event(
                &mut self,
                _ctx: &mut ggez::Context,
                x: f32,
                y: f32,
                _dx: f32,
                _dy: f32,
            ) {
                self.egui_backend.input.mouse_motion_event(x, y);
            }
        }
    */

    use eframe::{egui, epi};
    #[derive(Debug)]
    struct MyApp {
        opts: Opts,
        available_missions: Vec<String>,
        sender: mpsc::Sender<Opts>,
    }

    impl epi::App for MyApp {
        fn name(&self) -> &str {
            "BroaNetCo Launcher"
        }

        fn update(&mut self, ctx: &egui::CtxRef, frame: &mut epi::Frame<'_>) {
            trace!("update");
            let mut opts = &mut self.opts;
            let mut mode = opts.mode;
            let mut mission: String = opts
                .mission
                .get_or_insert(self.available_missions[0].clone())
                .to_string();

            egui::CentralPanel::default().show(ctx, |ui| {
                for tmode in [Mode::Hotseat, Mode::Headless, Mode::Host, Mode::Client] {
                    ui.selectable_value(&mut mode, tmode, tmode.to_string());
                }
                for tmission in self.available_missions.iter() {
                    ui.selectable_value(&mut mission, tmission.clone(), tmission);
                }
                egui::warn_if_debug_build(ui);
                if ui.button("Launch").clicked() {
                    self.sender.send(self.opts.clone()).unwrap();
                    frame.quit();
                }
            });
            self.opts.mode = mode;
            self.opts.mission = Some(mission);
        }

        fn on_exit(&mut self) {
            debug!("on_exit");
            let opts = self.opts.clone();
            debug!("on_exit_2");
            let ret = self.sender.send(opts);
            debug!("on_exit_3");
            ret.unwrap();
            debug!("on_exit_4");
        }
    }
    pub fn run(opts: Opts) -> Opts {
        let native_options = eframe::NativeOptions::default();
        let (sender, receiver) = mpsc::channel();
        debug!("starting");
        eframe::run_native(
            Box::new(MyApp {
                opts,
                available_missions: init::missions_names(get_path()),
                sender,
            }),
            native_options,
        );
        debug!("waiting");
        let ret = receiver.recv().unwrap();
        debug!("finished");
        ret
    }
}
