# BroaNetCo

A turn based hex game/engine written in Rust.

## Compiling

The master branch has been tested on rustc 1.47. Current version of ggez has issues with rustc 1.48.

### Dependencies

Cargo, rustc, a C compiler, pkg-config, alsa, libudev. The python gsinit generator requires python3 but is optional.

#### On Debian/Ubuntu

```apt-get install -yqq --no-install-recommends build-essential pkg-config libasound2-dev libudev-dev```

#### On Windows (msvc abi)

1) Install rust for Windows with msvc api: https://www.rust-lang.org/en-US/install.html

2) Open a command prompt on the project root folder (not a git for windows prompt!).

2b) You may have to run "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" with the correct arch set if rust can't find your visual studio install. Alternatively run cargo from inside a "x64/x86 natives tool prompt" installed with VS build tools. See https://github.com/rust-lang/rust/issues/38584#issuecomment-309737902 for details.

#### On Windows (gnu abi)

TODO

### Compile/Running

```cargo run --release -- -m rs_test1_sp```

Compiling without --release is faster, but the game may be very slow.

## Running

Run `cargo run --release -- ` to get a list of available missions. `cargo run --release -- -m MISSION_NAME` to run a mission. 

### Multiplayer

You can run hotseat multiplayer by default. For networked multiplayer run `cargo run --release -- --mode host -m MISSION_NAME_MP` on the host and `cargo run --release -- --mode client --address HOST_ADDRESS` on the client

### Playing

Move with 7-8-9 1-2-3 keys on the numpad. Numpad 0 skips turn. Numpad 5 releases or confirms selection. First letter of action name selects that action if a unit is selected.

### Missions

Select mission with the "-m" argument. py_yasrpg_* and py_w18_* are currently the most developed missions.

Available missions:

#### Hardcoded

* rs_test1_sp
* rs_test1_coop
* rs_test1_mp

Using Tiled map file:

* rs_test2_sp
* rs_test2_coop
* rs_test2_mp


#### TPTA

* rs_tpta_sp
* rs_tpta_mp
* rs_tpta_obs

#### Python missions

You can generate them by running ./py/tpy.py

* py_t1_coop
* py_t1_sp
* py_t1_mp

```
py
├── data.py: data structures from the rust code
├── t1.py: simple py generator test
├── tpy.py: Entry point/generator for py gen
├── w18.py: W18 game generator
└── yasrpg.py: YASRPG game generator
```

##### YASRPG 

A simple stereotypical RPG. Assets are in a git submodule that has to be initialized.

```
  git submodule init
  git submodule update
```
 

![YASRPG play](imgs/yasrpg.png "YASRPG play")

* py_yasrpg_sp
* py_yasrpg_mp
* py_yasrpg_obs
* py_yasrpg_endless_sp
* py_yasrpg_endless_mp
* py_yasrpg_endless_obs

##### W18

A simple strategy game inspired by 18th century warfare.

![W18 play](imgs/w18.png "YASW18RPG play")

* py_w18_sp
* py_w18_mp
* py_w18_obs
 
#### Other/Handwritten

You can put your own json formatted missions in resources/missions/

## Development

Docs at https://rardiol.gitlab.io/BroaNetCo/broanetco/

### Dependency Graph

![Dependency Graph](imgs/deps.svg "Dependency Graph")

## Licensing/Credits

Source code, both rust and python, found in src/*, crates/broanetco_* and py/* is released under version 3 of the GPL or any later version.

crates/rust-void is a fork of the original rust-void found in https://github.com/reem/rust-void.

### Sounds

- resources/sounds/Explosion-LS100155.ogg - https://commons.wikimedia.org/wiki/File:Explosion-LS100155.ogg - Public Domain

- resources/sounds/wood2.ogg - By Ogrebane - https://opengameart.org/content/metal-and-wood-impact-sound-effects - CC-BY-3.0

- resources/sounds/rifle_3.ogg - Edited version of rifle.ogg - By Q009 (Submitted by Calinou) - https://opengameart.org/content/q009s-weapon-sounds - CC-BY-SA-3.0

- resources/sounds/hit16.mp3.flac - By Independent.nu - (Submitted by qubodup) - https://opengameart.org/content/37-hitspunches - CC0-1.0

### Graphical assets

For files in the git submodule in resources/assets/Universal-LPC-spritesheet refer to resources/assets/Universal-LPC-spritesheet/README.md

All .svg files in resources/assets/ are released under CC0-1.0.

### Game files

All *.tmx files in resources/tiled/ are released under CC-BY-3.0.

### Fonts

- resources/fonts/DejaVuSerif.ttf - see LICENSES/DEJAVU-FONTS

### Code dependencies

TODO

### License files

The license for the source code (GPL-3.0) is available in COPYING. Other licenses are in ./LICENSES/.

